// JAtlasTxt2Xml.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "JAtlasTxt2Xml.h"

#define FREEIMAGE_LIB
#include "FreeImage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// The one and only application object


CWinApp theApp;

CString remove_upsell_string;

using namespace std;

bool processImage(const CString &fn, int &w, int &h)
{
	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilenameU((LPCTSTR)fn);
		
	if(fif == FIF_UNKNOWN)
	{
		return false;
	}

	FIBITMAP *dib = FreeImage_LoadU(fif, (LPCTSTR)fn, 0);

	if(!dib)
	{
		return false;
	}

	w = FreeImage_GetWidth(dib);
	h = FreeImage_GetHeight(dib);
	
	// free resources
	FreeImage_Unload(dib);

	return true;
}

CSimpleArray<CString> ParseLine(CString s, TCHAR delimiter, TCHAR delimiter2)
	{
		CSimpleArray<CString> a;

		int start = 0;

		s.Insert(0,delimiter);
		s.AppendChar(delimiter);
		s.Replace(delimiter2,delimiter);

		//while(1)
		for(int i = 0; i < s.GetLength(); i++)
		{
			TCHAR ch = s.GetAt(i);

			if(ch != delimiter)
			{
				if(start == 0)
					start = i;

				continue;
			}
			else
			{
				if(start == 0)
					continue;
			}

			CString res = s.Mid(start,i-start);

			res.Remove(delimiter);
			res.MakeLower();
			a.Add(res);


			start = 0;
		}

		return a;
	}


CString getFName(const CString &image_name)
{
	int id = image_name.ReverseFind('\\');
	
	if(id < 0)
		return image_name;

	return image_name.Right(image_name.GetLength() - id - 1);
}

CString cleanString(const CString &_s)
{
	CString s = _s;

	CString sarr[] = {_T("_ru"),
					_T("_en"),
					_T("_de"),
					_T("_es"),
					_T("_fr"),
					_T("_it"),
					_T("_ja"),
					_T("_ko"),
					_T("_pt_pt"),
					_T("_pt"),
					_T("_zh"),
					_T("@4x"),
					_T("@2x"),
					_T("")
	};

	for(int i = 0; ;i++)
	{
		if(sarr[i].GetLength() == 0)
			break;

		s.Replace(sarr[i],_T(""));
	}

	s.Replace(_T("achemy"),_T("alchemy"));


	s.Replace(_T("alcheny"),_T("alchemy"));
	

	if(!remove_upsell_string.IsEmpty())
	{

	}
	else
	{
		s.Replace(_T("alchemy_unlock"),_T("alchemy_upsell_unlock"));
	}

	return s;
}

int convertFile(const CString &image_name, const CString &txt_name, const CString &output_name)
{
	int w = 0,h = 0;
	if(!processImage(image_name,w,h))
		return 1;

	//----


	CStdioFile f;		
		
	if(!f.Open(txt_name, CFile::modeRead | CFile::typeText))
		return 2;
	
	CString str;

	CSimpleArray<CString> big_ar;

	while(f.ReadString(str))
	{
		CSimpleArray<CString> ar = ParseLine(str,' ','=');

		for(int i = 0; i < ar.GetSize(); i++)
		{
			//_tprintf(_T("\n%s"),(LPCTSTR)ar[i]);
			
			CString s = ar[i];
			
			big_ar.Add(cleanString(s));
		}
	}	

	f.Close();

	//---

/*	<!DOCTYPE AtlasXML>
<Atlas width="512" height="512" name="ipad_1024x768_en.png">
 <Texture width="192" x="310" y="90" height="95" filename="achemy_40off_en"/>
</Atlas>*/

	CStdioFile f2;		
		
	if(!f2.Open(output_name, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		return 3;

	f2.WriteString(_T("<!DOCTYPE AtlasXML>\n"));

	CString s;
	s.Format(_T("<Atlas width=\"%d\" height=\"%d\" name=\"%s\">\n"),w,h,(LPCTSTR)getFName(image_name));

	f2.WriteString(s);
		
	//_tprintf(_T("%d"),big_ar.GetSize());

	for(int k = 0; k < big_ar.GetSize(); k+=5)
	{
		s.Format(_T("<Texture x=\"%s\" y=\"%s\" width=\"%s\" height=\"%s\" filename=\"%s\"/>\n"),
			(LPCTSTR)big_ar[k+1],(LPCTSTR)big_ar[k+2],(LPCTSTR)big_ar[k+3],(LPCTSTR)big_ar[k+4],(LPCTSTR)big_ar[k]);

		//_tprintf(_T("%s"),(LPCTSTR)s);

		f2.WriteString(s);
	}
		

	f2.WriteString(L"</Atlas>\n");
	
	f2.Close(); 

	return 0;
}

	int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	::FreeImage_Initialise();

	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			nRetCode = 1;
		}
		else
		{
			// TODO: code your application's behavior here.
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		nRetCode = 1;
	}
	
	if(argc < 4)
	{
		_tprintf(_T("Usage: <image_full_path> <txt_atlas_name> <output_atlas_name> \n"));
		return 0;
	}

	CString image_name(argv[1]);
	CString txt_name(argv[2]);
	CString output_name(argv[3]);

	if(argc == 5)
		remove_upsell_string.Append(argv[4]);

	int res = convertFile(image_name, txt_name, output_name);

	if(res == 0)
	{
		_tprintf(_T("Converting %s to %s - okay\n"), argv[2],argv[3]);
	}
	else
	{
		_tprintf(_T("Convert error in %d argument\n"), res);
	}
	
	return nRetCode;
}
