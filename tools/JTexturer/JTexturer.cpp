// JTexturer.cpp : Defines the entry point for the console application.
//

//TODO list: 
//- ������� ����� ��� file_list.xml ���������� �� ����� settings.xml
//- �������, ����� ����� ���� ��������� ��������, �� ������ �� � ������ �������
//- ����� ���������� � ����� ��������� ������� - ������ ���������
//- �������� �������� ����������
//- ����������������� ����������

#include "stdafx.h"
#include "JTexturer.h"

#ifndef DEBUG
	#define FREEIMAGE_LIB
	#include "FreeImage.h"
#endif

#include <map>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

class CTexturesList;
extern CTexturesList gTexturesList;
CWinApp theApp;

int threads = 0;

namespace Utils
{
	//�������� ������� � ���� ������ ������
	CString QuoteString(const CString &s)
	{
		return CString("\"") + s + CString("\"");
	}

	//��� � �������
	CString toString(bool b)
	{
		if(b)
			return CString(_T("true"));
		else
			return CString(_T("false"));
	}

	//�������� � �������
	CString toString(ULONGLONG b)
	{
		CString s;
		s.Format(_T("%I64d"),b);
		return s;
	}

	//����� � �������
	CString toString(int i)
	{
		CString s;
		s.Format(_T("%d"),i);
		return s;
	}

	//����� � �������
	CString toString(const CTime &t)
	{
		CString s;
		s.Format(_T("%I64d"),t.GetTime());
		return s;
	}

	//��� �� �������
	int getInt(const CString &s)
	{
		return _wtoi((LPCTSTR)s);
	}

	//��� �� �������
	bool getBool(const CString &s)
	{
		if(s.CompareNoCase(_T("true")) == 0)
			return true;
		
		return false;
	}

	//���� ����������?
	bool fileExist(const CString &s)
	{
		CFileStatus filestatus;

		return (CFile::GetStatus( s, filestatus ) == TRUE);
	}

	void deleteFile(const CString &f)
	{
		_wunlink((LPCTSTR)f);
	}

	//�������� ���������� �����
	CString getExtWithDot(const CString &s)
	{
		int id = s.ReverseFind(L'.');

		return s.Mid(id,s.GetLength()-id);
	}

	CString getExt(const CString &s)
	{
		int id = s.ReverseFind(L'.');

		return s.Mid(id+1,s.GetLength()-id-1);
	}

	//������ ���������� �� ���� �����
	void removeExt(CString &s)
	{
		int id = s.ReverseFind(L'.');
		int del_num = s.GetLength() - id - 1;
		s.Delete(id + 1, del_num);
	}	

	//������� �������� ���� � �����
	void fixPathValue(CString &val)
	{
		int id = val.Find(_T(":"));
		
		if(id != -1)
		{
			if(val.GetAt(val.GetLength()-1) == '\\')
			{
				val.Truncate(val.GetLength()-1);
			}
		}
	}

	void scanFolder(const CString &start_folder, const CString &ignore_folder, const TCHAR *allow_types[], std::map<CString,CString> &res)
	{
		if (start_folder.IsEmpty())
		{
			_tprintf(L"Start folder empty!");
			return;
		}

		CFileFind finder;

		BOOL bWorking = finder.FindFile(start_folder + L"\\*.*");

		
		while (bWorking)
		{
			bWorking = finder.FindNextFile();

			//CString res = CString(finder.GetFileName());

			CString fn = finder.GetFileName();
			CString path = finder.GetFilePath();

			if(fn.GetAt(0) == '-')
			{
				_tprintf(L"scanFolder ignore %s file because of '-'!", (LPCTSTR)fn);
				continue;
			}


			fn.MakeLower();
			path.MakeLower();

			if(!finder.IsDirectory())
			{
				//find minus in extention
				if(fn.Find('-',fn.Find('.')) > 0)
				{
					_tprintf(L"scanFolder ignore %s file because of '-' in extention!", (LPCTSTR)fn);
					continue;
				}

				//do not search files in output folder
				if(finder.GetFilePath().Find(ignore_folder) >= 0)
					continue;


				bool true_type = false;

				for(const TCHAR **ch = allow_types; *ch != NULL; ++ch)
				{
					if(fn.Find(*ch) >= 0)
					{
						true_type = true;
						break;
					}
				}
				
				if(!true_type)
					continue;

				path.Replace(L"\\" + fn,L"");

				res.insert(std::pair<CString,CString>(fn,path));
			}
			else
			{
				if(!finder.IsDots())
				{
					scanFolder(start_folder + CString(L"\\") + fn,ignore_folder,allow_types,res);
				}
			}

		}
	}

	//���������� ������ � �����-������
	bool match(LPCSTR _str, LPCSTR _pattern)
	{
		std::string str(_str);
		std::string pattern(_pattern);

		std::string tmpStr = str;
		std::string tmpPattern = pattern;

		std::string::const_iterator strIt = tmpStr.begin();
		std::string::const_iterator patIt = tmpPattern.begin();
		std::string::const_iterator lastWildCardIt = tmpPattern.end();
		while (strIt != tmpStr.end() && patIt != tmpPattern.end())
		{
			if (*patIt == '*')
			{
				lastWildCardIt = patIt;
				// Skip over looking for next character
				++patIt;
				if (patIt == tmpPattern.end())
				{
					// Skip right to the end since * matches the entire rest of the string
					strIt = tmpStr.end();
				}
				else
				{
					// scan until we find next pattern character
					while(strIt != tmpStr.end() && *strIt != *patIt)
						++strIt;
				}
			}
			else
			{
				if (*patIt != *strIt)
				{
					if (lastWildCardIt != tmpPattern.end())
					{
						// The last wildcard can match this incorrect sequence
						// rewind pattern to wildcard and keep searching
						patIt = lastWildCardIt;
						lastWildCardIt = tmpPattern.end();
					}
					else
					{
						// no wildwards left
						return false;
					}
				}
				else
				{
					++patIt;
					++strIt;
				}
			}

		}
		// If we reached the end of both the pattern and the string, we succeeded
		if (patIt == tmpPattern.end() && strIt == tmpStr.end())
		{
			return true;
		}
		else
		{
			return false;
		}

	}
}

namespace Params
{
	static const CString params[] = {
		_T("textures_folder"),
		_T("materials_folder"),
		_T("debug_output"),
		_T("work_folder"),
		_T("output_format"),
		_T("output_folder"),
		_T("ddsconv_path"),
		_T("pvrtextool_path"),
		_T("max_texture_size"),
		_T("ignore_texture"),
		_T("default_mip_num"),
		_T("ignore_texture_for_mip"),
		_T("additional_pvr_converter_flags"),
		_T("check_coverter_capabilities"),
		_T("mask_texture_for_resize"),
	};

	static const CString params_help[] = {
		_T("textures_folder"),
		_T("materials_folder"),
		_T("debug_output"),
		_T("work_folder"),
		_T("output_format"),
		_T("output_folder"),
		_T("ddsconv_path"),
		_T("pvrtextool_path"),
		_T("max_texture_size"),
		_T("ignore_texture"),
		_T("default_mip_num"),
		_T("ignore_texture_for_mip"),
		_T("additional_pvr_converter_flags"),
		_T("check_coverter_capabilities"),
		_T("mask_texture_for_resize"),
	};

	enum param_id
	{
	//�����, � ������� ��������
	 TEX_SCAN_FOLDER = 0,

	//�����, � ������� ���������
	 MAT_SCAN_FOLDER,

	//������� ���?
	 DBG_OUT,

	//����� ��� ����������� ������
	 WORK_FOLDER,

	//���������, ��� ������� ������� ��������
	 TARGET_PLATFORM,

	//�����, ���� ��������� ��������
	 TARGET_FOLDER,

	//���� � ��� ����������
	 TEXCONV_PATH,

	//���� � ��� ���������
	 PVRTEXTOOL_PATH,
	
	//������������ ������ ��������
	 MAX_TEXTURE_SIZE,

	//������������ ��������
	 IGNORE_TEXTURE,

	//��������� ���������� �������� (3-4)
	 DEFAULT_MIP_NUM,

	//��������, ��� ������� ������������ �������� ��������
	 IGNORE_TEXTURE_FOR_MIP,

	 ADD_PVR_CONVERTER_FLAGS,

	 CHECK_CONVERTER_CAPABILITIES,
	 
	 //��������, ��� ������� ����������� ����������
	 MASK_TEXTURE_FOR_RESIZE,

	 MAX_VALUE,
	};
	  
};

inline void LogDebug(char *format, ...)
{
	char s[4096];
	va_list marker;
	va_start(marker, format);
	vsprintf_s(s, format, marker);
	va_end(marker);

	//OutputDebugString(s);
	printf("\n%s",s);

	/*if (Log::ShowDebug)
	{
		fprintf(stderr, "\n--- DEBUG: %s\n", s);
	}*/
}

inline void LogError(char *format, ...)
{
	char s[4096];
	va_list marker;
	va_start(marker, format);
	vsprintf_s(s, format, marker);
	va_end(marker);

	//OutputDebugString(s);
	printf("\n%s",s);

	/*if (Log::ShowDebug)
	{
		fprintf(stderr, "\n--- DEBUG: %s\n", s);
	}*/
}

//-----------

class CXMLParser
{
	char *buf;
	rapidxml::xml_document<> XMLDoc;

public:

	bool isOK()
	{
		return (buf != 0);
	}

	CXMLParser(const CString &fn)
	{
		buf = 0;
		CFile f;		
		
		if(!f.Open(fn, CFile::modeRead | CFile::typeBinary))
			return;
		
		unsigned len = (unsigned)f.GetLength();
		buf = new char[len+1];
		f.Read(buf,len);
		f.Close();
	
		buf[len] = 0;

		try
		{
			XMLDoc.parse<0>(buf);
		}
		catch(rapidxml::parse_error &e)
        {
			printf("XML parce error: %s",e.what());

            destroy();
	     }
	}

	operator XML_NODE*()
	{
		return XMLDoc.first_node();
	}

	void destroy()
	{
		delete[] buf;
		buf = 0;
	}

	~CXMLParser()
	{
		destroy();
	}

	//��� ���� � �������
	static CString getNodeName(const XML_NODE *n)
	{
		return CString(n->name());
	}

	//�������� ������� �� xml
	static CString getNodeAttribute(const XML_NODE *n, const char *attr_name)
	{
		XML_ATTRIB *attr = n->first_attribute(attr_name);
   
		if(attr)
			return CString(attr->value());
		else
			return CString("");
	}
};


//-----------

class CParamValues
{
	CString help;
	CSimpleArray<CString> mValues;

public:
	void add(const CString &val, const CString &h)
	{
		mValues.Add(val);
		help = h;
	}

	int size() const
	{
		return mValues.GetSize();
	}

	bool isTrue(int id) const
	{
		return mValues[id].CompareNoCase(L"true") == 0;
	}

	const CString& get(int id) const
	{
		return mValues[id];
	}

	const CString& getHelp() const
	{
		return help;
	}
};

//program config
//param name, param values
class CProgramConfig
{
	CString config_full_path;
	std::map<CString,CParamValues> mParams;
	std::map<CString,CParamValues>::iterator it;

public:

	const CString &getFullPath() const 
	{
		return config_full_path;
	}

	bool hasParam(const CString &name) const
	{
		return mParams.find(name) != mParams.end();
	}


	void addParam(const CString &name, const CString &val, const CString &help = CString(""))
	{
		CString val2 = val;

		Utils::fixPathValue(val2);

		mParams[name].add(val2,help);
	}

	bool getBoolean(const CString &name)
	{
		if(!hasParam(name))
			return false;

		return mParams[name].isTrue(0);
	}

	CString get(const CString &name, int id = 0)
	{
		if(!hasParam(name))
			return CString("");

		return mParams[name].get(id);
	}

	int getValuesCount(const CString &name)
	{
		return mParams[name].size();
	}

	int getI(const CString &name)
	{
		return _wtoi((LPCTSTR)get(name));
	}

	CString toString()
	{
		CString res,s;

		for ( it=mParams.begin() ; it != mParams.end(); it++ )
		{
			CParamValues p = (*it).second;

			CString h = p.getHelp();
			

			s.Format(L" <!-- %s-->\n",(LPCWSTR)h);
			res.Append(s);

			for(int j = 0; j < p.size(); j++)
			{
				//s.Format(L"<param n=\"%s\" v=\"%s\"/>\n",(LPCWSTR)(*it).first, (LPCWSTR)p.get(j));


				s.Format(L" <%s v=\"%s\"/>\n",(LPCWSTR)(*it).first, (LPCWSTR)p.get(j));
				res.Append(s);
			}
		}
		
		return res;
	}

	void save(const CString &fname)
	{
		CStdioFile f;		
		
		f.Open(fname, CFile::modeCreate | CFile::modeWrite | CFile::typeText);

		f.WriteString(L"<setting>\n");
		
		f.WriteString(toString());
		

		f.WriteString(L"</setting>");

		f.Close();
	}

	bool load(const CString &fname)
	{
		config_full_path = fname;

		CXMLParser	xml(fname);
		
		if(!xml.isOK())
			return false;

		//XML_NODE* pElement = ((XML_NODE*)xml)->first_node("param");
		XML_NODE* pElement = ((XML_NODE*)xml)->first_node(0);
    
		while(pElement)
		{
			//CString n = Utils::getNodeAttribute(pElement,"n");

			CString n = CXMLParser::getNodeName(pElement);
			CString v = CXMLParser::getNodeAttribute(pElement,"v");

			addParam(n,v);

			//pElement = pElement->next_sibling("param");
			pElement = pElement->next_sibling(0);
		}

		return true;
	}

	bool loadFromCmdline(const CString &cmd_line)
	{
		int param_start = 0;

		CString s = cmd_line;
		
		s = s.SpanExcluding(_T("\n\t"));
		
		int id = -1;

		CString param_begin(_T("--"));
		CString param_eq(_T("="));

		do
		{
			id = s.Find(param_begin);

			CString p;
			
			if(id >= 0)
				p = s.Mid(0,id-1);
			else
				p = s;

			int id2 = p.Find(param_eq);

			if(id2 >= 0)
			{
				CString param = p.Left(id2);
				CString value = p.Right(p.GetLength() - id2 - param_eq.GetLength());
				
				param.Trim();
				value.Trim();

				addParam(param,value);
			}

			//if(id < 0)
			//	break;

			s = s.Right(s.GetLength()-id-param_begin.GetLength());

		}while(id >= 0);


		

		return true;
	}

	bool isTextureMatchInParam(const CString &fn, const CString &param)
	{
		//Params::params[Params::IGNORE_TEXTURE
		for(int i = 0; i < mParams[param].size(); i++)
		{
			CString ignore = mParams[param].get(i);

			//without wild card
			if(ignore.Find(L'*') < 0 && fn == ignore)
			{
				_tprintf(_T("\nmatch my name! %s %s"),(LPCTSTR)fn,(LPCTSTR)ignore);

				int l = 0;
				return true;
			}

			if(Utils::match((LPCSTR)CStringA(fn),(LPCSTR)CStringA(ignore)))
			{
				_tprintf(_T("\nmatch my wild!"));

				return true;
			}
		}

		return false;
	}
};

CProgramConfig gConfig;

//--------------

class CFileInfo
{
	bool needConvert;
	bool mIsImage;
	bool mIgnore;
	CString mConverterName;

	map<CString,CString> mParams;
	map<CString,CString>::iterator it;
	map<CString,CString>::const_iterator cit;

	#define FI_NAME				_T("name")
	#define FI_FULL_PATH		_T("full_path")
	#define FI_REL_PATH			_T("rel_path")
	#define FI_TARGET_PATH		_T("target_path") 
	#define FI_TARGET_NAME		_T("target_name")
	#define FI_TARGET_EXT		_T("target_ext")
	#define FI_SRC_SIZE 		_T("src_size")
	#define FI_MOD_TIME 		_T("mod_time")
	#define FI_CREATE_TIME		_T("create_time") 

	#define FI_HAS_ALPHA 		_T("has_alpha")
	#define FI_SOURCE_WIDTH 	_T("source_width")
	#define FI_SOURCE_HEIGHT	_T("source_height")
	#define FI_TARGET_WIDTH 	_T("target_width")
	#define FI_TARGET_HEIGHT	_T("target_height")


	#define FI_MIP_NUM			_T("mip_num")

	#define FI_ADD_PVR_CONV_FLAGS	_T("add_pvr_converter_flags")

	//#define FI_CONVERTED		_T("converted")


public:
	CFileInfo() 
	{
		needConvert = false;
		mIgnore = false;

		//for ( it=mParams.begin() ; it != mParams.end(); it++ )
		//	(*it).second = CString();

		mParams[FI_NAME] = CString(); 
		mParams[FI_FULL_PATH] = CString(); 
		mParams[FI_REL_PATH] = CString(); 
		mParams[FI_TARGET_PATH] = CString(); 
		mParams[FI_TARGET_NAME] = CString();
		mParams[FI_TARGET_EXT] = CString();
		mParams[FI_SRC_SIZE] = CString();
		mParams[FI_MOD_TIME] = CString();
		mParams[FI_CREATE_TIME] = CString();
		
		mParams[FI_HAS_ALPHA] = CString();
		mParams[FI_SOURCE_WIDTH] = CString();
		mParams[FI_SOURCE_HEIGHT] = CString();
		mParams[FI_TARGET_WIDTH] = CString();
		mParams[FI_TARGET_HEIGHT] = CString();

		mParams[FI_MIP_NUM] = CString();

		mParams[FI_ADD_PVR_CONV_FLAGS] = CString();
	}

	void operator=(const CFileInfo&fi)
	{
		needConvert = fi.needConvert;
		mIsImage = fi.mIsImage;
		mIgnore= fi.mIgnore;
		mConverterName = fi.mConverterName;

		mParams = fi.mParams;
	}

	void setAddConverterFlags(const CString &s)
	{
		mParams[FI_ADD_PVR_CONV_FLAGS] = s;
	}

	void addImageSpecificParams()
	{
		//debug test values
		int w = 1024, h = 768;
		bool has_alpha = true;

#ifdef FREEIMAGE_LIB

		CString fn = getFullPathWithFName();

		FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilenameU((LPCTSTR)fn);
		
		if(fif == FIF_UNKNOWN) {
			mIsImage = false;
			return;
		}

		FIBITMAP *dib = FreeImage_LoadU(fif, (LPCTSTR)fn, 0);

		mIsImage = (dib != NULL);
		
		if(!mIsImage)
		{
			int l = 0;
			return;
		}

		has_alpha = FreeImage_GetBPP(dib) > 24;
		w = FreeImage_GetWidth(dib);
		h = FreeImage_GetHeight(dib);
	
		// free resources
		FreeImage_Unload(dib);

#else
		//without freeimage use default values
#endif

		//TODO: check max mip-maps num with converter capabilities
		mParams[FI_HAS_ALPHA] = Utils::toString(has_alpha);
		mParams[FI_SOURCE_WIDTH] = Utils::toString(w);
		mParams[FI_SOURCE_HEIGHT] = Utils::toString(h);

		if(gConfig.isTextureMatchInParam(getFileName(),Params::params[Params::IGNORE_TEXTURE_FOR_MIP]))
		{
			mParams[FI_MIP_NUM] = _T("1");
		}
		else
		{
			mParams[FI_MIP_NUM] = gConfig.get(Params::params[Params::DEFAULT_MIP_NUM]);
		}

	}

	void preprocessImage()
	{
		int max = Utils::getInt(gConfig.get(Params::params[Params::MAX_TEXTURE_SIZE]));

		int target_w = Utils::getInt(mParams[FI_SOURCE_WIDTH]);
		int target_h = Utils::getInt(mParams[FI_SOURCE_HEIGHT]);

		bool need_to_resize = gConfig.isTextureMatchInParam(getFileName(),Params::params[Params::MASK_TEXTURE_FOR_RESIZE]);
			

		if(max > 0 && need_to_resize)
		{
			//real max size
			if(max > 8)
			{
				if(target_w > max) target_w = max;
				if(target_h > max) target_h = max;
			}
			else //max is a scale size
			{
				if(target_w > 16)
					target_w /= max;

				if(target_h > 16)
					target_h /= max;
			}
		}

		
		mParams[FI_TARGET_WIDTH] = Utils::toString(target_w);
		mParams[FI_TARGET_HEIGHT] = Utils::toString(target_h);
	}

	void setSquareDestSize()
	{
		int target_w = Utils::getInt(mParams[FI_TARGET_WIDTH]);
		int target_h = Utils::getInt(mParams[FI_TARGET_HEIGHT]);

		int imax = max(target_w,target_h);

		mParams[FI_TARGET_WIDTH] = Utils::toString(imax);
		mParams[FI_TARGET_HEIGHT] = Utils::toString(imax);
	}

	bool isNeedConvert()
	{
		return needConvert;
	}

	bool isImage()
	{
		return mIsImage;
	}

	void setNeedConvert(bool nc)
	{
		needConvert = nc;
	}

	bool getNeedConvert()
	{
		return needConvert;
	}

	void setIgnore()
	{
		setConverterName(_T("none"));

		mIgnore = true;
	}

	bool getIgnore() const
	{
		return mIgnore;
	}

	void setConverterName(const CString &cn)
	{
		mConverterName = cn;
	}

	CString getConverterName() const 
	{
		return mConverterName;
	}

	bool isLimitedSame(const CFileInfo &fi) const
	{
		if(getFileName() != fi.getFileName())
			return false;
		
		if(getParam(FI_FULL_PATH) != fi.getParam(FI_FULL_PATH)) 
			return false;

		if(getParam(FI_SRC_SIZE) != fi.getParam(FI_SRC_SIZE)) 
			return false;
		
		if(getParam(FI_MOD_TIME) != fi.getParam(FI_MOD_TIME)) 
			return false;

		if(getParam(FI_CREATE_TIME) != fi.getParam(FI_CREATE_TIME)) 
			return false;

		return true;
	}

	bool operator ==(const CFileInfo& fi)
	{
		for ( it=mParams.begin() ; it != mParams.end(); it++ )
		{
			CString s1 = getParam((*it).first);
			CString s2 = fi.getParam((*it).first);

			if(s1 != s2)
				return false;
		}

		return true;
	}

	CString toString(const CString &tabs)
	{
		CString res;
		CString s;

		res.Append(tabs);
		res.Append(L" <file\n");

		for ( it=mParams.begin() ; it != mParams.end(); it++ )
		{
			s.Format(L"  %s=\"%s\"\n",(LPCWSTR)(*it).first, (LPCWSTR)(*it).second);
			res.Append(tabs);
			res.Append(s);
		}

		res.Append(tabs);
		res.Append(L"  />\n");

		return res;
	}

	CFileInfo(const XML_NODE* pElement)
	{
		*this = CFileInfo();

		for ( it=mParams.begin() ; it != mParams.end(); it++ )
		{
			(*it).second = CXMLParser::getNodeAttribute(pElement,(LPCSTR)CStringA((*it).first));
		}

		//CString s = Utils::getNodeAttribute(pElement,"name");				
	}

	CFileInfo(const CString &name, const CString &path)
	{
		//call default ctor
		*this = CFileInfo();
		
		CString rel_path = path;
		rel_path.Replace(gConfig.get(Params::params[Params::TEX_SCAN_FOLDER]) + L"\\",L"");

		CFileStatus filestatus;
		CFile::GetStatus( path + CString(L"\\") + name, filestatus );

		//size = filestatus.m_size;
		//mod_time = filestatus.m_mtime;
		///create_time = filestatus.m_ctime;

		//no subfolders
		CString target_path = gConfig.get(Params::params[Params::TARGET_FOLDER]);// + rel_path;
		CString target_name = name;
	
		//---------

		mParams[FI_NAME] = name; 
		mParams[FI_FULL_PATH] = path; 
		mParams[FI_REL_PATH] = rel_path; 
		mParams[FI_TARGET_PATH] = target_path; 
		mParams[FI_TARGET_NAME] = target_name; 

		//TODO: !!!
		mParams[FI_TARGET_EXT] = Utils::getExtWithDot(target_name);
		
		mParams[FI_MOD_TIME] = Utils::toString(filestatus.m_mtime);
		mParams[FI_CREATE_TIME] = Utils::toString(filestatus.m_ctime);
		mParams[FI_SRC_SIZE] = Utils::toString(filestatus.m_size);

		//TODO: �������� �������� �� �����, �������
		//???
				
		//mParams[_T("has_alpha")] = Utils::toString(has_alpha);

		addImageSpecificParams();
			
	}

	const CString& getParam(const CString &v) const 
	{
		const map<CString,CString>::const_iterator cit = mParams.find(v);

#ifdef DEBUG
		if(cit == mParams.end())
		{
			::_CrtDbgBreak();
		}
#endif

		return (*cit).second;
	}

	bool hasAlpha() const
	{
		return Utils::getBool(getParam(FI_HAS_ALPHA));
	}

	CString getMipmapNum() const
	{
		return getParam(FI_MIP_NUM);
	}

	const CString& getTargetFolder() const
	{
		return getParam(FI_TARGET_PATH);
	}

	const CString& getTargetExt() const
	{
		return getParam(FI_TARGET_EXT);
	}

	const CString& getTargetWidth() const
	{
		return getParam(FI_TARGET_WIDTH);
	}

	const CString& getTargetHeight() const
	{
		return getParam(FI_TARGET_HEIGHT);
	}
	
	const CString& getSourceWidth() const
	{
		return getParam(FI_SOURCE_WIDTH);
	}

	const CString& getSourceHeight() const
	{
		return getParam(FI_SOURCE_HEIGHT);
	}
		
	/*bool getConverted() const
	{
		return Utils::getBool(getParam(FI_CONVERTED));
	}

	void setConverted(bool v)
	{
		mParams[FI_CONVERTED] = v ? CString(_T("true")) : CString(_T("false"));
	}
	*/
	CString getFullPathWithFName() const
	{
		CString s = getParam(FI_FULL_PATH) + CString(_T("\\")) + getFileName();

		return s;		
	}

	const CString& getFileName() const
	{
		return getParam(FI_NAME); 
	}

	CString getRelPathWithFName() const
	{
		CString s = getParam(FI_REL_PATH) + CString(_T("\\")) + getFileName();

		return s; 
	}

	void changeTargetExt(const CString &new_ext)
	{
		CString tn = getTargetFName();

		Utils::removeExt(tn);

		tn.Append(new_ext);

		mParams[FI_TARGET_NAME] = tn;

		mParams[FI_TARGET_EXT] = CString(_T(".")) + new_ext;
	}

	const CString& getTargetFName() const
	{
		return getParam(FI_TARGET_NAME);		
	}

	CString getTargetFullPathWithFName() const
	{
		CString s = getParam(FI_TARGET_PATH) + CString(_T("\\")) + getTargetFName();

		return s;		
	}

	bool targetFileExist() const
	{
		return Utils::fileExist(getTargetFullPathWithFName());
	}
};

//--------------

class CConverter
{
	bool isOK;

public:

	CConverter()
	{
		isOK = true;
	}

	inline void SAFE_CLOSE_HANDLE(HANDLE &x)
	{
		if(x) { CloseHandle(x); x = 0; }
	}

	bool ExecWait(const CString& inExePath, CString inArgs)
	{
		bool result = false;
		STARTUPINFO start_info;
		memset(&start_info, 0, sizeof(STARTUPINFO));
		start_info.cb = sizeof(start_info);
		start_info.dwFlags = STARTF_USESHOWWINDOW;
		start_info.wShowWindow = SW_SHOW;

		PROCESS_INFORMATION process_info;
		memset(&process_info, 0, sizeof(PROCESS_INFORMATION));

		//LogDebug("ExecWait(): execute: %s %s", (LPCTSTR)inExePath, (LPCTSTR)inArgs);
		CString command_line = inExePath + _T(" ") + inArgs;
				
		if (CreateProcess(NULL, 
				command_line.GetBuffer(), 
				NULL, 
				NULL, 
				false, 
				DETACHED_PROCESS,
				NULL, 
				NULL, 
				&start_info, 
				&process_info))

		{
			//LogDebug("2");
		

			//LogDebug("3");
			// ��� ���������� ��������
			if (WaitForSingleObject(process_info.hProcess, INFINITE) != WAIT_FAILED)
			{
				DWORD exit_code;

				GetExitCodeProcess(process_info.hProcess, &exit_code);

				//LogDebug("4-1");

				if (exit_code == 0)
				{
					result = true;
				}
				else
				{
					result = false;
					LogError("Util::ExecWait(): non-zero exit code: %d, command line: %s", 
						exit_code, (LPCTSTR)command_line);
				}
			}
			else
			{
				//LogDebug("4-0");

				result = false;
				LogError("Util::ExecWait(): WaitForSingleObject() has returned WAIT_FAILED, command line: %s", 
					(LPCTSTR)command_line);
			}

			//LogDebug("5");

			// ����������� ������� �������������� ��������
			SAFE_CLOSE_HANDLE(process_info.hProcess);
			// ����������� �������  ��������� ����� ����������� ��������, �� ��� �� �����
			SAFE_CLOSE_HANDLE(process_info.hThread);
		}
		else
		{
			//LogDebug("6");

			LPTSTR string_buffer = NULL;
			CString reason;
			
			if (0 > FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, 0, GetLastError(), 0, (LPTSTR)&string_buffer, 0, 0))
			{
				reason = "unknown";
			}
			else
			{
				reason = string_buffer;
				LocalFree(string_buffer);
			}
				LogError("Util::ExecWait(): CreateProcess() can't execute command: %s !!! REASON: %s", 
					(LPCTSTR)command_line, (LPCTSTR)reason);
		}

		return result;
	};

	void convert(const CFileInfo &fi)
	{
		//printf(_T("convert %d\n"),isOK); 

		if(!isOK)
			return;

		_tprintf(_T("-"));

		if(fi.getIgnore())
		{
			CopyFile((LPCTSTR)fi.getFullPathWithFName(),(LPCTSTR)fi.getTargetFullPathWithFName(),FALSE);

			//_tprintf(_T("Copying %s to %s\n"),(LPCTSTR)fi.getFileName(),(LPCTSTR)fi.getTargetFName());
		}
		else
		{
			CString args = getArgString(fi);
			//CString exe_path = Utils::QuoteString(getCmdUtilPath())

			CString exe_path = getCmdUtilPath();

			if(exe_path.Find(_T(" ")) >= 0)
				exe_path = Utils::QuoteString(exe_path);
			
			while(1)
			{
				if(!ExecWait(exe_path, args))
				{
					_tprintf(_T("\nConverting %s to %s FAILED for some reason(ask convert tool for details)!"),(LPCTSTR)fi.getFileName(),(LPCTSTR)fi.getTargetFName());
					_tprintf(_T("\nCmd line: %s"),(LPCTSTR)exe_path);
					_tprintf(_T("\nArgs: %s"),(LPCTSTR)args);
					_tprintf(_T("\nAdd <ignore_texture v=\"%s\"/> to settings.xml to solve this problem!"),(LPCTSTR)fi.getFileName());
				}
				
				//_tprintf(_T("Converting %s to %s\n"),(LPCTSTR)fi.getFileName(),(LPCTSTR)fi.getTargetFName());

				if(Utils::fileExist(fi.getTargetFullPathWithFName()))
				{
					//_tprintf(_T("Converted file exist! %s!\n"),(LPCTSTR)fi.getTargetFullPathWithFName());
					_tprintf(_T("+"));
					break;
					
				}
				else
				{
					_tprintf(_T("\nConverted file do not exist! Another try!"));
				}

				
			}
		}
	}

	void preprocess(CFileInfo &fi)
	{
		bool sup = textureFormatSupportedByConverter(fi);
				
		if(!gConfig.getBoolean(Params::params[Params::CHECK_CONVERTER_CAPABILITIES]) && !sup)
		{
			if(!sup) _tprintf(_T("\nTexture %s do not supported by converter... but nobody cares"),(LPCTSTR)fi.getFileName());

			sup = true;
		}

		if(!sup)
		{
			_tprintf(_T("\nTexture %s do not supported by converter"),(LPCTSTR)fi.getFileName());
		}

		if(!sup || gConfig.isTextureMatchInParam(fi.getFileName(),Params::params[Params::IGNORE_TEXTURE]))
		{
			_tprintf(_T("\nignore: %s"),(LPCTSTR)fi.getFileName());

			fi.setIgnore();
		}
		else
		{
			fi.changeTargetExt(getTargetExt(fi));

			fi.preprocessImage();
	
			//size is not supported
			if(!textureFormatSupportedByConverter(fi))
			{
				fi.setSquareDestSize();
			}

			fi.setAddConverterFlags(getAddFlags());
		}
	}

	virtual CString getCmdUtilPath() = 0;
	virtual CString getArgString(const CFileInfo &file_info) const = 0;
	virtual CString getTargetExt(const CFileInfo &inf) = 0;

	virtual bool textureFormatSupportedByConverter(const CFileInfo &file_info) { return true; };
	virtual CString getAddFlags() { return CString(""); }
	
	virtual bool checkCmdUtil()
	{
		isOK = Utils::fileExist(getCmdUtilPath());

		if(!isOK)
		{
			_tprintf(_T("\nERROR: Cmd converted tool not found in %s!"),(LPCTSTR)getCmdUtilPath());
			return false;
		}

		return true;
	}


};

class CDDSConverter : CConverter
{
public:
	
	CDDSConverter() : CConverter()
	{
		checkCmdUtil();
	}

	CString getCmdUtilPath()
	{
		 return gConfig.get(Params::params[Params::TEXCONV_PATH]);
	}

	CString getTargetExt(const CFileInfo &inf)
	{
		 return CString(L"dds");
	}

	CString DxtType(const CFileInfo &inf) const
	{
		return inf.hasAlpha() ? _T("DXT5") : _T("DXT1");
	}

	CString MipmapNum(const CFileInfo &inf) const
	{
		return inf.getMipmapNum();
	}

	CString getArgString(const CFileInfo &fi) const
	{
		return CString(_T(" ")) 
				+ CString(_T(" -f ")) 
				+ DxtType(fi) 
				+ _T(" -m ") 
				+ MipmapNum(fi)
				+ _T(" -w ") 
				+ fi.getTargetWidth()
				+ _T(" -h ") 
				+ fi.getTargetHeight()
				+ _T(" -o ") 
				+ Utils::QuoteString(fi.getTargetFolder())
				+ _T(" ") 
				+ Utils::QuoteString(fi.getFullPathWithFName());
	}

	
};

class CPVRConverter : CConverter
{
public:
	CPVRConverter() : CConverter()
	{
		checkCmdUtil();
	}

	CString getCmdUtilPath()
	{
		return gConfig.get(Params::params[Params::PVRTEXTOOL_PATH]);
	}

	CString getTargetExt(const CFileInfo &inf)
	{
		 return CString(L"pvr");
	}

	CString MipmapNum(const CFileInfo &inf) const
	{
		if(inf.getMipmapNum() == _T("1"))
			return CString(_T(""));
		else
			return CString(_T("-m"));
	}

	CString getAddFlags(const CFileInfo &inf) const
	{
		return gConfig.get(Params::params[Params::ADD_PVR_CONVERTER_FLAGS]);
	}

	bool isPowerOfTwo(int x)
	{
		return (x & (x - 1)) == 0;
	}

	bool textureFormatSupportedByConverter(const CFileInfo &inf)
	{
		int h = Utils::getInt(inf.getSourceHeight());
		int w = Utils::getInt(inf.getSourceWidth());
		
		return (w == h) && isPowerOfTwo(w) && isPowerOfTwo(h) && w > 16;
	}

	CString getResizeString(const CFileInfo &inf) const
	{
		CString res;

		if(Utils::getInt(inf.getTargetWidth()) < Utils::getInt(inf.getSourceWidth()))
			res += _T(" -x") + inf.getTargetWidth();

		if(Utils::getInt(inf.getTargetHeight()) < Utils::getInt(inf.getSourceHeight()))
		 	res += _T(" -y") + inf.getTargetHeight();

		return res;
	}

	CString getArgString(const CFileInfo &fi) const
	{
		return 	MipmapNum(fi) 
				+ CString(_T(" "))
				+ getResizeString(fi) 
				+ CString(_T(" "))
				+ getAddFlags(fi)
				+ CString(_T(" -i"))
				+ Utils::QuoteString(fi.getFullPathWithFName()) 
				+ CString(_T(" -o"))
				+ Utils::QuoteString(fi.getTargetFullPathWithFName());

	}

};

class CNoneConverter : CConverter
{
public:
	CNoneConverter() : CConverter()
	{
		//checkCmdUtil();
	}
		
	bool checkCmdUtil()
	{
		_tprintf(_T("\nNone always here!"));
		return true;
	}

	CString getCmdUtilPath()
	{
		return CString(_T(""));
	}

	CString getTargetExt(const CFileInfo &inf)
	{
		return Utils::getExt(inf.getFileName());
	}

	bool textureFormatSupportedByConverter(const CFileInfo &inf)
	{
		return true;
	}

	CString getArgString(const CFileInfo &fi) const
	{
		return 	Utils::QuoteString(fi.getFullPathWithFName()) 
				+ CString(_T(" "))
				+ Utils::QuoteString(fi.getTargetFolder() + CString(_T("\\")));

	}

};

//------------
class CTexturesList
{
	//CSimpleArray<CFileInfo> mList;
	map<CString,CFileInfo> mList;
	map<CString,CFileInfo>::iterator it;
	//CConverter *mpConv;

	map<CString,CConverter*> mConv;
	bool texturesChanged;

public:

	CConverter *getCoverter(const CString &name) 
	{ 
		//return mpConv; 
		return mConv[name];
	}

	bool isExistAndSame(const CFileInfo &fi)
	{
		if(!hasFile(fi.getFileName()))
			return false;

		//???
		//if(mList[fi.getFileName()] == fi)
		//	return true;

		return (mList[fi.getFileName()] == fi);
			
	}

	bool hasFile(const CString &name)
	{
		return (mList.find(name) != mList.end());
	}

	CFileInfo& getFile(const CString &name)
	{
		return mList[name];
	}

	void _addToList(const CFileInfo &fi)
	{
		//mList[fi.getFileName()] = fi;

		mList.insert(std::pair<CString,CFileInfo>(fi.getFileName(),fi));
	}

	void AddFileToList(const CFileInfo &fi)
	{
		if(!hasFile(fi.getFileName()))
		{
			_addToList(fi);
		}
		else
		{
			CFileInfo old = mList[fi.getFileName()];
	
			if(old == fi && !fi.targetFileExist())
			{
				//_addToList(fi);
			}
			else
			{
				if(old.getFullPathWithFName() != fi.getFullPathWithFName())
				{
					_tprintf(_T("\nWARNING: Duplicate file found \n%s\nand\n%s"),(LPCTSTR)old.getFullPathWithFName(),
						(LPCTSTR)fi.getFullPathWithFName());
					
				}
				
				//delete previous
				Utils::deleteFile(old.getTargetFullPathWithFName());
				
			}

			mList[fi.getFileName()] = fi;
		}

	}

	void AddFolder(const CSimpleArray<CString> &f)
	{
		std::map<CString,CString> files;
		const TCHAR *types[] = {_T("tga"),_T("pvr"),_T("jpg"),_T("bmp"),_T("dds"),_T("png"),NULL};

		for(int i = 0; i < f.GetSize(); i++)
		{
			CString folder = f[i];
			_tprintf(_T("\nScanning texture folder: %s"), (LPCTSTR)folder);
			Utils::scanFolder(folder,gConfig.get(Params::params[Params::TARGET_FOLDER]),types,files);
		}
		
		//_tprintf(_T("\nLooking into folder %s"),(LPCTSTR)folder);


		CString default_converter = gConfig.get(Params::params[Params::TARGET_PLATFORM]);

		for (std::map<CString,CString>::iterator it=files.begin(); it!=files.end(); ++it)
		{
			CFileInfo fi(it->first, it->second);

			if(!fi.isImage())
			{
				_tprintf(_T("\nNot an image detected in %s"),(LPCTSTR)fi.getFullPathWithFName());
				continue;
			}

			fi.setConverterName(default_converter);


			mConv[default_converter]->preprocess(fi);

			//bool need_to_convert = false;

			//file not exist in list and it's not the same
			//OR
			//exist in list and the same, but target file doesn't exist 
			
			if(!isExistAndSame(fi) || !fi.targetFileExist())
			{

				if(!fi.targetFileExist())
				{
					_tprintf(_T("\nTarget not found %s (ignored: %d)"),(LPCTSTR)fi.getFileName(),fi.getIgnore());
				}
				else
				{
					_tprintf(_T("\nNeed to convert %s"),(LPCTSTR)fi.getFileName());
				}

				fi.setNeedConvert(true);

				//changed or not existed
				AddFileToList(fi);
								
				texturesChanged = true;
			}
			else
			{		
				fi.setNeedConvert(false);
				_tprintf(_T("\nNo changes detected in %s"),(LPCTSTR)fi.getFileName());
			}
		}


	}

	void CreateConverter()
	{
		CString platform = gConfig.get(Params::params[Params::TARGET_PLATFORM]);

		
		CConverter *cn = 0;
		

		//----

		cn = (CConverter *)new CDDSConverter();

		if(cn->checkCmdUtil())
		{
			mConv[_T("dds")] = cn;
		}
		
		cn = (CConverter *)new CPVRConverter();

		if(cn->checkCmdUtil())
			mConv[_T("pvr")] = cn;

		printf("\nChecking none!");
		CNoneConverter *cn2 = new CNoneConverter();

		//if(cn2->checkCmdUtil())
		//{
			printf("\nAdding none!");
			mConv[_T("none")] = (CConverter *)cn2;
		//}

		CString default_converter = gConfig.get(Params::params[Params::TARGET_PLATFORM]);

		if(mConv[default_converter] == 0)
		{
			printf("\nDefault converter not found! Use 'none', please!");
			exit(1);	
		}
		
		/*(platform == CString(L"dds"))
		{
			mpConv = (CConverter *)new CDDSConverter();
		}
		else if(platform == CString(L"pvr"))
		{
			mpConv = (CConverter *)new CPVRConverter();
		}	

		if(mpConv == 0)
		{
			_tprintf(_T("\nSpecified texture format do not found: %s"), (LPCTSTR)platform);
			return;
		}*/

		//_tprintf(_T("\nUsing texture tool: %s\n\n"), (LPCTSTR)mpConv->getCmdUtilPath());

		//create result folder
		SHCreateDirectoryEx(NULL,(LPCWSTR)gConfig.get(Params::params[Params::TARGET_FOLDER]),NULL);
	}

	bool texturesHasChanged()
	{
		return texturesChanged;
	}

	void startScan()
	{
		texturesChanged = false;

		
		CreateConverter();

		//��������� ������ ������ ������
		LoadFileInfo();

		//add new files and check changes

		CSimpleArray<CString> p;
		int size = gConfig.getValuesCount(Params::params[Params::TEX_SCAN_FOLDER]);

		for(int i = 0; i < size; i++)
		{
			p.Add(gConfig.get(Params::params[Params::TEX_SCAN_FOLDER],i));				

			//_tprintf(_T("\nScanning texture folder: %s"), (LPCTSTR)path);
		}

		AddFolder(p);

		//ProcessFiles();
		SaveFileInfo();

	}


	static UINT MyThreadProc( LPVOID pParam )
	{
		/*CFileInfo *f = (CFileInfo*)pParam;

		

		

		_tprintf(_T("\nconvert %s"),(LPCTSTR)f->getFileName());

		gTexturesList.getCoverter()->convert(*f);

		threads--;

		_tprintf(_T("\nstop thread %d"),threads);*/

		return 0;
	}

	void printStat()
	{
		int tga = 0, jpg = 0, pvr = 0, png = 0, dds = 0, bmp = 0;

		for ( it=mList.begin() ; it != mList.end(); it++ )
		{
			CFileInfo fi = (*it).second;

			if(fi.getTargetFName().Find(_T(".jpg")) >= 0)
				jpg++;

			if(fi.getTargetFName().Find(_T(".tga")) >= 0)
				tga++;

			if(fi.getTargetFName().Find(_T(".pvr")) >= 0)
				pvr++;

			if(fi.getTargetFName().Find(_T(".png")) >= 0)
				png++;

			if(fi.getTargetFName().Find(_T(".dds")) >= 0)
				dds++;

			if(fi.getTargetFName().Find(_T(".bmp")) >= 0)
				bmp++;
		}

		_tprintf(_T("\nIn the output folder should be \njpg: %d\ntga: %d\npvr %d\npng: %d\ndds: %d\nbmp: %d"),
			jpg,tga,pvr,png,dds,bmp);
	}

	void processConverting()
	{
		for ( it=mList.begin() ; it != mList.end(); it++ )
		{
			CFileInfo fi = (*it).second;

			if(fi.getNeedConvert())
			{
				CString cn = fi.getConverterName();
			
				int w = Utils::getInt(fi.getTargetWidth());
				int h = Utils::getInt(fi.getTargetHeight());

				_tprintf(_T("\nTrying to converting %s with %s to %dx%d"),fi.getFileName(),(LPCTSTR)cn,w,h);
				
				mConv[cn]->convert(fi);
			}
			else
			{

			}

			/*while(1)
			{
				if(::threads < 2)
				{
					_tprintf(_T("\nstop wait %d"),threads);
					break;
				}
				else
				{
					//_tprintf(_T("\nwaiting %d"),threads);
				}
			}

			threads++;

			_tprintf(_T("\nrun thread %d"),threads);

			AfxBeginThread(MyThreadProc, &(*it).second);*/


		}

		//AfxBeginThread(MyThreadProc, pNewObject);
		
		
	}

	void LoadFileInfo()
	{
		CXMLParser	xml(getFilelistFullPath());

		if(!xml.isOK())
			return;

		XML_NODE* pElement = ((XML_NODE*)xml)->first_node("file");
    
		while(pElement)
		{		
			CFileInfo cfi(pElement);

			if(Utils::fileExist(cfi.getFullPathWithFName()))
			{
				AddFileToList(CFileInfo(pElement));
			}
			else
			{
				_tprintf(_T("\nSource file listed in list %s, not exist. Delete compressed file if possible."),(LPCTSTR)cfi.getFileName());
				Utils::deleteFile(cfi.getTargetFullPathWithFName());
			}

			pElement = pElement->next_sibling("file");
		}
	}

	CString getFilelistFullPath()
	{
		//???

		CString cfg = gConfig.getFullPath();

		
		
		return gConfig.get(Params::params[Params::WORK_FOLDER]) + CString(L"\\file_list.xml");
	}

	void SaveFileInfo()
	{
		CString wf = gConfig.get(Params::params[Params::WORK_FOLDER]);

		//create working folder
		SHCreateDirectoryEx(NULL,(LPCWSTR)wf,NULL);

		CString pFileName = getFilelistFullPath();
		CStdioFile f;		
		
		f.Open(pFileName, CFile::modeCreate | CFile::modeWrite 
		   | CFile::typeText);

		f.WriteString(L"<file_list>\n");
		
		for ( it=mList.begin() ; it != mList.end(); it++ )
		{
			f.WriteString((*it).second.toString(L" "));
		}

		f.WriteString(L"</file_list>");

		f.Close();
	}

};

CTexturesList gTexturesList;

//----------------------

class CMaterialsList
{
	class CMaterial
	{
		CString fileName;
		CString fullPath;
		bool mProcessed;

	public:
		CMaterial() {}
		CMaterial(const CString &fn, const CString &p) : fileName(fn), mProcessed(false) { fullPath = p + CString(_T("\\")) + fn; }

		const CString &getFullPath() const {	return fullPath; }
		bool getProcessed() { return mProcessed; }
		void setProcessed() { mProcessed = true; }

	};

	map<CString,CMaterial> mList;
	map<CString,CMaterial>::iterator it;

public:

	bool AddFolder(const CSimpleArray<CString> &f)
	{
		std::map<CString,CString> files;
		const TCHAR *types[] = {_T("material"),NULL};

		for(int i = 0; i < f.GetSize(); i++)
		{
			CString folder = f[i];
			_tprintf(_T("\nScanning material folder: %s"), (LPCTSTR)folder);
			Utils::scanFolder(folder,gConfig.get(Params::params[Params::TARGET_FOLDER]),types,files);
		}

		//Utils::scanFolder(folder,gConfig.get(Params::params[Params::TARGET_FOLDER]),types,files);
		
		if(files.size() == 0)
			return false;

		for (std::map<CString,CString>::iterator it=files.begin(); it!=files.end(); ++it)
		{
			mList.insert(std::pair<CString,CMaterial>(it->first, CMaterial(it->first, it->second)));

			//mList[it->first] = CMaterial(finder.GetFileName(), finder.GetFilePath());
		}

		return true;
	}

	bool materialExist(const CString &mat)
	{
		return mList.find(mat) != mList.end();
	}

	CSimpleArray<CString> ParseLine(CString s, TCHAR delimiter, TCHAR delimiter2)
	{
		CSimpleArray<CString> a;

		int start = 0;

		s.Insert(0,delimiter);
		s.AppendChar(delimiter);
		s.Replace(delimiter2,delimiter);

		//while(1)
		for(int i = 0; i < s.GetLength(); i++)
		{
			TCHAR ch = s.GetAt(i);

			if(ch != delimiter)
			{
				if(start == 0)
					start = i;

				continue;
			}
			else
			{
				if(start == 0)
					continue;
			}

			CString res = s.Mid(start,i-start);

			res.Remove(delimiter);
			res.MakeLower();
			a.Add(res);


			start = 0;
		}

		return a;
	}

	CSimpleArray<CString> GetTextureFileNamesFromLine(CSimpleArray<CString> &a)
	{
		CSimpleArray<CString> file_names;

		if(a[0] == CString("texture"))
		{
			file_names.Add(a[1]);
		}
		else if(a[0] == CString("set_texture_alias"))
		{
			if(a.GetSize() < 3)
			{
				int k = 0;

			}

			file_names.Add(a[2]);
		}
		else if(a[0] == CString("cubic_texture"))
		{
			if(a.GetSize() > 3)
			{
				for(int i = 1; i < a.GetSize(); i++)
					file_names.Add(a[i]);
			}
			else //������ �������, ����� �������� �����, ���� �� ������
			{
				//CSimpleArray<CString> cubes;
				CString cubes[] = {_T("_fr"),_T("_bk"),_T("_up"),_T("_dn"),_T("_lf"),_T("_rt")};

				for(int i = 0; i < 6; i++)
				{
					CString n = a[1];
					n.Insert(n.Find('.'),cubes[i]);
					file_names.Add(n);
				}

			}
		}
		else if(a[0] == CString("anim_texture"))
		{
			int count = _wtoi(a[2]);

			for(int i = 0; i < count; i++)
			{
				CString n = a[1];
				CString num;
				num.Format(_T("_%d"),i);
				n.Insert(n.Find('.'),num);
				file_names.Add(n);
			}
		}


		return file_names;
	}

	void processMaterialString(CString &str)
	{
		if(str.GetLength() < 3)
			return;

		CString extention = _T("");
		if(str.Find(_T(".tga")) >= 0)
		{
			extention = _T(".tga");
		}
		else if(str.Find(_T(".jpg")) >= 0)
		{
			extention = _T(".jpg");
		}
		else if(str.Find(_T(".png")) >= 0)
		{
			extention = _T(".png");
		}

		if(extention.GetLength() == 0)
			return;

		CSimpleArray<CString> a = ParseLine(str,' ','\t');

		if(a.GetSize() == 0)
			return;

		CSimpleArray<CString> tex_names = GetTextureFileNamesFromLine(a);

		for(int i = 0; i < tex_names.GetSize(); i++)
		{
			if(gTexturesList.hasFile(tex_names[i]))
			{
				CFileInfo fi = gTexturesList.getFile(tex_names[i]);

				CString new_name = fi.getTargetFName();

				//str.Replace(tex_names[i],new_name);

				str.Replace(extention,fi.getTargetExt());
				
			}
		}
	
	}


	void processMaterial(CStdioFile &file, const CString &mat)
	{
		_tprintf(_T("\nProcess material %s"),(LPCTSTR)mat);

		if(!materialExist(mat))
			return;

		CMaterial *pMat = &mList[mat];

		if(pMat->getProcessed())
			return;

		pMat->setProcessed();

		//-------

		file.WriteString(_T("\n"));
		file.WriteString(_T("//") + pMat->getFullPath());
		file.WriteString(_T("\n"));
		file.WriteString(_T("\n"));

		CStdioFile mf(pMat->getFullPath(), CFile::modeRead | CFile::typeText);

		CString str;
		while(mf.ReadString(str))
		{
			if(str.Find(_T("import")) >= 0)
				continue;

			processMaterialString(str);
			
			if(str.Find(_T('.')) >= 0)
				str.MakeLower();

			file.WriteString(str);
			file.WriteString(_T("\n"));

		}


		mf.Close();

	}

	CString getResultMaterialFName() const
	{
		return gConfig.get(Params::params[Params::TARGET_FOLDER]) + _T("\\all.material");
	}

	void startScan()
	{
		//CString path = gConfig.get(Params::params[Params::MAT_SCAN_FOLDER]);

		//FIXME
		CSimpleArray<CString> p;
		int size = gConfig.getValuesCount(Params::params[Params::MAT_SCAN_FOLDER]);

		for(int i = 0; i < size; i++)
		{
			p.Add(gConfig.get(Params::params[Params::MAT_SCAN_FOLDER],i));				
			///_tprintf(_T("\nScanning texture folder: %s"), (LPCTSTR)path);
		}

		//-------

		if(!AddFolder(p))
			return;
		
		CStdioFile file(getResultMaterialFName(),CFile::modeCreate | CFile::modeWrite | CFile::typeText);

		processMaterial(file,_T("00_base.material"));
		processMaterial(file,_T("00_base2.material"));
		processMaterial(file,_T("00_base_ispy.material"));

		for ( it=mList.begin() ; it != mList.end(); it++ )
		{
			processMaterial(file,it->first);
		}

		file.Close();
	}

};

CMaterialsList gMaterialsList;

//+ �������� �� ���� ������ ���� ��������
//+ ��������� ������� ����� � ������, � ����� ������ � ����
//+- ��������� ����� �� �������� ����������(��������� �������?, �������, ��������������) � ������ ������
//+ ���������
//+ ������ ���������, ������ �������� ������� �� ������������


int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
#ifdef FREEIMAGE_LIB
	FreeImage_Initialise();
#endif // FREEIMAGE_LIB

	int nRetCode = 0;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: code your application's behavior here.
	}

	bool cfg_loaded = false;
	CString wf;

	//���� � xml c �����������
	if(argc == 2)
	{
		wf = CString(argv[1]);

		if(wf.Find(_T(".xml")) < 0)
		{
			Utils::fixPathValue(wf);

			if(wf.GetLength() > 0)
				wf += L"\\settings.xml";
			else
				wf = "settings.xml";
		}

		cfg_loaded = gConfig.load(wf);
	}

	//������ ���� ��� �������
	if(argc > 1 && !cfg_loaded)
	{
		/* --debug_output=true --ignore_texture=blue_fire_* --ignore_texture=11_island_05_fon --materials_folder=d:\Projects\MilleniumPuzzles\tmp\materials
		 --max_texture_size=1024
		 --pvrtextool_path=c:\Imagination\PowerVR\GraphicsSDK\PVRTexTool\CL\Windows_x86_64\PVRTexToolCL.exe
		 --pvrtextool_path=j:\Backups\ArtemHDDs\G\My Dropbox\Projects\ResPack\pvrtextool.exe
		 --output_folder=d:\Projects\MilleniumPuzzles\demo_221112\data.ios
		 --output_format=pvr
		 --ddsconv_path=c:\Program Files (x86)\Microsoft DirectX SDK (February 2010)\Utilities\bin\x64\texconv.exe
		 --textures_folder=d:\Projects\MilleniumPuzzles\tmp\textures
		 --work_folder=d:\Projects\MilleniumPuzzles\pack
		 --default_mip_num=3
		 --ignore_texture_for_mip=01*
		 --additional_pvr_converter_flags=-yflip0 -fOGLPVRTC4 -pvrtciterations1 -pvrtcfast */

		CString cmd;

		for(int i = 0; i < argc; i++)
			cmd += CString(argv[i]) + CString(_T(" "));

		cfg_loaded = gConfig.loadFromCmdline(cmd);
	}
	

	//������ ���� ��� �������
	if(!cfg_loaded)
	{

		printf("Bad path for config!\n");
		return 0;
	}

	
	if(!cfg_loaded)
	{
		CString def[] = {
		_T("d:\\Projects\\SuperQuest\\svn\\data\\packed\\textures\\00_alpha"),
		_T("d:\\Projects\\SuperQuest\\svn\\data\\packed\\materials"),
		_T("true"),
		_T("d:\\Projects\\SuperQuest\\data.x86\\packed\\textures"),
		_T("dds"),
		_T("d:\\Projects\\SuperQuest\\data.x86\\packed\\textures"),
		_T("j:\\Backups\\ArtemHDDs\\G\\My Dropbox\\Projects\\ResPack\\texconv.exe"),
		_T("j:\\Backups\\ArtemHDDs\\G\\My Dropbox\\Projects\\ResPack\\texconv.exe"),
	 	_T("2048"),
		_T("ignore"),
		_T("3"),
		_T("ignore_mip"),
		_T("-yflip0 -fOGLPVRTC4 -pvrtciterations1 -pvrtcfast"),
		_T("true"),
		};

		for(int i = 0; i < Params::MAX_VALUE; i++)
			gConfig.addParam(Params::params[i],def[i],Params::params_help[i]);


		_tprintf(_T("\nsave %s!"),(LPCTSTR)wf);

		//TODO: ��������� ��������
		gConfig.save(wf);

		return 0;
	}

	for(int i = 0; i < Params::MAX_VALUE; i++)
		_tprintf(_T("\n%s=%s"),(LPCTSTR)Params::params[i],(LPCTSTR)gConfig.get(Params::params[i])); 

	_tprintf(_T("\n\n"));
	
	gTexturesList.startScan();

	if(gTexturesList.texturesHasChanged() || !Utils::fileExist(gMaterialsList.getResultMaterialFName()))
	{
		gMaterialsList.startScan();
	}
	else
	{
		_tprintf(_T("\nDon't scan materials, because no changes detected in textures!"));
	}

	gTexturesList.printStat();


	DWORD t = GetTickCount();

	gTexturesList.processConverting();

	_tprintf(_T("\nCompressed in %u sec"),(GetTickCount() - t)/1000);

// call this ONLY when linking with FreeImage as a static library
#ifdef FREEIMAGE_LIB
	FreeImage_DeInitialise();
#endif // FREEIMAGE_LIB

	return nRetCode;
}
