#include "Ogre.h"

#pragma once

// The one and only application object

//material
//- name : char[64]

//technique
//- pass number : uchar

//pass
//- texure units number: uchar
//- lighting : bool
//- depth_check : bool
//- depth_write : bool
//- cull_software : bool (none, default)
//- cull_hardware : bool (none, default)
//- colour_write : bool
//- scene_blend: uchar[1..2] (modulate, add, one_minus_dest_colour one)
//- alpha_rejection : uchar[2] (default, greater_equal 128)

//- ambient : float[3], (1.0 1.0 1.0)
//- diffuse : float[3] (1.0 1.0 1.0)
//- specular : flaat[4] 0.0 0.0 0.0 0.0
//- emissive : float[3]  0.0 0.0 0.0

using namespace Ogre;

class CBinaryMaterials : public Ogre::Serializer, public Ogre::ScriptLoader
{
	static const int VERSION_IDD = 10002;
	
	struct MaterialDesc
	{
		char name[64];
		uchar pass_num;
	};

	static void setParam(ushort &param, uchar byte)
	{
		param |= byte;
	}

	static void clearParam(ushort &param, uchar byte)
	{
		param &= ~byte;
	}

	static bool getParam(ushort param, uchar byte)
	{
		return (param & byte) > 0;
	}

	#define PASS_LIGHTING		1
	#define PASS_DEPTH_CHECK	1<<1
	#define PASS_DEPTH_WRITE	1<<2
	#define PASS_CULL_HARD		1<<3
	#define PASS_CULL_SOFT		1<<4
	#define PASS_COLOR_WRITE	1<<5

	struct PassDesc
	{
		uchar tex_unit_num;
		
		ushort bool_params;
		
		uchar scene_blend[2];
		uchar alpha_rejection[2];

		uchar depth_func;

		ColourValue ambient;
		ColourValue diffuse;
		ColourValue specular;
		ColourValue emissive;

		float shininess;

		void setParam(uchar byte)
		{
			CBinaryMaterials::setParam(bool_params, byte);
		}

		bool getParam(uchar byte)
		{
			return CBinaryMaterials::getParam(bool_params, byte);
		}

		void save(Ogre::Pass *pPass)
		{
			ambient = pPass->getAmbient();
			diffuse	= pPass->getDiffuse();
			specular = pPass->getSpecular();
			emissive = pPass->getEmissive();
			shininess = pPass->getShininess();

			depth_func = (uchar)pPass->getDepthFunction();

			saveColorWrite(pPass);
			saveCullHardware(pPass);
			saveCullSoftware(pPass);
			saveDepthCheck(pPass);
			saveDepthWrite(pPass);
			saveLighting(pPass);
			saveSceneBlend(pPass);
			saveAlphaRejection(pPass);
		}

		void load(Ogre::Pass *pPass)
		{
			pPass->setAmbient(ambient);
			pPass->setDiffuse(diffuse);
			pPass->setSpecular(specular);
			pPass->setEmissive(emissive);
			pPass->setShininess(shininess);

			pPass->setDepthFunction((CompareFunction)depth_func);

			loadColorWrite(pPass);
			loadCullHardware(pPass);
			loadCullSoftware(pPass);
			loadDepthCheck(pPass);
			loadDepthWrite(pPass);
			loadLighting(pPass);
			loadSceneBlend(pPass);
			loadAlphaRejection(pPass);
		}

		void saveLighting(Ogre::Pass *pPass)
		{
			if(pPass->getLightingEnabled())
				setParam(PASS_LIGHTING);
		}

		void loadLighting(Ogre::Pass *pPass)
		{
			pPass->setLightingEnabled(getParam(PASS_LIGHTING));
		}

		void saveDepthCheck(Ogre::Pass *pPass)
		{
			if(pPass->getDepthCheckEnabled())
				setParam(PASS_DEPTH_CHECK);
		}

		void loadDepthCheck(Ogre::Pass *pPass)
		{
			pPass->setDepthCheckEnabled(getParam(PASS_DEPTH_CHECK));
		}

		void saveDepthWrite(Ogre::Pass *pPass)
		{
			if(pPass->getDepthWriteEnabled())
				setParam(PASS_DEPTH_WRITE);
		}

		void loadDepthWrite(Ogre::Pass *pPass)
		{
			pPass->setDepthWriteEnabled(getParam(PASS_DEPTH_WRITE));
		}

		void saveCullHardware(Ogre::Pass *pPass)
		{
			if(pPass->getCullingMode() == CULL_NONE)
				setParam(PASS_CULL_HARD);
		}

		void loadCullHardware(Ogre::Pass *pPass)
		{
			if(getParam(PASS_CULL_HARD))
				pPass->setCullingMode(CULL_NONE);
		}

		void saveCullSoftware(Ogre::Pass *pPass)
		{
			if(pPass->getManualCullingMode() == MANUAL_CULL_NONE)
				setParam(PASS_CULL_SOFT);
		}

		void loadCullSoftware(Ogre::Pass *pPass)
		{
			if(getParam(PASS_CULL_SOFT))
				pPass->setCullingMode(CULL_NONE);
		}

		void saveColorWrite(Ogre::Pass *pPass)
		{
			if(pPass->getColourWriteEnabled())
				setParam(PASS_COLOR_WRITE);
		}

		void loadColorWrite(Ogre::Pass *pPass)
		{
			pPass->setColourWriteEnabled(getParam(PASS_COLOR_WRITE));
		}

		void saveSceneBlend(Ogre::Pass *pPass)
		{
			scene_blend[0] = (uchar)pPass->getSourceBlendFactor();
			scene_blend[1] = (uchar)pPass->getDestBlendFactor();
		}

		void loadSceneBlend(Ogre::Pass *pPass)
		{
			pPass->setSceneBlending((SceneBlendFactor)scene_blend[0],(SceneBlendFactor)scene_blend[1]);
		}

		void saveAlphaRejection(Ogre::Pass *pPass)
		{
			alpha_rejection[0] = (uchar)pPass->getAlphaRejectFunction();
			alpha_rejection[1] = pPass->getAlphaRejectValue();
		}

		void loadAlphaRejection(Ogre::Pass *pPass)
		{
			pPass->setAlphaRejectFunction((CompareFunction)alpha_rejection[0]);
			pPass->setAlphaRejectValue(alpha_rejection[1]);
		}
	};

	
//texture_unit
//+ tex_coord_set: uchar
//+ tex_address_mode: uchar[1..2] (default, clamp)
//+ max_anisotropy: bool (8, default)
//+ filtering: uchar[3] (none, linear, bilinear, trilinear, anisotropic, linear linear linear)
//- colour_op_ex: uchar[3] (defauld, modulate_x2 src_texture src_current)
//+ texture: char[64], uint[1..3]
//- rotate_anim: float
//+ scale: float[2] 1.5 1.5
//- wave_xform: uchar[2], float[4] scale_x sine -1 0.5 0.0 -0.2
//+ anim_texture char[64], float[2]
//+ scroll: float[2], 0 -0.125
//+ rotate

	struct TexUnitDesc
	{
		char texture[64];

		uchar tex_coord_set;

		uchar tex_address_mode[2];
		uchar max_anisotropy;
		uchar filtering[3]; //TODO
		uchar colour_op_ex[3]; //TODO

		float rotate_anim; //TODO

		float scale[2];
		float scroll[2];

		uchar texture_type;

		uchar mipmaps_num;

		uchar frames_num;
		float duration;

		float rotate;

		LayerBlendModeEx color_blend_mode;

		uchar effects_num;
		TextureUnitState::TextureEffect tex_effects[3];

		/*void setParam(uchar byte)
		{
			CBinaryMaterials::setParam(bool_params, byte);
		}

		bool getParam(uchar byte)
		{
			return CBinaryMaterials::getParam(bool_params, byte);
		}*/

		void save(Ogre::TextureUnitState *pTex)
		{
			saveTexture(pTex);
			saveTexCoordSet(pTex);
			saveTexAddrMode(pTex);
			saveAnimTex(pTex);
		}

		void load(Ogre::TextureUnitState *pTex)
		{
			loadTexture(pTex);
			loadTexCoordSet(pTex);
			loadTexAddrMode(pTex);
			loadAnimTex(pTex);
		}

		//------
	
		void saveTexture(Ogre::TextureUnitState *pTex)
		{
			memcpy(texture,pTex->getTextureName().c_str(),sizeof(texture));

			texture_type = (uchar)pTex->getTextureType();
			mipmaps_num = (uchar)pTex->getNumMipmaps();
			
			scale[0] = pTex->getTextureUScale();
			scale[1] = pTex->getTextureVScale();

			scroll[0] = pTex->getTextureUScroll();
			scroll[1] = pTex->getTextureVScroll();

			max_anisotropy = pTex->getTextureAnisotropy();

			rotate = pTex->getTextureRotate().valueRadians();

			filtering[0] = (FilterOptions)pTex->getTextureFiltering(FT_MIN);
			filtering[1] = (FilterOptions)pTex->getTextureFiltering(FT_MAG);
			filtering[2] = (FilterOptions)pTex->getTextureFiltering(FT_MIP);

			color_blend_mode = pTex->getColourBlendMode();

			effects_num = (uchar)pTex->getEffects().size();

			if(effects_num > 0)
			{
				int i = 0;
				for (TextureUnitState::EffectMap::const_iterator j = pTex->getEffects().begin(); j != pTex->getEffects().end(); ++j)
				{
					tex_effects[i] = j->second;
					i++;
				}
			}
		}

		void loadTexture(Ogre::TextureUnitState *pTex)
		{
			pTex->setTextureName(texture,(TextureType)texture_type);
			pTex->setNumMipmaps(mipmaps_num);

			pTex->setTextureUScale(scale[0]);
			pTex->setTextureVScale(scale[1]);
			
			pTex->setTextureUScroll(scroll[0]);
			pTex->setTextureVScroll(scroll[1]);
			
			pTex->setTextureAnisotropy(max_anisotropy);
			
			pTex->setTextureRotate(Ogre::Radian(rotate));

			pTex->setTextureFiltering((FilterOptions)filtering[0],(FilterOptions)filtering[1],(FilterOptions)filtering[2]);

			if(color_blend_mode.blendType == LBT_COLOUR)
				pTex->setColourOperationEx(color_blend_mode.operation,color_blend_mode.source1,color_blend_mode.source2,color_blend_mode.colourArg1, color_blend_mode.colourArg2,color_blend_mode.factor);
			else
				pTex->setAlphaOperation(color_blend_mode.operation,color_blend_mode.source1,color_blend_mode.source2,color_blend_mode.alphaArg1, color_blend_mode.alphaArg2,color_blend_mode.factor);

			for(int i = 0; i < effects_num; i++)
			{
				pTex->addEffect(tex_effects[i]);
			}
		}

		void saveAnimTex(Ogre::TextureUnitState *pTex)
		{
			frames_num = (uchar)pTex->getNumFrames();
			duration = (float)pTex->getAnimationDuration();

			if(frames_num > 1)
			{
				Ogre::String fname;
				Ogre::String ext;
				Ogre::StringUtil::splitBaseFilename(texture,fname,ext);

				fname = fname.substr(0,fname.size()-2);

				sprintf(texture,"%s.%s",fname.c_str(),ext.c_str());
			}
		}

		void loadAnimTex(Ogre::TextureUnitState *pTex)
		{
			if(frames_num > 1)
			{
				pTex->setAnimatedTextureName(texture, frames_num, duration);
			}
		}
				
		void saveTexCoordSet(Ogre::TextureUnitState *pTex)
		{
			tex_coord_set = (uchar)pTex->getTextureCoordSet();
		}

		void loadTexCoordSet(Ogre::TextureUnitState *pTex)
		{
			pTex->setTextureCoordSet(tex_coord_set);
		}

		void saveTexAddrMode(Ogre::TextureUnitState *pTex)
		{
			tex_address_mode[0] = (uchar)pTex->getTextureAddressingMode().u;
			tex_address_mode[1] = (uchar)pTex->getTextureAddressingMode().v;
		}

		void loadTexAddrMode(Ogre::TextureUnitState *pTex)
		{
			pTex->setTextureAddressingMode((TextureUnitState::TextureAddressingMode)tex_address_mode[0],
											(TextureUnitState::TextureAddressingMode)tex_address_mode[1],
											TextureUnitState::TAM_WRAP);
		}
	};

public:

	//TODO
	void importMaterials(const Ogre::String &s)
	{
		std::fstream *f = OGRE_NEW_T(std::fstream, MEMCATEGORY_GENERAL)();
		f->open(s.c_str(), std::ios::binary | std::ios::in);
		DataStreamPtr stream(OGRE_NEW FileStreamDataStream(f));

		mStream = stream;

        loadMaterials("General");

		stream->close();
	}

	void exportMaterials(const Ogre::String &s)
	{
		std::map<Ogre::String,bool> mFiles;
		Ogre::MaterialManager::ResourceMapIterator it = Ogre::MaterialManager::getSingletonPtr()->getResourceIterator();

		while(it.hasMoreElements())
		{
			Ogre::ResourcePtr pMaterial = it.getNext();
			
			Ogre::String org = pMaterial->getOrigin();
			
			if(org.size() > 0 && mFiles.find(org) == mFiles.end())
			{
				printf("\nAdded origin %s", org.c_str());
				mFiles[org] = true;
			}
		}

		for(std::map<Ogre::String,bool>::iterator it = mFiles.begin(), end = mFiles.end(); it != end; ++it)
		{
			Ogre::String strOr = it->first;
			Ogre::String name = s + strOr;
			Ogre::String ext;
						
			//StringUtil::splitFilename(strOr,name,ext);

			name += ".bin";

			printf("\nSaving %s", name.c_str());

			std::fstream *f = OGRE_NEW_T(std::fstream, MEMCATEGORY_GENERAL)();
			f->open(name.c_str(), std::ios::binary | std::ios::out);
			DataStreamPtr stream(OGRE_NEW FileStreamDataStream(f));

			mStream = stream;

			saveMaterials(strOr);

			stream->close();
		}

		
	}

	void loadMaterials(const Ogre::String& groupName)
	{
		MaterialDesc md;

		int idd = 0;
		mStream->read(&idd, sizeof(int));

		if(idd != VERSION_IDD)
		{
			//LogManager::getSingleton().logMessage("ERROR: Wrong idd version in binary material! Update binaries with latest converter.");
			OGRE_EXCEPT(Exception::ERR_INTERNAL_ERROR, "ERROR: Wrong idd version in binary material! Update binaries with latest converter.","CBinaryMaterials::loadMaterials");
			return;
		}

		while(!mStream->eof())
		{
			mStream->read(&md, sizeof(MaterialDesc));

			Ogre::Technique *pTechniique = loadMaterialAndTechnique(md, mStream->getName(), groupName);

			for(int i = 0; i < md.pass_num; i++)
			{
				PassDesc pd;
				mStream->read(&pd, sizeof(PassDesc));
							
				//remove last pass without texture units
				#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
				if(pd.tex_unit_num == 0 && i == md.pass_num - 1)
				{
					continue;
				}
				#endif

				Pass *pPass = loadPass(pTechniique, pd);

				for(int j = 0; j < pd.tex_unit_num; j++)
				{	
					TexUnitDesc td;
					mStream->read(&td, sizeof(TexUnitDesc));

					loadTextireUnit(pPass,td);
				}
			}
		}
	}

	Ogre::Technique * loadMaterialAndTechnique(MaterialDesc &md, const Ogre::String &fname, const Ogre::String& groupName)
	{
		Ogre::Technique *pTechniique = 0;

		Ogre::MaterialPtr newMaterial = MaterialManager::getSingleton().create(md.name, groupName).staticCast<Material>(); 
		
		newMaterial->removeAllTechniques();

		newMaterial->_notifyOrigin(fname);

		return newMaterial->createTechnique();
	}

	Ogre::Pass * loadPass(Ogre::Technique *pTechniique, PassDesc &pd)
	{
		Ogre::Pass *pPass = pTechniique->createPass();

		pd.load(pPass);

		return pPass;
	}

	void loadTextireUnit(Ogre::Pass *pPass, TexUnitDesc &pd)
	{
		Ogre::TextureUnitState *pTex = pPass->createTextureUnitState();

		pd.load(pTex);
	}



	void writeMaterialAndTechnique(const char *name, uchar passCount)
	{
		MaterialDesc md;
		memset(&md,0,sizeof(MaterialDesc));

		memcpy(md.name,name,sizeof(md.name));
		md.pass_num = passCount;

		writeData(&md,sizeof(MaterialDesc),1);
	}

	void writePass(Ogre::Pass *pPass,uchar nTextureUnitCount)
	{
		PassDesc pd;
		memset(&pd,0,sizeof(PassDesc));

		//TODO: all
		pd.tex_unit_num = nTextureUnitCount;

		pd.save(pPass);

		writeData(&pd,sizeof(PassDesc),1);
	}

	void writeTextureUnit(Ogre::TextureUnitState *pTextureUnitState)
	{
		TexUnitDesc td;
		memset(&td,0,sizeof(TexUnitDesc));

		//TODO:
				
		td.save(pTextureUnitState);

		writeData(&td,sizeof(TexUnitDesc),1);
	}


	void saveMaterials(const Ogre::String &origin)
	{
		Ogre::MaterialManager::ResourceMapIterator it = Ogre::MaterialManager::getSingletonPtr()->getResourceIterator();

		int idd = VERSION_IDD;
		mStream->write(&idd, sizeof(int));
		
		while(it.hasMoreElements())
		{
			Ogre::MaterialPtr pMaterial = it.getNext().staticCast<Material>();
			
			if(origin != pMaterial->getOrigin())
				continue;

			Ogre::String mat_name = pMaterial->getName();

			Ogre::Material::TechniqueIterator techIter = pMaterial->getTechniqueIterator();
			while (techIter.hasMoreElements())
			{
				Ogre::Technique *pTechniique = techIter.getNext();

				Ogre::Technique::PassIterator passIter = pTechniique->getPassIterator();

				// ������� ���-�� �����
				uchar nPassCount = 0;
				while (passIter.hasMoreElements())
				{
					passIter.getNext();
					nPassCount++;
				}

				if (nPassCount == 0)
				{
					printf("\nThere's no passes in that material", mat_name.c_str());
					continue;
				}

				writeMaterialAndTechnique(mat_name.c_str(), nPassCount);

				passIter = pTechniique->getPassIterator();

				while (passIter.hasMoreElements())
				{
					Ogre::Pass *pPass = passIter.getNext();
					//				pPass->
					Ogre::Pass::TextureUnitStateIterator textureUnitStateIter = pPass->getTextureUnitStateIterator();

					// ������� ���-�� ���������� ������
					uchar nTextureUnitCount = 0;
					while (textureUnitStateIter.hasMoreElements())
					{
						textureUnitStateIter.getNext();
						nTextureUnitCount++;
					}
					
					writePass(pPass,nTextureUnitCount);
										
					textureUnitStateIter = pPass->getTextureUnitStateIterator();
					
					while (textureUnitStateIter.hasMoreElements())
					{
						Ogre::TextureUnitState *pTextureUnitState = textureUnitStateIter.getNext();

						writeTextureUnit(pTextureUnitState);
					}
				}

			}
			
			//printf("\n%s %d",mat_name.c_str(), mat_name.length());
		}
	}

	StringVector mScriptPatterns;

	CBinaryMaterials()
	{
		mScriptPatterns.push_back("*.bin");
		Ogre::ResourceGroupManager::getSingleton()._registerScriptLoader(this);
	}

	~CBinaryMaterials()
	{
		Ogre::ResourceGroupManager::getSingleton()._unregisterScriptLoader(this);
	}
	 
	Ogre::Real getLoadingOrder() const
    {
        return 89.0f;
    }
	
	const Ogre::StringVector& getScriptPatterns() const { return mScriptPatterns; }
	
	void parseScript(Ogre::DataStreamPtr& stream, const Ogre::String& groupName)
	{
		mStream = stream;

        loadMaterials(groupName);
	}
};