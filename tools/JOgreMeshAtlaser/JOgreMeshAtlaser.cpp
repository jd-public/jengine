// JOgreMeshAtlaser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "JOgreMeshAtlaser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#include "Ogre.h"
#include "OgreMeshSerializer.h"
#include <direct.h>

#include "JBinaryMaterials.h"

CWinApp theApp;

using namespace std;

using namespace Ogre;

// The one and only application object

//material
//- name : char[64]

//technique
//- pass number : uchar

//pass
//- texure units number: uchar
//- lighting : bool
//- depth_check : bool
//- depth_write : bool
//- cull_software : bool (none, default)
//- cull_hardware : bool (none, default)
//- colour_write : bool
//- scene_blend: uchar[1..2] (modulate, add, one_minus_dest_colour one)
//- alpha_rejection : uchar[2] (default, greater_equal 128)

//- ambient : float[3], (1.0 1.0 1.0)
//- diffuse : float[3] (1.0 1.0 1.0)
//- specular : flaat[4] 0.0 0.0 0.0 0.0
//- emissive : float[3]  0.0 0.0 0.0


/*class CBinaryMaterials : public Ogre::Serializer
{
	struct MaterialDesc
	{
		char name[64];
		uchar pass_num;
	};

	static void setParam(ushort &param, uchar byte)
	{
		param |= byte;
	}

	static void clearParam(ushort &param, uchar byte)
	{
		param &= ~byte;
	}

	static bool getParam(ushort param, uchar byte)
	{
		return param & byte;
	}

	#define PASS_LIGHTING		1
	#define PASS_DEPTH_CHECK	1<<1
	#define PASS_DEPTH_WRITE	1<<2
	#define PASS_CULL_HARD		1<<3
	#define PASS_CULL_SOFT		1<<4
	#define PASS_COLOR_WRITE	1<<5

	struct PassDesc
	{
		uchar tex_unit_num;
		
		ushort bool_params;
		
		uchar scene_blend[2];
		uchar alpha_rejection[2];

		void setParam(uchar byte)
		{
			CBinaryMaterials::setParam(bool_params, byte);
		}

		bool getParam(uchar byte)
		{
			return CBinaryMaterials::getParam(bool_params, byte);
		}

		void save(Ogre::Pass *pPass)
		{
			saveColorWrite(pPass);
			saveCullHardware(pPass);
			saveCullSoftware(pPass);
			saveDepthCheck(pPass);
			saveDepthWrite(pPass);
			saveLighting(pPass);
			saveSceneBlend(pPass);
			saveAlphaRejection(pPass);
		}

		void load(Ogre::Pass *pPass)
		{
			//- ambient : float[3], (1.0 1.0 1.0)
			//- diffuse : float[3] (1.0 1.0 1.0)
			//- specular : flaat[4] 0.0 0.0 0.0 0.0
			//- emissive : float[3]  0.0 0.0 0.0

			pPass->setAmbient(ColourValue::White);
			pPass->setDiffuse(ColourValue::White);
			pPass->setSpecular(ColourValue::Black);
			pPass->setEmissive(ColourValue::Black);

			loadColorWrite(pPass);
			loadCullHardware(pPass);
			loadCullSoftware(pPass);
			loadDepthCheck(pPass);
			loadDepthWrite(pPass);
			loadLighting(pPass);
			loadSceneBlend(pPass);
			loadAlphaRejection(pPass);
		}

		void saveLighting(Ogre::Pass *pPass)
		{
			if(pPass->getLightingEnabled())
				setParam(PASS_LIGHTING);
		}

		void loadLighting(Ogre::Pass *pPass)
		{
			pPass->setLightingEnabled(getParam(PASS_LIGHTING));
		}

		void saveDepthCheck(Ogre::Pass *pPass)
		{
			if(pPass->getDepthCheckEnabled())
				setParam(PASS_DEPTH_CHECK);
		}

		void loadDepthCheck(Ogre::Pass *pPass)
		{
			pPass->setDepthCheckEnabled(getParam(PASS_DEPTH_CHECK));
		}

		void saveDepthWrite(Ogre::Pass *pPass)
		{
			if(pPass->getDepthWriteEnabled())
				setParam(PASS_DEPTH_WRITE);
		}

		void loadDepthWrite(Ogre::Pass *pPass)
		{
			pPass->setDepthWriteEnabled(getParam(PASS_DEPTH_WRITE));
		}

		void saveCullHardware(Ogre::Pass *pPass)
		{
			if(pPass->getCullingMode() == CullingMode::CULL_NONE)
				setParam(PASS_CULL_HARD);
		}

		void loadCullHardware(Ogre::Pass *pPass)
		{
			if(getParam(PASS_CULL_HARD))
				pPass->setCullingMode(CullingMode::CULL_NONE);
		}

		void saveCullSoftware(Ogre::Pass *pPass)
		{
			if(pPass->getManualCullingMode() == ManualCullingMode::MANUAL_CULL_NONE)
				setParam(PASS_CULL_SOFT);
		}

		void loadCullSoftware(Ogre::Pass *pPass)
		{
			if(getParam(PASS_CULL_SOFT))
				pPass->setCullingMode(CullingMode::CULL_NONE);
		}

		void saveColorWrite(Ogre::Pass *pPass)
		{
			if(pPass->getColourWriteEnabled())
				setParam(PASS_COLOR_WRITE);
		}

		void loadColorWrite(Ogre::Pass *pPass)
		{
			pPass->setColourWriteEnabled(getParam(PASS_COLOR_WRITE));
		}

		void saveSceneBlend(Ogre::Pass *pPass)
		{
			scene_blend[0] = (uchar)pPass->getSourceBlendFactor();
			scene_blend[1] = (uchar)pPass->getDestBlendFactor();
		}

		void loadSceneBlend(Ogre::Pass *pPass)
		{
			pPass->setSceneBlending((SceneBlendFactor)scene_blend[0],(SceneBlendFactor)scene_blend[1]);
		}

		void saveAlphaRejection(Ogre::Pass *pPass)
		{
			alpha_rejection[0] = (uchar)pPass->getAlphaRejectFunction();
			alpha_rejection[1] = pPass->getAlphaRejectValue();
		}

		void loadAlphaRejection(Ogre::Pass *pPass)
		{
			pPass->setAlphaRejectFunction((CompareFunction)alpha_rejection[0]);
			pPass->setAlphaRejectValue(alpha_rejection[1]);
		}
	};

	
//texture_unit
//+ tex_coord_set: uchar
//+ tex_address_mode: uchar[1..2] (default, clamp)
//- max_anisotropy: bool (8, default)
//- filtering: uchar[3] (none, linear, bilinear, trilinear, anisotropic, linear linear linear)
//- colour_op_ex: uchar[3] (defauld, modulate_x2 src_texture src_current)
//+ texture: char[64], uint[1..3]
//- rotate_anim: float
//- scale: float[2] 1.5 1.5
//- wave_xform: uchar[2], float[4] scale_x sine -1 0.5 0.0 -0.2
//- anim_texture char[64], float[2]
//- scroll: float[2], 0 -0.125

	struct TexUnitDesc
	{
		char texture[64];

		uchar tex_coord_set;

		uchar tex_address_mode[2];
		uchar max_anisotropy;
		uchar filtering[3]; //TODO
		uchar colour_op_ex[3]; //TODO

		float rotate_anim; //TODO

		float scale[2];
		float scroll[2];

		uchar texture_type;

		uchar mipmaps_num;

		uchar frames_num;
		float duration;

		float rotate;

		
		void save(Ogre::TextureUnitState *pTex)
		{
			saveTexture(pTex);
			saveTexCoordSet(pTex);
			saveTexAddrMode(pTex);
			saveAnimTex(pTex);
		}

		void load(Ogre::TextureUnitState *pTex)
		{
			loadTexture(pTex);
			loadTexCoordSet(pTex);
			loadTexAddrMode(pTex);
			loadAnimTex(pTex);
		}

		//------
	
		void saveTexture(Ogre::TextureUnitState *pTex)
		{
			sprintf_s(texture,64,"%s",pTex->getTextureName().c_str());
			texture_type = (uchar)pTex->getTextureType();
			mipmaps_num = (uchar)pTex->getNumMipmaps();
			
			scale[0] = pTex->getTextureUScale();
			scale[1] = pTex->getTextureVScale();

			scroll[0] = pTex->getTextureUScroll();
			scroll[1] = pTex->getTextureVScroll();

			max_anisotropy = pTex->getTextureAnisotropy();

			rotate = pTex->getTextureRotate().valueRadians();

			filtering[0] = (FilterOptions)pTex->getTextureFiltering(FilterType::FT_MIN);
			filtering[1] = (FilterOptions)pTex->getTextureFiltering(FilterType::FT_MAG);
			filtering[2] = (FilterOptions)pTex->getTextureFiltering(FilterType::FT_MIP);

			
		}

		void loadTexture(Ogre::TextureUnitState *pTex)
		{
			pTex->setTextureName(texture,(TextureType)texture_type);
			pTex->setNumMipmaps(mipmaps_num);

			pTex->setTextureUScale(scale[0]);
			pTex->setTextureVScale(scale[1]);
			
			pTex->setTextureUScroll(scroll[0]);
			pTex->setTextureVScroll(scroll[1]);
			
			pTex->setTextureAnisotropy(max_anisotropy);
			
			pTex->setTextureRotate(Radian(rotate));

			pTex->setTextureFiltering((FilterOptions)filtering[0],(FilterOptions)filtering[1],(FilterOptions)filtering[2]);
		}

		void saveAnimTex(Ogre::TextureUnitState *pTex)
		{
			frames_num = (uchar)pTex->getNumFrames();
			duration = (float)pTex->getAnimationDuration();
		}

		void loadAnimTex(Ogre::TextureUnitState *pTex)
		{
			if(frames_num > 1)
			{
				pTex->setAnimatedTextureName(texture, frames_num, duration);
			}
		}
				
		void saveTexCoordSet(Ogre::TextureUnitState *pTex)
		{
			tex_coord_set = (uchar)pTex->getTextureCoordSet();
		}

		void loadTexCoordSet(Ogre::TextureUnitState *pTex)
		{
			pTex->setTextureCoordSet(tex_coord_set);
		}

		void saveTexAddrMode(Ogre::TextureUnitState *pTex)
		{
			tex_address_mode[0] = (uchar)pTex->getTextureAddressingMode().u;
			tex_address_mode[1] = (uchar)pTex->getTextureAddressingMode().v;
		}

		void loadTexAddrMode(Ogre::TextureUnitState *pTex)
		{
			pTex->setTextureAddressingMode((TextureUnitState::TextureAddressingMode)tex_address_mode[0],
											(TextureUnitState::TextureAddressingMode)tex_address_mode[1],
											TextureUnitState::TAM_WRAP);
		}
	};

public:

	//TODO
	void importMaterials(const String &s)
	{
		std::fstream *f = OGRE_NEW_T(std::fstream, MEMCATEGORY_GENERAL)();
		f->open(s.c_str(), std::ios::binary | std::ios::in);
		DataStreamPtr stream(OGRE_NEW FileStreamDataStream(f));

		mStream = stream;

        loadMaterials();

		stream->close();
	}

	void exportMaterials(const String &s)
	{
		std::fstream *f = OGRE_NEW_T(std::fstream, MEMCATEGORY_GENERAL)();
		f->open(s.c_str(), std::ios::binary | std::ios::out);
		DataStreamPtr stream(OGRE_NEW FileStreamDataStream(f));

		mStream = stream;

        saveMaterials();

		stream->close();
	}

	void loadMaterials()
	{
		MaterialDesc md;

		while(!mStream->eof())
		{
			mStream->read(&md, sizeof(MaterialDesc));

			Ogre::Technique *pTechniique = loadMaterialAndTechnique(md);

			for(int i = 0; i < md.pass_num; i++)
			{
				PassDesc pd;
				mStream->read(&pd, sizeof(PassDesc));

				Pass *pPass = loadPass(pTechniique, pd);

				for(int j = 0; j < pd.tex_unit_num; j++)
				{	
					TexUnitDesc td;
					mStream->read(&td, sizeof(TexUnitDesc));

					loadTextireUnit(pPass,td);
				}
			}
		}
	}

	Ogre::Technique * loadMaterialAndTechnique(MaterialDesc &md)
	{
		Ogre::Technique *pTechniique = 0;

		Ogre::MaterialPtr newMaterial = Ogre::MaterialManager::getSingleton().create(md.name,"General");
		
		return newMaterial->createTechnique();
	}

	Ogre::Pass * loadPass(Ogre::Technique *pTechniique, PassDesc &pd)
	{
		Ogre::Pass *pPass = pTechniique->createPass();

		pd.load(pPass);

		return pPass;
	}

	void loadTextireUnit(Ogre::Pass *pPass, TexUnitDesc &pd)
	{
		Ogre::TextureUnitState *pTex = pPass->createTextureUnitState();

		pd.load(pTex);
	}



	void writeMaterialAndTechnique(const char *name, uchar passCount)
	{
		MaterialDesc md;
		memset(&md,0,sizeof(MaterialDesc));

		sprintf(md.name,"%s",name);
		md.pass_num = passCount;

		writeData(&md,sizeof(MaterialDesc),1);
	}

	void writePass(Ogre::Pass *pPass,uchar nTextureUnitCount)
	{
		PassDesc pd;
		memset(&pd,0,sizeof(PassDesc));

		//TODO: all
		pd.tex_unit_num = nTextureUnitCount;

		pd.save(pPass);

		writeData(&pd,sizeof(PassDesc),1);
	}

	void writeTextureUnit(Ogre::TextureUnitState *pTextureUnitState)
	{
		TexUnitDesc td;
		memset(&td,0,sizeof(TexUnitDesc));

		//TODO:
				
		td.save(pTextureUnitState);

		writeData(&td,sizeof(TexUnitDesc),1);
	}


	void saveMaterials()
	{
		Ogre::MaterialManager::ResourceMapIterator it = Ogre::MaterialManager::getSingletonPtr()->getResourceIterator();

		while(it.hasMoreElements())
		{
			Ogre::MaterialPtr pMaterial = it.getNext();
			
			String mat_name = pMaterial->getName();

			Ogre::Material::TechniqueIterator techIter = pMaterial->getTechniqueIterator();
			while (techIter.hasMoreElements())
			{
				Ogre::Technique *pTechniique = techIter.getNext();

				Ogre::Technique::PassIterator passIter = pTechniique->getPassIterator();

				// ������� ���-�� �����
				uchar nPassCount = 0;
				while (passIter.hasMoreElements())
				{
					passIter.getNext();
					nPassCount++;
				}

				if (nPassCount == 0)
				{
					printf("\nThere's no passes in that material", mat_name.c_str());
					continue;
				}

				writeMaterialAndTechnique(mat_name.c_str(), nPassCount);

				passIter = pTechniique->getPassIterator();

				while (passIter.hasMoreElements())
				{
					Ogre::Pass *pPass = passIter.getNext();
					//				pPass->
					Ogre::Pass::TextureUnitStateIterator textureUnitStateIter = pPass->getTextureUnitStateIterator();

					// ������� ���-�� ���������� ������
					uchar nTextureUnitCount = 0;
					while (textureUnitStateIter.hasMoreElements())
					{
						textureUnitStateIter.getNext();
						nTextureUnitCount++;
					}
					
					writePass(pPass,nTextureUnitCount);
										
					textureUnitStateIter = pPass->getTextureUnitStateIterator();
					
					while (textureUnitStateIter.hasMoreElements())
					{
						Ogre::TextureUnitState *pTextureUnitState = textureUnitStateIter.getNext();

						writeTextureUnit(pTextureUnitState);
					}
				}

			}
			
			printf("\n%s %d",mat_name.c_str(), mat_name.length());
		}
	}
};*/


Root *mRoot;
StringVector materialFolder; //c:\\Projects\\Alchemy\\mesh_atl_test\\mat
String meshFolder; //\\\\SANYA\\Projects\\Alchemy\\data\\packed\\misc\\01_hall
String texturesFolder; //\\\\SANYA\\Projects\\Alchemy\\data\\packed\\textures\\01_hall
String workFolder;
int atlas_pad = 5;
StringVector atlasTextures;
StringVector atlasTextures2;
StringVector mIgnoredMaterials;
StringVector mIgnoredMeshes;
String mToolPath; //c:\\Projects\\JEngine6\\tools\\AtlasTool\\sspack.exe
int standart_border = 0;
String binMaterialsResult;
CBinaryMaterials *m;

void serializeMaterials()
{
	
	m->exportMaterials(binMaterialsResult);

	/*printf("\n\nexported materials");
	Sleep(10000);

	m->exportMaterials("mats.bin");

	
	
	Ogre::MaterialManager::getSingletonPtr()->removeAll();

	Ogre::MaterialManager::ResourceMapIterator it = Ogre::MaterialManager::getSingletonPtr()->getResourceIterator();

	printf("\n\swaiting 10 sec");

	
	Sleep(10000);

	DWORD t1 = GetTickCount();

	printf("\n\start loading materials");

	int mat_num = 0;
	while(it.hasMoreElements())
	{
		Ogre::MaterialPtr pMaterial = it.getNext();
		mat_num++;
	}

	printf("\n\nmat: %d",mat_num);

	m->importMaterials("mats.bin");

	printf("\n\nimporter materials in %u sec", (GetTickCount() - t1)/1000);
	Sleep(10000);

	delete m;*/
}

//---------------


bool findStringInVector(const String &s, const StringVector &v) 
{
	for (StringVector::const_iterator it = v.begin(), end = v.end(); it != end; it++)
	{
		if(s.find((*it)) != String::npos)
			return true;
	}

	return false;
}

int initMfc()
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			nRetCode = 1;
		}
		else
		{
			// TODO: code your application's behavior here.
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		nRetCode = 1;
	}

	return nRetCode;
}



void initializeOgre()
{
	LogManager * lm = new LogManager();
    lm->createLog("log.txt", true, false, false); //TODO: redirect to our own logger.

	mRoot = OGRE_NEW Root();

	mRoot->setRenderSystem(mRoot->getAvailableRenderers().at(0));
 	mRoot->initialise(false);
	mRoot->createRenderWindow("test",10,10,false);
	
	printf("\nOgre initialized!");

	//serializeMaterials();
	m = new CBinaryMaterials();

	for(StringVector::iterator it = materialFolder.begin(), end = materialFolder.end(); it != end; it++)
	{
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation((*it), "FileSystem", 
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	}

	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();



	serializeMaterials();


	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(meshFolder, "FileSystem", 
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(texturesFolder, "FileSystem", 
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	printf("\nInitialize resources done!");

	

	
}

void finalize()
{
	OGRE_DELETE mRoot;
}



Ogre::Image cropImage(const Ogre::Image& source, int offsetX, int offsetY, size_t width, size_t height, size_t srcWidth, size_t srcHeight)
{
/*   if(offsetX + width > source.getWidth())
      return source;
   else if(offsetY + height > source.getHeight())
      return source;*/

   size_t bpp = Ogre::PixelUtil::getNumElemBytes(source.getFormat());

   const unsigned char *srcData = source.getData();
   unsigned char *dstData = new unsigned char[width * height * bpp];

   size_t srcPitch = source.getRowSpan();
   size_t dstPitch = width * bpp;

   for(size_t row = 0; row < height; row++)
   {
	   int src_row = offsetY + row;
	   if(src_row < 0)
		   src_row = 0;
	   if(src_row >= srcHeight)
		   src_row = srcHeight - 1;

		for(size_t col = 0; col < width; col++)
		{
			int src_col = offsetX + col;
			if(src_col < 0)
				src_col = 0;
			if(src_col >= srcWidth)
				src_col = srcWidth - 1;

			for(size_t dat = 0; dat < bpp; dat++)
			{
				dstData[(row * dstPitch) + (col * bpp) + dat] = srcData[(src_row * srcPitch) + (src_col * bpp) + dat];
			}
		}
	}

   Ogre::Image croppedImage;
   croppedImage.loadDynamicImage(dstData, width, height, 1, source.getFormat());
   
   return croppedImage;
}



struct SubMeshDetails
{
	SubMesh* sm;
//	float uv_bounds[4];
	int uv_bounds_pixel[4];
	size_t original_texture_width;
	size_t original_texture_height;
	bool mesh_saved;
	Mesh *mesh;
	String mesh_name;
//	float borderX; //pixels
//	float borderY; //pixels

/*	float getTextrureArea()
	{
		float w = (uv_bounds[2] - uv_bounds[0])*original_texture_width;
		float h = (uv_bounds[3] - uv_bounds[1])*original_texture_height;
		return w*h;
	}*/
	size_t getTextrureArea()
	{
		return (uv_bounds_pixel[2] - uv_bounds_pixel[0])*(uv_bounds_pixel[3] - uv_bounds_pixel[1]);
	}
};

std::map<String,SubMeshDetails> mSubMeshes;

void deleteTile(float &tile)
{
	while(tile < 0.0f)
		tile += 1.0f;

	while(tile > 1.0f)
		tile -= 1.0f;
}

void saveTextureFromMesh(String mesh_name)
{
	Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().load(mesh_name,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	unsigned short submeshCount = mesh->getNumSubMeshes();
	for (unsigned short i = 0; i < submeshCount; i++) {
		SubMesh* submesh = mesh->getSubMesh(i);
		
		String mat = submesh->getMaterialName();
		printf("\nsubmesh %d | material: %s", i, mat.c_str());


		const VertexElement* posElem =
			submesh->vertexData->vertexDeclaration->findElementBySemantic(
			VES_TEXTURE_COORDINATES); 


		HardwareVertexBufferSharedPtr vbuf =
			submesh->vertexData->vertexBufferBinding->getBuffer(posElem->getSource());


		unsigned char* vertex =
			static_cast<unsigned char*>(
				vbuf->lock(HardwareBuffer::HBL_READ_ONLY));

		float* pFloat;

		float min_u = 1;
		float min_v = 1;

		float max_u = 0;
		float max_v = 0;

		for(size_t j = 0; j < submesh->vertexData->vertexCount; ++j, vertex += vbuf->getVertexSize())
		{
			posElem->baseVertexPointerToElement(vertex, &pFloat);

			Vector2 pt;

			pt.x = (*pFloat++);
			pt.y = (*pFloat++);
			
			deleteTile(pt.x);
			deleteTile(pt.y);
			
			if(pt.x < min_u)
				min_u = pt.x;
			if(pt.y < min_v)
				min_v = pt.y;

			if(pt.x > max_u)
				max_u = pt.x;
			if(pt.y > max_v)
				max_v = pt.y;
		}
		vbuf->unlock(); 

		/*if(min_u == 0.0f && min_v == 0.0f && max_u == 1.0f && max_v == 1.0f)
		{
			printf(" | no atlas needed, uv is whole texture", min_u, min_v, max_u, max_v);
			return;
		}*/

		printf(" | uv_rect: %f %f %f %f", min_u, min_v, max_u, max_v);


		MaterialPtr material = MaterialManager::getSingleton().getByName(mat).staticCast<Material>(); 

		if( material.isNull() || mat.compare("00_empty") == 0 || findStringInVector(mat,mIgnoredMaterials)) 
		{
			printf(" | !!!!!material %s or not found!", material.isNull() ? "empty" : mat.c_str());
		}
		else
		{
			if(material->getTechnique(0)->getNumPasses() == 0 || material->getTechnique(0)->getPass(0)->getNumTextureUnitStates() == 0)
			{
				printf(" | !!!!!material has no texture units or texture passes!");
				return;
			}

			String tex = material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureName();

			printf(" | texture: %s", tex.c_str());

			if(!Ogre::ResourceGroupManager::getSingleton().resourceExists("General",tex))
			{
				printf(" | !!!!!texture not found!");
				return;
			}

			/*if(tex.find("_bg") != String::npos)
			{
				printf(" :ignoring bg texture not found!");
				return;
			}*/

			Image img;
			img.load(tex,"General");

			size_t w = img.getWidth();
			size_t h = img.getHeight();

			String mFileName = workFolder + mesh_name;
			String mFileExtension = String("#") + StringConverter::toString(i) + String(".png");
			
			if(mesh_name.find("01_drova_01") != String::npos)
			{
				int l = 0;
			}
			
			int x1 = min_u*w;
			int y1 = min_v*h;
			int w1 = (max_u-min_u)*w;
			int h1 = (max_v-min_v)*h;

			int x2 = x1 - standart_border;
			int y2 = y1 - standart_border;
			int w2 = w1 + 2*standart_border;
			int h2 = h1 + 2*standart_border;

/*			if(x2 < 0) x2 = 0;
			if(y2 < 0) y2 = 0;
			
			if(x2 + w2 > w) w2 = w - x2;
			if(y2 + h2 > h) h2 = h - y2;*/
			
			Image im = cropImage(img, x2, y2, (size_t)w2, (size_t)h2, w, h);

			String t = StringUtil::replaceAll(mFileName,".mesh",mFileExtension);// + "." + mFileExtension;
		
			im.save(t);
			atlasTextures.push_back(t);
			
			SubMeshDetails smd;
			smd.sm = submesh;
/*			smd.uv_bounds[0] = min_u; 
			smd.uv_bounds[1] = min_v;
			smd.uv_bounds[2] = max_u;
			smd.uv_bounds[3] = max_v;*/
			smd.original_texture_width = w;
			smd.original_texture_height = h;
			smd.mesh_saved = false;
			smd.mesh = mesh.getPointer();
			smd.mesh_name = mesh_name;
//			smd.borderX = x1 - x2;
//			smd.borderY = y1 - y2;

			smd.uv_bounds_pixel[0] = x2;
			smd.uv_bounds_pixel[1] = y2;
			smd.uv_bounds_pixel[2] = x2 + w2;
			smd.uv_bounds_pixel[3] = y2 + h2;
			
			mSubMeshes.insert(std::pair<String,SubMeshDetails>(t,smd));
		}

		
	}
	

	//mesh.setNull();
}

bool exec(const char *prog, const char *args)
{
	bool res = false;

	printf("\n\nexec: %s %s",prog,args);

	STARTUPINFOA siStartupInfo;
	PROCESS_INFORMATION piProcessInfo;

	memset(&siStartupInfo, 0, sizeof(siStartupInfo));
	memset(&piProcessInfo, 0, sizeof(piProcessInfo));

	siStartupInfo.cb = sizeof(siStartupInfo);

	char buf[1024];

	sprintf_s(buf,1024,"%s",args);

	if(CreateProcess(prog, // Application name
		buf, // Additional application arguments
	NULL,
	NULL,
	FALSE,
	CREATE_NO_WINDOW | CREATE_DEFAULT_ERROR_MODE,
	NULL,
	NULL,
	&siStartupInfo,
	&piProcessInfo))
	{
		// Could not start application

		// Wait until application has terminated
		WaitForSingleObject(piProcessInfo.hProcess, INFINITE);

		printf("command line: %s", buf);


		res = true;
	}
	else
	{
		
	}

	// Close process and thread handles
	::CloseHandle(piProcessInfo.hThread);
	::CloseHandle(piProcessInfo.hProcess);

	return res;
}

/*std::vector<std::pair<String,RealRect>> rects;
CVirtualScript::loadRectArrayFromCfg(CMainApplication::getCfgPath("ipad_4x_zh2.txt"),rects);

for(int i = 0; i < rects.size(); i++)
{
	toLogEx("%s %f %f %f %f",rects[i].first.c_str(),rects[i].second.left,rects[i].second.top,rects[i].second.right,rects[i].second.bottom);
}
*/

void loadRectArrayFromCfg(const String fname, std::map<String,RealRect> &rects, size_t w, size_t h)
{
	Ogre::ConfigFile cf;

	try
	{
		cf.load(fname);
	}
	catch (Ogre::Exception ex)
	{
		printf("\nERROR: File %s not found!\n", fname.c_str());
		return;	
	}

	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	String secName, param, value;
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			param = i->first;
			value = i->second;
			
			StringVector sv = Ogre::StringConverter::parseStringVector(value);

			Real f[4];

			for(int i = 0; i < 4; i++)
			{
				f[i] = Ogre::StringConverter::parseReal(sv[i]);
			}

			RealRect rr(f[0]/w,f[1]/h,f[2]/w,f[3]/h);

			rects.insert(std::make_pair(workFolder + param + String(".png"),rr));
		}
	}
}


void scanningSubmeshes(float atlas_width, float atlas_height, std::map<String,RealRect> &rects)
{
	std::map<String,SubMeshDetails>::iterator it;
	std::map<String,SubMeshDetails>::iterator end = mSubMeshes.end();

	for(it = mSubMeshes.begin(); it != end; it++)
	{
		SubMeshDetails smd = (*it).second;
		String tname = (*it).first;

		if(rects.find(tname) == rects.end())
		{
			
			continue;
		}
				
/*		float w_ratio = smd.original_texture_width/atlas_width;
		float h_ratio = smd.original_texture_width/atlas_height;

		Vector2 ratio(w_ratio,h_ratio);
		Vector2 old_pos(smd.uv_bounds[0],smd.uv_bounds[1]);
		
		Vector2 border(smd.borderX/atlas_width,smd.borderY/atlas_height);

		Vector2 new_pos(rects[tname].left,rects[tname].top);*/

		//��������� �� ������������ ���� ��������
		//�������� ���������� �� w_ratio,h_ratio
		//��������� ����� ���������� � ������ ����� �������

		const VertexElement* posElem =	smd.sm->vertexData->vertexDeclaration->findElementBySemantic(VES_TEXTURE_COORDINATES); 
		HardwareVertexBufferSharedPtr vbuf = smd.sm->vertexData->vertexBufferBinding->getBuffer(posElem->getSource());
		unsigned char* vertex =	static_cast<unsigned char*>(vbuf->lock(HardwareBuffer::HBL_NORMAL));

		float* pFloat;

		for(size_t j = 0; j < smd.sm->vertexData->vertexCount; ++j, vertex += vbuf->getVertexSize())
		{
			posElem->baseVertexPointerToElement(vertex, &pFloat);

			Vector2 pt;

			float *p1 = pFloat++;
			float *p2 = pFloat++;

			pt.x = (*p1);
			pt.y = (*p2);
			
			/*pt -= old_pos;

			pt *= ratio;

			pt += new_pos;

			pt += border;

			(*p1) = pt.x;
			(*p2) = pt.y;*/

//			(*p1) = ((*p1 - smd.uv_bounds[0]) * smd.original_texture_width + smd.borderX)/atlas_width + rects[tname].left;
//			(*p2) = ((*p2 - smd.uv_bounds[1]) * smd.original_texture_height + smd.borderY)/atlas_height + rects[tname].top;
			
			deleteTile(pt.x);
			deleteTile(pt.y);

			(*p1) = (pt.x * smd.original_texture_width - smd.uv_bounds_pixel[0] + rects[tname].left*atlas_width) / atlas_width;
			(*p2) = (pt.y * smd.original_texture_height - smd.uv_bounds_pixel[1] + rects[tname].top*atlas_height) / atlas_height;

			
			
		}
		vbuf->unlock(); 


		printf("\nSubmesh %s changed",smd.mesh_name.c_str());
	}
}

void saveMeshes()
{
	MeshSerializer* meshSerializer = OGRE_NEW MeshSerializer(); 

	std::map<String,SubMeshDetails>::iterator it;
	std::map<String,SubMeshDetails>::iterator end = mSubMeshes.end();

	
	for(it = mSubMeshes.begin(); it != end; it++)
	{
		SubMeshDetails smd = (*it).second;

		if(!smd.mesh_saved)
		{

			meshSerializer->exportMesh(smd.mesh,workFolder + smd.mesh_name);

			printf("\nMesh %s saved",smd.mesh_name.c_str());

			smd.mesh_saved = true;
			smd.mesh = 0;
		}
		//MeshSerializer::exportMesh(
	}

	OGRE_DELETE meshSerializer;
}

bool DeleteDirectory(LPCTSTR lpszDir, bool noRecycleBin = true)
{
  int len = _tcslen(lpszDir);
  TCHAR *pszFrom = new TCHAR[len+2];
  _tcscpy(pszFrom, lpszDir);
  pszFrom[len] = 0;
  pszFrom[len+1] = 0;
  
  SHFILEOPSTRUCT fileop;
  fileop.hwnd   = NULL;    // no status display
  fileop.wFunc  = FO_DELETE;  // delete operation
  fileop.pFrom  = pszFrom;  // source file name as double null terminated string
  fileop.pTo    = NULL;    // no destination needed
  fileop.fFlags = FOF_NOCONFIRMATION|FOF_SILENT;  // do not prompt the user
  
  if(!noRecycleBin)
    fileop.fFlags |= FOF_ALLOWUNDO;

  fileop.fAnyOperationsAborted = FALSE;
  fileop.lpszProgressTitle     = NULL;
  fileop.hNameMappings         = NULL;

  int ret = SHFileOperation(&fileop);
  delete [] pszFrom;  
  return (ret == 0);
}

bool myCompare(String a, String b)
{
	return (mSubMeshes[a].getTextrureArea() > mSubMeshes[b].getTextrureArea());
}

void sortAtlasArray(StringVector &a)
{
	std::sort(a.begin(),a.end(),myCompare);
}


bool createAtlas(const String &atlas_texture, StringVector &atex)
{
	CStdioFile f;		
	
	String a = workFolder + atlas_texture + String(".cfg");

	f.Open(a.c_str(), CFile::modeCreate | CFile::modeWrite | CFile::typeText);


	for(StringVector::iterator it = atex.begin(), end = atex.end(); it != end; it++)
	{
		printf("\n%s",(*it).c_str());

		//sspack.exe /pad:10 /sqr /pow2 /image:%RESULT_PATH%\%%j_%%i.png /map:%RESULT_PATH%\%%j_%%i.txt /il:%TEMP_FILE%

		String s = (*it) + String("\n");

		f.WriteString(s.c_str());

		//f.WriteString("<setting>\n");
	}

	f.Close();
	
	char buf[1024];
	sprintf_s(buf,1024," /pad:%d /sqr /pow2 /image:%s%s.png /map:%s%s.txt /il:%s",atlas_pad,
		workFolder.c_str(),
		atlas_texture.c_str(),
		workFolder.c_str(),
		atlas_texture.c_str(),
		a.c_str());

	if(exec(mToolPath.c_str(), buf))
	{
		printf("\nAtlas created with sspack.exe");
	}
	else
	{
		printf("\nAtlas creating error with sspack.exe");
		return false;
	}

	return true;
}

void removeAllTexturesWithSameMesh(StringVector &source, StringVector &dest)
{
	int remove_elem = source.size()-1;

	//if(remove_elem > 0)
	//	remove_elem--;

	String orig = source[remove_elem];
	String base = orig.substr(0,orig.find("#"));

	for (StringVector::iterator it = source.begin(); it != source.end();)
	{
		String cur = (*it);

		if(cur.find(base) != String::npos)
		{
			dest.push_back(cur);
			it = source.erase(it);
		}
		else
			++it;
	}
}


int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	printf("\nSyntaxis: JOgreMesgAtlaser.exe <atlas_tool_full_path> <loc_prefix> <materials_folder> <mesh_folder> <textures_folder> <work_folder> [ignore_meshes1;ignore_meshes2]\n\nOr\n\nJOgreMesgAtlaser.exe <materials_folder1;materials_folder2> [result_path_and_name] - to create only binary materials");

	//debug cmd: c:\Projects\JEngine6\tools\AtlasTool\sspack.exe 01 c:\Projects\Alchemy\mesh_atl_test\mat\ \\SANYA\Projects\Alchemy\data\packed\misc_loc_old\01_hall\ \\SANYA\Projects\Alchemy\data\packed\textures_loc_old\01_hall\ c:\Projects\JEngine6\tools\JOgreMeshAtlaser\Debug\work\ 51
	
	if(argc > 1 && argc < 4)
	{
		//binMaterialsResult = String("mat.bin");

		materialFolder = Ogre::StringUtil::split(argv[1],";");
		
		if(argc == 3)
			binMaterialsResult = String(argv[2]);

		initializeOgre();

		finalize();

		return 0;
	}


	if(argc < 7)
	{
		return 0;
	}

	for(int i = 0; i < argc; i++)
	{
		printf("\nparam %d: %s",i,argv[i]);
	}

	printf("\n\n\n");

	int max_size = 1024;	
	atlas_pad = 5;
	standart_border = 5;
	String loc("54");
	
	//char my_path[] = {"c:\\Projects\\JEngine6\\tools\\JOgreMeshAtlaser\\Debug"};

	//materialFolder = String("c:\\Projects\\Alchemy\\mesh_atl_test\\mat\\");
	meshFolder = String("\\\\SANYA\\Projects\\Alchemy\\data\\packed\\misc\\04_street\\");
	texturesFolder = String("\\\\SANYA\\Projects\\Alchemy\\data\\packed\\textures\\04_street\\");
	workFolder = String("c:\\Projects\\JEngine6\\tools\\JOgreMeshAtlaser\\Debug\\work\\");
	

	//03 c:\Projects\Alchemy\mesh_atl_test\mat\ \\SANYA\Projects\Alchemy\data\packed\misc\03_cabinet\ \\SANYA\Projects\Alchemy\data\packed\textures\03_cabinet\ c:\Projects\JEngine6\tools\JOgreMeshAtlaser\Debug\03_work
	mToolPath = String(argv[1]);

	loc = String(argv[2]);

	materialFolder = Ogre::StringUtil::split(argv[3],";");
	//String(argv[2]);

	meshFolder = String(argv[4]);
	texturesFolder = String(argv[5]);
	workFolder = String(argv[6]) + loc + String("\\");
	String atlas_texture = loc + String("_atlas");

	if(argc == 8)
		mIgnoredMeshes = Ogre::StringUtil::split(argv[7],";");

	//----------

	initMfc();

	initializeOgre();

	//!!!!
	//return 0;
		
		
	DeleteDirectory(workFolder.c_str());
	SHCreateDirectoryEx(NULL,workFolder.c_str(),NULL);

	//------

	StringVectorPtr meshes = Ogre::ResourceGroupManager::getSingleton().findResourceNames("General",/*loc + */"*.mesh");
	
	if(meshes->size() == 0)
	{
		printf("\n\nThere's no meshes for convert!");
		return 0;
	}

	StringVector ignoreMeshesNames;
	mIgnoredMeshes.push_back("_az");
	mIgnoredMeshes.push_back("_bg");


	for (StringVector::iterator it = meshes->begin(), end = meshes->end(); it != end; it++)
    {
		String s = (*it);

		if(findStringInVector(s,mIgnoredMeshes))
			continue;
				
		printf("\n\n%s",(*it).c_str());

		saveTextureFromMesh(s);
		
    } 

	if(mSubMeshes.size() == 0)
	{
		printf("\nNone of submeshes found! Check your materials!");
		return 0;
	}

	//
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(workFolder, "FileSystem");
		
	printf("\n\nTextures for atlas");

	StringVector origAtlasTextures = atlasTextures;

	sortAtlasArray(atlasTextures);


			
	size_t w,h;

	do
	{
		if(!createAtlas(atlas_texture,atlasTextures))
		{
			return 0;
		}

		Image img;
		img.load(atlas_texture + String(".png"),"General");

		w = img.getWidth();
		h = img.getHeight();

		if(w <= max_size && h <= max_size)
			break;			
		
		printf("\n\nAtlas is bigger than %dx%d",w,h);


		removeAllTexturesWithSameMesh(atlasTextures,atlasTextures2);
			

		if(atlasTextures.size() == 0)
			break;

	}while(true);

	bool add_atlas_too_big = false;

	if(atlasTextures2.size() > 0)
	{
		printf("\n\nCreating additional atlas",w,h);

		createAtlas(atlas_texture + String("_add"),atlasTextures2);

		Image img2;
		img2.load(atlas_texture + String("_add") + String(".png"),"General");

		size_t w2 = img2.getWidth();
		size_t h2 = img2.getHeight();

		if(w2 <= max_size && h2 <= max_size)
		{
			std::map<String,RealRect> rects2;
			String txt_name = workFolder + atlas_texture + String("_add") + String(".txt");
			loadRectArrayFromCfg(txt_name,rects2,w2,h2);

			scanningSubmeshes(w2,h2,rects2);
		}
		else
		{
			printf("\n\nAdditional atlas too big");

			add_atlas_too_big = true;
		}
	}

	//add atlas too big, there's no reason to create it
	if(add_atlas_too_big)
	{
		printf("\n\nCreating main atlas of any possible size");

		//DeleteDirectory(workFolder.c_str());
		//SHCreateDirectoryEx(NULL,workFolder.c_str(),NULL);
		String t = workFolder + atlas_texture + String("_add") + String(".png");

		DeleteFile(t.c_str());
		//_unlink(t.c_str());

		if(!createAtlas(atlas_texture,origAtlasTextures))
		{
			return 0;
		}

		Image img2;
		img2.load(atlas_texture + String(".png"),"General");

		w = img2.getWidth();
		h = img2.getHeight();
	}

	//load atlas
	std::map<String,RealRect> rects;
	loadRectArrayFromCfg(workFolder + atlas_texture + ".txt",rects,w,h);
	
	scanningSubmeshes(w,h,rects);

	saveMeshes();

	finalize();

	return 0;

	
}	
