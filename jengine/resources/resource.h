//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by emerald.rc
//
#define IDD_DIALOG1                     101
#define IDD_SETUPDLG                    101
#define IDI_ICON1                       104
#define IDR_MAINFRAME                   104
#define IDC_COMBO1                      1001
#define IDC_COMBO2                      1002
#define IDC_COMBO3                      1003
#define IDC_COMBO4                      1004
#define IDC_CHECK1                      1005
#define IDC_CHECK2                      1006
#define IDC_CHECK3                      1007
#define IDC_PSFX                        1007
#define IDC_CHECK4                      1008
#define IDC_COMBO5                      1009
#define IDC_STATIC1                     1010
#define IDC_STATIC2                     1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
