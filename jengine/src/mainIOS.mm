#include "stdafx.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS

#import <UIKit/UIKit.h> 
#import <mach/mach.h>
#import <mach/mach_host.h>
#include <iOS/macUtils.h>

#include "mainIOS.h"
#include "MainApplication.h"
#include "AppOISListener.h"

#include <VirtualScript.h>

//#include "MoonWalkViewController.h"

//#include <AudioServices.h>
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVAudioSession.h>
#import <AVFoundation/AVAsset.h>

#import <MediaPlayer/MediaPlayer.h>


extern CMainApplication app;


#ifdef IOS_FREEMIUM
extern bool gIsInPurchaseNow;
void purchaseContinueGame();
#endif



vm_size_t getUsedMemory(void) 
{

//    NSObject
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&info, &size);
    return (kerr == KERN_SUCCESS) ? info.resident_size : 0;   // size in bytes
}

static void print_free_memory () {
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);        
	
    vm_statistics_data_t vm_stat;
	
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
        printf("Failed to fetch vm statistics");
}

GAME_API int StartApplication(int argc, char **argv)
{	
	//TODO: make common command line processing
	/*if(argc > 1)
	{
		if (strstr (argv[1], "-nosfx"))
			app.enableSounds(false);
		else 
			app.enableSounds();
	}
	//-------*/

	if(argc > 1)
		app.processCommandLine(argv[1]);
	
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	int retVal = UIApplicationMain(argc, argv, @"UIApplication", @"JEngineAppDelegate");
	[pool release];
	return retVal;
}

#	ifdef __OBJC__

//--------------------
#include <sys/sysctl.h>

NSString *getPlatform()
{
	size_t size;
	sysctlbyname("hw.machine",NULL,&size,NULL,0);
	char *m = (char*)malloc(size);
	sysctlbyname("hw.machine",m,&size,NULL,0);
	NSString *p = [NSString stringWithCString:m];
	free(m);
	return p;
}


bool isRetina()
{
	NSString *platform = getPlatform();
	
	
    bool bRetina =
	!(
	  [platform isEqualToString:@"i386"]
	  || [platform isEqualToString:@"x86_64"]
	  
	  || [platform isEqualToString:@"iPhone1,1"]
	  || [platform isEqualToString:@"iPhone1,2"]
	  || [platform isEqualToString:@"iPhone2,1"]
	  
	  || [platform isEqualToString:@"iPod1,1"]
	  || [platform isEqualToString:@"iPod2,1"]
	  || [platform isEqualToString:@"iPod3,1"]
	  
	  || [platform isEqualToString:@"iPad1,1"]
	  || [platform isEqualToString:@"iPad2,1"]
	  || [platform isEqualToString:@"iPad2,2"]
	  || [platform isEqualToString:@"iPad2,3"]
	  || [platform isEqualToString:@"iPad2,4"]
	  || [platform isEqualToString:@"iPad2,5"]
	  );
	
	if (!bRetina && [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"UIDeviceFamily"] isKindOfClass:[NSArray class]])
	{
		NSArray *deviceFamily = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"UIDeviceFamily"];
		NSNumber *item = [deviceFamily objectAtIndex:0];
		NSString *deviceModel = [UIDevice currentDevice].model;
		// if iPhone target device family on the iPad, then set scale 2
		if ([deviceFamily count] == 1 && [item intValue] == 1 && [deviceModel isEqualToString:@"iPad"])
			bRetina = true;
	}
	
	
	NSLog(@"!!!isRetina: %d (%@)", bRetina, platform);
	
	return bRetina;
}

bool isIPad()
{
	if([[UIDevice currentDevice].model isEqualToString:@"iPad"])
		return true;

	return false;
}			
			
/*
@interface AccDelegate : NSObject <UIAccelerometerDelegate>
{
	 UIAccelerationValue prev_x, prev_y, prev_z; 
}
@end

@implementation AccDelegate

-(void)_init
{
	[[UIAccelerometer sharedAccelerometer] setUpdateInterval: 1.0];
	[[UIAccelerometer sharedAccelerometer] setDelegate:self];
	prev_x = 0;
	prev_y = 0;
	prev_z = 0;
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
    // Get the event data
    UIAccelerationValue x, y, z; 
    
	#define FILTERFACTOR 0.1
	#define DELTA 0.1
	
	x = acceleration.x - (acceleration.x * FILTERFACTOR) + (prev_x * (1.0 -  FILTERFACTOR));
	prev_x = x;
	
	y = acceleration.y - (acceleration.y * FILTERFACTOR) + (prev_y * (1.0 -  FILTERFACTOR));
	prev_y = x;
	
	z = acceleration.z - (acceleration.z * FILTERFACTOR) + (prev_z * (1.0 -  FILTERFACTOR));
	prev_x = z;
	
    //x = acceleration.x;
    //y = acceleration.y;
    //z = acceleration.z;
	
	float angle = atan2(y, -x);

	//if(fabs(x - prev_x) > DELTA || fabs(y - prev_y) > DELTA || fabs(z - prev_z) > DELTA)
	//NSLog(@"%f %f %f ang:%f",x,y,z,angle);
	
	CVirtualScript::setCfgValue<float>("cameraAccX",x);
	CVirtualScript::setCfgValue<float>("cameraAccY",y);
	
	//CVirtualScript::sendCMsg("00_cam_ext",CMsg(10,0,0));
}


@end

//--------
 */



void applicationShowHighlightSkipVideoButton(bool bShow);

@interface TouchRecieverView : UIView 
{
	bool			bMultiTouch;
	bool			mMultiTouchMouseState;
	float			mSystemVersion;
	UIView*			superview;
	CGRect			mBounds;

        // ����� �� ������ �����-������, �� ������� ������ �����
    bool			mMoviePlayerView;
		// ���� �� �����������, true - ������ ��� �� ������ ����� �����
	bool			mTouchBeginOnButton;

	//for multitouch
	CGPoint mPrevPoint;
	CGFloat mPrevDistance;
	bool mIsZoom;
	bool mIsMove;
	
}

-(void) initWithSuperview: (UIView*)_superview _mtouch:(bool)_mtouch _mtouchms:(bool)_mtouchms _moviePlayer:(bool)_moviePlayer;

@end

@implementation TouchRecieverView

-(void) initWithSuperview: (UIView*)_superview _mtouch:(bool)_mtouch _mtouchms:(bool)_mtouchms _moviePlayer:(bool)_moviePlayer
{
	[super init];
	superview = [_superview retain];
	
	[self setUserInteractionEnabled: YES];
	
	[superview addSubview: self];
	
	mBounds = [superview bounds];
	
	[self setFrame: mBounds];
	
	mSystemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
	
	bMultiTouch = _mtouch;
	mMultiTouchMouseState = _mtouchms;
	
    mMoviePlayerView = _moviePlayer;
	
	mIsZoom = false;
	mIsMove = false;
    
	[superview setTag: 0xCAFEBABE];

	
	NSLog(@"TouchRecieverView: initWithSuperview %@", self);
}

-(void) dealloc
{
	[superview release];
	[super dealloc];
}

-(bool)active
{
	return !CVirtualScript::getCfgI("deactivateInput");
}


- (void) UpdateTouches:(NSSet*) allTouches
{
	if(![self active])
		return;

//	toLogEx("\n update touches %d", [allTouches count]);
	
	if (mIsZoom && allTouches.count != 2)
		mIsZoom = false;
	
	if ([allTouches count] == 1) 
	{
		UITouch* touch0 = [[allTouches allObjects] objectAtIndex:0];
		CGPoint point = [touch0 locationInView:self];

//		if (mIsZoom)
//			toLogEx("\n one touch with zoom");

		CGFloat dx = point.x - mPrevPoint.x;
		CGFloat dy = mPrevPoint.y - point.y;
		mPrevPoint = point;

		if (!mIsMove)
		{
			mIsMove = true;
			CVirtualScript::setSB("iscCamIsMove", true);
		}
		else
		{
			int iX = dx*1000.0;
			int iY = dy*1000.0;
			CVirtualScript::setSI("cameraTouchMoveX", iX);
			CVirtualScript::setSI("cameraTouchMoveY", iY);

			CMsg msg;
#if JENGINE_MOUSEOLDWORK
			msg.lparam = 32;
			msg.rparam = 0;
			CVirtualScript::sendCMsg("00_game_interface", msg);
#endif
			
			msg.lparam = 4;
			msg.rparam = 0;
			CVirtualScript::sendCMsg("00_cam_ext", msg);


		}

	} 
	else
	{ 
		UITouch* touch0 = [[allTouches allObjects] objectAtIndex:0];
		CGPoint point0 = [touch0 locationInView:self];
		UITouch* touch1 = [[allTouches allObjects] objectAtIndex:1];
		CGPoint point1 = [touch1 locationInView:self];

		//toLogEx("\n 2 touch");

		CGFloat fX = point1.x - point0.x;
		CGFloat fY = point1.y - point0.y;

		CGFloat distance = sqrtf(fX*fX + fY*fY);
		CGFloat delta = mPrevDistance - distance;
		mPrevDistance = distance;

		if (!mIsZoom)
		{
			mIsZoom = true;
		}
		else
		{
			/*
			 */
			int zoomParam = delta*1000.0;
			CVirtualScript::setSI("cameraTouchZoom", zoomParam);


			CMsg msg;
#if JENGINE_MOUSEOLDWORK
			msg.lparam = 31;
			msg.rparam = 0;
			CVirtualScript::sendCMsg("00_game_interface", msg);
#endif
			
			msg.lparam = 5;
			msg.rparam = 0;
			CVirtualScript::sendCMsg("00_cam_ext", msg);

			app.setMousePinch(zoomParam + app.getMousePinch());
//			if (zoomParam >= 1000)
//				toLogEx("\n zoom param %d", zoomParam);
		}

//		Zoom(-delta/200);

	}
}

- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
	if(![self active])
		return;

	mIsZoom = false;
	mIsMove = false;
	CVirtualScript::setSB("iscCamIsMove", false);

	
	NSSet* allTouches = [event allTouches];

	toLogEx("\n touches began: %d %d %d", [allTouches count], bMultiTouch, mMultiTouchMouseState);
	
	
	//if (!isZoom)
	if([allTouches count] == 1 || (bMultiTouch && mMultiTouchMouseState))
	{
		for (UITouch* myTouch in allTouches)
		{	
			//int tapCount = [myTouch tapCount];
			CGPoint touchLocationOrig = [myTouch locationInView:self];
			CGPoint touchLocation = touchLocationOrig;
			if (mSystemVersion >= 4.0)
			{	
				touchLocation.x *= self.contentScaleFactor;
				touchLocation.y *= self.contentScaleFactor;
			}
			

			if (mMoviePlayerView)
				[self mpvTouchBeginAtPoint: &touchLocationOrig];
			else
				app.getOIS()->_appMouseMoved(touchLocation.x/mBounds.size.width, touchLocation.y/mBounds.size.height, 0, true, false);

			app.getOIS()->_appMousePressed(0);
			
			NSLog(@"mousedown x: %f y: %f", touchLocation.x, touchLocation.y);

			//no multitouch now
			return;
		}
	}
}

- (void) touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
	if(![self active])
		return;
	
	mIsZoom = false;
	mIsMove = false;
	CVirtualScript::setSB("iscCamIsMove", false);

	//toLogEx("\n touches ended");

	NSSet* allTouches = [event allTouches];
	//[self SaveTouches:allTouches];
	
	int cnt = allTouches.count;
	toLogEx("\n touches ended: %d", cnt);
	
	if([allTouches count] == 1 || (bMultiTouch && mMultiTouchMouseState))
	{
		for (UITouch *myTouch in allTouches)
		{
			CGPoint touchLocationOrig = [myTouch locationInView:self];
			CGPoint touchLocation = touchLocationOrig;
			if (mSystemVersion >= 4.0)
			{
				touchLocation.x *= self.contentScaleFactor;
				touchLocation.y *= self.contentScaleFactor;
			}

		
			NSLog(@"mouseup x: %f y: %f", touchLocation.x, touchLocation.y);
			
			if (applicationIsPaused() && CVirtualScript::getCfgB("ios_pause_enabled"))
			{
				Ogre::Root::getSingleton().getAutoCreatedWindow()->setActive(true);
				app.enterForeground();
				
#ifdef IOS_FREEMIUM
				if (!gIsInPurchaseNow)
#endif
				{
					applicationHidePausePicture();

					applicationUnpauseVideo();
				}
			}
			else
			{
				if ([allTouches count] == 1)
				{
					if (mMoviePlayerView)
						[self mpvTouchReleaseAtPoint: &touchLocationOrig];
					else
						app.getOIS()->_appMouseMoved(touchLocation.x/mBounds.size.width, touchLocation.y/mBounds.size.height, 0, false, false);
				}
				else
					app.getOIS()->m_bLMouseButton = false;
					

				NSLog(@"mouse released!");
				app.getOIS()->_appMouseReleased(0);
			}

			//no multitouch now
			return;
		}
	}
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	//if(![self active])
	//	return;
	
	NSSet* allTouches = [event allTouches];
	
	if(bMultiTouch)
	{
		[self UpdateTouches:allTouches];
	}
	
	if([allTouches count] == 1 || (bMultiTouch && mMultiTouchMouseState))
	{
		for (UITouch *myTouch in allTouches)
		{
			CGPoint touchLocationOrig = [myTouch locationInView:self];
			CGPoint touchLocation = touchLocationOrig;
			if (mSystemVersion >= 4.0)
			{
				touchLocation.x *= self.contentScaleFactor;
				touchLocation.y *= self.contentScaleFactor;
			}
		
//			app.mOgreOISListener->_appMouseMoved(touchLocation.x/mBounds.size.width, touchLocation.y/mBounds.size.height, 0, true, false);
			
			if ([allTouches count] == 1)
			{
				if (mMoviePlayerView)
					[self mpvTouchMovedToPoint: &touchLocationOrig];
				else
					app.getOIS()->_appMouseMoved(touchLocation.x/mBounds.size.width, touchLocation.y/mBounds.size.height, 0, true, false);
			}

			return;
		}
	}
}

- (bool)mpvIsPointInSkipButton: (CGPoint *) point
{
#if IS_JENGINE_PLATFORM_IPHONE
	return (point->x > self.bounds.size.width - 60 && point->y < 15);
#else
	return (point->x > self.bounds.size.width - 120 && point->y < 30);
#endif
}

- (void)mpvTouchMovedToPoint: (CGPoint *) point
{
	applicationShowHighlightSkipVideoButton(mTouchBeginOnButton && [self mpvIsPointInSkipButton:point]);
}

- (void)mpvTouchBeginAtPoint: (CGPoint *) point
{
	mTouchBeginOnButton = [self mpvIsPointInSkipButton:point];
	applicationShowHighlightSkipVideoButton(mTouchBeginOnButton);
}

- (void)mpvTouchReleaseAtPoint: (CGPoint *) point
{
	applicationShowHighlightSkipVideoButton(false);

	if (mTouchBeginOnButton && [self mpvIsPointInSkipButton:point])
	{
		// skip current movie!
		applicationStopVideo();

		CVirtualScript::sendCMsg("00_game_interface", 10101, 0);
	}
}

@end




//------------------

@interface AppGestureView  : UIView
{
    CMainApplication *mApp;
	UITextView *textView;
	
	// �������� ��� ����� � ������� Tap To Continue
	UIImageView *mPauseImageView;
	UILabel *mPauseImageViewLabel;
		// ������ ���������� ���� (���������� �� ����� �������)
	UIButton *mPauseImageViewButton;
	NSTimer *mTimerButtonShow;

	// ���������� �������� (wait indicator)
	UIActivityIndicatorView *mActivityIndicator;
    
	MPMoviePlayerController *mMoviePlayer;
	// �������� ������ ����� �����
	UIImageView *mSkipButtonImgView;
	// �������� ��������� ����� �����
	UIImageView *mSkipButtonImgViewFx;

	std::string mKeyboardText;

		// do clear player name if it is a "Player" (if pressed Create New Player)
	bool m_bClearPlayerName;
}

//@property (retain) NSString *mKeyboardText;

@property (assign) CMainApplication *mApp;

- (void) applicationShowKeyboard: bClearPlayerName:(bool)bClearPlayerName;
- (void) applicationHideKeyboard;
- (NSString*) applicationGetKeyboardText;
- (void) applicationSetKeyboardText:(NSString*)text;

- (void) showPausePicture;
- (void) hidePausePicture;
- (void) showPausePictureButton:(id)sender;
- (void) pausePictureButtonPressed;

- (void) showActivityIndicator;
- (void) hideActivityIndicator;

- (float) playVideo:(NSURL*)url;
- (void) pauseVideo;
- (void) unpauseVideo;
- (void) hideVideoPlayer;
- (void) showHightlightSkipVideoButton: (bool)bShow;

@end

class MyGestureView : public CAppGestureViewCpp
{
public:	
	AppGestureView *mGestureView;
	
    
	void init(CMainApplication *application)
	{
		mGestureView = [[AppGestureView alloc] init];
		mGestureView.mApp = application;
		
		[[[UIApplication sharedApplication] keyWindow] addSubview:mGestureView];

		NSString *bundle = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
		NSLog(@"BundleId: %@",bundle);
    }
	
	void shutdown()
	{
		[mGestureView release];
	}

	void applicationShowKeyboard(bool bClearPlayerName)
	{
		//mGestureView.mKeyboardText = @"";
		[mGestureView applicationShowKeyboard:bClearPlayerName];
	}

	void applicationHideKeyboard()
	{
		[mGestureView applicationHideKeyboard];
	}

	std::string applicationGetKeyboardText() const
	{
		return [[mGestureView applicationGetKeyboardText] UTF8String];
	}

	void applicationSetKeyboardText(const std::string& text)
	{
		[mGestureView applicationSetKeyboardText:[NSString stringWithCString:text.c_str() encoding:NSUTF8StringEncoding]];
	}
	
	void showPausePicture()
	{
		[mGestureView showPausePicture];
	}

	void hidePausePicture()
	{
		[mGestureView hidePausePicture];
	}

	void showActivityIndicator()
	{
		[mGestureView showActivityIndicator];
	}
	void hideActivityIndicator()
	{
		[mGestureView hideActivityIndicator];
	}

    float playVideo(const char *chFileName)
    {
        NSString *filename = [NSString stringWithCString:chFileName];

        NSString *videoFilepath = [[NSBundle mainBundle] pathForResource:filename ofType:@"mp4"];
        NSLog(@"Filepath is: %@", videoFilepath);
        if(videoFilepath == NULL)
            return 1.f;
        NSURL *videoURL = [NSURL fileURLWithPath:videoFilepath];

        return [mGestureView playVideo:videoURL];
    }

    void pauseVideo()
    {
        [mGestureView pauseVideo];
    }

    void unpauseVideo()
    {
        [mGestureView unpauseVideo];
    }
    
    void stopVideo()
    {
        [mGestureView hideVideoPlayer];
    }
	
	void showHightlightSkipVideoButton(bool bShow)
	{
		[mGestureView showHightlightSkipVideoButton:bShow];
	}
};

@implementation AppGestureView

@synthesize mApp;
//@synthesize mKeyboardText;

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)dealloc {
    [super dealloc];
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event 
{
    //if (mBrowser && event.type == UIEventTypeMotion && event.subtype == UIEventSubtypeMotionShake)
    //    mBrowser->motionBegan();
	
    if ([super respondsToSelector:@selector(motionBegan:withEvent:)]) {
        [super motionBegan:motion withEvent:event];
    }
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    //if (mBrowser && event.type == UIEventTypeMotion && event.subtype == UIEventSubtypeMotionShake)
    //    mBrowser->motionEnded();
	
    if ([super respondsToSelector:@selector(motionEnded:withEvent:)]) {
        [super motionEnded:motion withEvent:event];
    }
}

- (void)motionCancelled:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    //if (mBrowser && event.type == UIEventTypeMotion && event.subtype == UIEventSubtypeMotionShake)
    //    mBrowser->motionCancelled();
	
    if ([super respondsToSelector:@selector(motionCancelled:withEvent:)]) {
        [super motionCancelled:motion withEvent:event];
    }
}

- (void) checkTextForCorrect:(NSNotification *)note
{
	#define limitTextLength_MAX_LEN 13

	bool bCutByLength = false;
	
	
	if ([[textView text] length] > limitTextLength_MAX_LEN)
	{
		[textView setText:[[textView text] substringToIndex:limitTextLength_MAX_LEN]];
		bCutByLength = true;
	}
	else
	{
		if ([[textView text] isEqualToString:@" "])
			[textView setText: @""];
	}

	bool bReturnPressed = false;
	NSRange textRange = [[textView text] rangeOfString:@"\n"];
	if (textRange.location != NSNotFound)
	{
		bReturnPressed = true;
		[textView setText:[[textView text]stringByReplacingOccurrencesOfString:@"\n" withString:@""]];
	}

	
	std::string strOldName = mKeyboardText;
	
	if ((strOldName == "Player"
		 || strOldName == "Player1" || strOldName == "Player2" || strOldName == "Player3" || strOldName == "Player4" || strOldName == "Player5")
		&& m_bClearPlayerName)
	{
		char chBuf[256];
		[[textView text] getCString:chBuf maxLength:256 encoding:NSASCIIStringEncoding];

		if (strlen(chBuf) > strlen(strOldName.c_str()))
		{
			mKeyboardText = &chBuf[strlen(chBuf) - 1];

			[textView setText:[[NSString stringWithCString:mKeyboardText.c_str() encoding:NSASCIIStringEncoding] uppercaseString]];
		}
		m_bClearPlayerName = false;
		
		NSString *ps = @"ret";
		NSString *str = [ps retain];
		NSLog(@"id str: %@", str);
	}

	
	mKeyboardText = [[textView text] UTF8String];
	std::string strCurName = [[textView text] UTF8String];
	//if ((strOldName == strCurName) && (strOldName != "") && !bCutByLength)
	if (bReturnPressed && strOldName != "")
	{
		// done button pressed
		// send message for hide menu in interface
		CVirtualScript::sendCMsg("interface", 20003, 0);
	}
}

- (void) textEndEditing:(NSNotification *)note
{
	// end edit text, hide button pressed
	// send message for hide menu in interface
	CVirtualScript::sendCMsg("interface", 20004, 0);
}

- (void) applicationShowKeyboard: (bool)bClearPlayerName
{
	NSArray *windows = [UIApplication sharedApplication].windows;
	UIWindow *twindow = [windows objectAtIndex:0];
	
	if (twindow)
	{
		mKeyboardText = "";
		m_bClearPlayerName = bClearPlayerName;

		textView = [[[UITextView alloc] initWithFrame:CGRectZero] autorelease];
		textView.returnKeyType = UIReturnKeyDone;
		textView.autocorrectionType = UITextAutocorrectionTypeNo;
		

		[twindow addSubview:textView];
		[textView becomeFirstResponder];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkTextForCorrect:) name:UITextViewTextDidChangeNotification object:textView];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textEndEditing:) name:UITextViewTextDidEndEditingNotification object:textView];
	}
	
}

- (void) applicationHideKeyboard
{
	NSArray *windows = [UIApplication sharedApplication].windows;
	UIWindow *twindow = [windows objectAtIndex:0];
	
	if (twindow)
	{
		[twindow willRemoveSubview:textView];
		[textView resignFirstResponder];
		textView = nil;
	}
}

- (NSString*) applicationGetKeyboardText
{
	if (textView)
		return [textView text];
	else
		return nil;
}

- (void) applicationSetKeyboardText:(NSString*)text
{
	if (textView)
		[textView setText:text];
}

- (void) showPausePicture
{
	if (mPauseImageView != nil)
		[self hidePausePicture];

	size_t v = 0;
	Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("VIEW", &v);
	UIView *twindow = (UIView *)v;

#ifdef IOS_FREEMIUM
	bool bIsInPurchaseNow = gIsInPurchaseNow;
#else
	bool bIsInPurchaseNow = false;
#endif
	
	if (twindow)
	{
		CGRect rectBounds = twindow.bounds;

		CGSize borderSize = CGSizeMake(rectBounds.size.width, rectBounds.size.height);
		CGPoint topLeft = CGPointMake(((float)rectBounds.size.width - borderSize.width)/2.f, ((float)rectBounds.size.height - borderSize.height)/2.f);
		CGRect imageRect = CGRectMake(topLeft.x, topLeft.y, borderSize.width, borderSize.height);

		mPauseImageView = [[UIImageView alloc] initWithFrame:imageRect];

		NSString *strImage = @"ios-labelcap.tga";
		[mPauseImageView setImage:[UIImage imageNamed:strImage]];
		mPauseImageView.opaque = NO; // explicitly opaque for performance
		//mPauseImageView.backgroundColor = [[UIColor alloc] initWithWhite:0.f alpha:0.f];


		Ogre::UTFString ogreStrText = CVirtualScript::getTextString(bIsInPurchaseNow ? 464 : 68); // "accessing iTunes..." : "tap anywhere to continue..."
		NSString *caption = [NSString stringWithCString:ogreStrText.asUTF8_c_str() encoding:NSUTF8StringEncoding];


		CGRect myImageLabelRect = CGRectMake(0.0f, 0.0f, borderSize.width, borderSize.height);
		mPauseImageViewLabel = [[UILabel alloc] initWithFrame:myImageLabelRect];
		//[mPauseImageViewLabel setTextAlignment:UITextAlignmentCenter];
		[mPauseImageViewLabel setText:caption];
		mPauseImageViewLabel.backgroundColor = [[UIColor alloc] initWithWhite:0.f alpha:0.f];
		mPauseImageViewLabel.textColor = [[UIColor alloc] initWithRed:1.f green:0.8f blue:0.f alpha:1.f];
		mPauseImageViewLabel.shadowColor = [[UIColor alloc] initWithRed:0.f green:0.f blue:0.f alpha:1.f];
		mPauseImageViewLabel.shadowOffset = CGSizeMake(1, 1);


		if (bIsInPurchaseNow)
		{
			ogreStrText = CVirtualScript::getTextString(463); // "continue game"
			caption = [NSString stringWithCString:ogreStrText.asUTF8_c_str() encoding:NSUTF8StringEncoding];

			mPauseImageViewButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
			[mPauseImageViewButton setTitle:caption forState:UIControlStateNormal];
			[[mPauseImageViewButton titleLabel] setFont: [UIFont boldSystemFontOfSize:14]];
			[mPauseImageViewButton sizeToFit];
			mPauseImageViewButton.center = CGPointMake(topLeft.x + borderSize.width/2, topLeft.y + borderSize.height/2 + 40);
			//button.tag = 777;
			[mPauseImageViewButton addTarget:self action:@selector(pausePictureButtonPressed) forControlEvents:UIControlEventTouchUpInside];
		}



		[twindow addSubview:mPauseImageView];
		[mPauseImageView addSubview:mPauseImageViewLabel];
		if (bIsInPurchaseNow)
		{
			[twindow addSubview:mPauseImageViewButton];
			mPauseImageViewButton.hidden = YES;

			mTimerButtonShow = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)(30.0f)
																target:self
															  selector:@selector(showPausePictureButton:)
															  userInfo:nil
															   repeats:NO];
		}

		[mPauseImageView release];
		[mPauseImageViewLabel release];
	}
}

- (void) hidePausePicture
{
	if (mTimerButtonShow != nil)
	{
		[mTimerButtonShow invalidate];
		mTimerButtonShow = nil;
	}

	if (!mPauseImageView)
		return;

	NSArray *windows = [UIApplication sharedApplication].windows;
	UIWindow *twindow = [windows objectAtIndex:0];
	
	if (twindow)
	{
		[mPauseImageView willRemoveSubview:mPauseImageViewLabel];
		[mPauseImageViewLabel removeFromSuperview];
		mPauseImageViewLabel = nil;

		[twindow willRemoveSubview:mPauseImageView];
		[mPauseImageView removeFromSuperview];
		mPauseImageView = nil;
		
		if (mPauseImageViewButton != nil)
		{
			[twindow willRemoveSubview:mPauseImageViewButton];
			[mPauseImageViewButton removeFromSuperview];
			mPauseImageViewButton = nil;
		}
	}
}

-(void) showPausePictureButton:(id)sender
{
#ifdef IOS_FREEMIUM
	bool bIsInPurchaseNow = gIsInPurchaseNow;
#else
	bool bIsInPurchaseNow = false;
#endif
	
//	NSLog(@"show pause picture button");
	if (bIsInPurchaseNow && mPauseImageView != nil && mPauseImageViewButton != nil)
	{
		[mTimerButtonShow invalidate];
		mTimerButtonShow = nil;

		mPauseImageViewButton.hidden = NO;
	}
}

- (void) pausePictureButtonPressed
{
//	NSLog(@"\n Hide Pause Picture Button Pressed");
	[self hidePausePicture];

#ifdef IOS_FREEMIUM
	purchaseContinueGame();
#endif
}

- (void) showActivityIndicator
{
	if (mActivityIndicator != nil)
		[self hideActivityIndicator];

#ifdef IOS_FREEMIUM
	bool bIsInPurchaseNow = gIsInPurchaseNow;
#else
	bool bIsInPurchaseNow = false;
#endif

	size_t v = 0;
	Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("VIEW", &v);
	UIView *twindow = (UIView *)v;
	
	if (twindow)
	{
		mActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		
		CGRect rectBounds = [twindow bounds];
		
		if (bIsInPurchaseNow)
			mActivityIndicator.center = CGPointMake(rectBounds.size.width/2, rectBounds.size.height/2 - 40);
		else
			mActivityIndicator.center = CGPointMake(rectBounds.size.width/2, rectBounds.size.height/2);

		[mActivityIndicator startAnimating];
		
		[twindow addSubview:mActivityIndicator];
		[mActivityIndicator release];
	}
}

- (void) hideActivityIndicator
{
	if (!mActivityIndicator)
		return;

	NSArray *windows = [UIApplication sharedApplication].windows;
	UIWindow *twindow = [windows objectAtIndex:0];
	
	if (twindow)
	{
		[twindow willRemoveSubview:mActivityIndicator];
		[mActivityIndicator removeFromSuperview];
		mActivityIndicator = nil;
	}
}

- (float) playVideo:(NSURL *)url
{
	size_t v = 0;
	Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("VIEW", &v);
	UIView *twindow = (UIView *)v;
	if (twindow)
    {
        mMoviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
        mMoviePlayer.controlStyle = MPMovieControlStyleNone;
		mMoviePlayer.scalingMode = MPMovieScalingModeAspectFill;


		mMoviePlayer.view.frame = CGRectMake(0, 0, twindow.bounds.size.width, twindow.bounds.size.height);

        
        [twindow addSubview:mMoviePlayer.view];



// <������ ����� ����� � ����������>
#if IS_JENGINE_PLATFORM_IPHONE
		CGRect rcSkipButton = CGRectMake(mMoviePlayer.view.frame.size.width - 128, 0, 128, 32);
#else
		CGRect rcSkipButton = CGRectMake(mMoviePlayer.view.frame.size.width - 256, 0, 256, 64);
#endif
		mSkipButtonImgView = [[UIImageView alloc] initWithFrame:rcSkipButton];

		NSString *strImage = @"ios-skipvideo.tga";
		[mSkipButtonImgView setImage:[UIImage imageNamed:strImage]];
		mSkipButtonImgView.opaque = NO; // explicitly opaque for performance
        [twindow addSubview:mSkipButtonImgView];


		Ogre::UTFString ogreStrText = CVirtualScript::getTextString(108); // "Skip"
		NSString *caption = [NSString stringWithCString:ogreStrText.asUTF8_c_str() encoding:NSUTF8StringEncoding];

#if IS_JENGINE_PLATFORM_IPHONE
		CGRect rcSkipLabelRect = CGRectMake(68.0f, 0.0f, 60.f, 13.f);
#else
		CGRect rcSkipLabelRect = CGRectMake(136.0f, 0.0f, 120.f, 26.f);
#endif
		UILabel *pSkipLabel = [[UILabel alloc] initWithFrame:rcSkipLabelRect];
		[pSkipLabel setTextAlignment:NSTextAlignmentCenter];
		[pSkipLabel setText:caption];

#if IS_JENGINE_PLATFORM_IPHONE
		pSkipLabel.font = [UIFont systemFontOfSize:7];
#else
		pSkipLabel.font = [UIFont systemFontOfSize:14];
#endif

		pSkipLabel.backgroundColor = [[UIColor alloc] initWithWhite:0.f alpha:0.f];
		pSkipLabel.textColor = [[UIColor alloc] initWithRed:0.7f green:0.9f blue:1.f alpha:1.f];
        [mSkipButtonImgView addSubview:pSkipLabel];
		[pSkipLabel release];


		//CGRect rcSkipButtonFx = CGRectMake(mMoviePlayer.view.frame.size.width - 256, 0, 256, 64);
		mSkipButtonImgViewFx = [[UIImageView alloc] initWithFrame:rcSkipButton];
		strImage = @"ios-skipvideofx.tga";
		[mSkipButtonImgViewFx setImage:[UIImage imageNamed:strImage]];
		mSkipButtonImgViewFx.opaque = NO; // explicitly opaque for performance
		[twindow addSubview:mSkipButtonImgViewFx];
		[self showHightlightSkipVideoButton:false];
// </������ ����� ����� � ����������>



		
		
        
        [[TouchRecieverView alloc] initWithSuperview:mMoviePlayer.view _mtouch:false _mtouchms:false _moviePlayer:true];

        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playbackVideoFinishedCallback:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:mMoviePlayer];
        [mMoviePlayer play];

        NSTimeInterval durationInSeconds = mMoviePlayer.duration;
        AVURLAsset *asset = [[[AVURLAsset alloc] initWithURL:url 
                                                     options:[NSDictionary dictionaryWithObjectsAndKeys:
                                                              [NSNumber numberWithBool:YES], 
                                                              AVURLAssetPreferPreciseDurationAndTimingKey,
                                                              nil]] autorelease];
		if (asset)
			durationInSeconds = CMTimeGetSeconds(asset.duration);

        return durationInSeconds;
    }
    
    return 0.f;
}

- (void) playbackVideoFinishedCallback:(NSNotification *)notification
{

	//MPMoviePlayerController *player = [notification object];
    
    [self hideVideoPlayer];
	//[player release];
}

- (void) pauseVideo;
{
    if (!mMoviePlayer)
        return;
	
    if (mMoviePlayer.playbackState == MPMoviePlaybackStatePlaying)
        [mMoviePlayer pause];
}

- (void) unpauseVideo;
{
    if (!mMoviePlayer)
        return;

    if (mMoviePlayer.playbackState == MPMoviePlaybackStatePaused)
        [mMoviePlayer play];
}

- (void) hideVideoPlayer
{
    if (!mMoviePlayer)
        return;
    
    [mMoviePlayer stop];
    
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification
												  object:mMoviePlayer];
    

	NSArray *windows = [UIApplication sharedApplication].windows;
	UIWindow *twindow = [windows objectAtIndex:0];
	
	if (twindow)
	{
//		[twindow willRemoveSubview:mMoviePlayer.view];
		[mMoviePlayer.view removeFromSuperview];

//		[mMoviePlayer release];
		mMoviePlayer = nil;

		// ������ ����
//		[twindow willRemoveSubview:mSkipButtonImgView];
		[mSkipButtonImgView removeFromSuperview];
//		[mSkipButtonImgView release];
		mSkipButtonImgView = nil;

		// ��������� ������ ����
//		[twindow willRemoveSubview:mSkipButtonImgViewFx];
		[mSkipButtonImgViewFx removeFromSuperview];
//		[mSkipButtonImgViewFx release];
		mSkipButtonImgViewFx = nil;
	}

	applicationUpdateCameraAspect();
}

- (void) showHightlightSkipVideoButton: (bool)bShow
{
	[mSkipButtonImgViewFx setAlpha:bShow ? 1.f : 0.f];
}

@end

//--------------------------------
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

MyGestureView mgv;

@interface JEngineAppDelegate : NSObject <UIApplicationDelegate>
{
	NSTimer *mTimer;
 
	// Use of the CADisplayLink class is the preferred method for controlling your animation timing.
    // CADisplayLink will link to the main display and fire every vsync when added to a given run-loop.
    // The NSTimer class is used only as fallback when running on a pre 3.1 device where CADisplayLink
    // isn't available.
	id mDisplayLink;
    NSDate *mDate;
    double mLastFrameTime;
    double mStartTime;
    BOOL mDisplayLinkSupported;
}


- (void)go;
- (void)renderOneFrame:(id)sender;
- (void)orientationChanged:(NSNotification *)notification;


@end

@implementation JEngineAppDelegate



- (CFAbsoluteTime)mLastFrameTime
{
    return mLastFrameTime;
}

- (void)setLastFrameTime:(CFAbsoluteTime)frameInterval
{
    // Frame interval defines how many display frames must pass between each time the
    // display link fires. The display link will only fire 30 times a second when the
    // frame internal is two on a display that refreshes 60 times a second. The default
    // frame interval setting of one will fire 60 times a second when the display refreshes
    // at 60 times a second. A frame interval setting of less than one results in undefined
    // behavior.
    if (frameInterval >= 1.)
    {
        mLastFrameTime = frameInterval;
    }
}

- (void)go 
{
    MPMusicPlayerController *pMediaPlayer = [MPMusicPlayerController iPodMusicPlayer];
    MPMusicPlaybackState iPodPlaybackState = [pMediaPlayer playbackState];
    bool bIPodPlaying = (iPodPlaybackState == MPMusicPlaybackStatePlaying);
    NSLog(@"\n iPod Playing = %d (state = %d)", bIPodPlaying, iPodPlaybackState);


	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
    try {
		app.setGetsureView(&mgv);
		[mgv.mGestureView becomeFirstResponder];

		app.go();
		
        Ogre::Root::getSingleton().getRenderSystem()->_initRenderTargets();
        
        // Clear event times
		Ogre::Root::getSingleton().clearEventTimes();
    }
	catch( Ogre::Exception& e ) 
	{
        std::cerr << "An exception has occurred: " <<
        e.getFullDescription().c_str() << std::endl;
	}

    if (bIPodPlaying)
        [pMediaPlayer play];


	if (mDisplayLinkSupported)
    {
        // CADisplayLink is API new to iPhone SDK 3.1. Compiling against earlier versions will result in a warning, but can be dismissed
        // if the system version runtime check for CADisplayLink exists in -initWithCoder:. The runtime check ensures this code will
        // not be called in system versions earlier than 3.1.
//#if 0
		mDate = [[NSDate alloc] init];
        mLastFrameTime = -[mDate timeIntervalSinceNow];
        
        mDisplayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(renderOneFrame:)];
        [mDisplayLink setFrameInterval:mLastFrameTime];
        [mDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
//#endif
    }
    else
    {
 		mLastFrameTime = CFAbsoluteTimeGetCurrent();
        mTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)(1.0f / 60.0f)
                                                  target:self
                                                selector:@selector(renderOneFrame:)
                                                userInfo:nil
                                                 repeats:YES];
    }
		
    // Register for orientation notifications
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification object:nil];

	[pool release];




    UInt32 propertySize;
	UInt32 categoryValue = kAudioSessionCategory_AmbientSound;
    propertySize = sizeof(categoryValue);
	AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, propertySize, &categoryValue);
    UInt32 allowMixing = true;
    propertySize = sizeof(allowMixing);
    AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryMixWithOthers, propertySize, &allowMixing);

//	CFStringRef state;
	AudioSessionInitialize(NULL, NULL, NULL, NULL);
}

- (void)applicationDidFinishLaunching:(UIApplication *)application 
{
	// Hide the status bar
	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	
	//mDisplayLinkSupported = false;
	//mLastFrameTime = CFAbsoluteTimeGetCurrent();
	//mDisplayLink = nil;
	//mTimer = nil;
	
    mDisplayLinkSupported = FALSE;
    mLastFrameTime = 1;
    mStartTime = 0;
    mTimer = nil;
	
    // A system version of 3.1 or greater is required to use CADisplayLink. The NSTimer
    // class is used as fallback when it isn't available.
#if USE_CADISPLAYLINK
    NSString *reqSysVer = @"3.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        mDisplayLinkSupported = TRUE;
#endif
	
	NSLog(@"Display link: %d",mDisplayLinkSupported);
		
	[self go];
	
	//--------
	
	bool mt = CVirtualScript::getCfgB("multi_touch");
	bool mtms = CVirtualScript::getCfgB("multi_touch_mouse_state");

	size_t v = 0;
	Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("VIEW", &v);
	UIView *twindow = (UIView *)v;
	
	[[TouchRecieverView alloc] initWithSuperview:twindow _mtouch:mt _mtouchms:mtms _moviePlayer:false];



}

Ogre::OrientationMode getViewportOrientation()
{
	size_t v = 0;
	Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("VIEWCONTROLLER", &v);
	UIInterfaceOrientation orientation = [(UIViewController *)v interfaceOrientation];
	
	if (UIInterfaceOrientationIsLandscape(orientation))
	{
		return (orientation==UIInterfaceOrientationLandscapeRight) ? Ogre::OR_DEGREE_270 : Ogre::OR_DEGREE_90;
	}
	else 
	{
		return (orientation==UIInterfaceOrientationPortrait) ? Ogre::OR_DEGREE_0 : Ogre::OR_DEGREE_180;
	}

}



- (void)orientationChanged:(NSNotification *)notification
{
	//return;
	
	static UIDeviceOrientation oldorientation = UIDeviceOrientationUnknown;
	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
	if (UIDeviceOrientationIsLandscape(orientation) && orientation!=oldorientation)
	{
		oldorientation = orientation;
		//size_t v = 0;
		//Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("VIEW", &v);
		//[(UIView *)v setNeedsLayout];
		
		
		toLogEx("\norientationChanged!!! ");

		applicationUpdateCameraAspect();
		//CVirtualScript::setSI("iphone_need_aspect", 1);
	}
}

- (void)renderOneFrame:(id)sender
{
//	toLogEx("render one!!!!!!!!!!!!!!!!!!!");
	
	BOOL bPaused = !Ogre::Root::getSingleton().getAutoCreatedWindow()->isActive();
	static BOOL bPausedPrev = false;
	BOOL bUnpaused = !bPaused && bPausedPrev;
	bPausedPrev = bPaused;
	
	if (!bPaused)
	{
		if (bUnpaused)
		{
			toLogEx("\n application unpaused!");
			mLastFrameTime = CFAbsoluteTimeGetCurrent();
		}

		CFAbsoluteTime currentFrameTime = CFAbsoluteTimeGetCurrent();
		CFTimeInterval differenceInSeconds = currentFrameTime - mLastFrameTime;
		mLastFrameTime = currentFrameTime;
		if (!Ogre::Root::getSingleton().renderOneFrame((Real)differenceInSeconds))
		{
			toLogEx("\n mainIOS: Render one frame: failed");

			[mTimer invalidate];
			mTimer = nil;
			app.shutdown();
			exit(0);
		}		
	}
	
}


- (void)applicationWillTerminate:(UIApplication *)application 
{
    toLogEx("\n mainIOS: Application will terminate");
    
	Ogre::Root::getSingleton().queueEndRendering();
	[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
	
	if (mDisplayLinkSupported)
	{
		[mDate release];
        mDate = nil;
        
        [mDisplayLink invalidate];
        mDisplayLink = nil;

	}
	else
	{
		[mTimer invalidate];
		mTimer = nil;
	}
	app.shutdown();
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	toLogEx("111!!!!!!!!!!!!!!!!!!!");
    
    
    
    // send message to ginterface to save game
    // and message to interface to suspend xpromo
    if (CVirtualScript::getCfgB("msg_to_gi_on_home_btn"))
    {
        CMsg msgSaveGame;
        msgSaveGame.lparam = 29;
        msgSaveGame.rparam = 0;
        CVirtualScript::sendCMsgNow("00_game_interface", msgSaveGame);

        CMsg msgResignActive;
        msgResignActive.lparam = 20010;
        msgResignActive.rparam = 0;
        CVirtualScript::sendCMsgNow("interface", msgResignActive);
    }


	Ogre::Root::getSingleton().getAutoCreatedWindow()->setActive(false);
	
    // Pause FrameListeners and rendering
	app.enterBackground();
    Ogre::Root::getSingleton().saveConfig();
	
	
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];

	if (CVirtualScript::getCfgB("ios_pause_enabled") && (CVirtualScript::getCfgI("game_state") == 100 || CVirtualScript::getCfgB("pause_on_deactive")))
	{
		applicationPauseVideo();
		applicationShowPausePicture();
	}


	toLogEx("222!!!!!!!!!!!!!!!!!!!");
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // send message to interface to resume xpromo

    if (CVirtualScript::getCfgB("msg_to_gi_on_home_btn"))
    {
        CMsg msgBecomeActive;
        msgBecomeActive.lparam = 20011;
        msgBecomeActive.rparam = 0;
        CVirtualScript::sendCMsgNow("interface", msgBecomeActive);
    }

	if (!CVirtualScript::getCfgB("ios_pause_enabled") || (CVirtualScript::getCfgI("game_state") != 100 && !CVirtualScript::getCfgB("pause_on_deactive")))
	{
		Ogre::Root::getSingleton().getAutoCreatedWindow()->setActive(true);
		app.enterForeground();
		
		if (CVirtualScript::getCfgB("ios_pause_enabled"))
			applicationHidePausePicture();
	}
	
	mLastFrameTime = CFAbsoluteTimeGetCurrent();

	applicationUpdateCameraAspect();
	//CVirtualScript::setSI("iphone_need_aspect", 1);
	
    // Resume FrameListeners and rendering
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification object:nil];
    
}

@end

void showKeyboard(bool bClearPlayerName)
{
	mgv.applicationShowKeyboard(bClearPlayerName);
}

void hideKeyboard()
{
	mgv.applicationHideKeyboard();
}

std::string getKeyboardText()
{
	return mgv.applicationGetKeyboardText();
}

void setKeyboardText(const std::string& text)
{
	mgv.applicationSetKeyboardText(text);
}

void applicationShowPausePicture()
{
	mgv.showPausePicture();
}

void applicationHidePausePicture()
{
	mgv.hidePausePicture();
}

bool applicationIsPaused()
{
	return !Ogre::Root::getSingleton().getAutoCreatedWindow()->isActive();
}

void applicationShowActivityIndicator()
{
	mgv.showActivityIndicator();
}

void applicationHideActivityIndicator()
{
	mgv.hideActivityIndicator();
}

float applicationPlayVideo(const char *chFileName)
{
    return mgv.playVideo(chFileName);
}

void applicationPauseVideo()
{
    mgv.pauseVideo();
}

void applicationUnpauseVideo()
{
    mgv.unpauseVideo();
}

void applicationStopVideo()
{
    mgv.stopVideo();
}

void applicationShowHighlightSkipVideoButton(bool bShow)
{
	mgv.showHightlightSkipVideoButton(bShow);
}

// ��������� ����������� ������ ������ 4/3
void applicationUpdateCameraAspect()
{
/*
	Ogre::Camera *pCamera = app.getCamera();
	if (pCamera)
	{
		pCamera->setAspectRatio(4.f/3.f);
		toLogEx("\n mainIOS.mm: iPhone aspect changed");
	}
*/
}

#	endif
#endif
