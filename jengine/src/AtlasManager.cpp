#include "stdafx.h"
#include "AtlasManager.h"

#include <OgreStableHeaders.h>
#include <OgreMaterialManager.h>
#include <OgreSceneManager.h>
#include <OgreMaterial.h>
#include <OgreTextureManager.h>
#include <OgreTechnique.h>
#include "RapidXmlFunc.h"

namespace Ogre
{
	AtlasTexture CAtlasManager::mAtlasTextureNull = AtlasTexture();
	
	bool AtlasTexture::operator==(const AtlasTexture& tex) const
	{
		return (x==tex.x &&
				y==tex.y &&
				width==tex.width &&
				height==tex.height &&
				(	rect.isNull() && tex.rect.isNull() ||
					!rect.isNull() && !tex.rect.isNull() &&
					abs(rect.left-tex.rect.left)<EPSILONREAL && abs(rect.right-tex.rect.right)<EPSILONREAL &&
					abs(rect.top-tex.rect.top)<EPSILONREAL && abs(rect.bottom-tex.rect.bottom)<EPSILONREAL ) &&
				rotate==tex.rotate &&
				name==tex.name &&
				atlas==tex.atlas);
	}
//---------------------------------------------------------------------
template<> CAtlasManager *Singleton<CAtlasManager>::msSingleton = NULL;
CAtlasManager* CAtlasManager::getSingletonPtr()
{
    return msSingleton;
}
CAtlasManager& CAtlasManager::getSingleton()
{  
    assert( msSingleton );  return ( *msSingleton );  
}

//---------------------------------------------------------------------
CAtlasManager::CAtlasManager()
{
	mScriptPatterns.push_back("*.atlas");
	Ogre::ResourceGroupManager::getSingleton()._registerScriptLoader(this);
}

CAtlasManager::~CAtlasManager()
{
	Ogre::ResourceGroupManager::getSingleton()._unregisterScriptLoader(this);
}

bool CAtlasManager::hasAtlasTexture(const String& name) const
{
	return mAtlasTextures.find(name)!=mAtlasTextures.end();
}

const AtlasTexture& CAtlasManager::getAtlasTexture(const String& name) const
{
	AtlasTextureMap::const_iterator it = mAtlasTextures.find(name);
	if (it!=mAtlasTextures.end())
		return it->second;
	return mAtlasTextureNull;
}

bool CAtlasManager::changeMaterialTexturesToAtlas(const String& materialName) const
{
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Material>(); 

	bool res = false;

	if (!material.isNull())
	{
		for (Material::TechniqueIterator::iterator iter = material->getTechniqueIterator().begin(); 
				iter != material->getTechniqueIterator().end();
				++iter)
		{
			Ogre::Technique* tech = *iter;
			if (tech)
			{
				for (unsigned short i=0; i<tech->getNumPasses(); i++)
				{
					Ogre::Pass* pass = tech->getPass(i);
					if (pass)
					{
						for (unsigned short k=0; k<pass->getNumTextureUnitStates(); k++)
						{
							Ogre::TextureUnitState* state = pass->getTextureUnitState(k);
							if (state)
							{
								String textureName = state->getTextureName();
								size_t pos = textureName.find_last_of(".");
								if ( pos != String::npos )
									textureName = textureName.substr(0, pos);

								toLogEx("\n set texture ok %s %d", textureName.c_str(), CAtlasManager::getSingleton().hasAtlasTexture(textureName));
								if (CAtlasManager::getSingleton().hasAtlasTexture(textureName))
								{
									res = true;

									const AtlasTexture& texture = CAtlasManager::getSingleton().getAtlasTexture(textureName);
									state->setTextureName(texture.atlas);

									Ogre::Matrix4 mat = Ogre::Matrix4::IDENTITY;
									mat.setTrans(Vector3(texture.rect.left, texture.rect.top, 0));
									mat.setScale(Vector3(texture.rect.width(), texture.rect.height(), 0));
									if (texture.rotate)
									{
							            Ogre::Matrix4 rot = Ogre::Matrix4::IDENTITY;
										Ogre::Radian theta ( Ogre::Degree(90) );
										Real cosTheta = Ogre::Math::Cos(theta);
										Real sinTheta = Ogre::Math::Sin(theta);

										rot[0][0] = cosTheta;
										rot[0][1] = -sinTheta;
										rot[1][0] = sinTheta;
										rot[1][1] = cosTheta;
										// Offset center of rotation to center of texture
										rot[0][3] = 0.5f + ( (-0.5f * cosTheta) - (-0.5f * sinTheta) );
										rot[1][3] = 0.5f + ( (-0.5f * sinTheta) + (-0.5f * cosTheta) );

									//	Quaternion rotate; rotate.FromAngleAxis(Ogre::Degree(90), Vector3(0.5f,0.5f,1.f));
										mat=mat*rot;
									}
									state->setTextureTransform(mat);
								}
							}
						}
					}
				}
			}
		}
	}
	else
	{
		OGRE_EXCEPT( Exception::ERR_ITEM_NOT_FOUND, "Could not find material "+materialName,
			"CAtlasManager::changeTexturesToAtlas" );

		return false;
	}

	return res;
}

bool CAtlasManager::changeMaterialTexturesToAtlas(const String& materialName, const String& textureName, bool bSingleState) const
{
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Material>();

	bool res = false;

	if (!material.isNull())
	{
		for (Material::TechniqueIterator::iterator iter = material->getTechniqueIterator().begin(); 
				iter != material->getTechniqueIterator().end();
				++iter)
		{
			Ogre::Technique* tech = *iter;
			if (tech)
			{
				if (bSingleState && tech->getNumPasses() != 1)
					continue;

				for (unsigned short i=0; i<tech->getNumPasses(); i++)
				{
					Ogre::Pass* pass = tech->getPass(i);
					if (pass)
					{

						if (bSingleState && pass->getNumTextureUnitStates() != 1)
							continue;

						for (unsigned short k=0; k<pass->getNumTextureUnitStates(); k++)
						{
							Ogre::TextureUnitState* state = pass->getTextureUnitState(k);

							if (state)
							{
								//String textureName = state->getTextureName();
								String textureNameSe = textureName;
								size_t pos = textureNameSe.find_last_of(".");
								if ( pos != String::npos )
									textureNameSe = textureNameSe.substr(0, pos);

								if (CAtlasManager::getSingleton().hasAtlasTexture(textureNameSe))
								{
									res = true;

									const AtlasTexture& texture = CAtlasManager::getSingleton().getAtlasTexture(textureNameSe);
									state->setTextureName(texture.atlas);

									Ogre::Matrix4 mat = Ogre::Matrix4::IDENTITY;
									mat.setTrans(Vector3(texture.rect.left, texture.rect.top, 0));
									mat.setScale(Vector3(texture.rect.width(), texture.rect.height(), 0));
									if (texture.rotate)
									{
							            Ogre::Matrix4 rot = Ogre::Matrix4::IDENTITY;
										Ogre::Radian theta ( Ogre::Degree(90) );
										Real cosTheta = Ogre::Math::Cos(theta);
										Real sinTheta = Ogre::Math::Sin(theta);

										rot[0][0] = cosTheta;
										rot[0][1] = -sinTheta;
										rot[1][0] = sinTheta;
										rot[1][1] = cosTheta;
										// Offset center of rotation to center of texture
										rot[0][3] = 0.5f + ( (-0.5f * cosTheta) - (-0.5f * sinTheta) );
										rot[1][3] = 0.5f + ( (-0.5f * sinTheta) + (-0.5f * cosTheta) );

									//	Quaternion rotate; rotate.FromAngleAxis(Ogre::Degree(90), Vector3(0.5f,0.5f,1.f));
										mat=mat*rot;
									}
									state->setTextureTransform(mat);
								}
							}
						}
					}
				}
			}
		}
	}
	else
	{
		OGRE_EXCEPT( Exception::ERR_ITEM_NOT_FOUND, "Could not find material "+materialName,
			"CAtlasManager::changeTexturesToAtlas" );

		return false;
	}

	return res;
}


void CAtlasManager::parseScript(Ogre::DataStreamPtr& stream, const String& groupName)
{
	XmlParser parser(stream->getAsString().c_str());

	const XmlNode* root = parser.getDoc().first_node("Atlas");
	if (root)
	{
		Atlas atlas;
		atlas.filename = getAttrib(root, "name");
		atlas.width = getAttribInt(root, "width");
		atlas.height = getAttribInt(root, "height");
			
		for (const XmlNode* elem_tex = root->first_node("Texture"); elem_tex != NULL; elem_tex = elem_tex->next_sibling("Texture"))
		{
			AtlasTexture texture;
			texture.name = getAttrib(elem_tex, "filename");
			texture.x = getAttribInt(elem_tex, "x");
			texture.y = getAttribInt(elem_tex, "y");
			texture.width = getAttribInt(elem_tex, "width");
			texture.height = getAttribInt(elem_tex, "height");
			texture.rect = RealRect(
				Real(texture.x)/atlas.width, Real(texture.y)/atlas.height,
				Real(texture.x+texture.width)/atlas.width, Real(texture.y+texture.height)/atlas.height);

			if (elem_tex->first_attribute("rotate"))
				texture.rotate = getAttribBool2(elem_tex, "rotate");
			texture.atlas = atlas.filename;
			mAtlasTextures[texture.name] = texture;
		}
		mAtlases[atlas.filename] = atlas;
	}
	stream->close();
}

}