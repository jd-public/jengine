#include "stdafx.h"

#include "HeroObject.h"
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	include <mmsystem.h>
#endif
#include "MainApplication.h"
#include "VirtualScript.h"
#ifndef NO_SFX
#	include "SfxCore.h"
#endif

#ifdef _DEBUG
#	include "DynamicLines.h"
#endif

#define WATER_MODE_SLOW 0.3f

#define WALK_MIN_DISTANCE	25.0f
#define WALK_SPEED			130.0f
#define RUN_COEF			2.0f
#define ROT_SPEED			2.5f
#define STATIC_BLEND_SPEED	0.5f
#define WALK_BLEND_SPEED	5.0f
#define RUN_BLEND_SPEED		5.0f
#define ANI_WALK_SPEED		1.0f

void CHeroObject::init()
{
#ifdef _DEBUG
	pWayLine = NULL;
	pWayLine2 = NULL;
#endif

	pTarget = NULL;

	setType(T_HERO);

	for(int i = 0; i < ANI_LAST; i++)
		pAnimState[i] = NULL;

	mMoving = false;
	mRotation = false;
	mRotProgress = 0;

	mIsTaunt=true;
	mEndTime=0;
	mElapsedTaunt = app.getTime();
    mInterval = 1000;

	bWaterMode = false;

#ifndef NO_SFX
	sfxCore.assignListener(mHandle);
#endif

	mLastPos = Vector3(0, 0, 999999.f); 
}

CHeroObject::CHeroObject() : CGameObject()
{
	init();
}

CHeroObject::CHeroObject(const String& scrName) : CGameObject(scrName)
{	
	init();
}

CHeroObject::~CHeroObject()
{

}

void CHeroObject::initAnim()
{
	Ogre::SkeletonInstance *pBaseSkel = pEntity->getSkeleton();
	pBaseSkel->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);
	pBaseSkel->addLinkedSkeletonAnimationSource("01_guy_idle.skeleton"); 
	pBaseSkel->addLinkedSkeletonAnimationSource("01_guy_walk.skeleton"); 
	pBaseSkel->addLinkedSkeletonAnimationSource("01_guy_dlg.skeleton");
	pBaseSkel->addLinkedSkeletonAnimationSource("01_guy_rassugd.skeleton");

	pEntity->refreshAvailableAnimationState();

	pAnimState[ANI_STATIC] = pEntity->getAnimationState("idle");
	pAnimState[ANI_WALK] = pEntity->getAnimationState("walk");
	pAnimState[ANI_RUN] = pEntity->getAnimationState("dlg");
    pAnimState[ANI_CUSTOM_01] = pEntity->getAnimationState("dlg");
	pAnimState[ANI_CUSTOM_02] = pEntity->getAnimationState("rassugd");

	for(int i = 0; i < ANI_LAST; i++)
	{
		if (pAnimState[i]) 
		{
			pAnimState[i]->setWeight(0);
			pAnimState[i]->setLoop(true);
			pAnimState[i]->setEnabled(false);
			mAnimBlendState[i].blendSpeed = 0;
		}
	}

	setState(ANI_STATIC);
}	

void CHeroObject::setState(AnimStates state, Real weight, bool updateSkel)
{
	if (!pAnimState[state]) return;
	
	if (!pAnimState[state]->getEnabled() && weight > 0) 
		pAnimState[state]->setEnabled(true);

    pAnimState[state]->setWeight(weight);

	if (weight == 0)
		pAnimState[state]->setEnabled(false);

	Real rest_weight = 1.0f - weight;

	for(int i = 0; i < ANI_LAST; i++)
	{
		if (pAnimState[i] && pAnimState[i] != pAnimState[state])
		{
			Real new_weight = pAnimState[i]->getWeight()*rest_weight;
			pAnimState[i]->setWeight(new_weight);

			if (new_weight == 0) 
				pAnimState[i]->setEnabled(false);
		}
	}

	if (updateSkel)
		updateSkeleton();
}

void CHeroObject::setBlendState(AnimStates state, Real speed)
{
	///! TODO: bad code
	for(size_t i = 0; i < ANI_LAST; i++)
		if (pAnimState[i])
			mAnimBlendState[i].blendSpeed = 0;
		
	mAnimBlendState[state].blendSpeed = speed;

	if (pAnimState[state] && !pAnimState[state]->getEnabled()) 
		pAnimState[state]->setEnabled(true);
}

Ogre::AnimationState* CHeroObject::findAnimState(const String& skelName) const
{
	///! TODO: bad code
	for(size_t i = 0; i < ANI_LAST; i++)
	{
		if (pAnimState[i] && skelName.find(pAnimState[i]->getAnimationName())!=String::npos)
			return pAnimState[i];
	}
	return NULL;
}

void CHeroObject::updateSkeleton()
{
	Ogre::SkeletonInstance *pBaseSkel = pEntity->getSkeleton();
	Ogre::Skeleton::LinkedSkeletonAnimSourceIterator linkIter = pBaseSkel->getLinkedSkeletonAnimationSourceIterator();
	
	int animNum = 0;

	while(linkIter.hasMoreElements())
	{
		animNum++;

		Ogre::LinkedSkeletonAnimationSource animSrc = linkIter.getNext();

		Ogre::Skeleton::BoneIterator boneIter = pBaseSkel->getBoneIterator();

		Ogre::AnimationState* pCurState = findAnimState(animSrc.skeletonName); 

		if (!pCurState)
		{
			toLogEx("Can`t find anim state for %s\n", animSrc.skeletonName.c_str());
			continue;
		}

		Real animWeight = pCurState->getWeight();

		if (animWeight == 0 || !pCurState->getEnabled()) continue;

		while(boneIter.hasMoreElements())
		{
			Ogre::Bone *skelBone = boneIter.getNext();
			Ogre::Bone *animBone = animSrc.pSkeleton->getBone(skelBone->getHandle());

			Vector3 v1 = skelBone->getInitialPosition();
			Vector3 v2 = animBone->getInitialPosition();

			if (animNum == 1)
				v1 = v2;
			else
				v1 += animWeight*(v2 - v1);

	        skelBone->setPosition(v1);		
		
			Quaternion q1 = skelBone->getInitialOrientation();
			Quaternion q2 = animBone->getInitialOrientation();

			if (animNum == 1)
				q1 = q2;
       		else
				q1 = Quaternion::Slerp(animWeight,q1,q2,true);

			skelBone->setOrientation(q1);
		
			skelBone->setInitialState();
		}
	}
}

void CHeroObject::processMovement(Real inc)
{
	if (mWayPoints.size())
	{
		Vector3 pos = pSNode->getPosition();

		Vector3 curWP = mWayPoints[mCurPoint];
		Vector3 nextWP = mWayPoints[mCurPoint+1];

		Vector3 dir = nextWP - pos;
		Real distance = dir.normalise();
		Real speed = WALK_SPEED;
		Real stop_dist = WALK_MIN_DISTANCE;

		if (mIsRun && !bWaterMode)
		{
			speed *= RUN_COEF;
			stop_dist *= RUN_COEF;
		}

		if (mCurPoint>0)
			pos += dir*(speed*inc*mRotProgress);
		else
			pos += dir*(speed*inc);
		pSNode->setPosition(pos);

		if (distance < stop_dist)
		{
			mCurPoint++;
			
			if (mCurPoint == mWayPoints.size()-1) //last
			{
				mElapsedTaunt = app.getTime();
				setBlendState(ANI_STATIC,STATIC_BLEND_SPEED);
				mWayPoints.clear();
                if (isMsgSend)
				{
					CScriptMsg m;
					m.messageId = CScriptMsg::MSG_NOTIFY;
					m.pTargetObj = pTarget;
					m.param.lparam = mHandle;
					app.postMsg(m);
				}
				if (mAssignRot)
				{
					mRotation = true;
					mRotProgress = 0;
					destRot = finalRot;
					scrRot = pSNode->getOrientation();
				}
				mMoving = true;
				mStartPos = pSNode->getPosition();
				mEndPos = nextWP;
			}
			else
			{
				setDestRotationToPoint(mWayPoints[mCurPoint+1]);
			}
		}
	}

	//-------------------
	if (mRotation)
	{
		mRotProgress += ROT_SPEED * inc;

		if (mRotProgress > 1.0f)
		{
			mRotation = false;
			mRotProgress = 1.0f;
		}
		
		Quaternion delta = Quaternion::Slerp(mRotProgress, scrRot, destRot,true);
		pSNode->setOrientation(delta);
	}
		
	if (mMoving && pAnimState[ANI_STATIC])
	{
		float a = pAnimState[ANI_STATIC]->getWeight()*4.0f;
		if (a > 0)
		{
			mMoving = false;
		}
		pSNode->setPosition((1-a)*mStartPos+a*mEndPos);
	}
}

void CHeroObject::normaliseAnimWeights(size_t n)
{
	Real max_weight = 0;
	
	if (pAnimState[n]) 
		max_weight = 1.0f - pAnimState[n]->getWeight();

    for(size_t i = 0; i < ANI_LAST; i++)
		if (pAnimState[i] && i != n) 
			pAnimState[i]->setWeight(pAnimState[i]->getWeight()*max_weight);

}

void CHeroObject::processFrame( const Ogre::FrameEvent& evt)
{
	CGameObject::processFrame(evt);

	//---------------
	Real inc = evt.timeSinceLastFrame; 
	processMovement(inc);
	bool hasBlending = false;
	for(size_t i = 0; i < ANI_LAST; i++)
	{
		if (pAnimState[i]) 
		{
			if (pAnimState[i]->getEnabled())
			{
				if (i == ANI_WALK)
					pAnimState[i]->addTime(inc*ANI_WALK_SPEED);
				else
					pAnimState[i]->addTime(inc);
			}

			if (mAnimBlendState[i].blendSpeed > 0)
			{
				Real weight = pAnimState[i]->getWeight();
				weight += mAnimBlendState[i].blendSpeed*inc;

				if (weight > 1.0f) 
				{ 
					weight = 1.0f; mAnimBlendState[i].blendSpeed = 0;
					if (pTarget && i == ANI_STATIC && !isMsgSend)
					{
						CScriptMsg m;
						m.messageId = CScriptMsg::MSG_NOTIFY;
						m.pTargetObj = pTarget;
						m.param.lparam = mHandle;
						app.postMsg(m);
					}
				}
				else if (weight < 0)
				{ 
					weight = 0; 
					mAnimBlendState[i].blendSpeed = 0;
				}

				pAnimState[i]->setWeight(weight);

				if (weight == 0)
					pAnimState[i]->setEnabled(false);

				normaliseAnimWeights(i);
				hasBlending = true;
			}
		}
	}

	if (hasBlending)
	{
		updateSkeleton();
	}

	int curTime = app.getTime();
	if (mWayPoints.size() == 0 && !mMoving)
	{
		if (curTime > mEndTime - 1000 && mElapsedTaunt == -1)
		{
			setBlendState(ANI_STATIC,STATIC_BLEND_SPEED*0.2f);
			mElapsedTaunt = app.getTime();
			mInterval = 10000 + rand()%3000;
		}
	}
}

void CHeroObject::findWPs(CGameObject* pWalkPlane, const Vector3 &_startPos, const Vector3 &_endPos)
{
	if (!pWalkPlane) return;

	Real step = WALK_MIN_DISTANCE*0.5f;
	Real side_step = 3.0f;
	Real up_step = 10.0f;
	Real max_step = WALK_MIN_DISTANCE*4.0f;
	Real max_side_step = 250.0f;

	Vector3 dirToEnd = _endPos - _startPos;
	dirToEnd.normalise();

	Vector3 endPos = _endPos;
	Vector3 curPos = _startPos + dirToEnd*step;
	Vector3 prevPos = curPos;

	Vector3 zz(0, 0, 1000.f);
	Vector3 res;

	Vector3 rayDir(0, 0, -1.f);
	Ogre::Ray ray;
	ray.setDirection(rayDir);

	while(true)
	{
		dirToEnd = endPos - curPos;
		dirToEnd.normalise();

		curPos += dirToEnd*step;

		if ((curPos - endPos).length() < max_step)
			break;

		ray.setOrigin(curPos + zz);

		if (app.polygonRaycast(ray,pWalkPlane,res))
		{
			if (fabsf(prevPos.z - res.z) > up_step ||
			   (prevPos - res).length() > max_step)
			{
				mWayPoints.push_back(res);
				prevPos = res;
			}
		}
		else
		{
			Vector3 sideVect;
			Real delt = 0;
			Real d = 0;

			bool result = false;

			while(!result)
			{
				d+=side_step;
				
				sideVect = rayDir.crossProduct(dirToEnd);

				int sign = (rand()%2) ? -1 : 1;
				delt = d * sign;
				ray.setOrigin(curPos + sideVect*delt + zz);

				result = app.polygonRaycast(ray,pWalkPlane,res);
				if (!result)
				{
					delt *= -1;
					ray.setOrigin(curPos + sideVect*delt + zz);
					
					result = app.polygonRaycast(ray,pWalkPlane,res);
				}

				if (d > max_side_step)
				{
					toLogEx("d > max_side_step! Can't find waypoints!");
					return;
				}
			}
			if ((prevPos - res).length() > max_step)
			{
				mWayPoints.push_back(res);
				prevPos = res;
			}
			curPos += sideVect*delt;
		}
	}
}


void CHeroObject::createWayPoints(CGameObject* pWalkPlane, const Vector3& endPos)
{
	Vector3 curPos = pSNode->getPosition();
	
	mWayPoints.clear();
	mWayPoints.push_back(curPos);
	findWPs(pWalkPlane, curPos, endPos);
	mWayPoints.push_back(endPos);

	mCurPoint = 0;
}

Quaternion CHeroObject::_getRotationTo(const Vector3& src, const Vector3& dest,
						  const Vector3& fallbackAxis = Vector3::ZERO)
{
	// Based on Stan Melax's article in Game Programming Gems
	Quaternion q;
	// Copy, since cannot modify local
	Vector3 v0 = src;
	Vector3 v1 = dest;
	v0.normalise();
	v1.normalise();

	Vector3 c = v0.crossProduct(v1);
	Real d = v0.dotProduct(v1);
	// If dot == 1, vectors are the same
	if (d >= 1.0f)
	{
		return Quaternion::IDENTITY;
	}

	if (d < -1 + 1e-4f)
	{
		q.FromAngleAxis(Ogre::Radian(Ogre::Math::PI), fallbackAxis);
		return q;
	}

	Real s = Ogre::Math::Sqrt( (1+d)*2 );
	if (s < 1e-6f)
	{
		if (fallbackAxis != Vector3::ZERO)
		{
			// rotate 180 degrees about the fallback axis
			q.FromAngleAxis(Ogre::Radian(Ogre::Math::PI), fallbackAxis);
		}
		else
		{
			// Generate an axis
			Vector3 axis = Vector3::UNIT_X.crossProduct(src);
			if (axis.isZeroLength()) // pick another if colinear
				axis = Vector3::UNIT_Y.crossProduct(src);
			axis.normalise();
			q.FromAngleAxis(Ogre::Radian(Ogre::Math::PI), axis);
		}
	}
	else
	{
		Real invs = 1 / s;

		q.x = c.x * invs;
		q.y = c.y * invs;
		q.z = c.z * invs;
		q.w = s * 0.5f;
	}
	return q;
}


void CHeroObject::setDestRotationToPoint(const Vector3 &point)
{
	mRotation = true;
	mRotProgress = 0;
	scrRot = pSNode->getOrientation();

	Vector3 dir = point - pSNode->getPosition();
	dir.normalise();
	dir.z = 0;

	Vector3 forwardDir = scrRot*Vector3::UNIT_X;
	Quaternion q = _getRotationTo(forwardDir,dir,Vector3::UNIT_Z);
	destRot = scrRot*q;
	destRot.normalise();
}

void CHeroObject::statAnim(CGameObject* _pTarget, const String& name)
{
	mMoving = false;
	mWayPoints.clear();
	pTarget = NULL;
	//for dialog
	String animName = name;
	if (animName == "dialog_01")
	{
		if (rand()%5 == 0)
			animName = "dialog_02";
	}
	//---------
	for(int i = 0; i < ANI_LAST; i++)
	{
		if (pAnimState[i] && pAnimState[i]->getAnimationName() == animName)
		{
            pAnimState[i]->setTimePosition(0);
			setBlendState((AnimStates)i, WALK_BLEND_SPEED);
	        
			if (i == ANI_STATIC)
				mElapsedTaunt = -1;
			else
				mElapsedTaunt = -2;
			
			break;
        }
	}
}


void CHeroObject::_statAnim(CGameObject* _pTarget, const String& name)
{
	mMoving = false;
	mWayPoints.clear();

	pTarget = _pTarget;

	for(size_t i = 0; i < ANI_LAST; i++)
	{
		if (pAnimState[i]->getAnimationName() == name)
		{
            pAnimState[i]->setTimePosition(0);
			
			if (!bWaterMode)
				mEndTime = app.getTime() + int(pAnimState[i]->getLength()*1000);
			else
				mEndTime = app.getTime() + int(pAnimState[i]->getLength()*(1000.f/WATER_MODE_SLOW));
			
			setBlendState((AnimStates)i, WALK_BLEND_SPEED);
	        break;
        }
	}
}

void CHeroObject::playAnim(const String& name)
{
	mMoving = false;
	mWayPoints.clear();

	pTarget = NULL;
	for(int i = 0; i < ANI_LAST; i++)
	{
		if (pAnimState[i]->getAnimationName() == name)
		{
			pAnimState[i]->setTimePosition(0);

           	setBlendState((AnimStates)i, WALK_BLEND_SPEED);

			if (i == ANI_STATIC)
				mElapsedTaunt = -1;
			else
				mElapsedTaunt = -2;

	        break;
        }
	}
}
	
void CHeroObject::lookTo(const Vector3 &pos, CGameObject* _pTarget)
{
	isMsgSend = true;
	mElapsedTaunt = -1;
	mMoving = false;
	pTarget = _pTarget;

	//-------------------
	Vector3 curPos = pSNode->getPosition();
	Vector3 d = Vector3(pos.x, pos.y, 0) - Vector3(curPos.x, curPos.y, 0);
	mCurPoint = 0;

	//-------------------
	setDestRotationToPoint(curPos + WALK_MIN_DISTANCE*d);
	mWayPoints.clear();
	mWayPoints.push_back(curPos);
	mWayPoints.push_back(curPos);
	mAssignRot = false;
		
	setBlendState(ANI_WALK,WALK_BLEND_SPEED);
}

void CHeroObject::_assignTo(CGameObject* pWalkPlane,
			const Vector3 &pos, 
			const Vector3 &rot,		
			bool isRun,		
			bool accuratePos,	
			bool assignRot,	
			CGameObject* _pTarget,
			bool _isMsgSend) 
{
	if (!pWalkPlane)
		return;
	mLastPos  = pos;
	isMsgSend=_isMsgSend;
	mElapsedTaunt = -1;
	mMoving = false;
	Vector3 cur_pos = pSNode->getPosition();

	pTarget = _pTarget;
	isRun = bWaterMode;
	mIsRun = bWaterMode;

	if ((cur_pos-pos).length() > WALK_MIN_DISTANCE)
	{
		createWayPoints(pWalkPlane, pos);
		setDestRotationToPoint(mWayPoints[mCurPoint+1]);
		mAssignRot = assignRot;

		if (assignRot)
		{
			Ogre::Matrix3 m;
			m.FromEulerAnglesXYZ(Ogre::Degree(rot.x),Ogre::Degree(rot.y),Ogre::Degree(rot.z));
			finalRot.FromRotationMatrix(m);
		}

		if (isRun)
			setBlendState(ANI_RUN,RUN_BLEND_SPEED);
		else
			setBlendState(ANI_WALK,WALK_BLEND_SPEED);
	}
	else 
	{
		if (assignRot)
		{
			mCurPoint = 0;
			Vector3 curPos = pSNode->getPosition();
			Vector3 p = curPos + 
				WALK_MIN_DISTANCE*Vector3(Ogre::Math::Cos(Ogre::Degree(rot.z)),Ogre::Math::Sin(Ogre::Degree(rot.z)), curPos.z);
			lookTo(p,pTarget);
			toLogEx("HERO: I'm in position but need a rot!\n");
		}
		else
		{
			CScriptMsg m;
			m.messageId = CScriptMsg::MSG_NOTIFY;
			m.pTargetObj = pTarget;
			m.param.lparam = mHandle;
			app.postMsg(m);
			pTarget = NULL;
			toLogEx("HERO: I'm in position!\n");
		}
	}
}	

void CHeroObject::assignTo(CGameObject* pWalkPlane,
			const Vector3& pos, 
			const Vector3& rot,		
			bool isRun,		
			bool accuratePos,	
			bool assignRot,	
			CGameObject* _pTarget) 
{
	_assignTo(pWalkPlane, pos, rot, isRun, accuratePos, assignRot, _pTarget, false);
}

void CHeroObject::assignToAlt(CGameObject* pWalkPlane,
			const Vector3& pos, 
			const Vector3& rot,		
			bool isRun,		
			bool accuratePos,	
			bool assignRot,	
			CGameObject* _pTarget) 
{
	_assignTo(pWalkPlane,pos,rot, isRun,accuratePos, assignRot, _pTarget, true);	
}

void CHeroObject::stop()
{
	mElapsedTaunt = app.getTime();
    pTarget = NULL;
	mLastPos = Vector3(0, 0, 999999.f); 
	if (mWayPoints.size() > 0)
	{
		mCurPoint = 0;
		Vector3 curPos = pSNode->getPosition();

		mWayPoints.clear();
		mWayPoints.push_back(curPos);
		mWayPoints.push_back(curPos);

		mAssignRot = false;

		setBlendState(ANI_WALK,WALK_BLEND_SPEED);

		mMoving = false;
		mRotation = false;
	}
}

void CHeroObject::resetTo(const Vector3 &pos, const Vector3 &rot)
{
	mLastPos = Vector3(0, 0, 999999.f); 

	mElapsedTaunt = app.getTime();
	pSNode->setPosition(pos);	
	
	Quaternion q;
	Ogre::Matrix3 m;
	m.FromEulerAnglesXYZ(Ogre::Degree(rot.x),Ogre::Degree(rot.y),Ogre::Degree(rot.z));
	
	pSNode->setOrientation(Quaternion(m));
    pTarget = 0;
	setState(ANI_STATIC);

	mMoving = false;
	mRotation = false;

	mWayPoints.clear();
	mCurPoint = 0;
}

void CHeroObject::setWaterMode(bool mode)
{
	bWaterMode = mode;
}