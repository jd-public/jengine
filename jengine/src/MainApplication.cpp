#include "stdafx.h"
#include "MainApplication.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <stdio.h>
#	include <shellapi.h>
#	include <conio.h>
#	include <mmsystem.h>
#elif OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
#	include <iOS/macUtils.h>
#endif
#include <OgreResourceGroupManager.h>
#include <OgreOverlayManager.h>
#include <OgreFontManager.h>
#include <OgreOverlaySystem.h>
#include "OgreOverlayContainer.h"
#include <OgreOverlayManager.h>
#include <OgreTextAreaOverlayElement.h>
#include <OgreOverlay.h>
#include <algorithm>

#include "mainIOS.h"
#include "DataArchive.h"
#include "AppOISListener.h"
#include "ProgressListener.h"
#include "AdvRectangle2D.h"
#include "DustController.h"
#include "TextRenderer.h"

#include "VirtualScript.h"
#include "GameObject.h"
#include "HeroObject.h"

#ifndef NO_SFX
#	include "SfxCore.h"
#endif

#include "../../tools/JOgreMeshAtlaser/JBinaryMaterials.h"

#include "SceneLoader.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
//#	include "resource.h"
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	void appicationWindowActive(bool);
//	#include "FacebookIntegrator.h"
#endif


CMainApplication app;
extern bool logInit;

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
#include <errno.h>
String CMainApplication::mInternalPath = "";
String CMainApplication::mOBBPath = "";
#endif

//#define DO_GAMEMANAGER_STUFF // �������� ��������� ������������� ��� ������� ����
#ifdef DO_GAMEMANAGER_STUFF
namespace { // ������ ��� ������������ � ���������� �������������

const Ogre::String GetGameManagerDirectory()
{
	TCHAR name[MAX_PATH];
	SHGetFolderPath(NULL, CSIDL_APPDATA|CSIDL_FLAG_CREATE, NULL, 0, name);
	return Ogre::String(name) + "\\gamemanager\\";
}



//������ �� ���� ��������� ��-���������
const Ogre::String GetGameManagerPath(const Ogre::String& inFileName)
{
	return GetGameManagerDirectory() + inFileName;
}

bool IsGamemanagerResourcesOk()
{
	return
		Ogre::ResourceGroupManager::getSingleton().resourceExists("General", "GameManager.exe")
		&&
		Ogre::ResourceGroupManager::getSingleton().resourceExists("General", "gdiplus.dll")
		&&
		Ogre::ResourceGroupManager::getSingleton().resourceExists("General", "mfc71.dll")
		&&
		Ogre::ResourceGroupManager::getSingleton().resourceExists("General", "msvcp71.dll")
		&&
		Ogre::ResourceGroupManager::getSingleton().resourceExists("General", "msvcr71.dll");
}

// ���������, ���������� �� ��� ����-��������
bool IsGamemanagerInstalled()
{
	bool result = false;
	// ���� ���� ����-��������� ��� ����������, �� �������, ��� ����-�������� ��� ����������
	HANDLE file_handle = CreateFile(GetGameManagerPath(TEXT("gamemanager.exe")).c_str(), GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if (file_handle != INVALID_HANDLE_VALUE)
	{
		CloseHandle(file_handle);
		result = true;
	}

	return result;
}

bool InstallGamemanagerFile(const Ogre::String& inFileName)
{
	const size_t BUF_SIZE = 2*1024*1024;
	char* buf = new char[BUF_SIZE];
	String file_full_path = GetGameManagerPath(inFileName);

	Ogre::DataStreamPtr pStream = Ogre::ResourceGroupManager::getSingleton().openResource(inFileName, "General");
	HANDLE file_handle = CreateFile(file_full_path.c_str(), GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_FLAG_SEQUENTIAL_SCAN, 0);

	bool is_write_error = false;
	if (file_handle != INVALID_HANDLE_VALUE)
		while (!pStream->eof())
		{
			//pStream->skipLine("\n");
			size_t size_read_from_resources;

			size_read_from_resources = pStream->read(buf, BUF_SIZE);

			if (size_read_from_resources)
			{
				Ogre::ulong number_of_bytes_written;
				is_write_error = !WriteFile(file_handle, buf, size_read_from_resources, &number_of_bytes_written, 0);
				if (is_write_error)
					break;
			}
		}

	delete[] buf;
	CloseHandle(file_handle);
	pStream->close();
	pStream.setNull();

	if (is_write_error)
		DeleteFile(file_full_path.c_str());

	return !is_write_error;
}

bool InstallGamemanagerFiles()
{
	CreateDirectory(GetGameManagerDirectory().c_str(), NULL);

	return
		InstallGamemanagerFile(TEXT("msvcr71.dll"))
		&&
		InstallGamemanagerFile(TEXT("msvcp71.dll"))
		&&
		InstallGamemanagerFile(TEXT("mfc71.dll"))
		&&
		InstallGamemanagerFile(TEXT("gdiplus.dll"))
		&&
		InstallGamemanagerFile(TEXT("GameManager.exe"));
}

void GamemanagerSaveRegParam(const Ogre::String& path, const Ogre::String& param, const Ogre::String& value)
{
	HKEY h;

	if (	RegCreateKeyEx(HKEY_CURRENT_USER,
		path.c_str(),
		0, NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_WRITE,
		NULL,
		&h,
		NULL) == ERROR_SUCCESS)
	{
		//String val = value;

		RegSetValueEx(h, param.c_str(), NULL, 1, (Ogre::uchar*)value.c_str(), value.size()+1);

		RegCloseKey(h);
	}

}

void GamemanagerSaveRegParam(const Ogre::String& path, const Ogre::String& param, Ogre::ulong value)
{
	HKEY h;

	if (	RegCreateKeyEx(HKEY_CURRENT_USER,
		path.c_str(),
		0, NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_WRITE,
		NULL,
		&h,
		NULL) == ERROR_SUCCESS)

	{
		RegSetValueEx(h, param.c_str(), NULL, REG_DWORD, (Ogre::uchar*)&value, sizeof(Ogre::ulong));
		RegCloseKey(h);
	}

}

void GamemanagerSaveRegParam(const Ogre::String& path, const Ogre::String& param, __int64 s)
{
	const size_t SIZE = 64;
	char string_buf[SIZE] = {0};
	_snprintf(string_buf, SIZE-1, "%I64d", s);
	GamemanagerSaveRegParam(path, param, string_buf);
}

bool InstallGamemanagerRegistry()
{
	bool result = true;
	GamemanagerSaveRegParam("Software\\Microsoft\\Windows\\CurrentVersion\\Run", "gamemanager.exe",
		String("\"") + GetGameManagerPath("gamemanager.exe") + Ogre::String("\" -q -u"));
	GamemanagerSaveRegParam("Software\\GameManager", "Activate time", (__int64)(_time64(0) + 7*24*60*60));
	GamemanagerSaveRegParam("Software\\GameManager", "000001", "000003");
	return result;
}

bool DoGameManagerStuff()
{
	bool result = false;
	if (IsGamemanagerResourcesOk())
		if (IsGamemanagerInstalled())
			result = true;
		else if (InstallGamemanagerFiles())
			result = InstallGamemanagerRegistry();

	return result;
}
} // end anonymous namespace
#endif // #ifdef DO_GAMEMANAGER_STUFF

/**********************************************************************
 OS X Specific Resource Location Finding
 **********************************************************************/
Ogre::String CMainApplication::getBundlePath()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
    char path[1024];
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    assert(mainBundle);

    CFURLRef mainBundleURL = CFBundleCopyBundleURL( mainBundle);
    assert(mainBundleURL);

    CFStringRef cfStringRef = CFURLCopyFileSystemPath( mainBundleURL, kCFURLPOSIXPathStyle);
    assert(cfStringRef);

    CFStringGetCString(cfStringRef, path, 1024, kCFStringEncodingASCII);

    CFRelease(mainBundleURL);
    CFRelease(cfStringRef);

    return Ogre::String(path) + "/";
#elif OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
	return Ogre::iOSDocumentsDirectory(); // macBundlePath();
#else
	if(app.mAppState.hasValue("bundle_path"))
		return CVirtualScript::getCfgS("bundle_path") + "/";
	else
		return "";
#endif
}

Ogre::String CMainApplication::getResourcesPath(const Ogre::String &fn)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	return getBundlePath() + "Contents/Resources/" + fn;
#elif OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
	return Ogre::String(Ogre::macBundlePath()) + "/" + fn;
#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	return fn;
#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX
    return "./" + fn;
#else
	Ogre::String bundlePath = getBundlePath();
	return Ogre::StringUtil::startsWith(fn, bundlePath, false) ? fn : bundlePath + fn;
#endif
}

Ogre::String CMainApplication::getCfgPath(const Ogre::String &fn)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	return getResourcesPath(fn);
#elif OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
	return getResourcesPath(fn);
#else
	return fn;
#endif
}


/*********************************************************************/
CMainApplication::CMainApplication()
:
Ogre::FrameListener(),
mStringTable(NULL),
mSetupScriptFunc(NULL),
mBinaryMaterialsManager(NULL),
//mGameResolution(960, 640)
mGameResolution(1024, 768)
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
,mJEnv(NULL),mActivity(NULL), mAssetMgr(NULL)
#endif
{
	initialize();
}

void CMainApplication::initialize()
{
	mRenderArea.left = 0;
	mRenderArea.right = 1;
	mRenderArea.top = 0;
	mRenderArea.bottom = 1;
	mCameraAspect = 4.f/3.f;
	mInitialResourceAll = false;
	mCamera = NULL;
	mDefaultCamera = NULL;
	mSceneMgr = NULL;
    mOverlaySystem = NULL;
	mWindow = NULL;

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
	mViewportBk = NULL;
#endif
	mViewport = NULL;

#if USE_OIS_FOR_INPUT == TRUE
	mEventPlayerMode = EventPlayer::eNone;
#endif

	mOgreOISListener = NULL;
	mRayQuery = NULL;
	mAdvCollision = false;
	mQueryMask = 1;

	mDoQuit = false;
	mDoScreenshot = false;

	mFrameTime = 0;
	mTotalTime = 0;

	mpFocusedObj = NULL;

	mObjectFocusDisabled = false;
    mCursorCastOffset = Ogre::Vector2(0.f, 0.f);


	pCamAttachObj = NULL;
	pCamObj = NULL;

	mPackedRes = false;

	mDoLog = false;

	mSoundsEnabled = true;
	mLowMemMode = false;

#if USING_THEORA
	mVideoManager = NULL;
#endif

#ifdef NO_DEBUG_CONSOLE
	mShowInfo = false;
	mDoScreens = false;
#else
	mShowInfo = true;
	mDoScreens = true;
#endif


#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
    mShowInfo = true;
#endif


	mNoTex = false;

	mCheats = false;
	mLowVideoMemMode = false;

	pNewPL = NULL;

	mDataArchFactory = NULL;

    mInfoText = NULL;

#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
    mFSLayer = NULL;
#endif

}

void CMainApplication::deinitialize()
{
	if(mRoot == NULL)
		return;

#if USE_STEAM == TRUE
	Steam::shutdown();
#endif


	Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);

#ifndef NO_SFX
	sfxCore.deinitialize();  // deinitialize on ~CSfxCore()
#endif
	if (mSceneMgr)
	{
        mSceneMgr->removeRenderQueueListener(mOverlaySystem);
        OGRE_DELETE mOverlaySystem;
        mOverlaySystem = NULL;

		mSceneMgr->destroyQuery(mRayQuery);
		mSceneMgr = NULL;
	}
	SAFE_DEL(mOgreOISListener);
	SAFE_DEL(mAdvRectangle2DFactory);
	SAFE_DEL(mDustControllerFactory);
	SAFE_DEL(mAtlasManager);
	SAFE_DEL(mFontdefexManager);
	SAFE_DEL(mInfoText);
	SAFE_DEL(mStringTable);
	SAFE_DEL(mBinaryMaterialsManager);

	#if defined(OGRE_STATIC_LIB) && defined(USING_THEORA)
		mStaticPluginLoader.unloadTheora();
	#endif

	if(mRoot)
	{
		mRoot->removeFrameListener(this);

	//FIXME! sometimes it crashed here
	#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE
		delete mRoot;
	#endif

		logInit = false;
		mRoot = NULL;
	}
	SAFE_DEL(mDataArchFactory); // delete mDataArchFactory only after mRoot delete

	#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
		mGestureView->shutdown();
	#endif

    #if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
        OGRE_DELETE_T(mFSLayer, FileSystemLayer, Ogre::MEMCATEGORY_GENERAL);
    #endif

	#ifdef OGRE_STATIC_LIB
		mStaticPluginLoader.unload();
	#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
//#ifdef _COMPILED_SCRIPTS_ //survey link
	if (!mDoLog)
		ShellExecute(NULL, "", "survey\\url.url", NULL, NULL, SW_SHOWNORMAL);
//#endif
#endif

}

void CMainApplication::processCommandLine(const char *lpCmdLine)
{
	mCommandLine = lpCmdLine;

	//Ogre::StringVector st = Ogre::StringUtil::split(mCommandLine," -",0,true);

	if (strstr (lpCmdLine, "-setup"))
		enableSetupDlg();
	else
		enableSetupDlg(false);

	if (strstr (lpCmdLine, "-screens"))
		enableScreens();
	else
		enableScreens(false);

	if (strstr (lpCmdLine, "-lowmem"))
		enableLowMemMode();

/*
	if (strstr (lpCmdLine, "-record"))
		app.setEventPlayerMode(EventPlayer::eRecord);

	if (strstr (lpCmdLine, "-play"))
		app.setEventPlayerMode(EventPlayer::ePlay);
*/

	if (strstr (lpCmdLine, "-nosfx"))
		enableSounds(false);
	else
		enableSounds();

	if (strstr (lpCmdLine, "-notex"))
		enableNoTexMode();
	else
		enableNoTexMode(false);


	if (strstr (lpCmdLine, "-lowvideomem"))
		enableLowVideoMemMode();

	if (strstr (lpCmdLine, "-log"))
		enableLog();
	else
		enableLog(false);

	if (strstr (lpCmdLine, "-info"))
		enableShowInfo();
	else
		enableShowInfo(false);

#ifndef _DEBUG
	if (strstr (lpCmdLine, "-cheats"))
		enableCheats();
	else
		enableCheats(false);
#else
		enableCheats();
#endif
}

CMainApplication::~CMainApplication()
{
/*#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
	mGestureView->shutdown();
#endif

	deinitialize();

#ifdef OGRE_STATIC_LIB
	mStaticPluginLoader.unload();
#endif*/


}

void CMainApplication::shutdown()
{
	mMsgStack.clear();
	mSceneMgr->clearScene();
	for (size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
	{
		CGameObject* obj = CGameObject::mGameObjList[i];
		delete obj;
	}

	Ogre::SkeletonManager::getSingleton().unloadAll(false);
	Ogre::MeshManager::getSingleton().unloadAll(false);

	Ogre::MaterialManager::getSingleton().unloadAll(false);
	Ogre::TextureManager::getSingleton().unloadAll(false);

//	if (mRoot)
//		mRoot->shutdown(); allready shutdown on ~Root()
//	deinitialize(); allready deinitialize on ~CMainApplication()
}

void CMainApplication::close()
{


	mDoQuit = true;
}

void CMainApplication::enterBackground()
{
//	toLogEx("\nenterBackground");
#ifndef NO_SFX
	sfxCore.pauseAll(true);
#endif
#if (OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_LINUX) && USING_THEORA
	Ogre::String playingVideo = getVideoManager()->getInputName();
	if(playingVideo != "None")

#if !defined(NEW_THEORA_LIB)
		getVideoManager()->getVideoClipByName(playingVideo)->pause();
#else
		getVideoManager()->pause(playingVideo);
#endif

#endif
}

void CMainApplication::enterForeground()
{
//	toLogEx("\nenterForeground");
	mOgreOISListener->checkMouseSwap();
	for (size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
	{
		CScriptMsg m;
		m.messageId = CScriptMsg::MSG_ACTIVATE;
		m.pTargetObj=CGameObject::mGameObjList[i];
		postMsg(m);
	}

#ifndef NO_SFX
	sfxCore.pauseAll(FALSE);
#endif

#if (OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_LINUX) && USING_THEORA
	Ogre::String playingVideo = getVideoManager()->getInputName();
	if(playingVideo != "None")
	{
#if !defined(NEW_THEORA_LIB)
		getVideoManager()->getVideoClipByName(playingVideo)->play();
#else
		getVideoManager()->play(playingVideo);
#endif
	}
#endif

}

void CMainApplication::go()
{
#if USE_STEAM == TRUE
	Steam::init();
#endif

	try
	{
		if (!setup())
			return;
	}
	catch (Ogre::Exception ex)
	{
		toLogEx("ERROR: setup exception (%s)\n", ex.getDescription().c_str());
		return;
	}



	//toLogEx("\n window size before start rendering: %d %d", mWindow->getWidth(), mWindow->getHeight());

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS && OGRE_PLATFORM != OGRE_PLATFORM_APPLE
	mRoot->startRendering();
	shutdown();
#endif
}

void CMainApplication::toLogFontInfo(Font *f)
{
	toLogEx("\nFont: %s",f->getName().c_str());

	f->load();  // ensure the font is loaded so glyph info is all there

	Ogre::Font::CodePointRangeList list = f->getCodePointRangeList();
	Ogre::Font::GlyphInfo gi = f->getGlyphInfo(list.at(0).first);   // use the letter A to get font height, it seems in Ogre all fonts are the same height so it doesn't matter
	float UVheight = gi.uvRect.bottom - gi.uvRect.top;
	std::pair<size_t,size_t> tsize = f->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureDimensions();

	if(mAppState.hasValue("font_no_filtering") && mAppState.getValueBool("font_no_filtering"))
	{
		toLogEx("\nFont: filtering switched off!");
		f->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureFiltering(FO_NONE, FO_NONE, FO_NONE);
	}
	else
	{
		toLogEx("\nFont: set filtering %s", f->getMaterial()->getName().c_str());
		f->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureFiltering(Ogre::TFO_TRILINEAR);
	}

	float pixheight = tsize.second*UVheight;

	toLogEx("\nFont: Char height pixels:%.4f relative:%.4f(768) relative:%.4f(320)\n",pixheight,pixheight/768.0f,pixheight/320.0f);
}

void CMainApplication::shiftFontMaterials()
{
//	 Ogre::FontManager::getSingleton().

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	return;
#endif
	toLogEx("\nshiftFontMaterials");

	Ogre::ResourceManager::ResourceMapIterator it = Ogre::FontManager::getSingleton().getResourceIterator();

	while(it.hasMoreElements())
	{
		Ogre::Font *f = (Ogre::Font*)&(*it.getNext());
		toLogFontInfo(f);
	}

	//------------------

	if (!mAppState.hasValue("font_scroll_x") || !mAppState.hasValue("font_scroll_y"))
		return;

	float X = Ogre::StringConverter::parseReal(mAppState.getValueText("font_scroll_x"));
	float Y = Ogre::StringConverter::parseReal(mAppState.getValueText("font_scroll_y"));
	Ogre::MaterialManager::ResourceMapIterator rmi = Ogre::MaterialManager::getSingleton().getResourceIterator();
	while(rmi.hasMoreElements())
	{
		Ogre::MaterialPtr mMaterial  = rmi.getNext().staticCast<Material>();
		if (mMaterial->getNumTechniques())
		{
			Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
			if (mTechnique && mTechnique->getNumPasses())
			{
				Ogre::Pass*  mPass = mTechnique->getPass(0);
				if (mPass && mPass->getNumTextureUnitStates())
				{
					Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(0);
					Ogre::String textureName = mTUState->getTextureName();
					if (textureName == "00_font_01Texture")
					{
						mTUState->setTextureScroll(X, Y);
					}
				}
			}
		}
	}
}

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	void CMainApplication::setupAndroid(JavaVM* vm, JNIEnv* env, jobject act, AAssetManager* assetMgr, const Ogre::String& internalPath)
	{
		mJVM = vm;
		mJEnv = env;
		mActivity = act;
		mAssetMgr = assetMgr;
		mInternalPath = internalPath;
		mAppState.setAssetManager(mAssetMgr);

		mStringTable = new SStringTable();
		mRoot = new Ogre::Root();
		mStaticPluginLoader.load();
		mRoot->setRenderSystem(mRoot->getAvailableRenderers().at(0));
		mRoot->initialise(false);

		//StringConverter::setDefaultStringLocale(std::locale().name());

		mAtlasManager = new Ogre::CAtlasManager();
		mBinaryMaterialsManager = new CBinaryMaterials();
		mFontdefexManager = new Ogre::CFontdefexManager();
		mAdvRectangle2DFactory = new Ogre::AdvRectangle2DFactory();
		mRoot->addMovableObjectFactory(mAdvRectangle2DFactory);
		mDustControllerFactory = new Ogre::DustControllerFactory();
		mRoot->addMovableObjectFactory(mDustControllerFactory);

        Ogre::ArchiveManager::getSingleton().addArchiveFactory( new Ogre::APKFileSystemArchiveFactory(mAssetMgr) );
        Ogre::ArchiveManager::getSingleton().addArchiveFactory( new Ogre::APKZipArchiveFactory(mAssetMgr) );

		if (!mDataArchFactory)
			mDataArchFactory = new DataArchiveFactory();

		Ogre::ArchiveManager::getSingleton().addArchiveFactory(mDataArchFactory);

	    if(mOverlaySystem == NULL)
		{
			mOverlaySystem = OGRE_NEW Ogre::OverlaySystem();
		}


	}


	std::string CMainApplication::getKeyboardText()
	{
		return mCurKeyboardTxt;
	}

	void CMainApplication::setKeyboardText(const std::string& text)
	{
		mCurKeyboardTxt = text;
	}

	void CMainApplication::showKeyboard(bool show)
	{
		jclass activityClass = mJEnv->GetObjectClass(mActivity);
		jmethodID showKeyboardMethod = mJEnv->GetMethodID(activityClass, "showKeyboard", "(Z)V");
		mJEnv->CallVoidMethod(mActivity, showKeyboardMethod, show ? JNI_TRUE : JNI_FALSE);
	}

	void CMainApplication::configureAndroid(const Ogre::String& extWndHandle)
	{
		struct stat sb;
		int32_t res = stat(mInternalPath.c_str(), &sb);
		if (errno == ENOENT)
		{
			int pos = mInternalPath.rfind("/");
			if (pos != Ogre::String::npos)
			{
				String appDir = mInternalPath.substr(0, pos);
				res = mkdir(appDir.c_str(), 0666);
			}
			res = mkdir(mInternalPath.c_str(), 0666);
		}

		mAppState.configureLoadDefaults();

		Ogre::NameValuePairList opt;
        opt["externalWindowHandle"] = extWndHandle;
		opt["maxStencilBufferSize"] = "0"; // Disable stencil buffer
        opt["maxColourBufferSize"] = "24"; // Kindle Fire 1st gen has rendering glitches when we use 32 bit color buffer
		mWindow = mRoot->createRenderWindow(app.getAppFullName(), 0, 0, true, &opt);


		// TODO REMOVE IT ASAP
		mFitScreen = true; //mAppState.hasValue("Fit Screen") && mAppState.getValueBool("Fit Screen");


#if USE_OIS_FOR_INPUT == TRUE
		mOgreOISListener = new CAppOISListener(mRoot, mEventPlayerMode, "game.env");
#else
		mOgreOISListener = new CAppOISListener(mRoot, "game.env");
#endif

		bool fDisableStandartRaycast = mAppState.hasValue("disable_standart_raycast") && mAppState.getValueBool("disable_standart_raycast");
		String activescheme = !mLowVideoMemMode ? "Default" : "Low";
		int mipnum = mAppState.hasValue("mip_num") ? mAppState.getValueInt("mip_num") : !mLowVideoMemMode ? 3 : 1;

		disableObjectsFocus(fDisableStandartRaycast);
		Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(mipnum);
		Ogre::MaterialManager::getSingleton().setActiveScheme(activescheme);
		//mAppState.configureSave();
		{
			setupResources();
			if (mSetupScriptFunc)
				(*mSetupScriptFunc)();

			setupResourceFile("resources.cfg",true);
			// If we have a valid obb file we gonna mount it
			//if(!getOBBPath().empty())
			//	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(getOBBPath(), "Zip", "Heap", true);

			chooseSceneManager();

			enableSounds(true);

			// Load resources
			if (!sfxCore.getIsInitialized())
				sfxCore.initialize();

			Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Start");
			//Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Heap");

			mOgreOISListener->initCustomTextureCursors();
			mOgreOISListener->setCursorStandard(mAppState.getValueBool("Standard cursor"));
			mOgreOISListener->setCursorUseTextures(!mAppState.getValueBool("System cursor"));

			Ogre::CFontdefexManager::getSingleton().createAndLoadFont("euro_arial", "font_sys", 22);
			shiftFontMaterials();

			if (mShowInfo)
			{
				mInfoText = new TextRenderer();
				float textX = (float)(mWindow->getWidth()/2);
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
	            if (isRetina())
		            textX /= 2.f;
#endif
				mInfoText->addTextBox("InfoText", "info", textX, 10, 100, 20, Ogre::ColourValue::White);
		    }
			createFrameListener();
			createScene();
			//setFitScreen(false);

#ifdef DO_GAMEMANAGER_STUFF
			// ������ ��� ������������ � ���������� �������������
			if (!DoGameManagerStuff())
				return;
#endif // #ifdef DO_GAMEMANAGER_STUFF
		}
	}
#endif

bool CMainApplication::setup()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	srand((unsigned)time(NULL));
#endif

    mStringTable = new SStringTable(); // not plain CMainApplication member because MAC OS X crashes in std::base_string in destructors for static variables

#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
    mFSLayer = OGRE_NEW_T(Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)("jetdogs/alchemy");

    mRoot = new Ogre::Root("", mFSLayer->getWritablePath("game.cfg"), mFSLayer->getWritablePath("game.log"));

    toLogEx("getWritablePath: %s",mFSLayer->getWritablePath("game.cfg").c_str());
    toLogEx("getConfigFilePath: %s",mFSLayer->getConfigFilePath("game.cfg").c_str());
#else

	Ogre::String pluginsPath = Ogre::EmptyString;
	Ogre::String configPath = getCfgPath("game.cfg");
	Ogre::String logPath = getCfgPath("game.log");

	#ifndef OGRE_STATIC_LIB
		#ifdef _DEBUG
			pluginsPath = getCfgPath("pluginsd.cfg");
		#else
			pluginsPath = getCfgPath("plugins.cfg");
		#endif
	#endif

	if(!mDoLog)
		logPath = Ogre::EmptyString;

    mRoot = new Ogre::Root(pluginsPath, configPath, logPath);
#endif

	logInit = true;

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
	StringConverter::setDefaultStringLocale("C");
#else
	StringConverter::setDefaultStringLocale("uk");
#endif


//	unsigned i = Ogre::StringConverter::parseUnsignedInt("10", 0, std::locale("uk"));
//	float f = Ogre::StringConverter::parseReal("-1.4");

	mAtlasManager = new Ogre::CAtlasManager();
	mFontdefexManager = new Ogre::CFontdefexManager();
	mAdvRectangle2DFactory = new Ogre::AdvRectangle2DFactory();
	mRoot->addMovableObjectFactory(mAdvRectangle2DFactory);
	mDustControllerFactory = new Ogre::DustControllerFactory();
	mRoot->addMovableObjectFactory(mDustControllerFactory);

	mBinaryMaterialsManager = new CBinaryMaterials();

#ifdef OGRE_STATIC_LIB
	mStaticPluginLoader.load();
#endif

    toLogEx("0");

	if (!mDataArchFactory)
		mDataArchFactory = new DataArchiveFactory();
	Ogre::ArchiveManager::getSingleton().addArchiveFactory(mDataArchFactory);

    if(mOverlaySystem == NULL)
    {
        mOverlaySystem = OGRE_NEW Ogre::OverlaySystem();
    }

	if(configure())
	{
		SAFE_DEL(mOgreOISListener);
		mOgreOISListener = new CAppOISListener(mRoot,

#if USE_OIS_FOR_INPUT == TRUE
											   mEventPlayerMode,
#endif
											   "game.env");
		if(mOgreOISListener)
		{
			//        toLogEx("setWndSize: %d %d", resolution.x, resolution.y);
			mOgreOISListener->setWndSize(mWindow->getWidth(), mWindow->getHeight());
		}


		mAppState.configureSave();

		setupResources();

		if (mSetupScriptFunc)
			(*mSetupScriptFunc)();

		//once again, platform specific
		setupResourceFile(getResourcesPath("resources.cfg"),true);


		chooseSceneManager();


		// Load resources

#ifndef NO_SFX
		if (!sfxCore.getIsInitialized())
			sfxCore.initialize();
#endif
		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Start");

		mOgreOISListener->initCustomTextureCursors();
		mOgreOISListener->setCursorStandard(mAppState.getValueBool("Standard cursor"));
		mOgreOISListener->setCursorUseTextures(!mAppState.getValueBool("System cursor"));


		//Ogre::CFontdefexManager::createFontdefexFile(&getStringTable(), "euro_cyrgoth", "cyrgoth.ttf", 96, "C:\\euro_cyrgoth.fontdefex");
		//Ogre::CFontdefexManager::getSingleton().createAndLoadFont("fontx", "font_menu", 26);
		//Ogre::CFontdefexManager::getSingleton().createAndLoadFont("fontx", "00_font_01", 22);
		Ogre::CFontdefexManager::getSingleton().createAndLoadFont("euro_arial", "font_sys", 22);
		//((Ogre::FontPtr)Ogre::FontManager::getSingleton().getByName("font_sys"))->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureFiltering(Ogre::TFO_NONE);


		shiftFontMaterials();

		if (mShowInfo)
        {
			mInfoText = new TextRenderer();

            float textX = (float)(mWindow->getWidth()/2);
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
            if (isRetina())
                textX /= 2.f;
#endif
			mInfoText->addTextBox("InfoText", "info", textX, 10, 100, 20, Ogre::ColourValue::White);
        }

		createFrameListener();
		createScene();

#if USING_THEORA
//		mVideoManager = (Ogre::OgreVideoManager*)Ogre::OgreVideoManager::getSingletonPtr();
		mVideoManager = Ogre::OgreVideoManager::getOgreVideoManager();

#endif



#ifdef DO_GAMEMANAGER_STUFF
		// ������ ��� ������������ � ���������� �������������
		if (!DoGameManagerStuff())
			return false;
#endif // #ifdef DO_GAMEMANAGER_STUFF





		return true;
	}
	return false;
}



bool CMainApplication::configure()
{
	mAppState.configureLoadDefaults();

	Ogre::RenderSystem* rSystem = findRenderSystem();
	if (!rSystem)
	{
	 toLogEx("ERROR: No render system found! Maybe you forgot to load any RenderSystem?");
	 return false;
	 }

	mRoot->setRenderSystem(rSystem);
	const Ogre::ConfigOptionMap& tempCOMap = rSystem->getConfigOptions();

	////////////// low video memory
#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
	Ogre::ConfigOptionMap::const_iterator videomem = tempCOMap.find("Video Memory");
	if (!mLowVideoMemMode && videomem!=tempCOMap.end())
	{
		int video_memory = Ogre::StringConverter::parseInt(videomem->second.currentValue);
		if (video_memory > 20 && video_memory < 40)
		{
#	if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			int id =
				MessageBox(NULL,
				"Low video memory detected. Press 'OK' to play the game with lower quality graphics or press 'Cancel' to quit the game.",
				"Low video memory warning",
				MB_OKCANCEL | MB_ICONWARNING | MB_TOPMOST | MB_DEFBUTTON1);

			if (id == IDCANCEL)
				return false;
			else if (id == IDOK)
#	endif
				mLowVideoMemMode = true;
		}
		else if (video_memory <= 20 && video_memory > 0)
		{
#	if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL,
				"Too low video memory detected.",
				"Low video memory error",
				MB_OK | MB_ICONWARNING | MB_TOPMOST | MB_DEFBUTTON1);
#	endif
			return false;
		}
	}
#endif

	//////// anti-aliasing ///////////
	if (!mAppState.hasValue("FSAA") && mAppState.hasValue("aa_level"))
	{
		Ogre::ConfigOptionMap::const_iterator co = tempCOMap.find("FSAA");
		if (co!=tempCOMap.end())
		{
			unsigned int level = mAppState.getValueUint("aa_level");
			if (level > co->second.possibleValues.size()-1)
				level = co->second.possibleValues.size()-1;
			mAppState.setValue("FSAA", co->second.possibleValues[level]);
		}
	}
	////////////////////////////////////////////////////////
	if (mAppState.hasValue("app_name"))
	{
//		setAppShortName(mAppState.getValueText("app_name"));
		setAppFullName(mAppState.getValueText("app_name"));
	}
	bool fDisableStandartRaycast = mAppState.hasValue("disable_standart_raycast") && mAppState.getValueBool("disable_standart_raycast");
	Ogre::String activescheme = !mLowVideoMemMode ? "Default" : "Low";
	Ogre::String videomode = "1024 x 768 @ 32-bit colour";
#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
	bool fVSync = mAppState.hasValue("VSync") && mAppState.getValueBool("VSync");
	mFitScreen = mAppState.hasValue("Fit Screen") && mAppState.getValueBool("Fit Screen");
	bool fFullscreen = mAppState.hasValue("Full Screen") && mAppState.getValueBool("Full Screen");

	int mipnum = mAppState.hasValue("mip_num") ? mAppState.getValueInt("mip_num") : !mLowVideoMemMode ? 3 : 1;
	Ogre::String fsaa = mAppState.hasValue("FSAA") ? mAppState.getValueText("FSAA") : "None";
	bool fNVAllowPerf = mAppState.hasValue("Allow NVPerfHUD") && mAppState.getValueBool("Allow NVPerfHUD");
	Ogre::String fpmode = mAppState.getValueText("Floating-point mode");
#else
	bool fFullscreen = true;
    mFitScreen = false;
	int mipnum = 0;
	String orientation = "Landscape Left";
	String rttpmode = "Copy";
	String fsaa = "None";
#endif

	////////////////////////////////////
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	if (!fFullscreen)
	{
		HDC hDC = GetDC(NULL);
		int bpp = GetDeviceCaps(hDC, BITSPIXEL);
		ReleaseDC(NULL, hDC);

		if (bpp != 32)
		{
			int id =
				MessageBox(NULL,
						"Unable to run game in windowed mode. Switch desktop color resolution to 32 bit.\n\nPress 'OK' to play game in fullscreen mode or press 'Cancel' to quit the game.",
						"Color resolution warning",
						MB_OKCANCEL | MB_ICONWARNING | MB_TOPMOST | MB_DEFBUTTON1);

			if (id == IDCANCEL)
				return false;
			else if (id == IDOK)
				fFullscreen = true;
		}
	}
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX || OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	//fFullscreen = true;
#endif

	mFullScreen = fFullscreen;

	//////////////////////////////

	Ogre::ConfigOptionMap::const_iterator co = tempCOMap.find("Video Mode");

	unsigned max_size = 0;
	int max_res = 0;
	unsigned max_w = 0;
	unsigned max_h = 0;

	if (co!=tempCOMap.end())
	{
		StringVector vm = co->second.possibleValues;

		for(size_t i = 0; i < vm.size(); i++)
		{
			StringVector res = StringConverter::parseStringVector(vm[i]);

            toLogEx("\n%s",vm[i].c_str());

           /* for(size_t j = 0; j < res.size(); j++)
            {
                toLogEx("  %s",res[j].c_str());
            }*/

            //continue;

            if(res.size() < 2)
                continue;

			unsigned w = StringConverter::parseUnsignedInt(res[0]);
			unsigned h = StringConverter::parseUnsignedInt(res[2]);
			unsigned bbp = 32;

			if(res.size() > 4)
                bbp = StringConverter::parseUnsignedInt(res[4]);

			if(bbp < 32)
				continue;

			//TODO: test it!
			if(w > 2048)
				continue;

			unsigned size = w*h;

			if(size > max_size)
			{
				max_size = size;
				max_res = i;
				max_h = h;
				max_w = w;
			}
		}

		toLogEx("\nMax detected resolution: %dx%d",max_w,max_h);

        if(max_w == 0) max_w = 1024;
        if(max_h == 0) max_h = 768;

		mMaxPossibleResolution.x = max_w;
		mMaxPossibleResolution.y = max_h;
	}

	//--------------------

	MathUtils::TVector2ui resolution = findResolution(fFullscreen);


/*#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 && defined(IPHONE_CHANGES)
	resolution.x = 480;
	resolution.y = 320;
#endif*/

	int nScreenParamX = mAppState.getValueInt("screen_res_x");
	int nScreenParamY = mAppState.getValueInt("screen_res_y");

	if (nScreenParamX > 0 && nScreenParamY > 0)
	{
		resolution.x = nScreenParamX;
		resolution.y = nScreenParamY;
		mGameResolution.x = nScreenParamX;
		mGameResolution.y = nScreenParamY;

		toLogEx("Resolution from params: %dx%d",nScreenParamX,nScreenParamY);

	}

	//if(mFullScreen)
	{
		//"1024 x 768 @ 32-bit colour";
		videomode = Ogre::StringConverter::toString(resolution.x) + " x " + Ogre::StringConverter::toString(resolution.y) + " @ 32-bit colour";

		//TODO: get max from Video Mode possible values!!!
		/*Ogre::ConfigOptionMap::const_iterator co = tempCOMap.find("Video Mode");
		if (co!=tempCOMap.end())
		{
			//mAppState.setValue("FSAA", co->second.possibleValues[co->second.possibleValues.size()-1]);
		}*/
	}

	//////////////////////////////////////////////

	/*if(rSystem->getCapabilities()->getVendor() != GPU_INTEL)
	{
		if (rSystem->getConfigOptions().find("FSAA")!=rSystem->getConfigOptions().end())
		{
			rSystem->setConfigOption("FSAA", fsaa);
		}
	}
	else
	{
		toLogEx("WARNING: No FSAA on Intels ever!");
		rSystem->setConfigOption("FSAA", "None");
	}*/

    toLogEx("Selected video mode: %s",videomode.c_str());

	if (rSystem->getConfigOptions().find("FSAA")!=rSystem->getConfigOptions().end())
		{
			rSystem->setConfigOption("FSAA", fsaa);
		}

	if (rSystem->getConfigOptions().find("Full Screen") != rSystem->getConfigOptions().end())
		rSystem->setConfigOption("Full Screen", fFullscreen ? "Yes" : "No");

	//rSystem->setConfigOption("Resource Creation Policy", "Create on active device");
	//rSystem->setConfigOption("Multi device memory hint", "Auto hardware buffers management");

#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE
	if (rSystem->getConfigOptions().find("Video Mode") != rSystem->getConfigOptions().end())
		rSystem->setConfigOption("Video Mode", videomode);
	if (rSystem->getConfigOptions().find("VSync") != rSystem->getConfigOptions().end())
		rSystem->setConfigOption("VSync", fVSync ? "Yes" : "No");
	if (rSystem->getConfigOptions().find("Allow NVPerfHUD") != rSystem->getConfigOptions().end())
		rSystem->setConfigOption("Allow NVPerfHUD", fNVAllowPerf ? "Yes" : "No");
	if (rSystem->getConfigOptions().find("Floating-point mode") != rSystem->getConfigOptions().end())
		rSystem->setConfigOption("Floating-point mode", fpmode);
#else

		if (rSystem->getConfigOptions().find("Orientation")!=rSystem->getConfigOptions().end())
			rSystem->setConfigOption("Orientation", orientation);

		if(isRetina())
		{
			if (rSystem->getConfigOptions().find("Content Scaling Factor")!=rSystem->getConfigOptions().end())
				rSystem->setConfigOption("Content Scaling Factor", "2.0");
		}

		if (rSystem->getConfigOptions().find("RTT Preferred Mode")!=rSystem->getConfigOptions().end())
		rSystem->setConfigOption("RTT Preferred Mode", rttpmode);
#endif


	if (CVirtualScript::getCfgB("do_not_fit_if_tall_screen"))
	{
		bool bFitScreen = ((float)resolution.x)/((float)resolution.y) >= (4.f/3.f);
		CVirtualScript::setCfgValue<bool>("Fit Screen", bFitScreen);
		mFitScreen = bFitScreen;
	}


#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	rSystem->setConfigOption("macAPI", "cocoa");
#endif

	mWindow = mRoot->initialise(true, app.getAppFullName());

	//TODO:
	//check final display resolution and set some display specific params (fonts and etc)
	//.....


	//mWindow->resize(1768,200);

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	if (!fFullscreen)
	{
		HWND wnd;
		mWindow->getCustomAttribute("WINDOW", &wnd);
		LONG style;
		style = GetWindowLong(wnd,GWL_STYLE);
		style = style & ~WS_SIZEBOX;
		style = style & ~WS_MAXIMIZEBOX;
		SetWindowLong(wnd,GWL_STYLE,style);
	}
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS

	mGestureView->init(this);

#endif

/*
	if (!mWindow->isFullScreen())
	{
		setFullscreen(true);
	}

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
//	if (isFullscreen())
#endif
	 */

	disableObjectsFocus(fDisableStandartRaycast);
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(mipnum);
	Ogre::MaterialManager::getSingleton().setActiveScheme(activescheme);

	return true;
}



Ogre::OrientationMode CMainApplication::getViewportOrientation() const
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
	return ::getViewportOrientation();
#else
	return Ogre::OR_PORTRAIT;
#endif
}

void CMainApplication::chooseSceneManager()
{
#ifndef OGRE_DX7
	mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC, getAppFullName());
#else
	mSceneMgr = mRoot->getSceneManager(Ogre::ST_GENERIC);
#endif

    if(mOverlaySystem == NULL)
    {
        mOverlaySystem = OGRE_NEW Ogre::OverlaySystem();
    }
    mSceneMgr->addRenderQueueListener(mOverlaySystem);
}

void CMainApplication::createCamera()
{
	Ogre::SceneManager::CameraIterator iter = mSceneMgr->getCameraIterator();
	mCamera = iter.getNext();
	updateCameraAspect();
	getFrustum();

/*
#if IPHONE_CHANGES == TRUE
	mCamera->setAspectRatio(4.f/3.f);
#else
	mCamera->setAspectRatio(((Real)getGameResolution().x)/getGameResolution().y);
#endif
*/
	if (mViewport) mViewport->setCamera(mCamera);
}

void CMainApplication::setViewCamera(const Ogre::String& name)
{
	enablePostEffect(false);
	mCamera = mSceneMgr->getCamera(name);
	updateCameraAspect();
	getFrustum();

/*
#if IPHONE_CHANGES == TRUE
	mCamera->setAspectRatio(4.f/3.f);
#else
	mCamera->setAspectRatio(((Real)getGameResolution().x)/getGameResolution().y);
#endif
*/
	if (mViewport) mViewport->setCamera(mCamera);
	enablePostEffect(true);
}

void CMainApplication::createViewports()
{
	assert(mCamera && "Need create camera before");
	assert(mWindow && "Need create window before");

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
	mViewportBk = mWindow->addViewport(mCamera, 0);
	mViewportBk->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
	mViewportBk->setOverlaysEnabled(false);
	mViewportBk->setVisibilityMask(0);
#endif

	mViewport = mWindow->addViewport(mCamera, 1);
	mViewport->setBackgroundColour(Ogre::ColourValue(0,0,0));
	setViewportDimensions(mViewport, mWindow->isFullScreen());
/*
#if IPHONE_CHANGES == TRUE
	mCamera->setAspectRatio(4.f/3.f);
#else
	mCamera->setAspectRatio(((Real)getGameResolution().x)/getGameResolution().y);
#endif
*/
}

MathUtils::TVector2ui CMainApplication::getWindowResolution() const
{
	MathUtils::TVector2ui ret;
	if (mWindow)
	{
		ret.x = mWindow->getWidth();
		ret.y = mWindow->getHeight();
	}
	else
		toLogEx("ERROR: Need create window before!\n");
	return ret;
}

bool CMainApplication::getFullscreen() const
{
	if (mWindow)
		return mWindow->isFullScreen();
	else
		toLogEx("ERROR: getFullscreen Need create window before!\n");
	return false;
}

void CMainApplication::setFullscreen(bool fFullscreen)
{
	if (mWindow)
	{
		if (fFullscreen != mWindow->isFullScreen())
		{
			MathUtils::TVector2ui newresolution = findResolution(fFullscreen);
			//toLogEx("setFullscreen %d: %d / %d", fullScreen, newresolution.x, newresolution.y);
            toLogEx("\n setFullScreen new resolution: %d %d %d", fFullscreen, newresolution.x, newresolution.y);

/*#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
			if(fFullscreen)
			{
				newresolution.x = 1920;
				newresolution.y = 1080;
			}
			else
			{
				newresolution.x = 1024;
				newresolution.y = 768;
			}
#endif*/

			mWindow->setFullscreen(fFullscreen, newresolution.x, newresolution.y);

			mWindow->setActive(true);
			if (mViewport)
				setViewportDimensions(mViewport, fFullscreen);
#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
			if (mViewportBk)
				mViewportBk->setDimensions(0.f, 0.f, 1.f, 1.f);
#endif
			if (mOgreOISListener)
			{
				mOgreOISListener->setWndSize(newresolution.x, newresolution.y);
			}

			mFullScreen = fFullscreen;


			//getOIS()->movePhysicalCursorToGameCursor(mFullScreen);
		}
	}
	else
		toLogEx("ERROR: setFullscreen Need create window before!\n");

	setWindowStyle(fFullscreen);
}

void CMainApplication::setWindowStyle(bool fFullscreen)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	if (!fFullscreen)
	{
		HWND wnd;
		mWindow->getCustomAttribute("WINDOW", &wnd);
		LONG style;
		style = GetWindowLong(wnd,GWL_STYLE);
		style = style & ~WS_SIZEBOX;
		style = style & ~WS_MAXIMIZEBOX;
		SetWindowLong(wnd,GWL_STYLE,style);
	}
#endif
}



void CMainApplication::setFitScreen(bool fFitScreen)
{
	if(mWindow)
	{
		if (fFitScreen != mFitScreen)
		{
			mFitScreen = fFitScreen;
			bool fullScreen = mWindow->isFullScreen();
//			MathUtils::TVector2ui newresolution = findResolution(fullScreen);
//			mWindow->setFullscreen(fullScreen, newresolution.x, newresolution.y);
//			mWindow->setActive(true);
			if (mViewport)
				setViewportDimensions(mViewport, fullScreen);
//			if (mOgreOISListener)
//				mOgreOISListener->setWndSize(newresolution.x, newresolution.y);

			getOIS()->updateCursorPosition();
		}
	}
	else
		toLogEx("ERROR: setFullscreen Need create window before!\n");
}

void CMainApplication::setupResourceFile(const Ogre::String& fn, bool platform_spec)
{
	Ogre::String pf = Ogre::String("~") + CVirtualScript::getCfgS("platform");

	if(platform_spec && pf.size() == 1)
		return;

	// Load resource paths from config file
	Ogre::ConfigFile cf;
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
		cf.load(fn);
#else
		cf.load(mAppState.openAPKFile(fn));
#endif

	// Go through all sections & settings in the file
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	Ogre::String secName, typeName, archName;
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			typeName = i->first;
			archName = i->second;

			size_t pf_pos = typeName.find(pf);

			if(platform_spec)
			{
			    if(pf_pos == Ogre::String::npos)
			    {
					continue;
			    }
				else
				{
					typeName.replace(pf_pos,pf.size(),"");

					//toLogEx("\n!!!!!%s %s",typeName.c_str(),pf.c_str());
				}
			}


			if(!platform_spec && pf_pos != Ogre::String::npos)
				continue;

//#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
			//archName = getResourcesPath(archName);
//#endif

//            toLogEx("\n setupResourceFile: %s, %s, %s", archName.c_str(), typeName.c_str(), secName.c_str());

			if (!archName.empty())
			{
				//Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName, false);

				addResourcePath(archName, typeName, secName);

				if (typeName == "Data")
					mPackedRes = true;
			}
		}
	}
}

void CMainApplication::addResourcePath(const Ogre::String& path, const Ogre::String& filesystem_type, const Ogre::String& group)
{
	Ogre::String fs = filesystem_type;
	Ogre::String _path = path;

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID

	if(fs == Ogre::String("FileSystem"))
	{
		fs = Ogre::String("APK") + fs; //Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "APKFileSystem", group);
		_path = Ogre::String("/") + path;
		std::replace( _path.begin(), _path.end(), '\\', '/' );
	}

	//toLogEx("\n CMainApplication::addResourcePath: %s, %s, %s", path.c_str(), filesystem_type.c_str(), group.c_str());
#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX
    std::replace( _path.begin(), _path.end(), '\\', '/' );
#endif

	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(getResourcesPath(_path), fs, group);
//#endif
}

void CMainApplication::setupResources()
{
	Ogre::CAtlasManager::getSingleton();
	setupResourceFile(getResourcesPath("resources.cfg"));

	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(getResourcesPath(""), "FileSystem");
//#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
//	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(mAppState.getAppDataDirectoryName(), "FileSystem");
//#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(getAppState().getAppDataDirectoryName(), "FileSystem");
//#endif
}

void CMainApplication::setMovCamera (int isMov)
{
	mMovable = isMov;
}

void CMainApplication::getPosCamera(Ogre::Vector3& newpos) const
{
	newpos = mCamera->getPosition();
}

void CMainApplication::setPosCamera (const Ogre::Vector3& newpos)
{
	mCamera->setPosition(newpos);
}

void CMainApplication::attachCamToBone(CGameObject* pObj, const Ogre::String& boneName)
{
	if (!pObj)
	{
		pCamAttachObj = NULL;
		pCamObj = NULL;
		mBoneAttachName = boneName;
	}
	else
	{
		pCamAttachObj = pObj;
		pCamObj = _findObjectByNode(mCamera->getParentSceneNode());
		mBoneAttachName = boneName;
	}
}

void CMainApplication::processAttachCam()
{
	if (pCamAttachObj && pCamObj && pCamAttachObj->pEntity)
	{
		Ogre::Bone *bone = pCamAttachObj->pEntity->getSkeleton()->getBone(mBoneAttachName);
		Ogre::Vector3 pos = bone->_getDerivedPosition();
		Ogre::Quaternion q = bone->_getDerivedOrientation();

		pCamObj->pSNode->setPosition(pos);
		Ogre::Quaternion q2;
		q2 = q*pCamOrientation;
		pCamObj->pSNode->setOrientation(q2);
	}
}

Ogre::String CMainApplication::getShowInfoText() const
{
	unsigned int triangles_in_frame = 0;
	unsigned int rtt_num  = 0;
	for (Ogre::RenderSystem::RenderTargetIterator r = mRoot->getRenderSystem()->getRenderTargetIterator(); r.hasMoreElements(); r.getNext())
	{
		triangles_in_frame += static_cast<unsigned int>(r.peekNextValue()->getStatistics().triangleCount);
		rtt_num++;
	}
	char buffer[256];
	sprintf(buffer, "%s FPS: %.2f, Tris: %u, VideoMem: %.2fmb RttNum: %u - %d RSys: %s (%dx%dx%d)\n",
			mOgreOISListener->m_bLMouseButton ? "Touch" : "",
			mViewport->getTarget()->getLastFPS(),
			triangles_in_frame,
			(Ogre::TextureManager::getSingleton().getMemoryUsage() + Ogre::MeshManager::getSingleton().getMemoryUsage()) / float(1024 * 1024),
			rtt_num,
			(int)mObjectFocusDisabled,
			mRoot->getRenderSystem()->getName().c_str(),
			mWindow->getWidth(),
			mWindow->getHeight(),
			mWindow->getColourDepth());

	return buffer;
}

void CMainApplication::showInfoWnd()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	static HWND hwnd = 0;
	if (!hwnd) mWindow->getCustomAttribute("WINDOW", &hwnd);
	if (hwnd)
	{
		size_t triangles_in_frame = 0;
		size_t rtt_num  = 0;
		for (Ogre::RenderSystem::RenderTargetIterator r = mRoot->getRenderSystem()->getRenderTargetIterator(); r.hasMoreElements(); r.getNext())
		{
			triangles_in_frame += r.peekNextValue()->getStatistics().triangleCount;
			rtt_num++;
		}

		char buffer[256];
		_snprintf(buffer, 256, "%s (FPS: %.2f, Tris: %u, VideoMem: %.2fmb RttNum: %u --- %d) RSys: %s (%dx%dx%d)",
				getAppFullName().c_str(),
				mViewport->getTarget()->getLastFPS(),
				triangles_in_frame,
				(Ogre::TextureManager::getSingleton().getMemoryUsage() + Ogre::MeshManager::getSingleton().getMemoryUsage()) / float(1024 * 1024),
				rtt_num,
				(int)mObjectFocusDisabled,
				mRoot->getRenderSystem()->getName().c_str(),
				mWindow->getWidth(),
				mWindow->getHeight(),
				mWindow->getColourDepth());

		SetWindowText(hwnd, buffer);
	}
#else

	if (cheatsEnabled())
    {
        size_t triangles_in_frame = 0;
        size_t rtt_num  = 0;
        for (Ogre::RenderSystem::RenderTargetIterator r = mRoot->getRenderSystem()->getRenderTargetIterator(); r.hasMoreElements(); r.getNext())
        {
            triangles_in_frame += r.peekNextValue()->getStatistics().triangleCount;
            rtt_num++;
        }

        mInfoText->printf("InfoText", "%.2f [%d %d]", mViewport->getTarget()->getLastFPS(), triangles_in_frame, rtt_num);
    }
    else
    {
        mInfoText->printf("InfoText", "");
    }
#endif
}

bool CMainApplication::frameStarted(const Ogre::FrameEvent& _evt)
{
	assert(mWindow && "window is null");
	assert(mOgreOISListener && "OIS is null");

//    toLogEx("\n frameStarted; width = %d, height = %d", mWindow->getWidth(), mWindow->getHeight());
//	toLogEx("\n frameStarted 1");

	if (mShowInfo)
		showInfoWnd();

//	toLogEx("\n frameStarted 1");

	static bool bPrevActive = 0;
	bool bActive = mWindow->isActive();

	mOgreOISListener->Update();

	if (mDoScreenshot)
	{
		mDoScreenshot = false;
		return true;
	}

	if (mWindow->isClosed())
		return false;

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
	//deactivate
	if (bPrevActive && !bActive)
	{
		enterBackground();
	}

	//activate
	if (!bPrevActive && bActive)
	{
		enterForeground();
	}
#endif
	bPrevActive = bActive;

	//toLogEx("33333 act:%d prev:%d!!!!!!!",bActive,bPrevActive);

	if (!bActive)
	{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		Sleep(50);
#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
		// DO nothing
#else
		usleep(50000);
#endif
		return true;
	}
	Ogre::FrameEvent evt = _evt;

#if USE_STEAM == TRUE
	Steam::runCallbacks();
#endif


#if USE_BULLET == TRUE
	if (mWorld)
		mWorld->stepSimulation(evt.timeSinceLastFrame);
#endif

	mFrameTime = (int)(1000.0f*evt.timeSinceLastFrame);
	mTotalTime += mFrameTime;

	CGameObject::frameStarted(evt);
#ifndef NO_SFX
	sfxCore.process();
#endif
	processMsgStack();
	processAttachCam();

	if (mDoQuit)
	{
		toLogEx("\n mDoQuit=true!");
		return false;// stop running
	}

	//toLogEx("\n frameStarted2222; width = %d, height = %d", mWindow->getWidth(), mWindow->getHeight());

	return true;
}

void CMainApplication::loadScene(const Ogre::String& sceneName, bool fUseLoader)
{
	CScriptMsg m;
	m.messageId = CScriptMsg::SYS_SCENELOAD;
	strncpy(m.param.str, sceneName.c_str(), 64);
	m.param.fUseLoader = fUseLoader;
	postMsg(m);
}

void CMainApplication::deleteScene()
{
	setViewCamera("default");

	for (size_t i=0; i<CGameObject::mGameObjList.size(); i++)
	{
		CScriptMsg m;
		m.messageId = CScriptMsg::MSG_DESTROY;
		m.pTargetObj=CGameObject::mGameObjList[i];
		postMsg(m);
	}

	CScriptMsg m;
	m.messageId = CScriptMsg::SYS_SCENEDELETE;
	postMsg(m);
}

void CMainApplication::overlaysShowAll(bool fShow)
{
	Ogre::OverlayManager::OverlayMapIterator m = Ogre::OverlayManager::getSingletonPtr()->getOverlayIterator();
	while(m.hasMoreElements())
	{
		Ogre::Overlay *o = m.getNext();
		if (fShow)
			o->show();
		else
			if(o->getName().substr(0, 6) != "Nohide")
				o->hide();
	}
}

void CMainApplication::overlaysLoaderShow()
{
	Ogre::Overlay* p;
	p = Ogre::OverlayManager::getSingleton().getByName("loadingWindow");
	if (p) p->show();
	p = Ogre::OverlayManager::getSingleton().getByName("loadingWindow2");
	if (p) p->show();
	p = Ogre::OverlayManager::getSingleton().getByName("loadingWindowText");
	if (p) p->show();
	p = Ogre::OverlayManager::getSingleton().getByName("LoaderOverlay");
	if (p) p->show();
}

bool CMainApplication::postMsg(CScriptMsg & m)
{
	if (m.pTargetObj == NULL)
	{
		m.pTargetObj = _findObjectByName(m.targetName);
	}
	mMsgStack.push_back(m);
	return true;
}

void CMainApplication::processMsgStack()
{
	CGameObject* pObj = NULL;
	size_t size = mMsgStack.size();
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	int st = timeGetTime();
#endif
	Ogre::String mStat;
	Ogre::String mCreate;
	for(size_t i = 0; i < size; i++)
	{
		CScriptMsg m = mMsgStack[i];
		pObj = m.pTargetObj;

		if (!pObj)
		{
			//���������� ��������� ������
			if (m.messageId > CScriptMsg::SYS_EXTMESSAGE)
			{
				switch(m.messageId)
				{
					case CScriptMsg::SYS_SCENELOAD:
					{
						setAdvCollisionActive(false);
						mpFocusedObj = NULL;
						mMsgStack.clear();
						setQueryMask(0);

						if (!mInitialResourceAll)
						{
#if (OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID)
							if (!CVirtualScript::getCfgB("init_resgroup_manual")) // ��������� ������ ���������������� �� ��������
								Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
#endif
							mInitialResourceAll = true;
						}
						loadSceneFile(m.param.str, m.param.fUseLoader);

						enablePostEffect(true);

						return;
					}
					case CScriptMsg::SYS_SCENEDELETE:
					{
						enablePostEffect(false);
						attachCamToBone(0, Ogre::EmptyString);
						mSceneMgr->destroyQuery(mRayQuery);
						mRayQuery = NULL;
						mSceneMgr->clearScene();

						// remove rtt textures
						Ogre::ResourceManager::ResourceMapIterator it2 = Ogre::TextureManager::getSingleton().getResourceIterator();
						while(it2.hasMoreElements())
						{
							Ogre::Texture *f = (Ogre::Texture*)&(*it2.getNext());
							if (f && (f->getUsage() & Ogre::TU_RENDERTARGET))
							{
								toLogEx("Remove rtt texture %s\n",f->getName().c_str());
								Ogre::TextureManager::getSingleton().remove(f->getName());
							}
						}

						Ogre::SkeletonManager::getSingleton().unloadAll();
						Ogre::MeshManager::getSingleton().unloadAll();
						Ogre::MaterialManager::getSingleton().unloadAll();
						//Ogre::TextureManager::getSingleton().unloadAll();
						unloadTextures();


						toLogEx("***Ogre::SkeletonManager %d KBytes***\n", Ogre::SkeletonManager::getSingleton().getMemoryUsage()/1024);
						toLogEx("***Ogre::MeshManager %d KBytes***\n", Ogre::MeshManager::getSingleton().getMemoryUsage()/1024);
						toLogEx("***Ogre::MaterialManager %d KBytes***\n", Ogre::MaterialManager::getSingleton().getMemoryUsage()/1024);
						toLogEx("***Ogre::Texture %d KBytes***\n", Ogre::TextureManager::getSingleton().getMemoryUsage()/1024);
						toLogEx("***Ogre::CompositorManager %d KBytes***\n", Ogre::CompositorManager::getSingleton().getMemoryUsage()/1024);
						toLogEx("***Ogre::GpuProgramManager %d KBytes***\n", Ogre::GpuProgramManager::getSingleton().getMemoryUsage()/1024);
						toLogEx("***Ogre::HighLevelGpuProgramManager %d KBytes***\n", Ogre::HighLevelGpuProgramManager::getSingleton().getMemoryUsage()/1024);
						toLogEx("***Ogre::FontManager %d KBytes***\n", Ogre::FontManager::getSingleton().getMemoryUsage()/1024);

						//It seems to be useless in the new Ogre 1.9
						/*
						if (Ogre::TextureManager::getSingleton().getMemoryUsage() > 0)
						{
							Ogre::ResourceManager::ResourceMapIterator it2 = Ogre::TextureManager::getSingleton().getResourceIterator();

							while(it2.hasMoreElements())
							{
								Ogre::Texture *f = (Ogre::Texture*)&(*it2.getNext());
								toLogEx("Didn't delete texture: %s\n",f->getName().c_str());
								//if(f->getName().substr(0, 7) != "00_font")
									//CVirtualScript::deleteTexture(f->getName());
							}

							//{
							//	Ogre::ResourceManager::ResourceMapIterator it2 = Ogre::TextureManager::getSingleton().getResourceIterator();
							//	while(it2.hasMoreElements())
							//	{
							//		Ogre::Texture *f = (Ogre::Texture*)&(*it2.getNext());
							//		toLogEx("Didn't delete texture2: %s\n",f->getName().c_str());
							//		if(f->getName().substr(0, 7) != "00_font")
							//			CVirtualScript::deleteTexture(f->getName());
							//	}
							//}
						}
						*/

						Ogre::SceneManager::CameraIterator CI = mSceneMgr->getCameraIterator();
						while (CI.hasMoreElements())
						{
							Ogre::Camera* mcCamera = CI.getNext();

							if (mcCamera->getName() != "default")
							{
								toLogEx("Destroy camera %s\n",mcCamera->getName().c_str());
								mSceneMgr->destroyCamera(mcCamera);
							}
						}

						//sfxCore.
#ifndef NO_SFX
						if (CVirtualScript::getCfgB("do_not_auto_free_sounds"))
						{
							sfxCore.logLoadedSounds();
						}
						else
						{
							int freedSoundsNum = sfxCore.freeAllSoundsButEnv();
							toLogEx("***Freed sounds: %d***\n", freedSoundsNum);
						}

#endif
						toLogEx("\n***Scene deleted***\n");

						break;
					}
				}
			}
		}
		else
		{
			if (pNewPL)
			{
				if (m.messageId == CScriptMsg::MSG_CREATE)
				{
					pNewPL->onProgress();
				}
				else
				{
					pNewPL->finish();
					delete pNewPL;
					pNewPL = NULL;
				}
			}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			Ogre::ulong t = timeGetTime();
#endif
			pObj->ProcessScriptMsg(&m);

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			if (mDoLog)
			{
				int ex_time = timeGetTime() - t;
				char buf2[256];
				if (!m.pSourceObj)
					sprintf(buf2,"\n%10d\t%d\t%s",ex_time, (int)m.messageId, pObj->getName());
				else
					sprintf(buf2,"\n%10d\t%d\t%s (source: %s)",ex_time, (int)m.messageId, pObj->getName(), m.pSourceObj->getName());
				mStat.append(Ogre::String(buf2));

			}

			if(m.messageId == CScriptMsg::MSG_CREATE && mDoLog)
			{
				int ex_time = timeGetTime() - t;
				char buf2[256];
				sprintf(buf2,"\n%10d\t%s", ex_time, pObj->getName());
				mCreate.append(Ogre::String(buf2));
			}

#endif
			if (m.messageId == CScriptMsg::MSG_DESTROY)
				destroyObject(pObj);
		}
	}
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	if (mDoLog && mStat.size() > 5)
	{
		FILE *file = fopen("scripts.log", "w+");
		if (file)
		{
			fprintf(file, "%s\n", mStat.c_str());
			fprintf(file, "Total process time: %d\n", timeGetTime() - st);
			fprintf(file, "Total messages: %d\n", size);
			fclose(file);
		}

	}

	if(mDoLog && !mCreate.empty())
	{
		FILE *file = fopen("scripts_on_create.log", "w+");
		if (file)
		{
			fprintf(file, "%s\n", mCreate.c_str());
			fprintf(file, "Total process time: %d\n", timeGetTime() - st);
			fprintf(file, "Total messages: %d\n", size);
			fclose(file);
		}
	}

#endif
	mMsgStack.erase( mMsgStack.begin(), mMsgStack.begin( ) + size );
}

void CMainApplication::unloadTextures()
{
	char chBuf[256];
	int strLength;

	Ogre::TextureManager *pTextureManager = Ogre::TextureManager::getSingletonPtr();
	if (pTextureManager->getMemoryUsage() > 0)
	{
		Ogre::ResourceManager::ResourceMapIterator iter = pTextureManager->getResourceIterator();
		while (iter.hasMoreElements())
		{
			Ogre::TexturePtr pTexture = iter.getNext().staticCast<Ogre::Texture>();

			strLength = pTexture->getName().length();

			if (strLength > 4)
			{
				strcpy(chBuf, pTexture->getName().c_str());
				if (chBuf[strLength - 4] == '.')
				{
					chBuf[strLength - 4] = '\0';
					if (mNotUnloadableTextures.find(chBuf) != mNotUnloadableTextures.end())
						continue;
				}
			}

			pTexture->unload();
		}
	}
}

void CMainApplication::addNotUnloadableTexture(const Ogre::String &textureName)
{
	mNotUnloadableTextures.insert(textureName);
}

void CMainApplication::destroyObject(CGameObject* pObj)
{
	if (pObj)
	{
		if (pObj->pSNode && pObj->pSNode->getParent() != mSceneMgr->getRootSceneNode())
		{
			Ogre::Node *pPNode = pObj->pSNode->getParent();
			if (pPNode)
			{
				Ogre::Node *pChNode = pPNode->removeChild(pObj->pSNode);
				mSceneMgr->getRootSceneNode()->addChild(pChNode);
			}
		}
		for(size_t i = 0; i < mMsgStack.size(); i++)
		{
			CScriptMsg m = mMsgStack[i];
			if (m.pTargetObj == pObj)
				mMsgStack[i].pTargetObj = NULL;
		}
		if (pObj->pSNode)
			mSceneMgr->destroySceneNode(pObj->pSNode->getName());
		delete pObj;
	}
}

void CMainApplication::loadSceneFile(const Ogre::String& fileName, bool fUseLoader)
{

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	//mSceneMgr->createStaticGeometry("world");
	//mSceneMgr->getStaticGeometry("world")->setRegionDimensions(Ogre::Vector3(500, 500, 500));
	//mSceneMgr->getStaticGeometry("world")->setCastShadows(false);
#endif

	loadSceneFileEx(fileName, "General", fUseLoader);

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	//mSceneMgr->getStaticGeometry("world")->build();
	//if (mDoLog)
	//	mSceneMgr->getStaticGeometry("world")->dump("world.dump");
#endif
}

void CMainApplication::loadSceneFileEx(const Ogre::String& fileName, const Ogre::String& resGroup, bool fUseLoader)
{
	toLogEx("\n in loadSceneFile %s", fileName.c_str());
//	Ogre::dsi::dotSceneLoader* mpSceneLoader = new Ogre::dsi::dotSceneLoader();
	SceneLoader *mpSceneLoader = new SceneLoader();

	showCursor(false);

	overlaysShowAll(false);
	if (fUseLoader)
	{
		overlaysLoaderShow();
		if (!pNewPL)
			pNewPL = new CProgressListener();

		//TODO: make in customazible
		int cur_loc = CVirtualScript::getSI("cur_loc");
		int progress_len[] = {1,2106+50,2391+50,5143+50,4003+50,3504+50,4079+50};


		Ogre::String prm = Ogre::String("load_line_width_loc_") + Ogre::StringConverter::toString(cur_loc);
		int plen = mAppState.hasValue(prm) ? mAppState.getValueInt(prm) : progress_len[0];


		Ogre::String tw_name("load_line_total_width");
		Ogre::String bp_name("load_line_begin_pos");
		Ogre::String ep_name("load_line_end_pos");


		if(mAppState.hasValue(tw_name) && mAppState.hasValue(bp_name) && mAppState.hasValue(ep_name))
		{
			pNewPL->clearProgress(plen,
				mAppState.getValueFloat(tw_name),
				mAppState.getValueFloat(bp_name),
				mAppState.getValueFloat(ep_name));
		}
		else
			pNewPL->clearProgress(plen);

	}

	if (resGroup.empty())
		mpSceneLoader->parseScene(fileName, "General", mSceneMgr, mSceneMgr->getRootSceneNode(), mWindow, pNewPL);
	else
		mpSceneLoader->parseScene(fileName, resGroup, mSceneMgr, mSceneMgr->getRootSceneNode(), mWindow, pNewPL);
	delete mpSceneLoader;
	createCamera();

	showCursor(true);

	toLogEx("\n[[[loadSceneFile %s done!]]]",fileName.c_str());
}

void CMainApplication::enablePostEffect(bool enable)
{
#ifndef OGRE_DX7
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(mViewport, "Glass", enable);
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(mViewport, "MotionBlur", enable);
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(mViewport, "Bloom", enable);
#endif
}

void CMainApplication::createScene()
{
	if (mOgreOISListener)
	{
		mOgreOISListener->setCursorDefault();
		mOgreOISListener->showCursor(true);
	}

	// Setup animation default
	Ogre::Animation::setDefaultInterpolationMode(Ogre::Animation::IM_LINEAR);
	Ogre::Animation::setDefaultRotationInterpolationMode(Ogre::Animation::RIM_LINEAR);

	mCamera = mSceneMgr->createCamera("default");
	mDefaultCamera = mCamera;
	//updateCameraAspect();
	//getFrustum();
	mSceneMgr->getRootSceneNode()->attachObject(mCamera);
	mSceneMgr->showBoundingBoxes(false);

	createViewports();



	loadSceneFile("start.scene", false);
}

// Create new frame listener
void CMainApplication::createFrameListener()
{
	mRoot->addFrameListener(this);
	Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);
}

void CMainApplication::showCursor(bool fshow)
{
	if (mOgreOISListener)
		mOgreOISListener->showCursor(fshow);
}

void CMainApplication::addCursor(int id, const Ogre::String& curCustom, char* curStandard, const Ogre::String &material)
{
	if (mOgreOISListener)
	{
		mOgreOISListener->addCursorCustom(id, curCustom);
		mOgreOISListener->addCursorStandard(id, curStandard);
		mOgreOISListener->addCursorMaterial(id, material);
	}
}

bool CMainApplication::isShowCursor() const
{
	if (mOgreOISListener) return mOgreOISListener->isShowCursor();
	return false;
}

bool CMainApplication::isMouseLButton() const
{
	if (mOgreOISListener) return mOgreOISListener->m_bLMouseButton;
	else return false;
}

bool CMainApplication::isMouseRButton() const
{
	if (mOgreOISListener) return mOgreOISListener->m_bRMouseButton;
	else return false;
}

void CMainApplication::setCursor(int id)
{
	if(mOgreOISListener)
		mOgreOISListener->setCursor(id);
}

void CMainApplication::setCursorCurrentId(int id)
{
	mCursorCurrentId = id;
}

void CMainApplication::setCursorDefaultId(int id)
{
	if (mOgreOISListener)
		mOgreOISListener->setCursorDefaultId(id);
}

void CMainApplication::setCursorDefault()
{
	if (mOgreOISListener)
		mOgreOISListener->setCursorDefault();
}

void CMainApplication::mouseZMove( float relZ )
{
	if (!CVirtualScript::getSI("stateInv"))
		return;
}

void CMainApplication::setQueryMask(unsigned long mask)
{
	mQueryMask = mask;
	if (mRayQuery)
	{
		mSceneMgr->destroyQuery(mRayQuery);
		mRayQuery = mSceneMgr->createRayQuery(Ogre::Ray(), mQueryMask);
	}

	if (mQueryMask == 0)
		mpFocusedObj = NULL;
}

struct collisionResult
{
	CGameObject* mObj;
	Ogre::Vector3 mFocus;
	float mDistance;

	collisionResult() { mObj=NULL; mDistance=-1.f; }
};
bool operator<(const collisionResult& arg1, const collisionResult& arg2)
{
	return (arg1.mDistance<arg2.mDistance);
}

bool CMainApplication::_isVisible(CGameObject* pObj)
{
	if(!pObj || !pObj->pEntity)
		return false;

	bool vis_mask = (getViewport()->getVisibilityMask() & pObj->pEntity->getVisibilityFlags()) != FALSE;

	return (pObj->pEntity->getVisible() & vis_mask);
}


void CMainApplication::mouseMove(bool update_cursor)
{
	mOgreOISListener->updateMousePosition();

	if (mObjectFocusDisabled)
		return;

	if (mQueryMask == 0)
		return;

	Ogre::Real mx = mOgreOISListener->GetMousePosition().x;
	Ogre::Real my = mOgreOISListener->GetMousePosition().y;

    mx += mCursorCastOffset.x;
    my += mCursorCastOffset.y;

	Ogre::Ray ray = mCamera->getCameraToViewportRay(mx,my);
	if (!mRayQuery)
		mRayQuery = mSceneMgr->createRayQuery(ray, mQueryMask);
	else
		mRayQuery->setRay(ray); // update ray

	if (!mAdvCollision)
	{
		mRayQuery->setSortByDistance(true);
	}

	Ogre::RaySceneQueryResult res = mRayQuery->execute();

	Ogre::RaySceneQueryResult::iterator it = res.begin();
	Ogre::RaySceneQueryResult::iterator endIT = res.end();

	CGameObject* pPrevFocusedObj = mpFocusedObj;
	CGameObject* pWP = NULL;
	mpFocusedObj = NULL;
	int collCount = 0;
	std::vector<collisionResult> collizResults;


// ����������� ���� � ��������� �����
	for (it = res.begin(); it != endIT; ++it)
	{
		CGameObject* pObj = (it->movable && !it->movable->getUserAny().isEmpty()) ? Ogre::any_cast<CGameObject*>( it->movable->getUserAny() ) : NULL;

		//if(pObj)
		//	toLogEx("\n mRayQuery->execute %s: %d (%d)", pObj->getName(), pObj->mIsSelectable, _isVisible(pObj));

		if (pObj && pObj->pSNode && pObj->mIsSelectable && _isVisible(pObj))
		{


			//rooms
			// TODO: remove this
#if JENGINE_MOUSEOLDWORK == TRUE
			if (pObj->pSNode && pObj->pSNode->getName().length() > 5)
			{
				if (atoi(pObj->pSNode->getName().c_str() + 3) != CVirtualScript::getSI("cur_room"))
					continue;
			}
#endif

			//toLogEx("\n mRayQuery->execute %s %s: %d (%d)", pObj->getName(),it->movable->getName().c_str(), pObj->mIsSelectable, _isVisible(pObj));
			

			/*if(Ogre::String(pObj->getName()) == "11_06_az_ispy_button_min")
			{
				int k = 0;
			}*/

			//toLogEx("\n mRayQuery->execute or:(%s) dir:(%s)", StringConverter::toString(ray.getOrigin()).c_str(), StringConverter::toString(ray.getDirection()).c_str());

			if (polygonRaycast(ray, pObj, mFocusedPoint))
			{
				mpFocusedObj = pObj;

				//toLogEx("\n mRayQuery->execute %s %f", pObj->getName(),it->distance);
				//toLogEx("\n true raycast %s: %d (%d)", pObj->getName(), pObj->pEntity->getVisibilityFlags(), getViewport()->getVisibilityMask());

				if (mAdvCollision)
				{
					collisionResult result;
					result.mObj = pObj;
					result.mFocus = mFocusedPoint;
					result.mDistance = (ray.getOrigin()-mFocusedPoint).length();
					collizResults.push_back(result);
				}
				else
				{
					//toLogEx("\n  focused %s", pObj->getName());
					break; //just one near object can be focused at the same time
				}
			}
		}
	}

//#define JENGINE_MOUSECIRCLECAST=0

	// ����������� ���������� � ��������� �����
#if JENGINE_MOUSECIRCLECAST == TRUE

	Ogre::Ray rayForward = mCamera->getCameraToViewportRay(0.5f, 0.5f);
	Ogre::Vector3 vecForwardPoint0 = rayForward.getPoint(0.f);
	Ogre::Vector3 vecForwardPoint1 = rayForward.getPoint(1.f);
	Ogre::Vector3 vecForwardDirection = vecForwardPoint1 - vecForwardPoint0;
	Ogre::Vector3 vecObjDirection;

//	mCamera->getF

	if (!mpFocusedObj)
	{
		//toLogEx("\n CC start");
		for (size_t ui = 0; ui < CGameObject::mGameObjList.size(); ui++)
		{
			CGameObject *pObj = CGameObject::mGameObjList[ui];


			if (pObj->mIsSelectable && pObj->pEntity && ((pObj->pEntity->getQueryFlags() & mQueryMask) != 0) && _isVisible(pObj))
			{
				//toLogEx(" CC %s", pObj->pSNode->getName().c_str());

			//rooms
			// copypaste
			// TODO: remove this
#if JENGINE_MOUSEOLDWORK == TRUE
				if (pObj->pSNode && pObj->pSNode->getName().length() > 5)
				{
					if (atoi(pObj->pSNode->getName().c_str() + 3) != CVirtualScript::getSI("cur_room"))
						continue;
				}
#endif

				if (!mCamera->isVisible(pObj->pSNode->convertLocalToWorldPosition(Ogre::Vector3(0.f, 0.f, 0.f)), NULL))
					continue;
/*
				// ���� ����� ������� ����� �� ������ ����, �� �� ����������
				vecObjDirection = vecForwardPoint0 - pObj->pSNode->convertLocalToWorldPosition(Vector3(0.f, 0.f, 0.f));
				vecObjDirection.normalise();
				//vecObjDirection = vecForwardPoint0 - pObj->pSNode->getPosition();

//				if (strcmp(pObj->getName(), "52_az_krovat_npz") == 0)
//					toLogEx("\n 52_az_krovat: %.2f %.2f %.2f / %.2f %.2f %.2f / %.2f %.2f %.2f (%f)", vecForwardPoint0.x, vecForwardPoint0.y, vecForwardPoint0.z, vecForwardPoint1.x, vecForwardPoint1.y, vecForwardPoint1.z,
//							pObj->pSNode->convertLocalToWorldPosition(Vector3(0.f, 0.f, 0.f)).x, pObj->pSNode->convertLocalToWorldPosition(Vector3(0.f, 0.f, 0.f)).y, pObj->pSNode->convertLocalToWorldPosition(Vector3(0.f, 0.f, 0.f)).z,
//							vecForwardDirection.dotProduct(vecObjDirection));

				if (vecForwardDirection.dotProduct(vecObjDirection) >= 0.f)
				{
					continue;
				}
 */


#if IPHONE_CHANGES == TRUE
#				define GAME_MOUSERADIUS 0.05f
#else
#	if IS_JENGINE_PLATFORM_IPHONE
#				define GAME_MOUSERADIUS 0.05f
#	else
#				define GAME_MOUSERADIUS 0.025f
#	endif
#endif


			// TODO: remove this
				// skip wp objects
#if JENGINE_MOUSEOLDWORK == TRUE
				std::map<Ogre::String, Ogre::String>::const_iterator itwc = pObj->getParams().find("walk_to_cursor");
				if (itwc!=pObj->getParams().end() && Ogre::StringConverter::parseInt(itwc->second)!=FALSE)
				{
					if (pWP==NULL) pWP = pObj;
					continue;
				}
#endif


				if (polygonCirclecast(Ogre::Vector2(mx,my), GAME_MOUSERADIUS, pObj))
				{
					mpFocusedObj = pObj;

					mFocusedPoint = pObj->pSNode->getPosition();

					//toLogEx("\n polygonCirclecast: %s", pObj->getName());

					if (mAdvCollision)
					{
						collisionResult result;
						result.mObj = pObj;
						result.mFocus = mFocusedPoint;
						result.mDistance = (ray.getOrigin()-mFocusedPoint).length();
						collizResults.push_back(result);
					}
					else
					{
						//toLogEx("\n  focused %s", pObj->getName());
						break; //just one near object can be focused at the same time
					}
				}
			}
		}
	}
	else
	{
		//toLogEx("\n not circle cast, mpFocusedObj = %s", mpFocusedObj->getName());
	}
#endif


#if JENGINE_MOUSECIRCLECAST == TRUE
	// check click on walkplane mesh if didn't find other object
	if (!mpFocusedObj && pWP)
		if (polygonRaycast(ray, pWP, mFocusedPoint))
			mpFocusedObj = pWP;
#endif

	/// TODO: check how it's work now after wp
	if (mAdvCollision && !collizResults.empty())
	{
		std::sort(collizResults.begin(), collizResults.end());
		mpFocusedObj = collizResults[0].mObj;
		mFocusedPoint = collizResults[0].mFocus;
	}

	if (mpFocusedObj != pPrevFocusedObj)
	{
		if (pPrevFocusedObj)
		{
			pPrevFocusedObj->mIsFocused = false;
			CScriptMsg m;
			m.messageId = CScriptMsg::MSG_KILLFOCUS;
			m.pTargetObj = pPrevFocusedObj;
			postMsg(m);
		}
		if (mpFocusedObj)
		{
			mpFocusedObj->mIsFocused = true;
			CScriptMsg m;
			m.messageId = CScriptMsg::MSG_SETFOCUS;
			m.pTargetObj = mpFocusedObj;
			postMsg(m);
		}
	}
	// update cursor
	if (update_cursor || mpFocusedObj!=pPrevFocusedObj)
	{
		updateCursor();
	}



}

void CMainApplication::setCursorStandard(bool state)
{
	if(mOgreOISListener)
		mOgreOISListener->setCursorStandard(state);
}
void CMainApplication::setCursorUseTextures(bool state)
{
	if(mOgreOISListener)
		mOgreOISListener->setCursorUseTextures(state);
}


void CMainApplication::updateCursor()
{
	if(isShowCursor())
	{
		if(mCursorCurrentId > 0)
		{
			setCursor(mCursorCurrentId);
		}
		else
		{
			int cursor_id = 0;
			if(mpFocusedObj && mpFocusedObj->pScript)
				cursor_id = mpFocusedObj->pScript->getCursor();

			if(cursor_id > 0)
				setCursor(cursor_id);
			else
				setCursorDefault();
		}
	}
	else
		setCursorDefault();
}


void CMainApplication::mouseReleasedAndPressed(bool cursorShow, bool left, bool pressed)
{
	mouseSendMsgToMouseEventReceiver(left, pressed);
	if(cursorShow)
		app.mouseSendMsgToFocusedObj(left, pressed);
}

void CMainApplication::mouseSendMsgToMouseEventReceiver(bool left, bool pressed)
{
	Ogre::Vector2 pos;
	CVirtualScript::mouseGetPosition(pos);

	CScriptMsg m;
	m.param.x = pos.x;
	m.param.y = pos.y;
	m.pSourceObj = mpFocusedObj;
	if (left)
	{
		if (!pressed)
		{
			m.messageId = CScriptMsg::MSG_LBUTTONUP;
		}
		else
		{
			m.messageId = CScriptMsg::MSG_LBUTTONDOWN;
		}
	}
	else
	{
		if (!pressed)
			m.messageId = CScriptMsg::MSG_RBUTTONUP;
		else
			m.messageId = CScriptMsg::MSG_RBUTTONDOWN;
	}

	for(std::vector<CGameObject*>::iterator i = CGameObject::mMouseEventListenerList.begin(); i != CGameObject::mMouseEventListenerList.end(); i++)
	{
		m.pTargetObj = *i;
		postMsg(m);
	}
}

void CMainApplication::mouseSendMsgToFocusedObj(bool left, bool pressed)
{
	if (mObjectFocusDisabled)
		return;

	if (CVirtualScript::getCfgB("disable_cursor_by_script"))
		if (mCursorCurrentId > 0) // �� ������������ �������, ���� ������ ���������� �������
			return;

	if (mpFocusedObj)
	{
		CScriptMsg m;
		Ogre::Vector3 v = mFocusedPoint;
		m.param.x = v.x;
		m.param.y = v.y;
		m.param.z = v.z;
		m.pTargetObj = mpFocusedObj;
		m.pSourceObj = mpFocusedObj;
		if (left)
		{
			if (!pressed)
			{
				m.messageId = CScriptMsg::MSG_LBUTTONUP;
			}
			else
			{
				m.messageId = CScriptMsg::MSG_LBUTTONDOWN;
			}
		}
		else
		{
			if (!pressed)
				m.messageId = CScriptMsg::MSG_RBUTTONUP;
			else
				m.messageId = CScriptMsg::MSG_RBUTTONDOWN;
		}
		postMsg(m);
	}


}

bool CMainApplication::polygonCirclecast(const Ogre::Vector2& point, Ogre::Real radius, CGameObject* pObj) const
{

	Ogre::Entity *ent = pObj->pEntity;

	// mesh data to retrieve
	size_t vertex_count;
	size_t index_count;
	Ogre::Vector3 *vertices;
	unsigned long *indices;

	if (pObj->mSoftwareAnim)
		ent->addSoftwareAnimationRequest(true);

	// get the mesh information
	getMeshInformation(ent, vertex_count, vertices, index_count, indices,
					   ent->getParentNode()->_getDerivedPosition(),
					   ent->getParentNode()->_getDerivedOrientation(),
					   ent->getParentNode()->getScale());

	if (pObj->mSoftwareAnim)
		ent->addSoftwareAnimationRequest(false);

	// test for hitting individual triangles on the mesh
	bool collide = false;
	for (size_t i = 0; i < index_count; i += 3)
	{
#if OGRE_NO_VIEWPORT_ORIENTATIONMODE == 0
		Ogre::OrientationMode orientation = mCamera->getOrientationMode();
#else
		Ogre::OrientationMode orientation = Ogre::OR_PORTRAIT;
#endif
		Ogre::Vector2 v[] = {	MathUtils::getScreenPoint2dFromPoint3d(mCamera->getProjectionMatrix(), mCamera->getViewMatrix(), orientation, vertices[indices[i+0]]),
								MathUtils::getScreenPoint2dFromPoint3d(mCamera->getProjectionMatrix(), mCamera->getViewMatrix(), orientation, vertices[indices[i+1]]),
								MathUtils::getScreenPoint2dFromPoint3d(mCamera->getProjectionMatrix(), mCamera->getViewMatrix(), orientation, vertices[indices[i+2]])
							};
		if (MathUtils::intersectCircle2dTri2d(point, radius, v[0], v[1], v[2]))
		{
			//toLogEx("\n Collided %f %f (%f)   fp: %f %f", point.x, point.y, radius, v[0].x, v[0].y);
			collide = true;
			break;
		}
	}

	// free the verticies and indicies memory
	delete[] vertices;
	delete[] indices;

	return collide;
}

bool CMainApplication::polygonRaycast(Ogre::Ray &ray, CGameObject* pObj, Ogre::Vector3& result) const
{

	Ogre::Entity *ent = pObj->pEntity;
	Ogre::Real closest_distance = -1.0f;
	Ogre::Vector3 closest_result;

	// mesh data to retrieve
	size_t vertex_count;
	size_t index_count;
	Ogre::Vector3 *vertices;
	unsigned long *indices;

	if (pObj->mSoftwareAnim)
		ent->addSoftwareAnimationRequest(true);

	// get the mesh information
	getMeshInformation(ent, vertex_count, vertices, index_count, indices,
					   ent->getParentNode()->_getDerivedPosition(),
					   ent->getParentNode()->_getDerivedOrientation(),
					   ent->getParentNode()->getScale());

	if (pObj->mSoftwareAnim)
		ent->addSoftwareAnimationRequest(false);

	// test for hitting individual triangles on the mesh
	bool collide = false;
	for (int i = 0; i < static_cast<int>(index_count); i += 3)
	{
		// check for a hit against this triangle
		std::pair<bool, Ogre::Real> hit = Ogre::Math::intersects(ray, vertices[indices[i]],
																 vertices[indices[i+1]], vertices[indices[i+2]], true, false);

		// if it was a hit check if its the closest
		if (hit.first)
		{
			if ((closest_distance < 0) ||
				(hit.second < closest_distance))
			{
				// this is the closest so far, save it off
				closest_distance = hit.second;
				collide = true;
			}
		}
	}
	// free the verticies and indicies memory
	delete[] vertices;
	delete[] indices;

	// if we found a new closest raycast for this object, update the
	// closest_result before moving on to the next object.
	if (collide)
	{
		closest_result = ray.getPoint(closest_distance);
		result = closest_result;
		return true;
	}
	else
	{
		return false;
	}
}

void CMainApplication::countIndicesAndVertices(const Ogre::Entity * entity, size_t & index_count, size_t & vertex_count) const
{
	const Ogre::Mesh* mesh = entity->getMesh().getPointer();

	bool added_shared = false;
	index_count  = 0;
	vertex_count = 0;

	// Calculate how many vertices and indices we're going to need
	for ( unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
	{
		const Ogre::SubMesh* submesh = mesh->getSubMesh( i );

		// We only need to add the shared vertices once
		if (submesh->useSharedVertices)
		{
			if ( !added_shared )
			{
				vertex_count += mesh->sharedVertexData->vertexCount;
				added_shared = true;
			}
		}
		else
		{
			vertex_count += submesh->vertexData->vertexCount;
		}

		// Add the indices
		index_count += submesh->indexData->indexCount;
	}
}

void CMainApplication::getMeshInformation(Ogre::Entity* entity,
										  size_t &vertex_count,
										  Ogre::Vector3* &vertices,
										  size_t &index_count,
										  unsigned long* &indices,
										  const Ogre::Vector3 &position,
										  const Ogre::Quaternion& orient,
										  const Ogre::Vector3 &scale) const
{
	countIndicesAndVertices(entity, index_count, vertex_count);

	vertices = new Ogre::Vector3[vertex_count];
	indices = new unsigned long[index_count];

	Ogre::MeshPtr mesh = entity->getMesh();
	bool added_shared = false;
	size_t current_offset = 0;
	size_t shared_offset = 0;
	size_t next_offset = 0;
	size_t index_offset = 0;

	bool useSoftwareBlendingVertices = entity->hasSkeleton();
	if (useSoftwareBlendingVertices)
	{
		entity->_updateAnimation();
	}

	// Run through the submeshes again, adding the data into the arrays
	for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh(i);
		bool useSharedVertices = submesh->useSharedVertices;

		if (vertices)
		{
			//----------------------------------------------------------------
			// GET VERTEXDATA
			//----------------------------------------------------------------
			const Ogre::VertexData * vertex_data;
			if (useSoftwareBlendingVertices)
#ifndef OGRE_DX7
				vertex_data = useSharedVertices ? entity->_getSkelAnimVertexData() : entity->getSubEntity(i)->_getSkelAnimVertexData();
#else
				vertex_data = useSharedVertices ? entity->_getSharedBlendedVertexData() : entity->getSubEntity(i)->_getBlendedVertexData();
#endif
			else
				vertex_data = useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

			if ((!useSharedVertices)||(useSharedVertices && !added_shared))
			{
				if (useSharedVertices)
				{
					added_shared = true;
					shared_offset = current_offset;
				}

				const Ogre::VertexElement* posElem =
					vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

				Ogre::HardwareVertexBufferSharedPtr vbuf =
					vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

				unsigned char* vertex =
					static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

				// There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
				//  as second argument. So make it float, to avoid trouble when Ogre::Real is
				//  comiled/typedefed as double:
				float* pReal;
				for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
				{
					posElem->baseVertexPointerToElement(vertex, &pReal);

					Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);
					vertices[current_offset + j] = (orient * (pt * scale)) + position;
				}
				vbuf->unlock();
				next_offset += vertex_data->vertexCount;
			}
		}

		if (indices)
		{
			//----------------------------------------------------------------
			// GET INDEXDATA
			//----------------------------------------------------------------
			Ogre::IndexData* index_data = submesh->indexData;
			size_t numTris = index_data->indexCount / 3;
			Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;
			bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);
			Ogre::uint32 *pLong = static_cast<Ogre::uint32*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
			Ogre::uint16* pShort = reinterpret_cast<Ogre::uint16*>(pLong);
			size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset;
			if ( use32bitindexes )
			{
				for ( size_t k = 0; k < numTris*3; ++k)
				{
					indices[index_offset++] = pLong[k] + static_cast<int>(offset);
				}
			}
			else
			{
				for ( size_t k = 0; k < numTris*3; ++k)
				{
					indices[index_offset++] = static_cast<int>(pShort[k]) + static_cast<int>(offset);
				}
			}
			ibuf->unlock();
		}
		current_offset = next_offset;
	}
}

unsigned int CMainApplication::getFrameTime() const
{
	return std::min((unsigned)30,mFrameTime);
}

unsigned int CMainApplication::getTime() const
{
	return mTotalTime;
}

CGameObject* CMainApplication::_findObjectByHandle(const OBJECT_HANDLE &h) const
{
	return CGameObject::mGameObjList.findByHandle(h);
}

CHeroObject* CMainApplication::_findHeroByHandle(const OBJECT_HANDLE &h) const
{
	if (h)
	{
		CGameObject* obj=CGameObject::mGameObjList.findByHandle(h);
		if (obj)
		{
			assert(obj->getType()==CGameObject::T_HERO && "finded object by handle, but it wasn't hero, in _findHeroByHandle");
			return static_cast<CHeroObject*>(obj);
		}
	}
	else
		for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
			if (CGameObject::mGameObjList[i]->getType()==CGameObject::T_HERO)
				return static_cast<CHeroObject*>(CGameObject::mGameObjList[i]);
	return NULL;
}

OBJECT_HANDLE CMainApplication::findObjectByName(const Ogre::String& name)
{
	CGameObject* p_obj = _findObjectByName(name);

	if (p_obj)
		return p_obj->getHandle();
	else
		return 0;
}

OBJECT_HANDLE CMainApplication::findHeroByName(const Ogre::String& name)
{
	CGameObject* p_obj = (CGameObject*)_findHeroByName(name);

	if (p_obj)
		return p_obj->getHandle();
	else
		toLogEx("ERROR: Can't find hero [%s] by name!\n", name.c_str());
	return 0;
}

CGameObject* CMainApplication::_findObjectByNode(const Ogre::SceneNode *node)
{
	if (node)
	{
		//CGameObject* pObj = Ogre::any_cast<CGameObject*>( node->getUserObjectBindings().getUserAny() );

		//if(pObj)
		//{
		//	return pObj;
		//}

		for (size_t i=0; i<CGameObject::mGameObjList.size(); i++)
			if (CGameObject::mGameObjList[i]->pSNode == node)
				return CGameObject::mGameObjList[i];
	}
	return NULL;
}

CGameObject* CMainApplication::_findObjectByName(const Ogre::String& name)
{
	if (name.empty())
		return NULL;
	if (mSceneMgr->hasSceneNode(name))
	{
		Ogre::SceneNode *pNode = mSceneMgr->getSceneNode(name);
		return _findObjectByNode(pNode);
	}
	return NULL;
}

CHeroObject* CMainApplication::_findHeroByName(const Ogre::String& name)
{
	if (name.empty())
		return NULL;

	if (mSceneMgr->hasEntity(name))
	{
		Ogre::Entity *ent = mSceneMgr->getEntity(name);
		if (ent)
		{
			CGameObject* pObj = Ogre::any_cast<CGameObject*>( ent->getUserObjectBindings().getUserAny() );
			if (pObj && pObj->getType() == CGameObject::T_HERO)
			{
				return (CHeroObject*)pObj;
			}
		}
	}
	return NULL;
}

void CMainApplication::disableObjectsFocus(bool state)
{
	mObjectFocusDisabled = state;
	mpFocusedObj = NULL;
}

void CMainApplication::setAdvCollisionActive(bool isAdvColl)
{
#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
	mAdvCollision = isAdvColl;
#endif
}

Ogre::RenderSystem* CMainApplication::findRenderSystem()
{
	Ogre::RenderSystem* render = NULL;
	const Ogre::RenderSystemList& rsList = mRoot->getAvailableRenderers();
	if (rsList.size()>0)
	{
		if (mAppState.hasValue("RenderSubsystem")) render = mRoot->getRenderSystemByName(mAppState.getValueText("RenderSubsystem"));
		if (!render) render = rsList.front();
	}
	return render;
}

// virtual WindowEventListener
void CMainApplication::windowFocusChange(Ogre::RenderWindow* rw)
{


#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	WindowRef windowSelf = 0;
	mWindow->getCustomAttribute("WINDOW", &windowSelf);

	WindowRef windowFocus = GetUserFocusWindow();

	toLogEx("\n focuschanged, active=%d focused=%d  isFullScreen=%d mFullScreen=%d", mWindow->isActive(), windowFocus, mWindow->isFullScreen(), mFullScreen);

	if(mWindow->isActive())
	{
		//FIXME: crash here while facebook working
		//applicationReleaseFacebook();
		toLogEx("\n !!!window active!");
	}
	else
	{
		toLogEx("\n !!!window active!");
	}

	appicationWindowActive(mWindow->isActive());
#endif
}

bool CMainApplication::windowClosing(Ogre::RenderWindow* rw)
{
	toLogEx("\n !!!windowClosing");

	app.close();
//	app.deinitialize();
//	app.close();

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	mWindow->setActive(true);
#endif

	return false;
}

void CMainApplication::windowClosed(Ogre::RenderWindow* rw)
{
	//app.close();
	toLogEx("\n !!!windowClosed");
}



MathUtils::TVector2ui CMainApplication::findResolution(bool fFullScreen) const
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

	//RETURN WRONG VALUES WHEN DPI NOT 72
	unsigned int width = GetSystemMetrics(SM_CXSCREEN);
	unsigned int height = GetSystemMetrics(SM_CYSCREEN);

	toLogEx("\nDesktop resolution: %dx%d",width,height);

	if (fFullScreen)
	{
		return MathUtils::TVector2ui(width, height);
	}
	else
	{
		if(width > 2048)
		{
			float aspect = (float)width/(float)height;
			width = 2048;
			height = (unsigned)((float)width/aspect);
		}

		return MathUtils::TVector2ui((unsigned)(width/1.2f),(unsigned)(height/1.2f));



		//for porting purposes
		#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 && JENGINE_PLATFORM

			#if IS_JENGINE_PLATFORM_IPHONE_LOW_RES

					return MathUtils::TVector2ui(480, 320);

			#elif IS_JENGINE_PLATFORM_IPHONE_MID_RES

					return MathUtils::TVector2ui(960, 640);

			#elif IS_JENGINE_PLATFORM_IPAD

					return MathUtils::TVector2ui(1024, 768);

			#endif

		#endif
		//*
		//#if IPHONE_CHANGES == TRUE
		//	resolution.x = 480;
		//	resolution.y = 320;
		//#endif
		//*/

		MathUtils::TVector2ui gameres = getGameResolution();
		if (CVirtualScript::getCfgB("do_not_fit_if_tall_screen"))
		{
			float wdh = (float)width/(float)height;
			if (wdh > 4.f/3.f)
				gameres.y = (unsigned)((float)gameres.y*(4.f/3.f)/wdh);
		}

		RECT rect = {0};
		rect.right = gameres.x;
		rect.bottom = gameres.y;
		AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);
		unsigned int borderheight = rect.bottom-rect.top-gameres.y;
		if (gameres.y+borderheight>height-75)
		{
			unsigned int winheight = height-75-borderheight;
			MathUtils::TVector2ui vecRes(winheight*gameres.x/gameres.y, winheight);
			return vecRes;
		}
		else
		{
			return gameres;
		}
	}

#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE

	unsigned int nScreenWidth = CGDisplayPixelsWide(CGMainDisplayID());
	unsigned int nScreenHeight = CGDisplayPixelsHigh(CGMainDisplayID());

	if (fFullScreen)
	{
		toLogEx("\n findResolution: %d %d", nScreenWidth, nScreenHeight);
//		unsigned int nScreenWidth = 1680;
//		unsigned int nScreenHeight = 1050;

		return MathUtils::TVector2ui(nScreenWidth, nScreenHeight);
	}
	else
	{
		//return MathUtils::TVector2ui(nScreenWidth/1.5f, (nScreenWidth*3/4)/1.5f);
		return MathUtils::TVector2ui(nScreenWidth/1.2f, nScreenHeight/1.2f);
	}

#else

	if (fFullScreen)
	{
		return mMaxPossibleResolution;
	}
	else
	{
		return MathUtils::TVector2ui(mMaxPossibleResolution.x/1.2f, mMaxPossibleResolution.y/1.2f);
	}

	//return mMaxPossibleResolution;

//    MathUtils::TVector2ui vecRes = getGameResolution();
//  toLogEx("\n findResolution: %d %d", vecRes.x, vecRes.y);
//    return vecRes;
//	return getGameResolution();

#endif


}

void CMainApplication::setViewportDimensions(Ogre::Viewport* viewport, bool fFullscreen)
{
	if (viewport)
	{
		Ogre::Real dw=1.f, dh=1.f;
		mWindowActiveResolution = getWindowResolution();

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
		if (fFullscreen && !mFitScreen)
		{
			Ogre::Real waspect = static_cast<Ogre::Real>(getWindowResolution().x) / getWindowResolution().y;
			Ogre::Real gaspect = static_cast<Ogre::Real>(getGameResolution().x) / getGameResolution().y;
//			if (waspect+EPSILONREAL<4/3.f)
			if (waspect + EPSILONREAL < gaspect)
			{
				mWindowActiveResolution.y = (unsigned)(mWindowActiveResolution.x / gaspect);
				dh = getWindowResolution().x/(getWindowResolution().y*gaspect);
			}
//			else if (waspect - EPSILONREAL > 4.f/3.f)
			else if (waspect - EPSILONREAL > gaspect)
			{
				mWindowActiveResolution.x = (unsigned)(mWindowActiveResolution.y * gaspect);
				dw = getWindowResolution().y*gaspect/getWindowResolution().x;
			}
		}
#endif
		toLogEx("\n setViewportDimensions: %f %f %f %f (%d %d)", (1.f - dw)/2.f, (1.f - dh)/2.f, dw, dh, getWindowResolution().x, getWindowResolution().y);

//#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
//		float kx = 1024.0f / getWindowResolution().x;
//		float ky = 768.0f / getWindowResolution().y;

//		viewport->setDimensions((1.f-dw)/2.f * kx , 1 - (1 - (1.f-dh)/2.f)*ky, dw * kx, dh*ky);
///#else
		viewport->setDimensions((1.f-dw)/2.f, (1.f-dh)/2.f, dw, dh);
//#endif
	}
	else
		toLogEx("ERROR: setViewportDimensions Viewport is null\n");
}

void CMainApplication::setCameraAspect(float aspect)
{
	if(mCamera == mDefaultCamera)
		return;

	mCameraAspect = aspect;
	updateCameraAspect();
}
void CMainApplication::setRenderArea(Ogre::RealRect area)
{
	if(mCamera == mDefaultCamera)
		return;

	mRenderArea = area;
	updateRenderArea();
}
void CMainApplication::setCameraFov(Ogre::Real fov)
{
	if(mCamera == mDefaultCamera)
		return;

	if(mCamera->getFOVy() == Ogre::Degree(fov))
		return;

	mFrustum.setNull();

	mCamera->resetFrustumExtents();
	mCamera->setFOVy(Ogre::Degree(fov));
	mCamera->setAspectRatio(mCameraAspect);
	getFrustum();
	updateRenderArea();
}
void CMainApplication::getFrustum()
{
	if(mCamera == mDefaultCamera)
		return;

	if(!mFrustum.isNull())
		return;

	mCamera->getFrustumExtents(mFrustum.left, mFrustum.right, mFrustum.top, mFrustum.bottom);
}
void CMainApplication::updateRenderArea()
{
	if(mCamera == mDefaultCamera)
		return;

	float f1 = MathUtils::interp(mFrustum.left, mFrustum.right, mRenderArea.left);
	float f2 = MathUtils::interp(mFrustum.left, mFrustum.right, mRenderArea.right);
	float f3 = MathUtils::interp(mFrustum.top, mFrustum.bottom, mRenderArea.top);
	float f4 = MathUtils::interp(mFrustum.top, mFrustum.bottom, mRenderArea.bottom);

	mCamera->setFrustumExtents(f1,f2,f3,f4);
}
void CMainApplication::updateCameraAspect()
{
	if(mCamera == mDefaultCamera)
		return;

	mFrustum.setNull();
	mCamera->resetFrustumExtents();

	mCamera->setAspectRatio(mCameraAspect);
	getFrustum();
	updateRenderArea();
}

