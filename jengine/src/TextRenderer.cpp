#include "stdafx.h"
#include "TextRenderer.h"
#include <OgreStableHeaders.h>
#include <OgreOverlayManager.h>
#include <OgreOverlayContainer.h>
#include <OgreFontManager.h>
#include <OgreTechnique.h>
#include <OgreOverlayElement.h>
#include <OgreOverlay.h>

//---------------------------------------------------------------------
template<> TextRenderer* Ogre::Singleton<TextRenderer>::msSingleton = NULL;

//---------------------------------------------------------------------
TextRenderer::TextRenderer()
{
//	toLogEx("\n in TextRenderer constructor");
	
	mOverlayMgr = Ogre::OverlayManager::getSingletonPtr();

	mOverlay = mOverlayMgr->create("TextOverlay");

//	toLogEx("\n  TextRenderer overlay created");

	mPanel = (Ogre::OverlayContainer*)(mOverlayMgr->createOverlayElement("Panel", "TextOverlayContainer"));
	mPanel->setDimensions(1, 1);
	mPanel->setPosition(0, 0);
	mOverlay->setZOrder(650);
	mOverlay->add2D(mPanel);
	mOverlay->show();

//	toLogEx("\n  TextRenderer overlay showed");

	mPixelHeight = 0;
	
	//return;
	
	Ogre::ResourceManager::ResourceMapIterator it = Ogre::FontManager::getSingleton().getResourceIterator();
	
	while(it.hasMoreElements())
	{
		//Ogre::FontPtr f((Ogre::Font*)&(*it.getNext()));
		Ogre::Font *f =(Ogre::Font*)&(*it.getNext());

		
//		toLogEx("\n font getting name");
		mFontName = f->getName();
//		toLogEx("\n loading font %s", f->getName().c_str());
		
		f->load();  // ensure the font is loaded so glyph info is all there

//		toLogEx("\n loaded font %s", f->getName().c_str());
//		toLogEx("\n loaded font true type size %f", f->getTrueTypeSize());
		
		Ogre::Font::CodePointRangeList list = f->getCodePointRangeList();
//		toLogEx("\n code point range list size = %d", list.size());
		Ogre::Font::GlyphInfo gi = f->getGlyphInfo('A');   // use the letter A to get font height, it seems in Ogre all fonts are the same height so it doesn't matter
		float UVheight = gi.uvRect.bottom - gi.uvRect.top;
		std::pair<size_t,size_t> tsize = f->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureDimensions();
		
		float ph = tsize.second*UVheight;
		if (ph < mPixelHeight)
			break;
	
		mPixelHeight = ph;
	}
	
}

TextRenderer::~TextRenderer()
{
}

void TextRenderer::addTextBox(const std::string& ID,
							  const std::string& text,
							  Ogre::Real x, Ogre::Real y,
							  Ogre::Real width, Ogre::Real height,
							  const Ogre::ColourValue& color)
{
    if (mFontName.size() == 0)
        return;
    
	Ogre::OverlayElement* textBox = NULL;
	if (!mOverlayMgr->hasOverlayElement(ID))
	{
		textBox = mOverlayMgr->createOverlayElement("TextArea", ID);
		textBox->setDimensions(width, height);
		textBox->setMetricsMode(Ogre::GMM_PIXELS);
        
        //toLogEx("\n textBoxSetPosition: %f %f", x, y);
		textBox->setPosition(x, y);
		textBox->setWidth(width);
		textBox->setHeight(height);
		textBox->setParameter("font_name", mFontName);
		textBox->setParameter("char_height", Ogre::StringConverter::toString(mPixelHeight));
		mPanel->addChild(textBox);
	}
	else
		textBox = mOverlayMgr->getOverlayElement(ID);
	textBox->setColour(color);
	textBox->setCaption(text);
}

void TextRenderer::removeTextBox(const std::string& ID)
{
	mPanel->removeChild(ID);
	mOverlayMgr->destroyOverlayElement(ID);
}

const Ogre::DisplayString& TextRenderer::getText(const std::string& ID)
{
	Ogre::OverlayElement* textBox = mOverlayMgr->getOverlayElement(ID);
	return textBox->getCaption();
}

void TextRenderer::setText(const std::string& ID, const std::string& Text)
{
	Ogre::OverlayElement* textBox = mOverlayMgr->getOverlayElement(ID);
	if (textBox)
	{
		if (mOverlay && !mOverlay->isVisible()) mOverlay->show();
		textBox->setCaption(Text);
	}
}

void TextRenderer::printf(const std::string& ID,  const char *fmt, /* args*/ ...)
{    
	char        text[256];
	va_list        ap;

	if (fmt == NULL)
		*text=0;

	else {
		va_start(ap, fmt);
		vsprintf(text, fmt, ap);
		va_end(ap);
	}

	Ogre::OverlayElement* textBox = mOverlayMgr->getOverlayElement(ID);
	if (textBox)
	{
		if (mOverlay && !mOverlay->isVisible()) mOverlay->show();
		textBox->setCaption(text);
        
//        toLogEx("\n textBox caption: %s", text);
	}
}
