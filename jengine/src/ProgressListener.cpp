#include "stdafx.h"
#include "ProgressListener.h"



#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	include <mmsystem.h>
#endif
#include "MainApplication.h"

#include "OgreOverlay.h"
#include "OgreOverlayManager.h"
#include "OgrePanelOverlayElement.h"

float CProgressListener::total_width = 0.375f;
float CProgressListener::start_pos = 0.039f;
float CProgressListener::end_pos = 0.9531f;

void CProgressListener::clearProgress(int p, float tw, float sp, float ep)
{
	//toLogEx("\nPROGRESS: %d %f %f %f", p, tw, sp, ep);

	total_width = tw;
	start_pos = sp;
	end_pos = ep;

	mMaxProgress = p;
	mProgress = 0;
	mOldTime = 0;
	setAlpha(0);
}

void CProgressListener::onProgress()
{
	const Ogre::ulong dt = 200;
	float z = (float)mProgress/mMaxProgress;
	if (z>1)	z=1.f;
	
	Ogre::ulong t;
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	t = timeGetTime();
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
	CFAbsoluteTime current_absolute_time =  CFAbsoluteTimeGetCurrent();
	static CFAbsoluteTime start_absolute_time = current_absolute_time;
	t = (current_absolute_time - start_absolute_time) * 1000;
#endif
	
	if (!mOldTime) mOldTime = t;
	
	if (t-mOldTime > dt)
	{
		//toLogEx("\nPROGRESS: value: %f cur: %d max: %d", z, mProgress, mMaxProgress);

		setAlpha(z,total_width,start_pos,end_pos);
		app.setScreenshot(true);
		Ogre::WindowEventUtilities::messagePump(); 
		app.getRoot()->renderOneFrame();
		while(t-mOldTime > dt)
			mOldTime += dt;
	}
	mProgress++;
}

void CProgressListener::finish()
{
	setAlpha(1);
}

void CProgressListener::setAlpha(float value, float tw, float sp, float ep)
{
	
	float sx;
	
	//sx = 0.375;
	sx = total_width;

	float uv1x, uv1y, uv2x, uv2y;
	uv1x = 0.0f;
	uv1y = 0.0f;
	
	//uv2x = 0.9531f*value+0.039f*(1.f-value);

	uv2x = end_pos*value+start_pos*(1.f-value);

	uv2y = 1.0f;
	sx *= uv2x;

	Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName("loadingWindow");
	if (overlay)
	{
		Ogre::PanelOverlayElement* container = (Ogre::PanelOverlayElement*)overlay->getChild("LoadingTimeFrame2");
		if (container)
		{
			Ogre::PanelOverlayElement* inner = (Ogre::PanelOverlayElement*)container->getChild("LoadingTimeLine");
			if (inner)
			{
				inner->setWidth(sx);
				inner->setUV(uv1x, uv1y, uv2x, uv2y);
			}
		}
	}
}


