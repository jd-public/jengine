#include <jni.h>
#include <android/api-level.h>
#include <android/native_window_jni.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/input.h>
#include "Ogre.h"
#include "OgreRenderWindow.h"
#include "OgreStringConverter.h"
#include <EGL/egl.h>
#include "Android/OgreAndroidEGLWindow.h"
#include "stdafx.cpp"
#include "MainApplication.h"
#include "VirtualScript.h"
#include "JNIStringProxy.h"
#include "AppOISListener.h"

#include <android/log.h>
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "OGRE", __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "OGRE", __VA_ARGS__))

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* gVM = NULL;
static bool gInit = false;
static bool gAndroidConfigureDone = false;
static Ogre::RenderWindow* gRenderWnd = NULL;

class CJAndroidApp
{
protected:
	bool mIsZoom;
	bool mIsMove;
	Ogre::Vector2 mTouchPos1, mTouchPos1Prev;
	Ogre::Vector2 mTouchPos2, mTouchPos2Prev;
	int mTouchCount;

public:
	CJAndroidApp():
		mIsZoom(false),
		mIsMove(false),
		mTouchPos1(0.f, 0.f),
		mTouchPos1Prev(0.f, 0.f),
		mTouchPos2(0.f, 0.f),
		mTouchPos2Prev(0.f, 0.f),
		mTouchCount(0)
	{};

	void BeginZoom()
	{
		//toLogEx("\n app begin zoom");
		mIsZoom = true;
	}
	void EndZoom()
	{
		//toLogEx("\n app end zoom");
		mIsZoom = false;
	}
	bool IsZoom()
	{
		return mIsZoom;
	}
	void BeginMove()
	{
		toLogEx("\n app begin move");
		mIsMove = true;
		CVirtualScript::setSB("iscCamIsMove", true);
	}
	void EndMove()
	{
		toLogEx("\n app end move");
		mIsMove = false;
		CVirtualScript::setSB("iscCamIsMove", false);
	}
	bool IsMove()
	{
		return mIsMove;
	}

	// @nTouchCount: 0, 1 or 2
	void TouchProgress(int nTouchCount, const Ogre::Vector2 &vecTouchPos1 = Ogre::Vector2(0.f, 0.f),
					const Ogre::Vector2 &vecTouchPos2 = Ogre::Vector2(0.f, 0.f))
	{
		int nTouchCountPrev = mTouchCount;
		mTouchCount = nTouchCount;

		static Ogre::Vector2 vecTouch1Began(0.f, 0.f);
		if (nTouchCount == 1 && nTouchCountPrev != 1)
			vecTouch1Began = vecTouchPos1;

		bool bZoom = IsZoom();


		if (nTouchCount != 2 && bZoom)
		{
			EndZoom();
			bZoom = false;
		}
		else
		if (nTouchCount == 2 && nTouchCountPrev != 2)
		{
			BeginZoom();
		}
		else
		if (nTouchCount == 1 && nTouchCountPrev == 1)
		{
			if (!IsMove())
			{
				if (vecTouch1Began.squaredDistance(vecTouchPos1) > 0.0004f)
					BeginMove();
			}
			else
			{
				int iX = (mTouchPos1.x - mTouchPos1Prev.x)*((float)gRenderWnd->getWidth())*300.f;
				int iY = -(mTouchPos1.y - mTouchPos1Prev.y)*((float)gRenderWnd->getHeight())*300.f;

				CVirtualScript::setSI("cameraTouchMoveX", iX);
				CVirtualScript::setSI("cameraTouchMoveY", iY);

				CMsg msg;
#if JENGINE_MOUSEOLDWORK
				msg.lparam = 32;
				msg.rparam = 0;
				CVirtualScript::sendCMsg("00_game_interface", msg);
#endif
			
				msg.lparam = 4;
				msg.rparam = 0;
				CVirtualScript::sendCMsg("00_cam_ext", msg);
			}
		}
		else
		if (IsMove())
		{
			EndMove();
		}

		if (nTouchCount > 0)
		{
			//toLogEx("\n Touch count > 0 (%d)", nTouchCount);

			mTouchPos1Prev = mTouchPos1;
			mTouchPos1 = vecTouchPos1;
		}

		if (nTouchCount == 2)
		{
			mTouchPos2Prev = mTouchPos2;
			mTouchPos2 = vecTouchPos2;

			if (bZoom)
			{
				Ogre::Vector2 vecSub = mTouchPos2 - mTouchPos1;
				float fDistance = sqrtf(vecSub.x*vecSub.x + vecSub.y*vecSub.y);
				vecSub = mTouchPos2Prev - mTouchPos1Prev;
				float fDistancePrev = sqrtf(vecSub.x*vecSub.x + vecSub.y*vecSub.y);

				float fDelta = fDistancePrev - fDistance;

				int nZoomParam = fDelta*100000.f;
				CVirtualScript::setSI("cameraTouchZoom", nZoomParam);

				CMsg msg;
#if JENGINE_MOUSEOLDWORK
				msg.lparam = 31;
				msg.rparam = 0;
				CVirtualScript::sendCMsg("00_game_interface", msg);
				
				
				//toLogEx("\n app send msg for zoom %d", nZoomParam);
#endif

				msg.lparam = 5;
				msg.rparam = 0;
				CVirtualScript::sendCMsg("00_cam_ext", msg);

				app.setMousePinch(nZoomParam + app.getMousePinch());
			}
		}
	}
};

CJAndroidApp gAndroidApp;

//----------------------------------------------------------------------
void setupScripts()
{
	String lang = CVirtualScript::getCfgS("language");
	toLogEx("Using language: %s\n", lang.c_str());

	if (!((lang == "jp") || (lang == "ko") || (lang == "ch")))
		CVirtualScript::addResourcePath("texts/euro", "Start");
	else
		CVirtualScript::addResourcePath("texts/" + lang, "Start");

	 CVirtualScript::setCfgValue<String>("platform","android");
}
JENGINE_APPLICATION(JGAME_COMPANY_NAME, JGAME_APP_NAME, JGAME_SHORT_APP_NAME, setupScripts);
//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_JEngineAndroidBridge_initEngine
  (JNIEnv * env, jclass obj, jobject act, jobject assetManager, jstring internalDataPath)
{
	if(gInit)
		return;
	gInit = true;

	android_main_jetdogs();

	AAssetManager* assetMgr = AAssetManager_fromJava(env, assetManager);
	JNIStringProxy strInternalDataPath(env, internalDataPath);
	
	env->GetJavaVM(&gVM);
	jobject gAct = env->NewGlobalRef(act);	

	jclass actClass = env->GetObjectClass(gAct);	
	jmethodID getOBBPath = env->GetMethodID(actClass, "getOBBPath", "()Ljava/lang/String;");	
	jstring obbPath = (jstring)env->CallObjectMethod(gAct, getOBBPath);
	JNIStringProxy strobbPath(env, obbPath);
	LOGI("++++++++++++++++++++++++++++++++++++++++++++: %s\n", strobbPath.toCString());
	
	app.setOBBPath(strobbPath.toCString());
	app.setupAndroid(gVM, env, gAct, assetMgr, strInternalDataPath.toCString());
}
//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_JEngineAndroidBridge_deInitEngine
  (JNIEnv * env, jclass obj)
{

}
//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_JEngineAndroidBridge_initRenderWindow
  (JNIEnv * env, jclass obj, jobject surface)
{
	ANativeWindow* nativeWnd = ANativeWindow_fromSurface(env, surface);
	if(nativeWnd != NULL)
	{		
		if(!gAndroidConfigureDone)
		{
			gAndroidConfigureDone = true;
			app.configureAndroid(Ogre::StringConverter::toString((int)nativeWnd));
			gRenderWnd = app.getWindow();
		}
		else
		{
			static_cast<Ogre::AndroidEGLWindow*>(gRenderWnd)->_createInternalResources(nativeWnd, NULL);
			app.enterForeground();
			
			if (CVirtualScript::getCfgB("msg_to_gi_on_home_btn"))
			{
				CMsg msgBecomeActive;
				msgBecomeActive.lparam = 20011;
				msgBecomeActive.rparam = 0;
				CVirtualScript::sendCMsgNow("interface", msgBecomeActive);
			}
		}
		Ogre::Root::getSingletonPtr()->clearEventTimes();
	}
}
//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_JEngineAndroidBridge_termRenderWindow
  (JNIEnv * env, jclass obj)
{
	if(gRenderWnd)
	{
		if (CVirtualScript::getCfgB("msg_to_gi_on_home_btn"))
		{
			CMsg msgSaveGame;
			msgSaveGame.lparam = 29;
			msgSaveGame.rparam = 0;
			CVirtualScript::sendCMsgNow("00_game_interface", msgSaveGame);

			CMsg msgResignActive;
			msgResignActive.lparam = 20010;
			msgResignActive.rparam = 0;
			CVirtualScript::sendCMsgNow("interface", msgResignActive);
		}
		app.enterBackground();
		static_cast<Ogre::AndroidEGLWindow*>(gRenderWnd)->_destroyInternalResources();
	}
}
//----------------------------------------------------------------------
static bool shiftPressed = false;
static std::string convertKeyCodeToString(int keyCode)
{
	switch(keyCode)
	{
	case 62:
	 return " ";
	case 29:
	 return shiftPressed ? "A" : "a";
	case 30:
	 return shiftPressed ? "B" : "b";	
	case 31:
	 return shiftPressed ? "C" : "c";	
	case 32:
	 return shiftPressed ? "D" : "d";	
	case 33:
	 return shiftPressed ? "E" : "e";	
	case 34:
	 return shiftPressed ? "F" : "f";	
	case 35:
	 return shiftPressed ? "G" : "g";	
	case 36:
	 return shiftPressed ? "H" : "h";
	case 37:
	 return shiftPressed ? "I" : "i";	
	case 38:
	 return shiftPressed ? "J" : "j";	
	case 39:
	 return shiftPressed ? "K" : "k";	
	case 40:
	 return shiftPressed ? "L" : "l";	
	case 41:
	 return shiftPressed ? "M" : "m";	
	case 42:
	 return shiftPressed ? "N" : "n";		
	case 43:
	 return shiftPressed ? "O" : "o";	
	case 44:
	 return shiftPressed ? "P" : "p";		
	case 45:
	 return shiftPressed ? "Q" : "q";		
	case 46:
	 return shiftPressed ? "R" : "r";	
	case 47:
	 return shiftPressed ? "S" : "s";	
	case 48:
	 return shiftPressed ? "T" : "t";	
	case 49:
	 return shiftPressed ? "U" : "u";	
	case 50:
	 return shiftPressed ? "V" : "v";	
	case 51:
	 return shiftPressed ? "W" : "w";	
	case 52:
	 return shiftPressed ? "X" : "x";
	case 53:
	 return shiftPressed ? "Y" : "y";	
	case 54:
	 return shiftPressed ? "Z" : "z";
	}
	return "";
}
//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_JEngineAndroidBridge_renderOneFrame
  (JNIEnv * env, jclass obj)
{
	if(gRenderWnd != NULL)
    {
		gRenderWnd->windowMovedOrResized();
		Ogre::Root::getSingletonPtr()->renderOneFrame();
    }
}
//----------------------------------------------------------------------
JNIEXPORT jboolean JNICALL Java_com_jetdogs_SinisterCity_JEngineAndroidBridge_onKeyDown(
	JNIEnv * env, jclass obj, int keyCode, jobject keyEvent)
{
	jclass keyClass = env->GetObjectClass(keyEvent);	
	jmethodID getAction = env->GetMethodID(keyClass, "getAction", "()I");	
	int act = env->CallIntMethod(keyEvent, getAction);	
	
	if(keyCode == AKEYCODE_BACK)
	{
		return JNI_TRUE;
	}
	else if(keyCode == 67)
	{
		std::string curTxt = app.getKeyboardText();
		if(curTxt.length() > 0)
		{
			app.setKeyboardText(curTxt.length() == 1 ? "" : curTxt.substr(0, curTxt.length() - 1));
		}
		return 1;
	}
	else if(keyCode == 66)
	{
		std::string curTxt = app.getKeyboardText();
		if(curTxt.length() > 0)
		{
			app.showKeyboard(false);
			CVirtualScript::sendCMsg("interface", 20003, 0);
		}
		return 1;
	}
	else if(keyCode == 59)
	{
		shiftPressed = true;
	}
	else
	{
		std::string curTxt = app.getKeyboardText();
		std::string curChar = convertKeyCodeToString(keyCode);
		if(curChar.length() > 0)
		{
			app.setKeyboardText(curTxt + curChar);
			shiftPressed = false;
			return 1;
		}
	}
			
	return JNI_FALSE;
}
//----------------------------------------------------------------------
JNIEXPORT jboolean JNICALL Java_com_jetdogs_SinisterCity_JEngineAndroidBridge_onKeyUp(
	JNIEnv * env, jclass obj, int keyCode, jobject keyEvent)
{
	return JNI_FALSE;
}
//----------------------------------------------------------------------
JNIEXPORT jboolean JNICALL Java_com_jetdogs_SinisterCity_JEngineAndroidBridge_onTouchEvent(
	JNIEnv * env, jclass obj, jobject motionEvent)
{
	if(!gInit)
		return JNI_FALSE;
		
	jclass motionClass = env->GetObjectClass(motionEvent);	
	jmethodID getPointerCount = env->GetMethodID(motionClass, "getPointerCount", "()I");
	jmethodID getX = env->GetMethodID(motionClass, "getX", "()F");
	jmethodID getY = env->GetMethodID(motionClass, "getY", "()F");
	jmethodID getAction = env->GetMethodID(motionClass, "getAction", "()I");
	
	int pointerCount = env->CallIntMethod(motionEvent, getPointerCount);
	float rawX = env->CallFloatMethod(motionEvent, getX);
	float rawY = env->CallFloatMethod(motionEvent, getY);
	int act = env->CallIntMethod(motionEvent, getAction);
	
	Ogre::Vector2 vecTouch1(0.f, 0.f), vecTouch2(0.f, 0.f);	
	
	int action = (int)(AMOTION_EVENT_ACTION_MASK & act);      
	if (pointerCount > 0)
	{
		vecTouch1.x = rawX / (float)gRenderWnd->getWidth();
		vecTouch1.y = rawY / (float)gRenderWnd->getHeight();
	}	
	if (pointerCount > 1)
	{
		jmethodID getXN = env->GetMethodID(motionClass, "getX", "(I)F");
		jmethodID getYN = env->GetMethodID(motionClass, "getY", "(I)F");
		float rawX2 = env->CallFloatMethod(motionEvent, getXN, 1);
		float rawY2 = env->CallFloatMethod(motionEvent, getYN, 1);
	
		vecTouch2.x = rawX2 / (float)gRenderWnd->getWidth();
		vecTouch2.y = rawY2 / (float)gRenderWnd->getHeight();
	}
			
	switch(action)
	{
		case 0:
			app.getOIS()->_appMouseMoved(vecTouch1.x, vecTouch1.y, 0, true, false);
			app.mouseReleasedAndPressed(true, true, true);

			//toLogEx("\n android mouse down, count = %d, x = %f, y = %f", pointerCount, vecTouch1.x, vecTouch1.y);
			break;
		case 1:
			app.getOIS()->_appMouseMoved(vecTouch1.x, vecTouch1.y, 0, false, false);
			app.mouseReleasedAndPressed(true, true, false);

			//toLogEx("\n android mouse up, count = %d, x = %f, y = %f", pointerCount, vecTouch1.x, vecTouch1.y);
			break;
		case 2:
		case 10:
			app.getOIS()->_appMouseMoved(vecTouch1.x, vecTouch1.y, 0, true, false);

			//toLogEx("\n android mouse moved, count = %d, x = %f, y = %f", pointerCount, vecTouch1.x, vecTouch1.y);
			break;
		default:
			break;
	}
	
	if (app.isMouseLButton())
		gAndroidApp.TouchProgress(pointerCount >= 2 ? 2 : pointerCount, vecTouch1, vecTouch2);
	else
		gAndroidApp.TouchProgress(0);
				
	return JNI_TRUE;
}
//----------------------------------------------------------------------
#ifdef __cplusplus
}
#endif
