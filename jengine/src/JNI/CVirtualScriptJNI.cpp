#include <jni.h>
#include "VirtualScript.h"
#include "JNIStringProxy.h"

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_setSS
  (JNIEnv * env, jclass obj, jstring name, jstring value)
{
	JNIStringProxy strName(env, name);
	JNIStringProxy strValue(env, value);
	CVirtualScript::setSS(strName.toCString(), strValue.toCString());
}

//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_setSB
  (JNIEnv * env, jclass obj, jstring name, jboolean value)
{
	JNIStringProxy strName(env, name);
	CVirtualScript::setSB(strName.toCString(), (bool)(value == JNI_TRUE));
}

//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_setSI
  (JNIEnv * env, jclass obj, jstring name, jint value)
{
	JNIStringProxy strName(env, name);
	CVirtualScript::setSI(strName.toCString(), (int)value);
}

//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_setSF
  (JNIEnv * env, jclass obj, jstring name, jfloat value)
{
	JNIStringProxy strName(env, name);
	CVirtualScript::setSF(strName.toCString(), (float)value);
}

//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_setII
  (JNIEnv * env, jclass obj, jint idx, jint value)
{
	CVirtualScript::setII((int)idx, (int)value);
}

//----------------------------------------------------------------------
JNIEXPORT jboolean JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_getCfgB
  (JNIEnv * env, jclass obj, jstring key)
{
	JNIStringProxy strKey(env, key);
	return CVirtualScript::getCfgB(strKey.toStdString()) ? JNI_TRUE : JNI_FALSE;
}

//----------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_getCfgI
  (JNIEnv * env, jclass obj, jstring key)
{
	JNIStringProxy strKey(env, key);
	return CVirtualScript::getCfgI(strKey.toStdString());
}

//----------------------------------------------------------------------
JNIEXPORT jfloat JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_getCfgF
  (JNIEnv * env, jclass obj, jstring key)
{
	JNIStringProxy strKey(env, key);
	float result;
	CVirtualScript::getCfgF(strKey.toStdString(), result);
	return result;
}

//----------------------------------------------------------------------
JNIEXPORT jstring JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_getCfgS
  (JNIEnv * env, jclass obj, jstring key)
{
	JNIStringProxy strKey(env, key);
	String result = CVirtualScript::getCfgS(strKey.toStdString());
	return env->NewStringUTF(result.c_str());
}
//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_sendCMsg
  (JNIEnv * env, jclass obj, jstring name, jint lparam, jint objHandle)
{
	JNIStringProxy strName(env, name);
	CVirtualScript::sendCMsg(strName.toStdString(), lparam, objHandle);
}
//----------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_jetdogs_SinisterCity_CVirtualScriptJNI_sendCMsgNow
  (JNIEnv * env, jclass obj, jstring name, jint lparam, jint objHandle)
{
	JNIStringProxy strName(env, name);
	CVirtualScript::sendCMsgNow(strName.toStdString(), lparam, objHandle);
}
//----------------------------------------------------------------------
#ifdef __cplusplus
}
#endif
