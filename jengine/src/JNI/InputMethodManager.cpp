#include "InputMethodManager.h"

//-----------------------------------------------------------------------
InputMethodManager::InputMethodManager(JNIEnv* env, jobject inputMethodManager)
	: mEnv(env)
{
	mIMM = mEnv->NewGlobalRef(inputMethodManager);	
	mIMMClazz = (jclass)mEnv->NewGlobalRef(mEnv->FindClass("android/view/inputmethod/InputMethodManager"));
	mViewClazz = (jclass)mEnv->NewGlobalRef(mEnv->FindClass("android/view/View"));
	
	mGetWindowToken = mEnv->GetMethodID(mViewClazz, "getWindowToken", "()Landroid/os/IBinder;");
	mShowSoftInput = mEnv->GetMethodID(mIMMClazz, "showSoftInput", "(Landroid/view/View;I)Z");
	mHideSoftInput = mEnv->GetMethodID(mIMMClazz, "hideSoftInputFromWindow", "(Landroid/os/IBinder;I)Z");
}
//-----------------------------------------------------------------------
InputMethodManager::~InputMethodManager()
{

}
//-----------------------------------------------------------------------
void InputMethodManager::showSoftKeyboard(jobject view)
{	
	mEnv->CallBooleanMethod(mIMM, mShowSoftInput, view, 0);
}
//-----------------------------------------------------------------------
void InputMethodManager::hideSoftKeyboard(jobject view)
{
	jobject wndToken = mEnv->CallObjectMethod(view, mGetWindowToken);
	mEnv->CallBooleanMethod(mIMM, mHideSoftInput, wndToken, 0);
}
//-----------------------------------------------------------------------