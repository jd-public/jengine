
#include "stdafx.h"

//define USE_BULLET in project settings to enable Bullet physics
#if USE_BULLET == TRUE

	#include "PhysObject.h"

	#include "MainApplication.h"
	 
	#include "OgreAnimation.h"
	#include "CMsg.h"

	void CPhysObject::init()
	{
		setType(T_PHYS_OBJ);
	}

	CPhysObject::CPhysObject() : CGameObject()
	{
		init();
	}

	CPhysObject::CPhysObject(String& scrName) : CGameObject(scrName)
	{	
		init();
	}

	CPhysObject::~CPhysObject()
	{

	}

	void CPhysObject::processFrame( const Ogre::FrameEvent& evt )
	{
		UpdateBodies();
	}

#endif