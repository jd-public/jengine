#include "stdafx.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_LINUX

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	include <conio.h>
#	define _CRTDBG_MAP_ALLOC
#	include <crtdbg.h>
#endif
#include "MainApplication.h"

#include "EventPlayer.h"
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
//#	include "resource.h"
#endif

GAME_API int StartApplication(const char* lpCmdLine)
{
	int i = 0;
	setlocale(LC_CTYPE, "");
//*********************************************
//*******FOR DEMO-VERSION ONLY*****************
//**FIXME-FIXME-FIXME-FIXME-FIXME-FIXME-FIXME**

/*	SYSTEMTIME stt;
	GetLocalTime(&stt);

	if (stt.wYear != 2014)
		return 0;

	if (stt.wMonth != 12)
		return 0;

	if (stt.wDay > 25)
		return 0;
*/

//*******FOR DEMO-VERSION ONLY*****************
//*********************************************

#if OGRE_PLATFORM==OGRE_PLATFORM_WIN32
	HWND wnd;
	if ((wnd = FindWindow("OgreD3D9Wnd", app.getAppFullName().c_str())) != NULL)
	{
		ShowWindow(wnd,SW_SHOWNORMAL);
		return 0;
	}

//	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
	
	app.processCommandLine(lpCmdLine);


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 

	#ifdef _DEBUG
		app.enableLog();
	#endif

	if (app.logEnabled())
		AllocConsole();
#endif
	
	try 
	{
		app.go();
	} 
	catch( Ogre::Exception& e )
	{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 
			MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
			toLogEx("\nEXCEPTION: %s", e.getFullDescription().c_str());
#endif
		
	}

	app.deinitialize();

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 
	if (app.logEnabled())
		FreeConsole();
//	_CrtDumpMemoryLeaks();
#endif
	return 0;
}

#endif