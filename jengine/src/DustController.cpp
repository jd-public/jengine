#include "stdafx.h"


#include "DustController.h"

#include "OgreStableHeaders.h"
#include "OgreHardwareBufferManager.h"
#include "OgreControllerManager.h"


#define DYNAMIC_BUFFER 0
#define STATIC_BUFFER 1


namespace Ogre{

//-------------DustControllerUpdateValue--------------------------------
class DustControllerUpdateValue : public ControllerValue<Ogre::Real>
{
protected:
	DustController* mTarget;
public:
	DustControllerUpdateValue(DustController* target) : mTarget(target) 
	{}

	Ogre::Real getValue(void) const
	{
		return 0; 
	}

	void setValue(Ogre::Real value)
	{ 
		mTarget->update(value); 
	}

}; 

//-----------------ParamIndexer--------------------
size_t DustController::ParamIndexer::getIndex(size_t vertex_offset) const
{
	return vertex_offset * (*mSize) + mOffset;
}



//----------------VertexCounter--------------------
size_t DustController::VertexCounter::getVertexIndex()
{
	size_t rez = mCount;
	mCount++;
	return rez;
}
size_t DustController::VertexCounter::getCount()
{
	return mCount;
}



//-----------------Triangle----------------------
void DustController::Triangle::getIndex(VertexCounter &counter)
{
	for(int i = 0; i < 3; i++)
		mVertexIndex[i] = counter.getVertexIndex();
}
void DustController::Triangle::setParam(float *pos_buf, const ParamIndexer& indexer, const Ogre::Vector3 &v1, const Ogre::Vector3 &v2, const Ogre::Vector3 &v3)
{
	setVector3(pos_buf, indexer.getIndex(mVertexIndex[0]), v1);
	setVector3(pos_buf, indexer.getIndex(mVertexIndex[1]), v2);
	setVector3(pos_buf, indexer.getIndex(mVertexIndex[2]), v3);
}
void DustController::Triangle::setParam(float *pos_buf, const ParamIndexer& indexer, const Ogre::Vector2 &v1, const Ogre::Vector2 &v2, const Ogre::Vector2 &v3)
{
	setVector2(pos_buf, indexer.getIndex(mVertexIndex[0]), v1);
	setVector2(pos_buf, indexer.getIndex(mVertexIndex[1]), v2);
	setVector2(pos_buf, indexer.getIndex(mVertexIndex[2]), v3);
}
void DustController::Triangle::setVector3(float *pos_buf, int i, const Ogre::Vector3 &pos)
{
	pos_buf[i + 0] = pos.x;
	pos_buf[i + 1] = pos.y;
	pos_buf[i + 2] = pos.z;
}
void DustController::Triangle::setVector2(float *pos_buf, int i, const Ogre::Vector2 &pos)
{
	pos_buf[i + 0] = pos.x;
	pos_buf[i + 1] = pos.y;
}



//-----------------DustParticle------------------------
void DustController::DustParticle::initialise(VertexCounter& counter, float size_x, float size_y)
{
	mPosition = Ogre::Vector2(Ogre::Math::RangeRandom(0, 10)*0.1f, Ogre::Math::RangeRandom(0, 10)*0.1f);
	mSizeX = size_x;
	mSizeY = size_y;
	Ogre::Real angle = Ogre::Math::RangeRandom(0, 360);
	mVelocity = 0.015f * Ogre::Vector2(Ogre::Math::Cos(Degree(angle)), Ogre::Math::Sin(Degree(angle)));
	mPhase = 0;
	mPhaseSpeed = Ogre::Math::RangeRandom(2, 4) * 0.3f;

	for(int i = 0; i < TRIANGLE_COUNT; i++)
	{
		mTriangle[i].getIndex(counter);
	}
}

void DustController::DustParticle::update(Ogre::Real ellapseTime)
{
	mPosition += mVelocity * ellapseTime;
	mPhase += mPhaseSpeed * ellapseTime;

	if(mPosition.x > 1)
		mPosition.x -= 1;
	if(mPosition.y > 1)
		mPosition.y -= 1;

	if(mPosition.x < 0)
		mPosition.x += 1;
	if(mPosition.y < 0)
		mPosition.y += 1;
}


Ogre::Vector2 DustController::DustParticle::getCorner(int hor, int vert)
{
	Ogre::Real f = Math::Sin(mPhase);
	Ogre::Vector2 shift = f * Ogre::Vector2(-mVelocity.y, mVelocity.x);
	return mPosition + Ogre::Vector2(mSizeX*hor, mSizeY*vert) + shift;
}
Ogre::Vector3 DustController::DustParticle::convertPos(Ogre::Vector2 pos)
{
	return Ogre::Vector3(-1 + 2*pos.x, +1 - 2*pos.y, -1);
}


void DustController::DustParticle::updatePosition(float *pos_buf, const ParamIndexer& indexer)
{
	Ogre::Vector3 leftTop		= convertPos(getCorner(-1, -1));
	Ogre::Vector3 rightTop		= convertPos(getCorner(+1, -1));
	Ogre::Vector3 leftBottom	= convertPos(getCorner(-1, +1));
	Ogre::Vector3 rightBottom	= convertPos(getCorner(+1, +1));

	mTriangle[0].setParam(pos_buf, indexer, leftTop, rightTop, leftBottom);
	mTriangle[1].setParam(pos_buf, indexer, rightBottom, rightTop, leftBottom);
}

void DustController::DustParticle::updateTCoord1(float *pos_buf, const ParamIndexer& indexer)
{
	Ogre::Vector2 leftTop		= Ogre::Vector2(0, 0);
	Ogre::Vector2 rightTop		= Ogre::Vector2(1, 0);
	Ogre::Vector2 leftBottom	= Ogre::Vector2(0, 1);
	Ogre::Vector2 rightBottom	= Ogre::Vector2(1, 1);

	mTriangle[0].setParam(pos_buf, indexer, leftTop, rightTop, leftBottom);
	mTriangle[1].setParam(pos_buf, indexer, rightBottom, rightTop, leftBottom);
}

void DustController::DustParticle::updateTCoord2(float *pos_buf, const ParamIndexer& indexer)
{
	Ogre::Vector2 leftTop		= getCorner(-1, -1);
	Ogre::Vector2 rightTop		= getCorner(+1, -1);
	Ogre::Vector2 leftBottom	= getCorner(-1, +1);
	Ogre::Vector2 rightBottom	= getCorner(+1, +1);

	mTriangle[0].setParam(pos_buf, indexer, leftTop, rightTop, leftBottom);
	mTriangle[1].setParam(pos_buf, indexer, rightBottom, rightTop, leftBottom);
}


//------------------------Buffer---------------------------------
DustController::ParamIndexer DustController::Buffer::addElement(const RenderOperation &renderOp, VertexElementType type, VertexElementSemantic semantic, unsigned short tcoord)
{
	size_t cur_size = renderOp.vertexData->vertexDeclaration->getVertexSize(mId);
	renderOp.vertexData->vertexDeclaration->addElement(mId, cur_size, type, semantic, tcoord);
	mSize = renderOp.vertexData->vertexDeclaration->getVertexSize(mId) / VertexElement::getTypeSize(VET_FLOAT1);
	return ParamIndexer(cur_size / VertexElement::getTypeSize(VET_FLOAT1), &mSize);
}

void DustController::Buffer::create(const RenderOperation &renderOp, HardwareBuffer::Usage usage)
{
	mBuffer = HardwareBufferManager::getSingleton().createVertexBuffer(
        mSize * VertexElement::getTypeSize(VET_FLOAT1),
        renderOp.vertexData->vertexCount,
		usage);
	renderOp.vertexData->vertexBufferBinding->setBinding(mId, mBuffer);
}
float* DustController::Buffer::lock()
{
	return static_cast<float*>(mBuffer->lock(HardwareBuffer::HBL_DISCARD));
}
void DustController::Buffer::unlock()
{
	mBuffer->unlock();
}



//-------------------------DustController-------------------------
DustController::DustController(int count, Ogre::Real width, Ogre::Real height): 
	mParticleCount(count), 
	mWidth(width),
	mHeight(height),
	mTimeController(0), 
	mDynamicBuffer(DYNAMIC_BUFFER), 
	mStaticBuffer(STATIC_BUFFER)
{
	initialise();
	updateConstBuffer();
	updateDynamicBuffer();
}
DustController::~DustController()
{
	if(mTimeController)
    {
        ControllerManager::getSingleton().destroyController(mTimeController);
        mTimeController = 0;
    }
}
void DustController::initialise()
{
	for(int i = 0; i < mParticleCount; i++)
	{
		DustParticle particle;
		particle.initialise(mVertexCounter, mWidth, mHeight);
		mParticle.push_back(particle);
//		mParticle[i].initialise();
	}


    mUseIdentityProjection = true;
    mUseIdentityView = true;

	mRenderOp.vertexData = new VertexData();

    mRenderOp.indexData = 0;
    mRenderOp.useIndexes = false; 
	mRenderOp.vertexData->vertexCount = mVertexCounter.getCount(); 
    mRenderOp.vertexData->vertexStart = 0; 
	mRenderOp.operationType = RenderOperation::OT_TRIANGLE_LIST; 

	mPositionIndexer = mDynamicBuffer.addElement(mRenderOp, VET_FLOAT3, VES_POSITION);
	mTCoord2Indexer = mDynamicBuffer.addElement(mRenderOp, VET_FLOAT2, VES_TEXTURE_COORDINATES, 1);
	mDynamicBuffer.create(mRenderOp, HardwareBuffer::HBU_STATIC_WRITE_ONLY);

	mTCoord1Indexer = mStaticBuffer.addElement(mRenderOp, VET_FLOAT2, VES_TEXTURE_COORDINATES, 0);
	mStaticBuffer.create(mRenderOp, HardwareBuffer::HBU_STATIC);


    ControllerValueRealPtr updValue(new DustControllerUpdateValue(this));
    mTimeController = ControllerManager::getSingleton().createFrameTimePassthroughController(updValue);

}
void DustController::update(Ogre::Real ellapseTime)
{
	for(std::vector<DustParticle>::iterator i = mParticle.begin(); i != mParticle.end(); i++)
	{
		i->update(ellapseTime);
	}
	updateDynamicBuffer();
}
void DustController::updateDynamicBuffer()
{
	float *pos_buf = mDynamicBuffer.lock();
	for(std::vector<DustParticle>::iterator i = mParticle.begin(); i != mParticle.end(); i++)
	{
		i->updatePosition(pos_buf, mPositionIndexer);
		i->updateTCoord2(pos_buf, mTCoord2Indexer);
	}
	mDynamicBuffer.unlock();
}
void DustController::updateConstBuffer()
{
	float *tcoord1_buf = mStaticBuffer.lock();
	for(std::vector<DustParticle>::iterator i = mParticle.begin(); i != mParticle.end(); i++)
		i->updateTCoord1(tcoord1_buf, mTCoord1Indexer);
	mStaticBuffer.unlock();
}
void DustController::getWorldTransforms( Matrix4* xform ) const
{
    *xform = Matrix4::IDENTITY;
}
void DustController::setUserAny(const Any& anything)
{
	Ogre::MovableObject::setUserAny(anything); 
}






//---------------------------DustControllerFactory---------------------------
const String DustControllerFactory::FACTORY_TYPE_NAME = "DustController";
//-----------------------------------------------------------------------
const String& DustControllerFactory::getType(void) const
{
	return FACTORY_TYPE_NAME;
}
//-----------------------------------------------------------------------
MovableObject* DustControllerFactory::createInstanceImpl( const String& name,
														 const NameValuePairList* params)
{
	bool autounload = true;
	NameValuePairList::const_iterator iterator;
	iterator = params->find("count");
	int count = StringConverter::parseInt(iterator->second, 1);
	iterator = params->find("width");
	Ogre::Real width = StringConverter::parseReal(iterator->second, 1);
	iterator = params->find("height");
	Ogre::Real height = StringConverter::parseReal(iterator->second, 1);
/*	if (params != 0)
	{
		NameValuePairList::const_iterator ni = params->find("autoUnload");
		if (ni != params->end())
			autounload = StringConverter::parseBool(ni->second);
	}*/
	return OGRE_NEW DustController(count, width, height);
}
//-----------------------------------------------------------------------
void DustControllerFactory::destroyInstance( MovableObject* obj)
{
	OGRE_DELETE obj;
}
DustControllerFactory::DustControllerFactory()
{}
DustControllerFactory::~DustControllerFactory()
{}



}