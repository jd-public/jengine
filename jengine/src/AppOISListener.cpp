#include "stdafx.h"

#include "AppOISListener.h"
#include "OgreOverlaySystem.h"
#include "OgreOverlay.h"
#include "OgreOverlayContainer.h"
#include "OgreOverlayManager.h"
#include "MainApplication.h"
#include "GameObject.h"
#include "VirtualScript.h"
#include "EventPlayer.h"
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
//#	include "resource.h"
#endif

//#include "NSAccessibility.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	int applicationLoadCursorFromFile(const char *cur);
	void applicationSetCursor(int i);
#endif


//----------------------------------------------
CAppOISListener::CAppOISListener(Ogre::Root *pRoot,
#if USE_OIS_FOR_INPUT == TRUE
								 EventPlayer::TPlayerMode eventPlayerMode,
#endif
								 const Ogre::String& filename) :
	mCursorOverlay(NULL),
	mCursorContainer(NULL),
	mCursorDefaultId(0),
	m_vMousePos(0.5f,0.5f),
	m_bLMouseButton(false),
	m_bRMouseButton(false),
	mCursorStandard(false),
	mCursorUseTextures(false),
	mCursorId(0)

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	,mMacCursorVisible(true)
#endif

#if USE_OIS_FOR_INPUT == TRUE
	,
	mInputManager(NULL),
	mEventPlayer(NULL)
#endif
{
	assert(app.getWindow() && "window==NULL in constructor of CAppOISListener");


	mCursorShow = false;
	mAzVisible = false;

#if USE_OIS_FOR_INPUT == TRUE
	switch (eventPlayerMode)
	{
		case EventPlayer::ePlay:
		case EventPlayer::eRecord:
			mEventPlayer = new EventPlayer(eventPlayerMode, filename);
			break;
		default:
			break;
	}
#endif


    m_pRoot = pRoot;
	memset(mKeys,0,sizeof(mKeys));
	checkMouseSwap();

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	memset(mCursorsCustom,0,sizeof(mCursorsCustom));
#endif



#if USE_OIS_FOR_INPUT == TRUE

		// using buffered input
#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE || OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
		OIS::ParamList pl;
	#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		HWND hwnd;
		app.getWindow()->getCustomAttribute("WINDOW", &hwnd);
		pl.insert(std::make_pair(String("WINDOW"), Ogre::StringConverter::toString((Ogre::uint)hwnd)));
		pl.insert(std::make_pair(String("w32_mouse"), String("DISCL_FOREGROUND" )));
		pl.insert(std::make_pair(String("w32_mouse"), String("DISCL_NONEXCLUSIVE")));
		pl.insert(std::make_pair(String("w32_keyboard"), String("DISCL_FOREGROUND")));
		pl.insert(std::make_pair(String("w32_keyboard"), String("DISCL_NONEXCLUSIVE")));
		SetClassLong(hwnd, GCL_HCURSOR, NULL);
	#else
		size_t hwnd;
		app.getWindow()->getCustomAttribute("WINDOW", &hwnd);
		pl.insert(std::make_pair(String("WINDOW"), Ogre::StringConverter::toString(hwnd)));

		pl.insert(std::make_pair(String("x11_keyboard_grab"), String("false" )));
		pl.insert(std::make_pair(String("x11_mouse_grab"), String("false" )));

    #if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
		pl.insert(std::make_pair(String("x11_mouse_hide"), String("true" )));
	#else
        pl.insert(std::make_pair(String("x11_mouse_hide"), String("false" )));
    #endif

		//pl.insert(std::make_pair(std::string("MacGrabMouse"), std::string("false")));

		//mWindow = (WindowRef)hwnd;

	#endif
		mInputManager = OIS::InputManager::createInputSystem( pl );

		mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));
		mKeyboard->setEventCallback(this);
		mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));
#else
		mMouse = static_cast<OIS::MultiTouch*>(mInputManager->createInputObject(OIS::OISMultiTouch, true));
		mAccelerometer = static_cast<OIS::JoyStick*>(mInputManager->createInputObject(OIS::OISJoyStick, true));
#endif
		mMouse->setEventCallback(this);

#endif
}


void CAppOISListener::checkMouseSwap()
{
	bMouseButtonsSwapped = false;

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	bMouseButtonsSwapped = (GetSystemMetrics(SM_SWAPBUTTON) != 0);
#endif
}



CAppOISListener::~CAppOISListener()
{
#if USE_OIS_FOR_INPUT == TRUE
	SAFE_DEL(mEventPlayer);

	if (mInputManager)
	{
		mInputManager->destroyInputObject(mMouse);
#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
		mInputManager->destroyInputObject(mKeyboard);
#else
		mInputManager->destroyInputObject(mAccelerometer);
#endif
		OIS::InputManager::destroyInputSystem(mInputManager);
		mInputManager = NULL;
	}
#endif
}

void CAppOISListener::addCursorCustom(int id, const String& name)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	mCursorsCustom[id] = /*LoadCursorFromFile(name.c_str()); */ (HICON)LoadImage(NULL,name.c_str(),IMAGE_CURSOR, 48, 48, LR_LOADFROMFILE);
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	mCursorsCustom[id] = applicationLoadCursorFromFile(name.c_str());
	//toLogEx("addCursorCustom %d %d",mCursorsCustom[id],id);
#endif
}

void CAppOISListener::addCursorStandard(int id, char* name)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	mCursorsStandard[id] = LoadCursor(NULL, name);
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	//mCursors[id] = name;
#endif
}

void CAppOISListener::addCursorMaterial(int id, const Ogre::String& material)
{
//#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	mCursorsMaterial[id] = material;
//#endif
}



void CAppOISListener::setCursor(int id)
{
	mCursorId = id;
	updateCursorImage();
}

void CAppOISListener::setCursorStandard(bool state)
{
	mCursorStandard = state;
	updateCursorImage();
}
void CAppOISListener::setCursorUseTextures(bool state)
{
	mCursorUseTextures = state;

	updateCursorPosition();
	updateCursorImage();
}


void CAppOISListener::showCursor(bool fshow)
{
	mCursorShow = fshow;
}

void CAppOISListener::Update()
{
#if USE_OIS_FOR_INPUT == TRUE
	if (mEventPlayer && mEventPlayer->isPlay())
	{
		mEventPlayer->process();
	}
	else
	{

	#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
			mKeyboard->capture();
	#else
			mAccelerometer->capture();
	#endif
			mMouse->capture();

	}
#endif
}

void CAppOISListener::setWndSize(unsigned int width, unsigned int height)
{
#if USE_OIS_FOR_INPUT == TRUE
	#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
		const OIS::MouseState &ms = mMouse->getMouseState();
		ms.width = static_cast<int>(width);
		ms.height = static_cast<int>(height);
	#endif
#endif
}

//-------------------------------------------

void CAppOISListener::updateCursorImage()
{
	//system cursor
	if(mCursorStandard)
	{
		updateCursorStandard();
		updateCursorUseTextures(false);
	}
	else
	{
		if(mCursorUseTextures)
		{
			updateCursorUseTextures(true);
		}
		else
		{
			updateCursorCustom();
			updateCursorUseTextures(false);
		}
	}
}

void CAppOISListener::updateCursorCustom()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

	HCURSOR hCur = mCursorsCustom[mCursorId];
	SetWin32Cursor(hCur);
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	applicationSetCursor(mCursorsCustom[mCursorId]);
#endif


}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

void CAppOISListener::SetWin32Cursor(HCURSOR hCur)
{
	HWND wnd;
	app.getWindow()->getCustomAttribute("WINDOW", &wnd);


	static BOOL bShown = TRUE;
	BOOL bShow = (hCur != NULL ? TRUE : FALSE);

	if (bShow != bShown)
	{
		bShown = bShow;
		ShowCursor(bShown);
	}

	SetClassLong(wnd, GCL_HCURSOR, (LONG)hCur);
	SetCursor(hCur);
}

#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
void CAppOISListener::SetMacCursor(bool visible)
{
	if(visible != mMacCursorVisible)
	{
		if(visible)
		{
			CGDisplayShowCursor(kCGDirectMainDisplay);
		}
		else
			CGDisplayHideCursor(kCGDirectMainDisplay);

		mMacCursorVisible = visible;
	}
}
#endif



void CAppOISListener::updateCursorStandard()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

	HCURSOR hCur = mCursorsStandard[mCursorId];
	SetWin32Cursor(hCur);

#endif
}


void CAppOISListener::updateCursorUseTextures(bool visible)
{
	if (!mCursorOverlay)
		return;

	if(visible)
	{
//#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
//		SetMacCursor(false);
//#endif

		if(!mCursorOverlay->isVisible())
			mCursorOverlay->show();

		mCursorContainer->setMaterialName(mCursorsMaterial[mCursorId]);
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		SetWin32Cursor(0);
#endif
	}
	else
	{
		if(mCursorOverlay->isVisible())
			mCursorOverlay->hide();

//#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
//		SetMacCursor(true);
//#endif
	}
}

void CAppOISListener::initCustomTextureCursors()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	mCursorOverlay = NULL;
#else
	mCursorOverlay = Ogre::OverlayManager::getSingletonPtr()->getByName("CursorOverlay");
#endif
	if (!mCursorOverlay)
	{
		mCursorContainer = NULL;
		return;
	}

	mCursorOverlay->show();
	mCursorContainer = (Ogre::OverlayContainer*)mCursorOverlay->getChild("CursorContainer");

	if (!mCursorContainer)
		return;

	mCursorContainer->show();
}

/*#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	Ogre::Vector2 applicationGetWindowPos();
#endif

void CAppOISListener::movePhysicalCursorToGameCursor(bool in_fullscreen)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE

	CGPoint p;

	if(in_fullscreen)
	{
		float w = CGDisplayPixelsWide(CGMainDisplayID());
		float h = CGDisplayPixelsHigh(CGMainDisplayID());

		p.x = (m_vMousePos.x + app.getViewport()->getLeft())*w;
		p.y = (m_vMousePos.y + app.getViewport()->getTop())*h;

		toLogEx("\n!!!%f %f %f %f", p.x, m_vMousePos.x, app.getViewport()->getLeft(), w);

		CGDisplayMoveCursorToPoint(kCGDirectMainDisplay, p);
	}
	else
	{
		Vector2 v;
		v = applicationGetWindowPos();

		toLogEx("\n!!!%f %f", v.x, v.y);

		//float w = CGDisplayPixelsWide(CGMainDisplayID());
		//float h = CGDisplayPixelsHigh(CGMainDisplayID());

		p.x = v.x + m_vMousePos.x*app.getWindowActiveResolution().x;
		p.y = v.y + m_vMousePos.y*app.getWindowActiveResolution().y;

	}


	mMouse->capture();

#endif
}*/

void CAppOISListener::updateCursorPosition()
{
	if(mCursorContainer != NULL)
	{
		mCursorContainer->setPosition((m_vMousePos.x), (m_vMousePos.y));

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE ||  OGRE_PLATFORM == OGRE_PLATFORM_LINUX
		mCursorContainer->setWidth(48.f/app.getWindowActiveResolution().x);
		mCursorContainer->setHeight(48.f/app.getWindowActiveResolution().y);
#else
		mCursorContainer->setWidth(48.f/app.getWindowResolution().x);
		mCursorContainer->setHeight(48.f/app.getWindowResolution().y);
#endif
	}
}



void CAppOISListener::_appMouseMoved(float rel_screen_x, float rel_screen_y, float rel_z, bool lbutton, bool rbutton)
{
	m_vMousePosNew.x = rel_screen_x;
	m_vMousePosNew.y = rel_screen_y;

	m_bLMouseButton = lbutton;
	m_bRMouseButton = rbutton;

#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE
	app.mouseZMove(rel_z);
#endif

	updateCursorPosition();

#if NO_MOUSE_MOVE == TRUE
	if(lbutton || rbutton)
#else
	if(true)
#endif
		app.mouseMove(false);
}

void CAppOISListener::_appMousePressed(int button) //0 - left, 1 - right, 2 - middle
{
	app.mouseMove(false);
	switch(button)
	{
	case 0:
		app.mouseReleasedAndPressed(mCursorShow, true, true);
		break;
	case 1:
		app.mouseReleasedAndPressed(mCursorShow, false, true);
		break;
	}
}


void CAppOISListener::_appMouseReleased( int button ) //0 - left, 1 - right, 2 - middle
{
	app.mouseMove(false);
	switch (button)
	{
		case 1:
			app.mouseReleasedAndPressed(mCursorShow, false, false);
			break;
		case 2:
			if (app.screensEnabled())
				app.getWindow()->writeContentsToTimestampedFile(app.getAppShortName()+"_", ".png");
			break;
		case 0:
			app.mouseReleasedAndPressed(mCursorShow, true, false);
			break;
		default:
			break;
	}
}


//-------------------------------------------

#if USE_OIS_FOR_INPUT == TRUE


#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
	bool CAppOISListener::appMouseMoved (const OIS::MouseEvent &e)
#else
	bool CAppOISListener::appMouseMoved (const OIS::MultiTouchEvent &e)
#endif
{
	float mx = 0;
	float my = 0;
	float mz = 0;
	bool lbutton = 0;
	bool rbutton = 0;

	Ogre::Viewport *pViewport = app.getViewport();

	MathUtils::TVector2ui resolution = app.getWindowResolution();
//	toLogEx("\n res %d %d", resolution.x, resolution.y);

//	toLogEx("\n res %d %d", e.state.X.abs, e.state.Y.abs);

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
	if (pViewport)
	{

		mx = (float)e.state.X.abs;
		my = (float)e.state.Y.abs;


		mx = ((Real)mx/resolution.x - pViewport->getLeft())/pViewport->getWidth();
		my = ((Real)my/resolution.y - pViewport->getTop())/pViewport->getHeight();


		mx = std::min(mx, 1.f);
		mx = std::max(mx, 0.f);
		my = std::min(my, 1.f);
		my = std::max(my, 0.f);

	}
#else
	mx = (Real)e.state.X.abs/resolution.x;
	my = (Real)e.state.Y.abs/resolution.y;

#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	my = 1 - my;
#endif



#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
	if(!bMouseButtonsSwapped)
	{
		lbutton = e.state.buttonDown(OIS::MB_Left);
		rbutton = e.state.buttonDown(OIS::MB_Right);
	}
	else
	{
		lbutton = e.state.buttonDown(OIS::MB_Right);
		rbutton = e.state.buttonDown(OIS::MB_Left);
	}
#else
	lbutton = e.state.touchIsType(OIS::MT_Pressed)|e.state.touchIsType(OIS::MT_Moved);
#endif
	mz = e.state.Z.rel / 1000.0f;

	_appMouseMoved(mx, my, mz, lbutton, rbutton);

	return mCursorShow;

}

#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
bool CAppOISListener::appMousePressed ( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	if(bMouseButtonsSwapped)
	{
		if(id == OIS::MB_Right)
			id = OIS::MB_Left;
		else if(id == OIS::MB_Left)
			id = OIS::MB_Right;
	}

	if(id == OIS::MB_Left)
		_appMousePressed(0);
	if(id == OIS::MB_Right)
		_appMousePressed(1);

#else
bool CAppOISListener::appMousePressed ( const OIS::MultiTouchEvent &arg)
{
	if (mCursorShow)
		app.mouseReleasedAndPressed(true, true);
#endif
	return true;
}

#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
bool CAppOISListener::appMouseReleased( const OIS::MouseEvent &evt, OIS::MouseButtonID id )
{
	if(bMouseButtonsSwapped)
	{
		if(id == OIS::MB_Right)
			id = OIS::MB_Left;
		else if(id == OIS::MB_Left)
			id = OIS::MB_Right;
	}
	_appMouseReleased((int)id);
#else
bool CAppOISListener::appMouseReleased ( const OIS::MultiTouchEvent &arg)
{
	if (mCursorShow)
		app.mouseReleasedAndPressed(true, false);
#endif
	return true;
}

//------------------------------------
bool CAppOISListener::appKeyPressed(const OIS::KeyEvent &event)
{
	if (event.key < 256)
		mKeys[event.key] = true;

	return true;
}

bool CAppOISListener::appKeyReleased(const OIS::KeyEvent &event)
{
	if (event.key < 256)
		mKeys[event.key] = false;

	if (app.getWindow()->isFullScreen())
	{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		if (event.key == OIS::KC_LWIN || event.key == OIS::KC_RWIN)
		{
			HWND wnd;
			app.getWindow()->getCustomAttribute("WINDOW", &wnd);
			ShowWindow(wnd,SW_MINIMIZE);
			ShowWindow(wnd,SW_FORCEMINIMIZE);
			ChangeDisplaySettings(NULL, CDS_RESET);
		}

		//TODO: if ( fullscreen ) !!!
		if (event.key == OIS::KC_TAB && mKeys[OIS::KC_LMENU])
		{
			ChangeDisplaySettings(NULL, CDS_RESET);
		}
#endif
	}

	/*if (event.key == OIS::KC_F && mKeys[OIS::KC_LWIN] && mKeys[OIS::KC_LCONTROL])
	{
		toLogEx("CMD+CTRL+F!!!!");
	}*/

/*#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	//CMD-Tab
	if (event.key == OIS::KC_TAB && mKeys[OIS::KC_LWIN])
	{
		if (app.getWindow()->isFullScreen())
		{
			//CGReleaseAllDisplays();

			WindowRef window = 0;
			Ogre::RenderWindow *pWindow = app.getWindow();
			if (app.getWindow()->isFullScreen())
			{
				app.setFullscreen(false);
			}
			pWindow->getCustomAttribute("WINDOW", &window);
			CollapseWindow(window, true);

		}
	}
#endif*/

//#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	//CMD-q
//	if (event.key == OIS::KC_Q && mKeys[OIS::KC_LWIN])
//#else
	//ALT-F4
	if (event.key == 62 && mKeys[OIS::KC_LMENU])
//#endif
	{
		app.close();
	}



	if (app.logEnabled())
	{
		if (event.key == 65 && app.getCamera()) // F7
		{
			if (app.getCamera()->getPolygonMode() == Ogre::PM_WIREFRAME)
				app.getCamera()->setPolygonMode(Ogre::PM_SOLID);
			else
				app.getCamera()->setPolygonMode(Ogre::PM_WIREFRAME);
		}
		if (event.key == 66) // F8
		{
			Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName("00_empty").staticCast<Ogre::Material>();
			Ogre::Technique* technique = material->getTechnique(0);
			Ogre::Pass* pass = technique->getPass(0);
			mAzVisible = !mAzVisible;
			if (mAzVisible)
				pass->setSceneBlending(Ogre::SBT_ADD);
			else
				pass->setSceneBlending(Ogre::SBF_ZERO, Ogre::SBF_ONE);
		}
	}
	return true;
}

//-----------------------------------
//mouse and key listener overrides
#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE || OGRE_PLATFORM == OGRE_PLATFORM_ANDROID

Ogre::OrientationMode getViewportOrientation();

void CAppOISListener::transformInputState(OIS::MultiTouchState &state)
{

	Ogre::Viewport* viewport = app.getViewport();
	if (viewport)
	{
		int w = viewport->getActualWidth();
		int h = viewport->getActualHeight();
		int absX = state.X.abs;
		int absY = state.Y.abs;
		int relX = state.X.rel;
		int relY = state.Y.rel;

		Ogre::OrientationMode o = Ogre::OR_DEGREE_0;

		#if OGRE_NO_VIEWPORT_ORIENTATIONMODE == 0
			o = viewport->getOrientationMode();
		#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
		#else
			o = getViewportOrientation();
		#endif

			switch (o)
			{
				case Ogre::OR_DEGREE_90:
					state.X.abs = w - absY;
					state.Y.abs = absX;
					state.X.rel = -relY;
					state.Y.rel = relX;
					break;
				case Ogre::OR_DEGREE_180:
					state.X.abs = w - absX;
					state.Y.abs = h - absY;
					state.X.rel = -relX;
					state.Y.rel = -relY;
					break;
				case Ogre::OR_DEGREE_270:
					state.X.abs = absY;
					state.Y.abs = h - absX;
					state.X.rel = relY;
					state.Y.rel = -relX;
					break;
			}

	}
}

bool CAppOISListener::touchMoved(const OIS::MultiTouchEvent& evt)
{
	OIS::MultiTouchState s = evt.state;

	transformInputState(s);


	OIS::MultiTouchEvent e(0,s);
	return appMouseMoved(e);
}

bool CAppOISListener::touchPressed(const OIS::MultiTouchEvent& evt)
{
	OIS::MultiTouchState s = evt.state;

	transformInputState(s);


	OIS::MultiTouchEvent e(0,s);
	appMouseMoved(e);
	return appMousePressed(e);
}

bool CAppOISListener::touchReleased(const OIS::MultiTouchEvent& evt)
{
	OIS::MultiTouchState s = evt.state;

	transformInputState(s);


	OIS::MultiTouchEvent e(0,s);
	appMouseMoved(e);
	return appMouseReleased(e);
}

bool CAppOISListener::touchCancelled(const OIS::MultiTouchEvent& evt)
{

	return true;

}

#else

bool CAppOISListener::mouseMoved(const OIS::MouseEvent &event)
{
	if (mEventPlayer)
	{
		if (!mEventPlayer->isPlay())
			mEventPlayer->saveEvent(event,0);
		else
			return true;
	}

	return appMouseMoved(event);
}

bool CAppOISListener::mousePressed ( const OIS::MouseEvent &event, OIS::MouseButtonID id )
{
	if (mEventPlayer)
	{
		if (!mEventPlayer->isPlay())
			mEventPlayer->saveEvent(event, 0);
		else
			return true;
	}
	appMouseMoved(event);
	return appMousePressed(event, id);
}

bool CAppOISListener::mouseReleased( const OIS::MouseEvent &event, OIS::MouseButtonID id )
{
	if (mEventPlayer)
	{
		if (!mEventPlayer->isPlay())
			mEventPlayer->saveEvent(event,0);
		else
			return true;
	}
	appMouseMoved(event);
	return appMouseReleased(event, id);
}

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
bool CAppOISListener::mouseOtherEvents( int idd )
{
	//system cursor
	if(mCursorStandard)
	{
		//do nothing
	}
	else
	{
		if(mCursorUseTextures)
		{
			//set cursor visible, if outside of window
			SetMacCursor(idd == 0);

			//updateCursorUseTextures(true);
		}
		else
		{
			//updateCursorCustom();
			//updateCursorUseTextures(false);

			if(idd == 0)
			{
				//system arrow
				applicationSetCursor(-1);
			}
			else
			{
				applicationSetCursor(0);
			}
		}
	}



	if(idd == 1) //enter window
	{
		Update();
	}

	toLogEx("Mouse other event %d",idd);
}
#endif


bool CAppOISListener::keyPressed(const OIS::KeyEvent &event)
{
	if (mEventPlayer)
	{
		if (!mEventPlayer->isPlay())
			mEventPlayer->saveEvent(event,0);
		else
			return true;
	}
	return appKeyPressed(event);
}

bool CAppOISListener::keyReleased(const OIS::KeyEvent &event)
{
	if (mEventPlayer)
	{
		if (!mEventPlayer->isPlay())
			mEventPlayer->saveEvent(event,0);
		else
			return true;
	}
	return appKeyReleased(event);
}
#endif


#endif //#if USE_OIS_FOR_INPUT == TRUE
