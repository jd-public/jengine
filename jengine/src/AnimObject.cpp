#include "stdafx.h"

#include "AnimObject.h"
#include <OgreAnimation.h>
#include "MainApplication.h"
#include "ScriptMsg.h"
#include "CMsg.h"

#include "VirtualScript.h"

#ifndef NO_SFX
#	include "SfxCore.h"
#endif

void CAnimatableObject::init()
{
	pBaseSkel = NULL;
	mIsPlay = false;
	mCurPoint = 0;
	mIsLooped = false;

	mRotation = false;
	mRotProgress = 0.0;

	setType(T_ANIM_OBJ);
}

int CAnimatableObject::findBoneInLayer(Ogre::Node *pNode, size_t layer)
{
	for(size_t j = 0; j < mLayers[layer].mBones.size(); j++)
	{
		const Ogre::Node *node = mLayers[layer].mBones[j];
		if (node->getName() == pNode->getName())
			return 1;
	}
	return 0;
}

void CAnimatableObject::initLayers()
{
	std::vector<String> mLipBones;
	mLipBones.push_back("rot");
	mLipBones.push_back("rot_01");
	mLipBones.push_back("rot_02");
	mLipBones.push_back("rot_03");
	mLipBones.push_back("rot_04");
	mLipBones.push_back("rot_05");
	

//-----------------------------

	mLayers.push_back(BlendLayer(0,"Body"));
	mLayers.push_back(BlendLayer(1,"Lip"));

	Ogre::Skeleton::BoneIterator boneIter = pBaseSkel->getBoneIterator();

	while(boneIter.hasMoreElements())
	{
		Ogre::Bone *skelBone = boneIter.getNext();

		int layer = 0;
		for(size_t i = 0; i < mLipBones.size(); i++)
		{
			String lname = mLipBones[i];
			String nname = skelBone->getName();

			if (nname.find(lname) != String::npos)
			{
				layer = 1;
				break;
			}
		}

		mLayers[layer].mBones.push_back(skelBone);

	}

	
}

void CAnimatableObject::initAnim()
{
	if (!pBaseSkel)
	{
		pBaseSkel = pEntity->getSkeleton();
		
		pBaseSkel->removeAllLinkedSkeletonAnimationSources();
		pBaseSkel->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);

		initLayers();
	}
}

CAnimatableObject::CAnimatableObject() : CGameObject()
{
	init();
}

CAnimatableObject::CAnimatableObject(const String& scrName) : CGameObject(scrName)
{	
	init();
}

CAnimatableObject::~CAnimatableObject()
{
	mPointList.clear();
	mAnimsList.clear();
}

void CAnimatableObject::addPoint(const AnimationPoint &p)
{
	size_t l = p.maxLayer();
	if (mPointList.size() > 0 && 
	   mPointList[mPointList.size()-1].anim(l) == p.anim(l) &&
	   mAnimsList[p.anim(l)].timeToBlend > 0 &&
	   mAnimsList[p.anim(l)].layer == 0)
	{
		toLogEx("WARRING: Anim object '%s'. You can't blend animation '%s' to itself!\n",
			getName(),
			mAnimsList[mPointList[mCurPoint].anim(l)].pAnimState->getAnimationName().c_str());
		return;
	}
	mPointList.push_back(p);
}

int CAnimatableObject::findAnim(const String& name)
{
	for(size_t i = 0; i < mAnimsList.size(); i++)
	{
		if (mAnimsList[i].pAnimState->getAnimationName() == name)
			return static_cast<int>(i);
	}
	toLogEx("ERROR: Can't find animation '%s' on the object '%s'\n",name.c_str(), getName());
	return -1;
}

void CAnimatableObject::addPoint(const String& anim, int ntimes, int speed, OBJECT_HANDLE target, int param)
{
	AnimationPoint p;
	p.mEndPos = pSNode->getPosition();
	p.mStartPos = pSNode->getPosition();
	p.mRotation = pSNode->getOrientation();
	p.addLayer(mAnimsList[findAnim(anim)].layer, findAnim(anim), ntimes, speed);
	p.mNeedToRot = false;
	p.mNeedToMove = false;
	p.mMoveSpeed = 0;
	p.mTarget = target;
	p.mParam = param;

	addPoint(p);
}

namespace { // begin anonymous namespace

// ��������������� ������� ��� addPoint
// ���������� ����� �������� � ������ anim, ���� anim �� ����� ��� ����� �������� �� ���������� AnimationPoint
int getAnimIdxByName(CAnimatableObject& anim_obj, const String& anim, int layer_idx)
{
	int anim_idx = -1;

	if (anim.size() > 1) 
		anim_idx = anim_obj.findAnim(anim);
	else 
		if (!anim_obj.mPointList.empty())
			anim_idx = anim_obj.mPointList.back().anim(layer_idx);

	return anim_idx;
}

} // end anonymous namespace

void CAnimatableObject::addPoint(const String& anim, int ntimes, int speed, 
								 const String& anim2, size_t snd_id, 
								 OBJECT_HANDLE target, int param)
{
	/*AnimationPoint p = {pNode->getPosition(),
						pNode->getPosition(),
						pNode->getOrientation(),
						findAnim(anim),
						ntimes,
						speed,
						FALSE,
						FALSE,
						0,
						target,
						param};*/

	AnimationPoint p;
	p.mEndPos = pSNode->getPosition();
	p.mStartPos = pSNode->getPosition();
	p.mRotation = pSNode->getOrientation();

	int anim_idx = getAnimIdxByName(*this, anim, 0);
	if (anim_idx >= 0)
		p.addLayer(0, anim_idx, ntimes, speed);
	
	int anim2_idx = getAnimIdxByName(*this, anim2, 1);
	if (anim2_idx >= 0)
		p.addLayer(1, anim2_idx, snd_id);

	p.mNeedToRot = false;
	p.mNeedToMove = false;
	p.mMoveSpeed = 0;
        		
	p.mTarget = target;
	p.mParam = param;

	addPoint(p);
}

void CAnimatableObject::addPoint(const String& anim, const Vector3 &endPos, const Quaternion& endRot, bool carePos, bool careRot, OBJECT_HANDLE target, int param)
{
	AnimationPoint p;
	p.mEndPos = endPos;
	p.mStartPos = endPos;
	p.mRotation = endRot;
	p.addLayer(mAnimsList[findAnim(anim)].layer, findAnim(anim), -1, 1000);
	p.mNeedToRot = careRot;
	p.mNeedToMove = carePos;
	p.mMoveSpeed = mAnimsList[findAnim(anim)].moveSpeed;
	p.mTarget = target;
	p.mParam = param;

	addPoint(p);
}

void CAnimatableObject::addPoint(const String& anim, const Vector3 &endPos, const Quaternion& endRot, bool carePos, bool careRot, const String& anim2, size_t snd_id, OBJECT_HANDLE target, int param)
{
	AnimationPoint p;
	p.mEndPos = endPos;
	p.mStartPos = endPos;
	p.mRotation = endRot;
	p.addLayer(mAnimsList[findAnim(anim)].layer, findAnim(anim), -1, 1000);

	int anim2_idx = getAnimIdxByName(*this, anim2, 1);
	if (anim2_idx >= 0)
		p.addLayer(1, anim2_idx, snd_id);

	p.mNeedToRot = careRot;
	p.mNeedToMove = carePos;
	p.mMoveSpeed = mAnimsList[findAnim(anim)].moveSpeed;
	p.mTarget = target;
	p.mParam = param;

	addPoint(p);
}

void CAnimatableObject::addAnimation(const String& skeleton, 
									 const String& anim, 
									 Real moveSpeed, 
									 Real blendPercent,
									 Real blendSpeed,
									 int layer)
{
	pBaseSkel->addLinkedSkeletonAnimationSource(skeleton); 
	
    pEntity->refreshAvailableAnimationState();

	AnimationStateEx a;
	a.pAnimState = pEntity->getAnimationState(anim);
	a.blendSpeed = 0;
	a.moveSpeed = moveSpeed;
	a.pAnimState->setEnabled(true);
	a.pAnimState->setLoop(false);
	a.pAnimState->setWeight(0);
	a.pAnimState->setTimePosition(0);
	a.enable = false;
	a.blendType = NoneBlend;
	a.fLen = a.pAnimState->getLength();
	a.timeToBlend = a.fLen*blendPercent/100.0f;

	Ogre::Animation *an = findLinkedAnimByName(anim);

	assert(a.fLen != 0.0f);

	/*if(an && a.fLen == 0.0f)
	{
		a.fLen = an->getLength();
	}*/

	

	a.layer = layer;

	if (a.timeToBlend > a.pAnimState->getLength()/2.0f)
		a.timeToBlend = a.pAnimState->getLength()/2.0f;

	a.maxBlendSpeed = blendSpeed;


	//-----------------
	Ogre::Animation *pAnim = findLinkedAnimByName(anim);

	if (pAnim)
	{
		std::vector<unsigned short> delete_handles;

		Ogre::Animation::NodeTrackIterator nit = pAnim->getNodeTrackIterator();

		while(nit.hasMoreElements())
		{
			Ogre::NodeAnimationTrack *nt = nit.getNext();

			bool del_bone = true;

			if (findBoneInLayer(nt->getAssociatedNode(),layer))
				del_bone = false;

			if (del_bone)
			{
				delete_handles.push_back(nt->getHandle());
			}
		}

		for(size_t i = 0; i < delete_handles.size(); i++)
		{
			pAnim->destroyNodeTrack(delete_handles[i]);
		}
	}
	mAnimsList.push_back(a);
}

Ogre::Animation *CAnimatableObject::findLinkedAnimByName(const String& name)
{
	if (!pBaseSkel)
		return NULL;

	Ogre::Skeleton::LinkedSkeletonAnimSourceIterator linkIter = pBaseSkel->getLinkedSkeletonAnimationSourceIterator();
		
	while(linkIter.hasMoreElements())
	{
		Ogre::LinkedSkeletonAnimationSource animSrc = linkIter.getNext();
		Ogre::Animation *a = animSrc.pSkeleton->getAnimation(0);
		if (a && a->getName() == name)
			return a;
	}

	return NULL;
}

float CAnimatableObject::getAnimationRotSpeed(const String& animName)
{
	AnimationStateEx *a = &mAnimsList[findAnim(animName)];

	return a->rotSpeed;
}

void CAnimatableObject::setAnimationParam(const String& animName, int param, float val)
{
	AnimationStateEx *a = &mAnimsList[findAnim(animName)];

	switch(param)
	{
		case 0: a->rotSpeed = val; break;
		case 1: 
			
			a->timeToBlend = val*0.04f; 

			if (a->timeToBlend > a->pAnimState->getLength()/2.0f)
				a->timeToBlend = a->pAnimState->getLength()/2.0f;

			a->blendSpeed = 1.0f/a->timeToBlend;
			a->maxBlendSpeed = a->blendSpeed;
		
			break;
	}
}

void CAnimatableObject::clear()
{
	mCurPoint = 0;

	mPointList.clear();
	
	mIsPlay = false;
	mIsLooped = false;

	mRotation = false;
	mRotProgress = 0.0;
	mRotSpeed = 0.0;
	mDestRot = Quaternion::IDENTITY;
	mScrRot = Quaternion::IDENTITY;

	for (size_t i = 0; i < mAnimsList.size(); i++)
	{
		AnimationStateEx *a = &mAnimsList[i];

		a->blendSpeed = 0;
		a->pAnimState->setEnabled(true);
		a->pAnimState->setLoop(false);
		a->pAnimState->setWeight(0);
		a->pAnimState->setTimePosition(0);
		a->enable = false;
		a->blendType = NoneBlend;
		a->totalPlayTime = 0;
	}
}

void CAnimatableObject::start()
{
	if (!mPointList.empty())
	{
		mPointList[mCurPoint].init(*this);
		updateSkeleton();
		mIsPlay = true;
	}
	else
		toLogEx("Error: CAnimatableObject::start(): mPointList is empty! anim obj: %s\n", getName());
}

void CAnimatableObject::stop()
{
	mIsPlay = false;
}

size_t CAnimatableObject::nextPointIdx()
{
	size_t result = mCurPoint + 1;
	if (mIsLooped)
		result %= mPointList.size();
	return result;
}

bool CAnimatableObject::isEndPoint(size_t point_idx)
{
	return !mIsLooped && (point_idx >= (mPointList.size() - 1));
}

bool CAnimatableObject::isNextAnimNotSame(size_t layer, bool skip_all)
{
	bool res = true;

	size_t next_point_idx = skip_all ? mPointList.size() - 1 : nextPointIdx();

	if (!isEndPoint(mCurPoint))
	{
		if ( mPointList[next_point_idx].hasLayer(layer) && mPointList[mCurPoint].hasLayer(layer))
			res = mPointList[mCurPoint].anim(layer) != mPointList[next_point_idx].anim(layer);
	}
	return res;
}

void CAnimatableObject::normalizeAnimBlendings()
{
	const int MAX_LAYERS_NUM = 2;
	float weights[MAX_LAYERS_NUM] = {0};

	if (!mAnimsList.empty())
	{
		// ������� ����� ����� �������� ��� ������� ���� ��������
		for(size_t anim_idx = mAnimsList.size(); anim_idx > 0; --anim_idx)
		{
			AnimationStateEx &anim_state_ex = mAnimsList[anim_idx-1];
			weights[anim_state_ex.layer] += anim_state_ex.pAnimState->getWeight();
		}

		// ������������ ��� �������� �������� ���, ����� ����� ����� ���� �������� ��� ������� ���������� ���� ���� ����� 1
		int layer_num = 0; // ��������� ����� ����� ����� � 1 ����� ����� ������ ��� ���� 0 (�������� ��������), ��� �������� �������� ���� 1 ����������� ������, ���������� �� ��������
		Ogre::AnimationState *anim_state = mAnimsList[mPointList[mCurPoint].anim(layer_num)].pAnimState;
		anim_state->setWeight(anim_state->getWeight() + 1.0f - weights[layer_num]);
	}
}

void CAnimatableObject::skip(bool skip_all)
{
	if (mCurPoint<mPointList.size())
	{
		const AnimationPoint& point = mPointList[mCurPoint];
		for(size_t i = 0, max_layer = point.maxLayer(); i <= max_layer; i++)
		{
			//���� �������� � ���� ������������ � ��������� ���� - �������
			if (point.hasLayer(i))
			{
				int a = point.anim(i);

				if (isNextAnimNotSame(i, skip_all)) // ������� ������ ���� ��������� �������� ���������� �� �������
				{
					if (i == 0 && !isEndPoint(mCurPoint) && mPointList[nextPointIdx()].hasLayer(i))
					{
						mAnimsList[a].blendSpeed = -mAnimsList[a].maxBlendSpeed;
						mAnimsList[a].blendType = SpeedBlend;				
					}

					if (i == 1)
					{
						mAnimsList[a].blendSpeed = -mAnimsList[a].maxBlendSpeed;
						mAnimsList[a].blendType = SpeedBlend;
					}
				}
			}
		}
		if (!isEndPoint(mCurPoint))
			finishPoint(point, skip_all);
	}
	else
		toLogEx("ERROR: mPointList size < mCurPoint in CAnimatableObject::skip\n");
}

bool CAnimatableObject::incrementCurPoint()
{
	bool has_stop = false;

	if (isEndPoint(mCurPoint))
		has_stop = true;
	else
		mCurPoint = nextPointIdx();
	return has_stop;
}

void CAnimatableObject::sendMsgToAnimPointTarget(const AnimationPoint& p)
{
	if (p.mTarget)
	{
		CScriptMsg m;
		m.messageId = CScriptMsg::MSG_NOTIFY;
		m.pTargetObj = app._findObjectByHandle(p.mTarget);
		m.param.lparam = p.mParam;
		m.param.rparam = MT_STOP_ANIMATION;
		m.pSourceObj = this;

		app.postMsg(m);
	}
}

void CAnimatableObject::AnimationPoint::init(CAnimatableObject& animObj, size_t previous)
{
	for(size_t i = 0; i < layers(); i++)
	{
		mLayers[i].ntimes = mLayers[i].max_ntimes; // �� ������, ���� ������� �������� ���������, ����� ������ ������ ���� ��������������� ntimes ������ ��������
		int a = mLayers[i].anim;
		size_t layer = mLayers[i].layer;
		if (previous != (unsigned int)-1 && // ���� � ������� �������� ���� ����������
			(!animObj.mPointList[previous].hasLayer(layer) ||
			a != animObj.mPointList[previous].anim(layer))
			)
			animObj.mAnimsList[a].pAnimState->setTimePosition(0);

		if (mLayers[i].layer != 1) // layer 1 - ��� �������� ��� ��� ��������, ��� ��� ������������ ������ ������� �����
		{
			animObj.mAnimsList[a].pAnimState->setWeight(1.0f);
		}

		animObj.mAnimsList[a].enable = true;
		animObj.mAnimsList[a].curPoint = animObj.mCurPoint;
		animObj.mAnimsList[a].animSpeed = mLayers[i].anim_speed/1000.f;	
		animObj.mAnimsList[a].totalPlayTime = 0;
	}

	if (mNeedToMove)
	{
		mStartPos = animObj.pSNode->getPosition();
		animObj.mRotSpeed = animObj.mAnimsList[anim(0)].rotSpeed;
		animObj.setDestRotationToPoint(mEndPos);
	}
}

void CAnimatableObject::finishPoint(const AnimationPoint& p, bool all)
{
	sendMsgToAnimPointTarget(p);
	size_t old_cur_point = mCurPoint;

	if (all)
	{
		mCurPoint = mPointList.size()-1;  // ��������� � ��������� �������� � �������
	}
	else if (incrementCurPoint())
	{
		stop();
		return;
	}
	mPointList[mCurPoint].init(*this, old_cur_point);
}

void CAnimatableObject::processAnimations(Real inc, const AnimationPoint& point)
{
	bool hasBlending = false;
	for(size_t i = 0; i < mAnimsList.size(); i++)
	{
		AnimationStateEx *pAnimStateEx = &mAnimsList[i];
		Ogre::AnimationState *pAnimState = pAnimStateEx->pAnimState;

		if (!pAnimStateEx->enable)
			continue;
		
		float _inc = inc;
		size_t anim_layer = pAnimStateEx->layer;
		//������� ����� ��� ��������
		if (pAnimState->getTimePosition() + _inc >= pAnimState->getLength())
		{
			if (pAnimStateEx->curPoint == mCurPoint)
			{
				point.ntimes(anim_layer)--;

				CVirtualScript::sendCMsgHandle(getHandle(), CMsg(1000, 0));
			}
		
			float d = pAnimState->getTimePosition() + _inc - pAnimState->getLength(); 
			pAnimState->setTimePosition(d);
		}
		else
		{
			pAnimState->addTime(_inc);
		}

		pAnimStateEx->totalPlayTime += _inc;
		
		float curAnimTime = pAnimState->getTimePosition();

		//-------------------------------------------------
//		for(size_t k = 0; k < mPointList.size(); k++)
		{
			bool foundlayer = false;
			size_t k = mCurPoint;
			for(size_t j = 0; j < mPointList[k].mLayers.size(); j++)
			{
				if (mPointList[k].mLayers[j].layer == anim_layer)
					foundlayer = true;
			}

			if (!foundlayer)
			{
				if (anim_layer == 1) 
				{
					pAnimStateEx->blendType = SpeedBlend;
					pAnimStateEx->blendSpeed = -pAnimStateEx->maxBlendSpeed;
				}
				else
				{
					toLogEx("Warning: Current point '%u' has no layer '%d' for active animation '%s'!\n",
						mCurPoint,
						anim_layer,
						pAnimState->getAnimationName().c_str());
					return;
				}
			}
		}

		//-------------------------------------------------
		if (anim_layer == 0)
		{
			//��� �� �����, �� ��� ���� ��������
			if (pAnimStateEx->timeToBlend == 0.0)
			{
				//������� ��������
				if (point.anim(anim_layer) == (int)i && point.ntimes(anim_layer) == 1 && curAnimTime + _inc >= pAnimState->getLength())
				{
					finishPoint(point);

					pAnimStateEx->enable = false;
					pAnimStateEx->pAnimState->setWeight(0);

					//if (mPointList[mCurPoint].anim )
					mAnimsList[mPointList[mCurPoint].anim(anim_layer)].pAnimState->setTimePosition(0);
					mAnimsList[mPointList[mCurPoint].anim(anim_layer)].pAnimState->setWeight(1.0f);
					updateSkeleton();
					return;
				}
			}
			else
			{
				if (point.anim(anim_layer) == (int)i && // ���� �������������� �������� ������� (����������� ������� anim point)
					pAnimStateEx->blendType == NoneBlend)
					{
						if (isEndPoint(mCurPoint)) // ���� ��� ����� ������ ������� �������� � ���� ������ �� ��������
						{
							if (point.ntimes(anim_layer) == 0)
							{
								sendMsgToAnimPointTarget(point); // ���������� ������ �������� ����� ������, �� �������� ��������� � ����������
								point.ntimes(anim_layer) = -1;  // ������������� �������� ������������ � �������� ��������, ��� ��������� � ���������� ��� �������
							}
						}
						else
						{ //  ��������� � ���������� ������ ������
							if (point.ntimes(anim_layer) == 1 
								&& curAnimTime >= pAnimState->getLength() - pAnimStateEx->timeToBlend)
							{
								if (isNextAnimNotSame(anim_layer))
								{
									pAnimStateEx->blendSpeed = -1.0f;
									pAnimStateEx->blendZeroTime = pAnimState->getLength();
									pAnimStateEx->blendOneTime = curAnimTime;
									pAnimStateEx->blendType = TimeBlend;
								}

								finishPoint(point);
							}
						}
					}
			}
		
			if (pAnimStateEx->blendType != NoneBlend)
			{
				Real weight = 0;

				if (pAnimStateEx->blendType == TimeBlend)
				{
					Real _curAnimTime = curAnimTime;

					if (curAnimTime < std::min(pAnimStateEx->blendZeroTime,pAnimStateEx->blendOneTime))
						_curAnimTime += std::max(pAnimStateEx->blendZeroTime,pAnimStateEx->blendOneTime);
								
					weight = (pAnimStateEx->blendZeroTime - _curAnimTime)/(pAnimStateEx->blendZeroTime - pAnimStateEx->blendOneTime);
					
				}
				else
				{
					weight = pAnimState->getWeight();
					weight += pAnimStateEx->blendSpeed*_inc;

					if (pAnimStateEx->blendSpeed == 0.0)
						weight = -0.01f;
				}


				if (weight > 1.0f) 
				{ 
					weight = 1.0f; 
					pAnimStateEx->blendSpeed = 0;
					pAnimStateEx->blendType = NoneBlend;

					mAnimsList[mPointList[mCurPoint].anim(anim_layer)].blendType = NoneBlend;
				}

				if (weight < 0) 
				{ 
					weight = 0; 
					pAnimStateEx->blendSpeed = 0;
					pAnimStateEx->blendType = NoneBlend;
					
					mAnimsList[mPointList[mCurPoint].anim(anim_layer)].blendType = NoneBlend;
					pAnimStateEx->enable = false;
				}

				pAnimState->setWeight(weight);

				hasBlending = true;
			}
		
		}
		else if (anim_layer == 1)
		{
			if (pAnimStateEx->blendType == NoneBlend)
			{
				int snd_id = mPointList[mCurPoint].sound_id(anim_layer);

#ifndef NO_SFX
				float weight = sfxCore.getLevel(snd_id, pAnimStateEx->totalPlayTime);
#else
				float weight = 1;
#endif

				if (fabsf(weight) <= 1.0f)
					pAnimState->setWeight(weight);

				hasBlending = true;
			}
			else
			{
				Real weight = 0;
				weight = pAnimState->getWeight();
				weight += pAnimStateEx->blendSpeed*_inc;

				if (pAnimStateEx->blendSpeed == 0.0)
					weight = -0.01f;

				if (weight < 0) 
				{ 
					weight = 0; 
					pAnimStateEx->blendSpeed = 0;
					pAnimStateEx->blendType = NoneBlend;
					pAnimStateEx->enable = false;
				}

				pAnimState->setWeight(weight);

				hasBlending = true;
			}
		}
	}

	normalizeAnimBlendings();

	if (hasBlending)
	{
		updateSkeleton();
	}
}

void CAnimatableObject::processMovement(Real inc, const AnimationPoint& point)
{
	if (point.mNeedToMove)
	{
		Vector3 curPos = pSNode->getPosition();
		Vector3 dir = point.mEndPos - curPos;
		Real distance = dir.normalise();
		curPos += dir*(point.mMoveSpeed*inc*mRotProgress);
		pSNode->setPosition(curPos);
		Real stop_dist = 2.0f*point.mMoveSpeed*inc;
		if (distance < stop_dist)
		{
			int a = mPointList[mCurPoint].anim(0);
			mAnimsList[a].blendSpeed = -mAnimsList[a].maxBlendSpeed;
			mAnimsList[a].blendType = SpeedBlend;				
            finishPoint(point);
			if (point.mNeedToRot)
			{
				mRotation = true;
				mRotProgress = 0;
				mDestRot = point.mRotation;
				mScrRot = pSNode->getOrientation();
			}
		}
	}
	if (mRotation)
	{
		mRotProgress += mRotSpeed * inc;

		if (mRotProgress > 1.0f)
		{
			mRotation = false;
			mRotProgress = 1.0f;
		}
		
		Quaternion delta = Quaternion::Slerp(mRotProgress, mScrRot, mDestRot,true);
		pSNode->setOrientation(delta);
	}
}

void CAnimatableObject::setDestRotationToPoint(Vector3 &point)
{
	mRotation = true;
	mRotProgress = 0;
	mScrRot = pSNode->getOrientation();
	Vector3 dir = point - pSNode->getPosition();
	dir.normalise();
	dir.z = 0;

	Vector3 forwardDir = mScrRot*Vector3::UNIT_X;
	Quaternion q = _getRotationTo(forwardDir,dir,Vector3::UNIT_Z);
	mDestRot = mScrRot*q;
	mDestRot.normalise();
}

void CAnimatableObject::processFrame( const Ogre::FrameEvent& evt )
{
	if (mIsPlay)
	{
		Real inc = evt.timeSinceLastFrame;
		const AnimationPoint& point = mPointList[mCurPoint];
		processMovement(inc, point);
		processAnimations(inc, point);
	}
}

void CAnimatableObject::updateSkeleton()
{
	//TODO: ����� ��������������!

	Ogre::Skeleton::LinkedSkeletonAnimSourceIterator linkIter = pBaseSkel->getLinkedSkeletonAnimationSourceIterator();
	int animNum = 0;
	while(linkIter.hasMoreElements())
	{
		Ogre::LinkedSkeletonAnimationSource animSrc = linkIter.getNext();

		Ogre::Skeleton::BoneIterator boneIter = pBaseSkel->getBoneIterator();

		//Ogre::AnimationState* pCurState = findAnimState(animSrc.skeletonName.c_str()); 
		String animName = animSrc.pSkeleton->getAnimation(0)->getName();

		int animId = findAnim(animName);

		Ogre::AnimationState* pCurState = mAnimsList[animId].pAnimState;// findAnimState(animSrc.skeletonName.c_str()); 

		if (!pCurState)
		{
			toLogEx("Can't find anim state for %s\n", animSrc.skeletonName.c_str());
			continue;
		}

		Real animWeight = pCurState->getWeight();

		if (animWeight == 0 /*|| !pCurState->getEnabled()*/) continue;

		animNum++;

		while(boneIter.hasMoreElements())
		{
			Ogre::Bone *skelBone = boneIter.getNext();
			Ogre::Bone *animBone = animSrc.pSkeleton->getBone(skelBone->getName());

			//-----------------------------

			if (!findBoneInLayer(skelBone,mAnimsList[animId].layer))
				continue;

			//-----------------------------

			Vector3 v1 = skelBone->getInitialPosition();
			Vector3 v2 = animBone->getInitialPosition();

			if (animNum == 1 || v1.isNaN())
				v1 = v2;
			else
				v1 += animWeight*(v2 - v1);

	        skelBone->setPosition(v1);		
		
			Quaternion q1 = skelBone->getInitialOrientation();
			Quaternion q2 = animBone->getInitialOrientation();

			if (animNum == 1 || q1.isNaN())
				q1 = q2;
       		else
				q1 = Quaternion::nlerp(animWeight,q1,q2,true);

			skelBone->setOrientation(q1);
			skelBone->setInitialState();
		}
	}
}

void CAnimatableObject::setBlendState(int anim, Real speed)
{
	for(size_t i = 0; i < mAnimsList.size(); i++)
		mAnimsList[i].blendSpeed = 0;
		
	mAnimsList[anim].blendSpeed = speed;
	mAnimsList[anim].enable = true;
}

void CAnimatableObject::normaliseAnimWeights(size_t n)
{
	Real max_weight = 0;
	
	if (mAnimsList[n].pAnimState) 
		max_weight = 1.0f - mAnimsList[n].pAnimState->getWeight();

    for(size_t i = 0; i < mAnimsList.size(); i++)
	{
		Ogre::AnimationState *pAnimState = mAnimsList[i].pAnimState;
		if (i != n) 
			pAnimState->setWeight(pAnimState->getWeight()*max_weight);
	}
}

Quaternion CAnimatableObject::_getRotationTo(const Vector3& src, const Vector3& dest,
						  const Vector3& fallbackAxis)
{
	// Based on Stan Melax's article in Game Programming Gems
	Quaternion q;
	// Copy, since cannot modify local
	Vector3 v0 = src;
	Vector3 v1 = dest;
	v0.normalise();
	v1.normalise();
	Vector3 c = v0.crossProduct(v1);
	Real d = v0.dotProduct(v1);
	// If dot == 1, vectors are the same
	if (d >= 1.0f)
	{
		return Quaternion::IDENTITY;
	}

	if (d < -1 + 1e-4f)
	{
		q.FromAngleAxis(Ogre::Radian(Ogre::Math::PI), fallbackAxis);
		return q;
	}

	Real s = Ogre::Math::Sqrt( (1+d)*2 );
	if (s < 1e-6f)
	{
		if (fallbackAxis != Vector3::ZERO)
		{
			// rotate 180 degrees about the fallback axis
			q.FromAngleAxis(Ogre::Radian(Ogre::Math::PI), fallbackAxis);
		}
		else
		{
			// Generate an axis
			Vector3 axis = Vector3::UNIT_X.crossProduct(src);
			if (axis.isZeroLength()) // pick another if colinear
				axis = Vector3::UNIT_Y.crossProduct(src);
			axis.normalise();
			q.FromAngleAxis(Ogre::Radian(Ogre::Math::PI), axis);
		}
	}
	else
	{
		Real invs = 1 / s;

		q.x = c.x * invs;
		q.y = c.y * invs;
		q.z = c.z * invs;
		q.w = s * 0.5f;
	}
	return q;
}
