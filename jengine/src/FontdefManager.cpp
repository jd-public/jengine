

#include "stdafx.h"

#include "FontdefManager.h"

#include "StringTable.h"
#include <OgreFontManager.h>
#include <OgreFont.h>
#include <OgreMaterial.h>
#include <OgreTechnique.h>

#include <fstream>
#include "RapidXmlFunc.h"



using namespace Ogre;

CFontdefexManager::CFontdefexManager()
{
	mScriptPatterns.push_back("*.fontdefex");
	Ogre::ResourceGroupManager::getSingleton()._registerScriptLoader(this);
}

CFontdefexManager::~CFontdefexManager()
{
	clearFontdefexes();
	Ogre::ResourceGroupManager::getSingleton()._unregisterScriptLoader(this);
}

CFontdefexManager::CFontdefex* CFontdefexManager::addFontdefex()
{
	CFontdefex *pFontdefex = new CFontdefex();

	mFontdefexes.push_back(pFontdefex);

	return pFontdefex;
}

void CFontdefexManager::clearFontdefexes()
{
	for (std::vector<CFontdefex*>::iterator iter = mFontdefexes.begin(); iter != mFontdefexes.end(); ++iter)
		delete *iter;

	mFontdefexes.clear();
}

CFontdefexManager::CFontdefex* CFontdefexManager::getFontdefex(const Ogre::String &strFontdefex)
{
	for (std::vector<CFontdefex*>::iterator iter = mFontdefexes.begin(); iter != mFontdefexes.end(); ++iter)
		if ((*iter)->strName == strFontdefex)
			return *iter;

	return NULL;
}

Ogre::FontPtr CFontdefexManager::createTrueTypeFont(const Ogre::String &strFont, const Ogre::String &strSource,
													int nResolution, int nSize, std::vector<std::pair<int, int> > *pCodePointRanges)
{
	Ogre::FontPtr pFont = Ogre::FontManager::getSingleton().getByName(strFont, "Start").staticCast<Font>();
	if (!pFont.isNull())
	{
		pFont->clearCodePointRanges();
		pFont->unload();
	}
	else
	{
		pFont = Ogre::FontManager::getSingleton().create(strFont, "Start").staticCast<Font>();
	}

	pFont->setType(Ogre::FT_TRUETYPE);
	pFont->setTrueTypeResolution(nResolution);
	pFont->setTrueTypeSize((float)nSize);
	pFont->setSource(strSource);
	pFont->setCharacterSpacer(10);

	for (std::vector<std::pair<int, int> >::iterator iter = pCodePointRanges->begin(); iter != pCodePointRanges->end(); ++iter)
		pFont->addCodePointRange(Ogre::Font::CodePointRange(iter->first, iter->second));

	pFont->load();

	return pFont;
}

Ogre::FontPtr CFontdefexManager::createAndLoadFont(const Ogre::String &strFontdefex, const Ogre::String &strFont, int nCharHeight)
{
	CFontdefex *pFontdefex = getFontdefex(strFontdefex);
	if (pFontdefex == NULL)
		return Ogre::FontPtr();

	if (!Ogre::ResourceGroupManager::getSingleton().resourceExistsInAnyGroup(pFontdefex->strSource))
		return Ogre::FontPtr();

	int nWantedSizePairIndex = pFontdefex->findNearestCharHeight(nCharHeight);
	if (nWantedSizePairIndex == -1)
		return Ogre::FontPtr();

	FontPtr font = createTrueTypeFont(strFont, pFontdefex->strSource, pFontdefex->nResolution,
		pFontdefex->listSizePairs[nWantedSizePairIndex].first, &pFontdefex->listCodePointRanges);

	return font;
}

Ogre::FontPtr CFontdefexManager::createAndLoadFont(const Ogre::String &strFontdefex, const Ogre::String &strFont, int nCharHeight, SStringTable *pStringTable)
{
	CFontdefex *pFontdefex = getFontdefex(strFontdefex);
	if (pFontdefex == NULL)
		return Ogre::FontPtr();

	int nWantedSizePairIndex = pFontdefex->findNearestCharHeight(nCharHeight);
	if (nWantedSizePairIndex == -1)
		return Ogre::FontPtr();

	std::vector<std::pair<int, int> > pRanges;
	calcCodePointRanges(pStringTable, &pRanges);

	return createTrueTypeFont(strFont, pFontdefex->strSource, pFontdefex->nResolution,
		pFontdefex->listSizePairs[nWantedSizePairIndex].first, &pRanges);
}

Ogre::Real CFontdefexManager::getLoadingOrder() const
{
	// Load late, but before Atlas Manager
	return 88.0f;
}

const Ogre::StringVector& CFontdefexManager::getScriptPatterns() const
{
	return mScriptPatterns;
}

void CFontdefexManager::parseScript(Ogre::DataStreamPtr& stream, const Ogre::String& groupName)
{
	XmlParser parser(stream->getAsString().c_str());
	const XmlNode* pRoot = parser.getDoc().first_node("font");
	if(pRoot)
	{
		Ogre::String strName = getAttrib(pRoot, "name");
		Ogre::String strSource = getAttrib(pRoot, "source");
		int nResolution = getAttribInt(pRoot, "resolution", 96);

		CFontdefex *pFontdefex = addFontdefex();
		pFontdefex->strName = strName;
		pFontdefex->strSource = strSource;
		pFontdefex->nResolution = nResolution;

		// codepoint ranges
		for (const XmlNode* pElem = pRoot->first_node("codepoints"); pElem != NULL;
			pElem = pElem->next_sibling("codepoints"))
		{
			int nCodePointRangeFrom = getAttribInt(pElem, "from");
			int nCodePointRangeTo = getAttribInt(pElem, "to");
			pFontdefex->listCodePointRanges.push_back(std::pair<int, int>(nCodePointRangeFrom, nCodePointRangeTo));
		}

		// sizes pairs
		for (const XmlNode* pElem = pRoot->first_node("sizeinfo"); pElem != NULL;
			pElem = pElem->next_sibling("sizeinfo"))
		{
			int nFontSize = getAttribInt(pElem, "size");
			int nCharHeight = getAttribInt(pElem, "charheight");
			pFontdefex->listSizePairs.push_back(std::pair<int, int>(nFontSize, nCharHeight));
		}
	}
}

void CFontdefexManager::calcCodePointRanges(SStringTable *pStringTable, std::vector<std::pair<int, int> > *pCodePointRanges)
{
	pCodePointRanges->clear();

	SStringTable::TStringMap* pStringMap = pStringTable->getStringMapPtr();

	bool pPoints[0xFFFF];
	memset(pPoints, 0, 0xFFFF*sizeof(bool));

	for (SStringTable::TStringMap::iterator iter = pStringMap->begin(); iter != pStringMap->end(); ++iter)
	{
		Ogre::DisplayString &strData = (*iter).second;

		std::set<Ogre::DisplayString::code_point>::iterator it;
		for(size_t c = 0; c < strData.size(); c++)
		{
			Ogre::DisplayString::code_point cp = strData.at(c);
			pPoints[cp] = true;
		}
	}

	int nInRange = -1;
	for (Ogre::uint32 i = 0; i < 0xFFFF; i++)
	{
		if (pPoints[i] == (nInRange != -1))
			continue;

		if (pPoints[i])
			nInRange = i;
		else
		{
			pCodePointRanges->push_back(Ogre::Font::CodePointRange(nInRange, i - 1));
//			toLogEx("\n add range %d - %d", nInRange, i - 1);
			nInRange = -1;
		}
	}
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

void CFontdefexManager::createFontdefexFile(SStringTable *pStringTable, const Ogre::String &strName,
										  const Ogre::String &strSource, int nResolution, const Ogre::String &strFileName)
{
	std::vector<std::pair<int, int> > pRanges;
	calcCodePointRanges(pStringTable, &pRanges);

	Ogre::FontPtr pFont = Ogre::FontManager::getSingleton().create(strName, "Start").staticCast<Font>();
	pFont->setType(Ogre::FT_TRUETYPE);
	for (std::vector<std::pair<int, int> >::iterator iter = pRanges.begin(); iter != pRanges.end(); ++iter)
		pFont->addCodePointRange(*iter);
	pFont->setTrueTypeResolution(nResolution);
	pFont->setSource(strSource);

	std::vector<std::pair<int, int> > pSizes;

	for (int i = 5; i < 50; i++)
	{// ������ �� ���� ��������
		pFont->setTrueTypeSize((float)i);
		pFont->load();
		Ogre::Font::GlyphInfo gi = pFont->getGlyphInfo('A');

		float fHeightUV = gi.uvRect.bottom - gi.uvRect.top;

		float fPixelHeight = fHeightUV*pFont->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureDimensions().second;
		//toLogEx("\n:Font: Char height pixels:%.4f / %d = relative:%.4f\n", pixheight, app.getWindowResolution().y, pixheight/((float)app.getWindowResolution().y));
		pSizes.push_back(std::pair<int, int>(i, (int)fPixelHeight));
		pFont->unload();
	}

	std::ofstream of(strFileName.c_str());
	of << "<font name=\"" << strName << "\" source=\"" << strSource << "\" resolution=\"" << nResolution << "\">\n";
	of << "\n";
	for (std::vector<std::pair<int, int> >::iterator iter = pRanges.begin(); iter != pRanges.end(); ++iter)
		of << "\t<codepoints from=\"" << iter->first << "\" to=\"" << iter->second << "\" />\n";
	of << "\n";
	for (std::vector<std::pair<int, int> >::iterator iter = pSizes.begin(); iter != pSizes.end(); ++iter)
		of << "\t<sizeinfo size=\"" << iter->first << "\" charheight=\"" << iter->second << "\" />\n";
	of << "\n";
	of << "</font>\n";
	of.close();
}

#endif // OGRE_PLATFORM_WIN32

//---------------------------------------------------------------------
template<> CFontdefexManager *Singleton<CFontdefexManager>::msSingleton = NULL;
CFontdefexManager* CFontdefexManager::getSingletonPtr()
{
    return msSingleton;
}
CFontdefexManager& CFontdefexManager::getSingleton()
{
    assert( msSingleton );  return ( *msSingleton );
}
