#include "stdafx.h"
#include "StringTable.h"
#include <OgreResourceGroupManager.h>

SStringTable::SStringTable()
{
	mScanCodePoints = false;
}

SStringTable::~SStringTable()
{
}

void SStringTable::add(const String& key, Ogre::DisplayString &data)
{
	size_t f; 
	while( (f = data.find("\\n")) != Ogre::DisplayString::npos)
	{
		data.replace(f,2,"\n");
	}

	DisplayString space(1, 160);
	while( (f = data.find(space)) != Ogre::DisplayString::npos)
	{
		data.replace(f, 1, " ");
	}

	// remove double spaces
	while( (f = data.find("  ")) != Ogre::DisplayString::npos)
	{
		data.replace(f,2," ");
	}


	if(mScanCodePoints)
	{
		std::set<Ogre::DisplayString::code_point>::iterator it;

		for(size_t c = 0; c < data.size(); c++)
		{
			Ogre::DisplayString::code_point cp = data.at(c);

			it = mCodePoints.find(cp);

			if( it == mCodePoints.end() ) 
				mCodePoints.insert(cp);
		}
	}

	mTable.insert(make_pair(key,data));
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
void SStringTable::load(const String& fileName)
{
	char buf[2048];
	wchar_t wbuf[2048];

	memset(buf,0,sizeof(buf));

	bool isUtf8 = false;
	
	if (!Ogre::ResourceGroupManager::getSingleton().resourceExistsInAnyGroup(fileName))
	//if (!CVirtualScript::resourceExists(fileName))
	{
		if (Ogre::ResourceGroupManager::getSingleton().resourceExistsInAnyGroup(fileName + String(".utf8")))
			isUtf8 = true;
		else
			toLogEx("ERROR: String table %s not found\n", fileName.c_str());
	}
	String tfilename = fileName;
	if (isUtf8) tfilename += ".utf8";

	Ogre::DataStreamPtr pStream = Ogre::ResourceGroupManager::getSingleton().openResource( tfilename );

	size_t strings_num = 0;
	size_t bytes_num = 0;

	FILE *file = fopen("test.xml","wb");
	if (file)
	{
		fprintf(file, "%c%c%c<scene>\n",0xef,0xbb,0xbf);
		fprintf(file, "  <strings_table lang=\"en\">\n");

		while (!pStream->eof())
		{
			size_t s;
			s = pStream->readLine(buf, 2048, "\n");

			if (s)
			{
				char* delim = strstr(buf,":");
				if (!delim)
					continue;
				if (!isUtf8)
				{
					MultiByteToWideChar( CP_ACP, 0, buf,
						static_cast<int>(strlen(buf)+1), wbuf,
						sizeof(wbuf)/sizeof(wbuf[0]) );
				}
				delim[0] = 0;
				size_t delim_place = delim - buf;
				Ogre::DisplayString ds;
				if (!isUtf8)
				{
					wchar_t* wbuf2 = wbuf + delim_place + 2;
					ds = Ogre::DisplayString(wbuf2); 
				}
				else
				{
					char* buf2 = buf + delim_place + 2;
					ds = Ogre::DisplayString(buf2); 
				}

				String key(buf);
				add(key, ds);
				fprintf(file,"    <str id=\"%s\">\n",key.c_str());
				fprintf(file,"      <en data=\"%s\"/>\n", ((String)ds).c_str());
				fprintf(file,"    </str>\n");

				strings_num++;

				if (!isUtf8)
					bytes_num += sizeof(wbuf[0])*s; 
				else
					bytes_num += sizeof(buf[0])*s; 
			}
		}

		if (!isUtf8)
			toLogEx("STRING TABLE: <%s> - %d strings in %.2f Kb loaded\n", tfilename.c_str(), strings_num, bytes_num/1024.f);
		else
			toLogEx("STRING TABLE: <%s> - %d utf8-strings in %.2f Kb loaded\n", tfilename.c_str(), strings_num, bytes_num/1024.f);

		pStream->close();
		pStream.setNull();

		//----------------------
		fprintf(file,"  </strings_table>\n");
		fprintf(file,"</scene>\n");


		fclose(file);
	}
}
#endif

bool SStringTable::isStringExist(const String& id)
{
	return mTable.find(id) != mTable.end();
}

Ogre::DisplayString SStringTable::getString(const String& id)
{
	OGRE_HashMap<String, Ogre::DisplayString>::iterator iter = mTable.find(id);
	if ( iter != mTable.end() ) 
		return iter->second;


	Ogre::DisplayString strRet(L"str ");
	strRet.append(id);
	strRet.append(" not found");

	return strRet;
}

void SStringTable::createFontdef()
{
	if(mScanCodePoints)
	{
		FILE *file = fopen("C:\\out.fontdef","wb");

		fprintf(file,"\n//Auto-generated fontdef file for %d code points\n\n",mCodePoints.size());
		fprintf(file,"00_font_01\n{\n");
		fprintf(file,"type\ttruetype\n");
		fprintf(file,"source\tgeorgia.ttf\n");
		fprintf(file,"size\t16\n");
		fprintf(file,"resolution\t96\n");

		fprintf(file,"code_points\t");

		std::set<Ogre::DisplayString::code_point>::iterator it;

		for (it=mCodePoints.begin(); it!=mCodePoints.end(); ++it)
			fprintf(file,"%u-%u ",(*it),(*it));

		fprintf(file,"\n}");
			
		fclose(file);
	}
}

Ogre::DisplayString SStringTable::getString(int i)
{
	char chBuf[32];
	sprintf(chBuf, "%05d", i);
	Ogre::String strBuf = String(chBuf);
	if (isStringExist(strBuf))
		return getString(strBuf);

	sprintf(chBuf, "%d", i);
	strBuf = String(chBuf);
	return getString(strBuf);
}

SStringTable::TStringMap& SStringTable::getStringMap()
{
	return mTable;
}

SStringTable::TStringMap* SStringTable::getStringMapPtr()
{
	return &mTable;
}
