#include "stdafx.h"

#include "DataArchive.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	#include <sys/stat.h>
#endif

#include <zlib.h>

#include "MainApplication.h"




//-----------------------------------------------------------------------
SimpleMemManager DataArchive::mMem;


#ifdef __USE_WINDOWS_LOW_LEVEL_IO__

	HANDLE FileDataStream::hFile = 0;
	Ogre::ulong FileDataStream::mPos = 0;
	Ogre::ulong FileDataStream::mState = 0;

	OVERLAPPED FileDataStream::ov;
	Ogre::uchar *FileDataStream::mBuf = 0;
	size_t FileDataStream::mBufSize = 0;
	size_t FileDataStream::mBufPos = 0;


	//-----------------------------------------------------------------------
	void FileDataStream::open(const String& name)
	{
		mPos = 0;

		hFile = CreateFile(name.c_str(),   // file to open
			GENERIC_READ,          // open for reading
			FILE_SHARE_READ,       // share for reading
			NULL,                  // default security
			OPEN_EXISTING,         // existing file only
			FILE_ATTRIBUTE_READONLY/* | FILE_FLAG_OVERLAPPED*/ | FILE_FLAG_RANDOM_ACCESS, // normal file
			NULL);                 // no attr. template
	};

	FileDataStream::~FileDataStream()
	{
		close();
	}

	size_t FileDataStream::read(void* buf, Ogre::ulong count)
	{
		if (!buf)
			return 0;

		Ogre::ulong  dwBytesRead = 0;

		bool read_from_buf = false;
		OVERLAPPED _ov = {0};
		_ov.Offset = mPos;
		ReadFile(hFile, buf, count, &dwBytesRead, &_ov);
		mPos+=dwBytesRead;

		return dwBytesRead;
	}

	void FileDataStream::skip(Ogre::ulong count)
	{

	}

	void FileDataStream::seek( Ogre::ulong pos )
	{
		mPos = pos;
	}

	size_t FileDataStream::tell()
	{
		return mPos;
	}

	bool FileDataStream::eof()
	{
		return true;
	}

	void FileDataStream::close()
	{
		CloseHandle(hFile);
	}

#endif

//-----------------------------------------------------------------------
DataDataStream::DataDataStream(Ogre::ulong _dataPos,
							   Ogre::ulong _fileSize,
							   Ogre::ulong _filePakedSize,
							   DataStream *_mainStream,
							   const String& name)
								:
								mDataPos(_dataPos),
								mFileSize(_fileSize),
								mPackedSize(_filePakedSize),
								mMainStream(_mainStream),
								mDecompressBuffer(NULL),
								mDecompBufferSize(NULL),
								mReadBufferSize(mSize),
								mReadBuffer(NULL)

{
	mSize = _fileSize;
	mName = name;

	_init();
}

void DataDataStream::_init()
{
	mReadBufferSize = mSize;
	mReadBuffer = DataArchive::mMem.alloc(mReadBufferSize);

	mPos = mReadBuffer;
	mEnd = mReadBuffer + mSize;

	memset(mReadBuffer,0,mReadBufferSize);

	mDataReady = false;

	_readData();
}

void DataDataStream::_readData()
{
	if (mDataReady)
		return;

	#ifndef __USE_WINDOWS_LOW_LEVEL_IO__
		mMainStream->seek(mDataPos);
	#else
		FileDataStream::seek(mDataPos);
	#endif

	if (mSize == mPackedSize)
	{
		#ifndef __USE_WINDOWS_LOW_LEVEL_IO__
			mMainStream->read(mReadBuffer, mSize);
		#else
			FileDataStream::read(mReadBuffer, mSize);
		#endif

	}
	else
	{
		mDecompBufferSize = mPackedSize;
		mDecompressBuffer = DataArchive::mMem.alloc(mDecompBufferSize);
		memset(mDecompressBuffer,0,mDecompBufferSize);

		#ifndef __USE_WINDOWS_LOW_LEVEL_IO__
			mMainStream->read(mDecompressBuffer, mPackedSize);
		#else
			FileDataStream::read(mDecompressBuffer, mPackedSize);
		#endif


		uLongf size = mSize;
		int res = uncompress(mReadBuffer, &size, mDecompressBuffer, mPackedSize);

		DataArchive::mMem._free(mDecompressBuffer);

		if (res != Z_OK)
			toLogEx("ERROR: Zlib decompress error on file %s\n", mName.c_str());
	}
	mDataReady = true;
}


DataDataStream::~DataDataStream()
{
	close();
}

size_t DataDataStream::readLine(char* buf, size_t maxCount, const String& delim )
{
	// Deal with both Unix & Windows LFs
	bool trimCR = false;
	if (delim.find_first_of('\n') != String::npos)
	{
		trimCR = true;
	}
	size_t pos = 0;

	// Make sure pos can never go past the end of the data
	while (pos < maxCount && mPos < mEnd)
	{
		if (delim.find(*mPos) != String::npos)
		{
			// Trim off trailing CR if this was a CR/LF entry
			if (trimCR && pos && buf[pos-1] == '\r')
			{
				// terminate 1 character early
				--pos;
			}

			// Found terminator, skip and break out
			++mPos;
			break;
		}

		buf[pos++] = *mPos++;
	}

	// terminate
	buf[pos] = '\0';

	return pos;
}

size_t DataDataStream::skipLine(const String& delim)
{
	size_t pos = 0;

	// Make sure pos can never go past the end of the data
	while (mPos < mEnd)
	{
		++pos;
		if (delim.find(*mPos++) != String::npos)
		{
			// Found terminator, break out
			break;
		}
	}

	return pos;
}

size_t DataDataStream::read(void* buf, size_t count)
{
	 size_t cnt = count;

	 // Read over end of memory?
     if (mPos + cnt > mEnd)
          cnt = mEnd - mPos;
     if (cnt == 0)
          return 0;

     memcpy(buf, mPos, cnt);
     mPos += cnt;
     return cnt;
}

void DataDataStream::skip(long count)
{
	size_t newpos = (size_t)( ( mPos - mReadBuffer ) + count );
    assert( mReadBuffer + newpos <= mEnd );
    mPos = mReadBuffer + newpos;
}

void DataDataStream::seek( size_t pos )
{
	assert( mReadBuffer + pos <= mEnd );
    mPos = mReadBuffer + pos;
}

size_t DataDataStream::tell() const
{
	return mPos - mReadBuffer;
}

bool DataDataStream::eof() const
{
	return mPos >= mEnd;
}

void DataDataStream::close()
{
	DataArchive::mMem._free(mReadBuffer);
	mMainStream = 0;
}
//-----------------------------------------------------------------------
DataArchive::DataArchive(const String& name, const String& archType )
: Archive(name, archType)
{
}
//-----------------------------------------------------------------------
DataArchive::~DataArchive()
{
	unload();
}

//-----------------------------------------------------------------------
bool DataArchive::findFile(String& _filename, FileListIterator &it) const
{
	String filename = _filename;
	it = mFileList.find(filename);

	if ( it != mFileList.end() )
	return true;

	return false;
}
//-----------------------------------------------------------------------
void DataArchive::load()
{
	mSize = 0;
	mDataPos = 0;
	char* str = NULL;

	/*bool bLowMemModeEnabled = app.lowMemModeEnabled();
	if (bLowMemModeEnabled)
	{
		FileDataStream::open(mName.c_str());
		toLogEx("Use file stream\n");
	}*/

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID

	 mFileMemStream = app.getAppState().openAPKFile(mName);

#else


	#ifdef __USE_WINDOWS_LOW_LEVEL_IO__
		FileDataStream::open(mName.c_str());
	#else
		file = fopen(mName.c_str(),"rb");

		if(!file)
		{
			toLogEx("\n\nERROR: Can't open data archive %s",mName.c_str());
		}

		/*Ogre::DataStreamPtr*/ mFileMemStream = Ogre::DataStreamPtr(new Ogre::FileHandleDataStream(file));
	#endif

#endif

#ifndef __USE_WINDOWS_LOW_LEVEL_IO__

	char buf[10];
	mFileMemStream->read(buf,10);
	mFileMemStream->read(&mSize,sizeof(int));
	mFileMemStream->read(&totalSize,sizeof(long));

	int data_pos = 0;

	int buf_size = 1024;

	str = new char[buf_size];

	for(int i = 0; i < mSize; i++)
	{
		FileInfo inf;

		mFileMemStream->read(&inf.size,sizeof(long));
		mFileMemStream->read(&inf.packed_size,sizeof(long));

		mFileMemStream->read(&inf.flags,sizeof(int));
		mFileMemStream->read(&inf.unused,sizeof(int));

		if(inf.packed_size == 0)
		{
			inf.packed_size = inf.size;
		}

		int len;
		mFileMemStream->read(&len,sizeof(int));

		if(buf_size < len)
		{
			buf_size = len+1;

			delete[] str;
			str = new char[buf_size];
		}

		memset(str,0,buf_size);
		mFileMemStream->read(str,len);

		inf.name = String(str);


		mFileMemStream->read(&len,sizeof(int));

		if(buf_size < len)
		{
			buf_size = len+1;

			delete[] str;
			str = new char[buf_size];
		}

		memset(str,0,buf_size);

		mFileMemStream->read(str,len);

		inf.path = String(str);

		Ogre::StringUtil::toLowerCase(inf.name);
		Ogre::StringUtil::toLowerCase(inf.path);

		//
		inf.pos = data_pos;
		data_pos += inf.packed_size + sizeof(int);

		mFileList.insert(make_pair(inf.name,inf));

	}

	delete[] str;


	mDataPos = mFileMemStream->tell();


#else

	char buf[10];
	FileDataStream::read(buf,10);
	FileDataStream::read(&mSize,sizeof(int));
	FileDataStream::read(&totalSize,sizeof(long));

	int data_pos = 0;
	int buf_size = 1024;
	str = new char[buf_size];
	for(int i = 0; i < mSize; i++)
	{
		FileInfo inf;

		FileDataStream::read(&inf.size,sizeof(long));
		FileDataStream::read(&inf.packed_size,sizeof(long));

		FileDataStream::read(&inf.flags,sizeof(int));
		FileDataStream::read(&inf.unused,sizeof(int));

		if (inf.packed_size == 0)
		{
			inf.packed_size = inf.size;
		}

		int len;
		FileDataStream::read(&len,sizeof(int));

		if (buf_size < len)
		{
			buf_size = len+1;

			delete[] str;
			str = new char[buf_size];
		}

		memset(str,0,buf_size);
		FileDataStream::read(str,len);

		inf.name = String(str);

		FileDataStream::read(&len,sizeof(int));
		if (buf_size < len)
		{
			buf_size = len+1;

			delete[] str;
			str = new char[buf_size];
		}

		memset(str,0,buf_size);

		FileDataStream::read(str,len);

		inf.path = String(str);

		Ogre::StringUtil::toLowerCase(inf.name);
		Ogre::StringUtil::toLowerCase(inf.path);

		//
		inf.pos = data_pos;
		data_pos += inf.packed_size + sizeof(int);

		mFileList.insert(make_pair(inf.name,inf));
	}
	delete[] str;
	mDataPos = static_cast<Ogre::ulong>(FileDataStream::tell());

#endif
}

//-----------------------------------------------------------------------
void DataArchive::unload()
{
	if (file)
	{
		DataArchive::mMem.freeAll();

		#ifdef __USE_WINDOWS_LOW_LEVEL_IO__
			FileDataStream::close();
		#else

			#if OGRE_PLATFORM != OGRE_PLATFORM_LINUX
                fclose(file);
			#endif

			//mFileMemStream->close();
			//mFileMemStream.setNull();
		#endif


		mFileList.clear();
		mSize = 0;
		file = 0;
	}
}
//-----------------------------------------------------------------------
#if OGRE_VERSION_MINOR == 9
	Ogre::DataStreamPtr DataArchive::open(const Ogre::String& _filename, bool readOnly) const
#else
	Ogre::DataStreamPtr DataArchive::open(const Ogre::String& _filename, bool readOnly)
#endif

{
	String filename = _filename;
	Ogre::StringUtil::toLowerCase(filename);

	FileListIterator it;
	findFile(filename,it);
	FileInfo fi = it->second;

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	//FIXME: For testing purposes!!!
	//-------------
	if (app.logEnabled())
	{
		FILE *ldr = fopen("load.log","a+");
		fprintf(ldr,"%s\n",fi.name.c_str());
		fclose(ldr);
	}
#endif

	//DataDataStream *pStream = new DataDataStream(mDataPos + fi.pos, fi.size, fi.packed_size, NULL, fi.name);

	DataDataStream *pStream = 0;

#ifdef __USE_WINDOWS_LOW_LEVEL_IO__
	pStream = new DataDataStream(mDataPos + fi.pos, fi.size, fi.packed_size, NULL, fi.name);
#else
	pStream = new DataDataStream(mDataPos + fi.pos, fi.size, fi.packed_size, mFileMemStream.get(),fi.name);
#endif


	return Ogre::DataStreamPtr(pStream);
}

//-----------------------------------------------------------------------
Ogre::StringVectorPtr DataArchive::list(bool recursive, bool dirs)
{
	Ogre::StringVectorPtr ret = Ogre::StringVectorPtr(new Ogre::StringVector());

	for (FileListIterator it = mFileList.begin(); it != mFileList.end(); ++it)
	    ret->push_back(it->first);

	return ret;
}
//-----------------------------------------------------------------------
Ogre::FileInfoListPtr DataArchive::listFileInfo(bool recursive, bool dirs)
{
 	Ogre::FileInfoList* fil = new Ogre::FileInfoList();

	for (FileListIterator it = mFileList.begin(); it != mFileList.end(); ++it)
	{
		Ogre::FileInfo fi;
		FileInfo f = it->second;

		fi.archive = this;
		fi.basename = f.name;
		//fi.filename = f.path + String("\\") + f.name;
		fi.filename = f.name;
		fi.path = f.path;
		fi.compressedSize = f.packed_size;
		fi.uncompressedSize = f.size;

		fil->push_back(fi);
	}
	return Ogre::FileInfoListPtr(fil);
}
//-----------------------------------------------------------------------
Ogre::StringVectorPtr DataArchive::find(const String& pattern,
                                 bool recursive,
								 bool dirs)
{
	Ogre::StringVectorPtr ret = Ogre::StringVectorPtr(new Ogre::StringVector());

	for (FileListIterator it = mFileList.begin(); it != mFileList.end(); ++it)
	{
	   if (Ogre::StringUtil::match(it->second.name, pattern, false))
	         ret->push_back(it->second.name);
	}

	return ret;
}
//-----------------------------------------------------------------------
//Ogre::FileInfoListPtr DataArchive::findFileInfo(const String& pattern,
//                                         bool recursive,
//										 bool dirs) 
#if OGRE_VERSION_MINOR == 9
	Ogre::FileInfoListPtr DataArchive::findFileInfo(const Ogre::String& pattern, bool recursive, bool dirs) const
#else
	Ogre::FileInfoListPtr DataArchive::findFileInfo(const Ogre::String& pattern, bool recursive, bool dirs)
#endif
{
	Ogre::FileInfoList* fil = new Ogre::FileInfoList();

	for (FileListIterator it = mFileList.begin(); it != mFileList.end(); ++it)
	{
		if (Ogre::StringUtil::match(it->second.name, pattern, false))
		{
			Ogre::FileInfo fi;
			FileInfo f = it->second;

			fi.archive = this;
			fi.basename = f.name;
			//fi.filename = f.path + String("\\") + f.name;
			fi.filename = f.name;
			fi.path = f.path;
			fi.compressedSize = f.packed_size;
			fi.uncompressedSize = f.size;

			if (f.name == "00_base.material") //�� ������ ���� ������, �� ������� ��� ���������
				fil->insert(fil->begin(),fi);
			else
				fil->push_back(fi);
	   }
  }

  return Ogre::FileInfoListPtr(fil);
}

//-----------------------------------------------------------------------
bool DataArchive::exists(const String& _filename)
{
	String filename = _filename;
	Ogre::StringUtil::toLowerCase(filename);
	FileListIterator it;
	return findFile(filename,it);
}
//-----------------------------------------------------------------------
const String& DataArchiveFactory::getType() const
{
	static String name = "Data";
	return name;
}
