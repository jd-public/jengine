#include "stdafx.h"

#if USE_OIS_FOR_INPUT == TRUE

#include "EventPlayer.h"

#include <OgrePlatform.h>

#include "MainApplication.h"
#include "AppOISListener.h"
#include "VirtualScript.h"

EventPlayer::EventPlayer(TPlayerMode m, const Ogre::String& filename) : mSaveFile(NULL), mPlayerMode(m), mCurEventNum(0)
{
	if (mPlayerMode == ePlay)
	{
		loadEvents(filename);
	}
	else
	{
		mSaveFile = fopen(filename.c_str(),"wb");
		fprintf(mSaveFile, "//ENGINE EVENT PLAYER FILE\n");
	}		

	mCreateTime = app.getTime();
}

EventPlayer::~EventPlayer()
{
	if (mSaveFile)
		fclose(mSaveFile);
}

OIS::MouseEvent EventPlayer::getMouseEvent(Event &e) const
{
	/// todo: calc with viewport demensions
	OIS::MouseState ms;
	Ogre::Real wndW = (Ogre::Real)app.getWindow()->getWidth();
	Ogre::Real wndH = (Ogre::Real)app.getWindow()->getHeight();
	ms.X.abs = static_cast<int>(e.pos.x*wndW);
	ms.Y.abs = static_cast<int>(e.pos.y*wndH);
	OIS::MouseEvent me(NULL,ms);
	return me;
}

void EventPlayer::process()
{
#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE && OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
	Event e = mEvents[mCurEventNum];

	if (e.time + mCreateTime < app.getTime())
	{
		mCurEventNum++;

		if (mCurEventNum > mEvents.size())
			return;

		//mouse
		if (e.type == 0)
		{
			OIS::MouseState ms;
			Real wndW = (Real)app.getWindow()->getWidth();
			Real wndH = (Real)app.getWindow()->getHeight();
			ms.X.abs = static_cast<int>(e.pos.x*wndW);
			ms.Y.abs = static_cast<int>(e.pos.y*wndH);
			OIS::MouseEvent me(NULL,ms);

#	if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			HWND hwnd;
			app.getWindow()->getCustomAttribute("WINDOW", &hwnd);
			
			RECT rcClip; 
			GetWindowRect(hwnd, &rcClip); 
			
			POINT p;
			p.x = me.state.X.abs;
			p.y = me.state.Y.abs;
			ClientToScreen(hwnd,&p);

			SetCursorPos(p.x,p.y);
#	endif			
			//move
			if (e.pressed == -1)
			{
				app.getOIS()->appMouseMoved(me);
			}
			//press
			else 
			{
				CGameObject* obj = app._findObjectByName(e.focused_obj.c_str());

				//check focus
				int tries = 0;

				while(1)
				{
					if (!obj)
						break;

					if (app.getFocusedObj() == obj)
						break;

					app.getOIS()->appMouseMoved(me);

					tries++;

					if (tries > 100)
					{
						toLogEx("ERROR: Event player can't found %s (try %d) %s!\n* * * * * * *\n",
							e.focused_obj.c_str(),
							tries,
							app.getFocusedObj() ? app.getFocusedObj()->pSNode->getName().c_str() : "null");
						
						Ogre::Vector2 pos;
						CVirtualScript::getViewPos(app.findObjectByName(e.focused_obj.c_str()), pos);

						float x = pos.x;
						float y = pos.y;

						Event _e = {e.time,e.type, e.key, e.pressed, Vector2(x, y), e.focused_obj};

						mEvents.insert(mEvents.begin() + mCurEventNum, _e);

						break;
					}
				}

				if (e.pressed == 1)
				{
					app.getOIS()->appMousePressed(me, (OIS::MouseButtonID)e.key);
				}
				else
				{
					app.getOIS()->appMouseReleased(me, (OIS::MouseButtonID)e.key);
				}
			}

		}
	}
#endif //#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE
}

void EventPlayer::loadEvents(const Ogre::String& fname)
{
	FILE *file = fopen(fname.c_str(),"rb");
	if (file)
	{
		Ogre::DataStreamPtr ptr(new Ogre::FileHandleDataStream(file));
		char buf[512];
		while(ptr->readLine(buf,sizeof(buf)))
		{
			Ogre::String s(buf);
			if (s.size() <= 1)
				continue;

			//comments
			if (s.find("//") == String::npos)
			{
				Ogre::StringUtil::trim(s);

				Ogre::StringVector strs = Ogre::StringUtil::split(s," ");

				Event e;
				e.time = Ogre::StringConverter::parseInt(strs[0]);
				e.type = strs[1].find("mouse") != String::npos ? 0 : 1;
				e.key = Ogre::StringConverter::parseInt(strs[2]);
				e.pressed = strs[3].find("prs") != String::npos ? 1 : 0;
				if (!e.pressed)
					e.pressed = strs[3].find("rel") != String::npos ? 0 : -1;
				e.pos.x = Ogre::StringConverter::parseReal(strs[4]);
				e.pos.y = Ogre::StringConverter::parseReal(strs[5]);
				e.focused_obj = strs[6];

				mEvents.push_back(e);
			}
		}
		fclose(file);
	}
}

void EventPlayer::saveEvent(const Event &e)
{
	String pr;

	if (e.pressed == -1)
		pr.assign("mov");
	else if (e.pressed == 0)
		pr.assign("rel");
	else
		pr.assign("prs");

	fprintf(mSaveFile,"%08u %s %02d %s %03.2f %03.2f %s\n",
		app.getTime() - mCreateTime, 
		e.type ? "kbrd" : "mouse",
        e.key,
		pr.c_str(),
		e.pos.x,
		e.pos.y,
		e.focused_obj.size() > 1 ? e.focused_obj.c_str() : "null");
}

void EventPlayer::saveEvent(const OIS::MouseEvent &e, int pressed, OIS::MouseButtonID id)
{
	Real wndW = (Real)app.getWindow()->getWidth();
	Real wndH = (Real)app.getWindow()->getHeight();
	
	Event _e = {0,0,
		(int)id, pressed,
		Vector2(e.state.X.abs/wndW,e.state.Y.abs/wndH),
		app.getFocusedObj() ? app.getFocusedObj()->pSNode->getName() : Ogre::EmptyString};

	saveEvent(_e);
}

void EventPlayer::saveEvent(const OIS::KeyEvent &e, int pressed)
{
	Event _e = { 0, 1, (int)e.key, pressed, Vector2::ZERO, app.getFocusedObj() ? app.getFocusedObj()->pSNode->getName() : Ogre::EmptyString };
	saveEvent(_e);
}

#endif
