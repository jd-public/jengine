
#include <stdafx.h>

#include "SceneLoader.h"
#include <Ogre.h>

//#include "MainApplication.h"
//#include "AdvRectangle2D.h"
//#include "AtlasManager.h"
#include "HeroObject.h"
#include "AnimObject.h"
#include "VirtualScript.h"
#include "DustController.h"
#include "AdvRectangle2D.h"
#include "ScriptMsg.h"
#include "MainApplication.h"
#include "OgreOverlayManager.h"
#include "OgreOverlay.h"

#pragma warning(disable:4390)
#pragma warning(disable:4305)

//using namespace Forests;


SceneLoader::SceneLoader() : mSceneMgr(0), mUniqueNameCounter(0), mDefaultFisibleFlag(0), mShowLog(true), mShowError(true)
{
	//if(mShowLog)
	//	Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Initializing" );
}

SceneLoader::~SceneLoader()
{
  
}

void ParseStringVector(Ogre::String &str, Ogre::StringVector &list)
{
    list.clear();
    Ogre::StringUtil::trim(str,true,true);
    if(str == "") 
        return;

    int pos = str.find(";");
    while(pos != -1)
    {
        list.push_back(str.substr(0,pos));
        str.erase(0,pos + 1);
        pos = str.find(";");
    }
  
    if(str != "") 
        list.push_back(str);
}

void SceneLoader::parseScene(const Ogre::String &SceneName, 
								const Ogre::String &groupName, 
								Ogre::SceneManager *yourSceneMgr,
								Ogre::SceneNode *pAttachNode,
								Ogre::RenderWindow *pWindow,
								ProgressListener *pProgressListener)
{
    // set up shared object values
    m_sGroupName = groupName;
    mSceneMgr = yourSceneMgr;
	mWindow = pWindow;
	mProgressListener = pProgressListener;

    // if the resource group doesn't exists create it
    //if(!Ogre::ResourceGroupManager::getSingleton().resourceGroupExists(m_sGroupName)){
    //    Ogre::ResourceGroupManager::getSingleton().createResourceGroup(m_sGroupName);
    //}

	mCurrentFile = SceneName;

    Ogre::DataStreamPtr stream = Ogre::ResourceGroupManager::getSingleton().openResource( SceneName, groupName );
	XmlParser parser(stream->getAsString().c_str());

    // Grab the scene node
	XML_NODE* XMLRoot = parser.getDoc().first_node("scene");

	if (mShowLog)
		toLogEx("\n[[[SceneLoader]]] parse scene: %s", SceneName.c_str());

    // Process the scene
    processScene(XMLRoot, pAttachNode);

    //Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Scene parsing finished" );
}

void SceneLoader::processScene(XML_NODE* XMLRoot, Ogre::SceneNode *pAttachNode)
{
	//if(mShowLog)
	//	Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] process scene");
	updateProgress();

	XML_NODE* pElement;

    // TODO: Process resources (?)
    //pElement = XMLRoot->first_node("resourceLocations");
    //if(pElement)
    //    processResourceLocations(pElement);
	mDefaultFisibleFlag = CVirtualScript::getCfgI("default_visible_flag");

	pElement = XMLRoot->first_node("strings_table");
	if(pElement)
		this->processStringsTable(pElement);

    // Process environment (?)
    pElement = XMLRoot->first_node("environment");
    if(pElement)
        processEnvironment(pElement);

	pElement = XMLRoot->first_node("includes");
    if(pElement)
        processIncludes(pElement, pAttachNode);

	//TODO:
	//<script_aliases/>
	pElement = XMLRoot->first_node("script_aliases");
    if(pElement)
        processScriptAliases(pElement);

    // Process nodes (?)
    pElement = XMLRoot->first_node("nodes");
    if(pElement)
        processNodes(pElement, pAttachNode);
}

void SceneLoader::processResourceLocations(XML_NODE* XMLNode)
{
   
}

void SceneLoader::processNodes(XML_NODE* XMLNode, Ogre::SceneNode *pAttachNode)
{
	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Nodes" );
	updateProgress();

	XML_NODE* pElement = XMLNode->first_node("node");
    while(pElement)
    {
        processNode(pElement, pAttachNode);
        pElement = pElement->next_sibling("node");
    }
}

void SceneLoader::processScriptAliases(XML_NODE* XMLNode)
{
	XML_NODE* pElement = XMLNode->first_node("alias");
    
    while(pElement)
	{
		String p = _getAttrib(pElement, "pattern");
		_getAttrib(pElement, "script");

        if(p.empty() || mTempStr.empty())
			continue;

		CVirtualScript::mScriptAliases.insert(make_pair(p,mTempStr));

        pElement = pElement->next_sibling("alias");
    }
}


void SceneLoader::processIncludes(XML_NODE* XMLNode, Ogre::SceneNode *pAttachNode)
{
	XML_NODE* pElement = XMLNode->first_node("include");
    
    while(pElement)
	{
		bool ignore = false;

		_getAttrib(pElement, "cfg_param");	
        
        if(!mTempStr.empty())
		{
			int val = getAttribInt(pElement, "cfg_value",-1);

			if(CVirtualScript::getCfgI(mTempStr) != val)
			{
				ignore = true;
			}
		}	
			
        _getAttrib(pElement, "name");	
        
        if(!mTempStr.empty() && !ignore)
		{
			String curSceneFileName = mCurrentFile;
			//if(mShowLog)
			//	Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] process include scene: " + mTempStr);
			updateProgress();
			parseScene(mTempStr, m_sGroupName, mSceneMgr, pAttachNode, mWindow, mProgressListener);
			mCurrentFile = curSceneFileName;
        }

        pElement = pElement->next_sibling("include");
    }
}

void SceneLoader::processEnvironment(XML_NODE* XMLNode)
{
	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Environment" );
	updateProgress();

    XML_NODE* pElement;

    // Process fog (?)
    pElement = XMLNode->first_node("fog");
    if(pElement)
        processFog(pElement);

    // Process colourAmbient (?)
    pElement = XMLNode->first_node("colourAmbient");
    if(pElement)
        mSceneMgr->setAmbientLight(parseColour(pElement));

    // Process colourBackground (?)
    //! @todo Set the background colour of all viewports (RenderWindow has to be provided then)
    pElement = XMLNode->first_node("colourBackground");
    if(pElement)
        ;//mSceneMgr->set(parseColour(pElement));

}

void SceneLoader::processLight(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
    // Process attributes
    Ogre::String name = getAttrib(XMLNode, "name");
    Ogre::String id = getAttrib(XMLNode, "id");

	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Light: " + name );
	updateProgress();

    // Create the light
    Ogre::Light *pLight = mSceneMgr->createLight(name);
	pObj->pLight = pLight;
    if(pParent)
        pParent->attachObject(pLight);

    Ogre::String sValue = getAttrib(XMLNode, "type");
    if(sValue == "point")
        pLight->setType(Ogre::Light::LT_POINT);
    else if(sValue == "directional")
        pLight->setType(Ogre::Light::LT_DIRECTIONAL);
    else if(sValue == "spot")
        pLight->setType(Ogre::Light::LT_SPOTLIGHT);
    else if(sValue == "radPoint")
        pLight->setType(Ogre::Light::LT_POINT);

    pLight->setVisible(getAttribBool(XMLNode, "visible", true));
    pLight->setCastShadows(getAttribBool(XMLNode, "castShadows", true));

	if(XMLNode->first_attribute("visibleFlag"))
		pLight->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
	else
		setDefaultVisibleFlag(pLight);


    XML_NODE* pElement;

    // Process position (?)
    pElement = XMLNode->first_node("position");
    if(pElement)
        pLight->setPosition(parseVector3(pElement));

    // Process normal (?)
    pElement = XMLNode->first_node("normal");
    if(pElement)
        pLight->setDirection(parseVector3(pElement));

    pElement = XMLNode->first_node("directionVector");
    if(pElement)
    {
        pLight->setDirection(parseVector3(pElement));
        //mLightDirection = parseVector3(pElement);
    }

    // Process colourDiffuse (?)
    pElement = XMLNode->first_node("colourDiffuse");
    if(pElement)
        pLight->setDiffuseColour(parseColour(pElement));

    // Process colourSpecular (?)
    pElement = XMLNode->first_node("colourSpecular");
    if(pElement)
        pLight->setSpecularColour(parseColour(pElement));

    if(sValue != "directional")
    {
        // Process lightRange (?)
        pElement = XMLNode->first_node("lightRange");
        if(pElement)
            processLightRange(pElement, pLight);

        // Process lightAttenuation (?)
        pElement = XMLNode->first_node("lightAttenuation");
        if(pElement)
            processLightAttenuation(pElement, pLight);
    }
    // Process userDataReference (?)
    pElement = XMLNode->first_node("userDataReference");
    if(pElement)
        ;//processUserDataReference(pElement, pLight);
}

void SceneLoader::processCamera(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
    // Process attributes
    Ogre::String name = getAttrib(XMLNode, "name");
    Ogre::String id = getAttrib(XMLNode, "id");

	Ogre::Degree fov_y = Ogre::Degree(getAttribReal(XMLNode, "old_fov", -1));

	if(fov_y.valueDegrees() < 0.0f)
	{
		Ogre::Degree fov_x = Ogre::Degree(getAttribReal(XMLNode, "fov", 45));
		fov_y = 2 * Ogre::Math::ATan(Ogre::Math::Tan(fov_x / 2.f) / 4.f * 3.f);
	}

    Ogre::String projectionType = getAttrib(XMLNode, "projectionType", "perspective");

	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Camera: " + name );
	updateProgress();

    // Create the camera
    Ogre::Camera *pCamera = mSceneMgr->createCamera(name);
	pObj->pCamera = pCamera;
    
    // set auto Aspest ratio true. If later aspect ratio comes this can be deleted
//    pCamera->setAutoAspectRatio(true);

    //TODO: make a flag or attribute indicating whether or not the camera should be attached to any parent node.
    if(pParent)
        pParent->attachObject(pCamera);

    // Set the field-of-view
    //! @todo Is this always in degrees?
    pCamera->setFOVy(Ogre::Degree(fov_y));

    // Set the aspect ratio
    //pCamera->setAutoAspectRatio(false);
	if(XMLNode->first_node("aspectRatio"))
		pCamera->setAspectRatio(getAttribReal(XMLNode, "aspectRatio"));
    
    // Set the projection type
    if(projectionType == "perspective")
        pCamera->setProjectionType(Ogre::PT_PERSPECTIVE);
    else if(projectionType == "orthographic")
        pCamera->setProjectionType(Ogre::PT_ORTHOGRAPHIC);

    XML_NODE* pElement;

    // Process clipping (?)
    pElement = XMLNode->first_node("clipping");
    if(pElement)
    {
        Ogre::Real nearDist = getAttribReal(pElement, "near");
        pCamera->setNearClipDistance(nearDist);

        Ogre::Real farDist =  getAttribReal(pElement, "far");
        pCamera->setFarClipDistance(farDist);
    }

	for(XML_NODE *pRttNode = XMLNode->first_node("rtt"); pRttNode != NULL; pRttNode = pRttNode->next_sibling("rtt"))
	{
		String rtt_name = getAttrib(pRttNode, "name");
		bool platform_pc = getAttribBool(pRttNode, "platform_pc", false);
		int x = getAttribInt(pRttNode, "x");
		int y = getAttribInt(pRttNode, "y");
		int visibleFlag = getAttribInt(pRttNode, "visibleFlag");
		int bpp = getAttribInt(pRttNode, "bpp", 24);

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
		bool platform_pc2 = false;
#else
		bool platform_pc2 = true;
#endif
		if(platform_pc == platform_pc2)
			CVirtualScript::_createRTT(rtt_name, name, x, y, 0, visibleFlag, 0, 0, 1, 1, bpp);
	}


    // Process position (?)
    pElement = XMLNode->first_node("position");
    if(pElement)
        pCamera->setPosition(parseVector3(pElement));

    // Process rotation (?)
    pElement = XMLNode->first_node("rotation");
    if(pElement)
        pCamera->setOrientation(parseQuaternion(pElement));

    // Process normal (?)
    pElement = XMLNode->first_node("normal");
    if(pElement)
		pCamera->setDirection(parseVector3(pElement));

    // Process lookTarget (?)
   /* pElement = XMLNode->first_node("lookTarget");
    if(pElement)
        ;//!< @todo Implement the camera look target

    // Process trackTarget (?)
    pElement = XMLNode->first_node("trackTarget");
    if(pElement)
        ;//!< @todo Implement the camera track target

    // Process userDataReference (?)
    pElement = XMLNode->first_node("userDataReference");
    if(pElement)*/
        ;//!< @todo Implement the camera user data reference
/*
    // construct a scenenode is no parent
    if(!pParent)
    {
        Ogre::SceneNode* pNode = mAttachNode->createChildSceneNode(name);
        pNode->setPosition(pCamera->getPosition());
        pNode->setOrientation(pCamera->getOrientation());
        pNode->scale(1,1,1);
    }
*/
}

void SceneLoader::processNode(XML_NODE* XMLNode, Ogre::SceneNode *pParent)
{
	if(XMLNode->first_attribute("retina_only"))
	{
		if(!CVirtualScript::getCfgB("retina"))
			return;
	}			

	CGameObject* pObj = NULL;

	// Create the scene node
    Ogre::SceneNode *pSceneNode = pParent->createChildSceneNode(_getAttrib(XMLNode, "name"));

	if(mShowLog)
		Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] process Node: " + _getAttrib(XMLNode, "name"));
	updateProgress();

	int type = 0;
	String &sType = _getAttrib(XMLNode, "type");

	if(!sType.empty())
	{
		if (sType.compare("T_HERO_OBJ") == 0)
			type = 1;
		else if (sType.compare("T_ANIM_OBJ") == 0) 
			type = 2;
		else if (sType.compare("T_PHYS_OBJ") == 0)
			type = 3; 
	}
	
    String &sScript = _getAttrib(XMLNode, "script");

	if(!sScript.empty())
	{
		switch(type)
		{
		case 0:	pObj = new CGameObject(sScript); break;
		case 1:	pObj = new CHeroObject(sScript); break;
		case 2:	pObj = new CAnimatableObject(sScript); break;
#if USE_BULLET == TRUE
		case 3:	pObj = new CPhysObject(sScript); break;						
#endif
		}

		//send create message
		CScriptMsg m;
		m.messageId = CScriptMsg::MSG_CREATE;
		m.pTargetObj = pObj;
		app.postMsg(m);
	}
	else
	{
		switch(type)
		{
		case 0:	pObj = new CGameObject(); break;
		case 1:	pObj = new CHeroObject(); break;
		case 2:	pObj = new CAnimatableObject(); break;
#if USE_BULLET == TRUE
		case 3:	pObj = new CPhysObject(); break;
#endif
		}
	}

	if(!pObj)
	{
		//TODO: MEGAERROR!!!
	}

	pObj->mSceneFileName = mCurrentFile;
	pObj->pSNode = pSceneNode;
	//for catching destroy message
	pSceneNode->setListener(pObj);
	pSceneNode->setUserAny(Ogre::Any(pObj));

	//-----------
    XML_NODE* pElement;

	pElement = XMLNode->first_node("params");
	if(pElement)
	{
		processParams(XMLNode->first_node("params"), pObj);
	}

    // Process position (?)
    pElement = XMLNode->first_node("position");
    if(pElement)
    {
        pSceneNode->setPosition(parseVector3(pElement));
        pSceneNode->setInitialState();
    }

    // Process rotation (?)
    pElement = XMLNode->first_node("rotation");
    if(pElement)
    {
        pSceneNode->setOrientation(parseQuaternion(pElement));
        pSceneNode->setInitialState();
    }

    // Process scale (?)
    pElement = XMLNode->first_node("scale");
    if(pElement)
    {
        pSceneNode->setScale(parseVector3(pElement));
        pSceneNode->setInitialState();
    }

	Ogre::Vector3 BBS(0, 0, 0);
	if(CVirtualScript::getCfgI("default_bbsize") > 0)
	{
		BBS.x = (Ogre::Real)CVirtualScript::getCfgI("default_bbsize");
		BBS.y = (Ogre::Real)CVirtualScript::getCfgI("default_bbsize");
		BBS.z = (Ogre::Real)CVirtualScript::getCfgI("default_bbsize");
	}
    pElement = XMLNode->first_node("bbsize");
    if(pElement)
    {
        BBS = parseVector3(pElement);
	}



    // Process lookTarget (?)
    //pElement = XMLNode->first_node("lookTarget");
    //if(pElement)
    //    processLookTarget(pElement, pNode);

    // Process trackTarget (?)
    //pElement = XMLNode->first_node("trackTarget");
    //if(pElement)
    //    processTrackTarget(pElement, pNode);

    // Process node (*)
    pElement = XMLNode->first_node("node");
    while(pElement)
    {
        processNode(pElement, pSceneNode);
        pElement = pElement->next_sibling("node");
    }

    // Process entity (*)
    pElement = XMLNode->first_node("entity");
    while(pElement)
    {
        processEntity(pElement, pSceneNode, pObj, BBS, type);
        pElement = pElement->next_sibling("entity");
    }

    pElement = XMLNode->first_node("rect2d");
    while(pElement)
    {
        processRect2d(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("rect2d");
    }

	pElement = XMLNode->first_node("dust");
    while(pElement)
    {
		processDust(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("dust");
    }

	pElement = XMLNode->first_node("overlay");
    while(pElement)
    {
		processOverlay(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("overlay");
    }

	// Process light (*)
    pElement = XMLNode->first_node("light");
    while(pElement)
    {
        processLight(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("light");
    }

    // Process camera (*)
    pElement = XMLNode->first_node("camera");
    while(pElement)
    {
        processCamera(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("camera");
    }

    // Process billboardSet (*)
    pElement = XMLNode->first_node("billboardSet");
    while(pElement)
    {
        processBillboardSet(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("billboardSet");
    }

	// Process particleSystem (*)
    pElement = XMLNode->first_node("particleSystem");
    while(pElement)
    {
        processParticleSystem(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("particleSystem");
    }

    pElement = XMLNode->first_node("ribbonTrail");
    while(pElement)
    {
        processRibbonTrail(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("ribbonTrail");
    }

    pElement = XMLNode->first_node("animations");
    if(pElement)
        processAnimations(pElement, pObj);

#if USE_BULLET == TRUE
	pElement = XMLNode->first_node("rigitBody");
    while(pElement)
    {
        processRigitBody(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("rigitBody");
    }

    pElement = XMLNode->first_node("constraint");
    while(pElement)
    {
        processConstraint(pElement, pSceneNode, pObj);
        pElement = pElement->next_sibling("constraint");
    }
#endif

}

void SceneLoader::processEntity(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj, const Ogre::Vector3 &BBS, int type)
{
    // Process attributes
    Ogre::String name = getAttribUnique(XMLNode, "name");
    Ogre::String id = getAttrib(XMLNode, "id");
    Ogre::String meshFile = getAttrib(XMLNode, "meshFile");
    Ogre::String material = getAttrib(XMLNode, "material");
    bool isStatic = getAttribBool(XMLNode, "static", false);;
    bool castShadows = getAttribBool(XMLNode, "castShadows", true);

	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Entity: " + name );
	updateProgress();

    // TEMP: Maintain a list of static and dynamic objects
    //if(isStatic)
    //    staticObjects.push_back(name);
    //else
    //    dynamicObjects.push_back(name);

    XML_NODE* pElement;

    // Process vertexBuffer (?)
    pElement = XMLNode->first_node("vertexBuffer");
    if(pElement)
        ;//processVertexBuffer(pElement);

    // Process indexBuffer (?)
    pElement = XMLNode->first_node("indexBuffer");
    if(pElement)
        ;//processIndexBuffer(pElement);

    // Create the entity
    Ogre::Entity *pEntity = 0;
    try
    {
        Ogre::MeshManager::getSingleton().load(meshFile, m_sGroupName);
        pEntity = mSceneMgr->createEntity(name, meshFile);
        pEntity->setCastShadows(castShadows);
        pParent->attachObject(pEntity);
		pEntity->setUserAny(Ogre::Any(pObj));
		pObj->pEntity = pEntity;

		if (type == 1)
			((CHeroObject*)pObj)->initAnim();
		else if (type == 2)
			((CAnimatableObject*)pObj)->initAnim();

		pObj->mIsSelectable = getAttribBool(XMLNode, "selectable");
		if(pObj->mIsSelectable)
			pEntity->setQueryFlags(1);
		else
			pEntity->setQueryFlags(0);

		if(!app.cheatsEnabled())
			CVirtualScript::setSubmeshVisible(pObj->getHandle(), "00_empty", 0);


		if(BBS.length() > 0 && !getAttribBool(XMLNode, "noDefaultBbsize"))
		{
			Ogre::AxisAlignedBox Box (-BBS, +BBS);
			pEntity->getMesh()->_setBounds(Box, 1);
			pParent->_updateBounds();
		}
		/*else
		{
			Ogre::AxisAlignedBox aabInf;
			aabInf.setInfinite();
			pEntity->getMesh()->_setBounds(aabInf);
			pParent->_updateBounds();
		}*/

		if(XMLNode->first_attribute("renderQueue"))
			pEntity->setRenderQueueGroup(getAttribInt(XMLNode, "renderQueue"));

		if(XMLNode->first_attribute("visibleFlag"))
			pEntity->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
		else
			setDefaultVisibleFlag(pEntity);

		bool softwareAnim = getAttribBool(XMLNode, "softwareAnim");
		pObj->mSoftwareAnim = softwareAnim;  
		if(softwareAnim)
			pEntity->addSoftwareAnimationRequest(softwareAnim);


        if(!material.empty())
            pEntity->setMaterialName(material);
        
        // Process subentity (*)
        /* if materials defined within subentities, those
           materials will be used instead of the materialFile */
        pElement = XMLNode->first_node("subentities");

        if(pElement)
        {
            processSubEntity(pElement, pEntity);
        }else
		{
            // if the .scene file contains the subentites without
            // the <subentities> </subentities>
            processSubEntity(XMLNode, pEntity);
        }
    }
    catch(Ogre::Exception &/*e*/)
    {
		if(mShowError)
	        Ogre::LogManager::getSingleton().logMessage("[SceneLoader] Error loading an entity!");
    }

    // Process userDataReference (?)
    //pElement = XMLNode->first_node("userDataReference");
    //if(pElement)
    //    processUserDataReference(pElement, pEntity);

    
}

void SceneLoader::processSubEntity(XML_NODE* XMLNode, Ogre::Entity *pEntity)
{
	return;
/*
    XML_NODE* pElement;
    int index = 0;
    Ogre::String materialName;
    Ogre::String sIndex;

    // Process subentity
    pElement = XMLNode->first_node("subentity");
    
    while(pElement)
	{      
        sIndex.clear();
        materialName.clear();

        sIndex = getAttrib(pElement, "index");				// submesh index
        materialName = getAttrib(pElement, "materialName");	// new material for submesh
        
        if(!sIndex.empty() && !materialName.empty()){
            
            index = Ogre::StringConverter::parseInt(sIndex);
            try{
                pEntity->getSubEntity(index)->setMaterialName(materialName);
            } catch (...)
			{
				if(mShowError)
					Ogre::LogManager::getSingleton().logMessage("[SceneLoader] Subentity material index invalid!");
            }
        }
        pElement = pElement->next_sibling("subentity");
    }
 */
}

void SceneLoader::processParticleSystem(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
    // Process attributes
    Ogre::String name = getAttrib(XMLNode, "name");
    Ogre::String id = getAttrib(XMLNode, "id");
    Ogre::String scriptName = getAttrib(XMLNode, "templateName");

	if(mShowLog)
		Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process ParticleSystem: " + name );
	updateProgress();


    // Create the particle system
    try
    {
        Ogre::ParticleSystem *pParticles = mSceneMgr->createParticleSystem(name, scriptName);

		if(XMLNode->first_attribute("attach_bone"))
		{	
			Ogre::MovableObject *mObj = pParent->getAttachedObject(0);
			if (mObj)
			{
				((Ogre::Entity*)mObj)->attachObjectToBone(
					getAttrib(XMLNode, "attach_bone"),
					(Ogre::MovableObject *)pParticles);

			}
		}
		else
			pParent->attachObject(pParticles);

		pParticles->setVisible(getAttribBool(XMLNode, "show", true));

		if(!getAttribBool(XMLNode, "play", true))
			for (unsigned short i=0; i < pParticles->getNumEmitters(); i++)
				pParticles->getEmitter(i)->setEnabled(false);

		if(XMLNode->first_attribute("material"))
			pParticles->setMaterialName(getAttrib(XMLNode, "material"));

		if(XMLNode->first_attribute("renderQueue"))
			pParticles->setRenderQueueGroup(getAttribInt(XMLNode, "renderQueue"));

		if(XMLNode->first_attribute("visibleFlag"))
			pParticles->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
		else
			setDefaultVisibleFlag(pParticles);

		pObj->pParticle = pParticles;
        //particleSystemList.push_back(pParticles);
        // there is a bug with particles and paged geometry if particle's
        // renderQueue is value is smaller than the grass's renderQueue
        //if(mGrassLoaderHandle)
        //    pParticles->setRenderQueueGroup(mGrassLoaderHandle->getRenderQueueGroup());
    }
    catch(Ogre::Exception &/*e*/)
    {
		if(mShowError)
	        Ogre::LogManager::getSingleton().logMessage("[SceneLoader] Error creating a particle system!");
    }
}

void SceneLoader::processBillboardSet(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
    //! @todo Implement this
    Ogre::String name = getAttrib(XMLNode, "name");
	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process BillboardSet: " + name );
	updateProgress();

	Ogre::BillboardSet *pBS = NULL;
	//try to create a billboard
	try
	{
		pBS = mSceneMgr->createBillboardSet(name);
		pObj->pBillboard = pBS;
	}
	catch(...)
	{
//		dotSceneInfo::_logLoadError("Failed to create billboardSet '" + name);
		pBS = NULL;
	}
	if (!pBS)
		return;
	// type :(
	Ogre::BillboardType bt = Ogre::BBT_POINT;
    Ogre::String type = getAttrib(XMLNode, "type", "point");
	if(!type.empty())
	{
		if (type == "point")
			bt = (Ogre::BBT_POINT);
		else if (type == "orientedCommon")
			bt = (Ogre::BBT_ORIENTED_COMMON);
		else if (type == "orientedSelf")
			bt = (Ogre::BBT_ORIENTED_SELF);
		else
		{
//			dotSceneInfo::_logLoadWarning("unknown billboardSet type '" + type + "' will skip...");
			//FIXME!! mpSceneManager->removeBillboardSet(pBS);
			return;
		}
	}
	pBS->setBillboardType(bt);
	// origin :(
	Ogre::BillboardOrigin bo = Ogre::BBO_CENTER;

    Ogre::String origin = getAttrib(XMLNode, "origin", "center");
	if (!origin.empty())
	{
		if (origin == "topLeft")
			bo = Ogre::BBO_TOP_LEFT;
		if (origin == "topCenter")
			bo = Ogre::BBO_TOP_CENTER;
		if (origin == "topRight")
			bo = Ogre::BBO_TOP_RIGHT;
		if (origin == "centerLeft")
			bo = Ogre::BBO_CENTER_LEFT;
		if (origin == "center")
			bo = Ogre::BBO_CENTER;
		if (origin == "centerRight")
			bo = Ogre::BBO_CENTER_RIGHT;
		if (origin == "bottomLeft")
			bo = Ogre::BBO_BOTTOM_LEFT;
		if (origin == "bottomCenter")
			bo = Ogre::BBO_BOTTOM_CENTER;
		if (origin == "bottomRight")
			bo = Ogre::BBO_BOTTOM_RIGHT;
	}
	pBS->setBillboardOrigin(bo);
	// dimensions
	Real width = getAttribReal(XMLNode, "width", -1);
	Real height = getAttribReal(XMLNode, "height", -1);
	if(width > 0)
		pBS->setDefaultWidth(width);
	if(height > 0)
		pBS->setDefaultHeight(height);

	if(XMLNode->first_attribute("renderQueue"))
	{
		int renderQueue = getAttribInt(XMLNode, "renderQueue");
		pBS->setRenderQueueGroup(static_cast<Ogre::uint8>(renderQueue));
	}

	if(XMLNode->first_attribute("visibleFlag"))
	{
		pBS->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
	}
	else
		setDefaultVisibleFlag(pBS);

	pBS->setVisible(getAttribBool(XMLNode, "show", true));

	// material
	String matName = "BaseWhite";
	if(XMLNode->first_attribute("material"))
	{
		matName = getAttrib(XMLNode, "material");

		try
		{
			pBS->setMaterialName(matName);
		}
		catch (...)
		{
//			dotSceneInfo::_logLoadWarning("material '" + matName + "' failed to load, billboardSet '" + name + "'");
			pBS->setMaterialName("BaseWhite");
		}
	}
	// do all billboards
	for(XML_NODE *pBillboardNode = XMLNode->first_node("billboard"); pBillboardNode != NULL; pBillboardNode = pBillboardNode->next_sibling("billboard"))
	{
		processBillboard(pBillboardNode, pBS);
	}
	// update the dotSceneInfo graph
	pParent->attachObject(pBS);
}

void SceneLoader::processBillboard(XML_NODE* XMLNode, Ogre::BillboardSet *pBS)
{
//	String name = getAttrib(XMLNode, "name");

	Vector3 pos = Vector3::ZERO;
	Ogre::ColourValue dif = Ogre::ColourValue::White;

	XML_NODE* pPositionNode = XMLNode->first_node("position");
	if(pPositionNode)
		pos = parseVector3(pPositionNode);

	XML_NODE* pColourNode = XMLNode->first_node("colourDiffuse");
	if(pColourNode)
		dif = parseColour(pColourNode);

	try
	{
		Ogre::Billboard *pBB = pBS->createBillboard(pos, dif);
		if(XMLNode->first_attribute("width") && XMLNode->first_attribute("height")) 
		{
			Real width = getAttribReal(XMLNode, "width");
			Real height = getAttribReal(XMLNode, "height");
			pBB->setDimensions(width, height);
		}
		if(XMLNode->first_node("rotation"))
		{
			pBB->setRotation(Ogre::Angle(parseQuaternion(XMLNode->first_node("rotation")).w));
		}
	}
	catch (...)
	{
		if(mShowError)
			Ogre::LogManager::getSingleton().logMessage("[SceneLoader] ERROR: Failed to create billboard ");
	}

}


void SceneLoader::processFog(XML_NODE* XMLNode)
{
	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Fog" );
	updateProgress();

    // Process attributes
    Ogre::Real expDensity = getAttribReal(XMLNode, "density", 0.001);
    Ogre::Real linearStart = getAttribReal(XMLNode, "start", 0.0);
    Ogre::Real linearEnd = getAttribReal(XMLNode, "end", 1.0);

    Ogre::FogMode mode = Ogre::FOG_NONE;
    Ogre::String sMode = getAttrib(XMLNode, "mode");
    if(sMode == "none")
        mode = Ogre::FOG_NONE;
    else if(sMode == "exp")
        mode = Ogre::FOG_EXP;
    else if(sMode == "exp2")
        mode = Ogre::FOG_EXP2;
    else if(sMode == "linear")
        mode = Ogre::FOG_LINEAR;
    else
        mode = (Ogre::FogMode)Ogre::StringConverter::parseInt(sMode);

    XML_NODE* pElement;

    // Process colourDiffuse (?)
    Ogre::ColourValue colourDiffuse = Ogre::ColourValue::White;
    pElement = XMLNode->first_node("colour");
    if(pElement)
        colourDiffuse = parseColour(pElement);

    // Setup the fog
    mSceneMgr->setFog(mode, colourDiffuse, expDensity, linearStart, linearEnd);
}

void SceneLoader::processLightRange(XML_NODE* XMLNode, Ogre::Light *pLight)
{
    // Process attributes
    Ogre::Real inner = getAttribReal(XMLNode, "inner");
    Ogre::Real outer = getAttribReal(XMLNode, "outer");
    Ogre::Real falloff = getAttribReal(XMLNode, "falloff", 1.0);

    // Setup the light range
    pLight->setSpotlightRange(Ogre::Angle(inner), Ogre::Angle(outer), falloff);
}

void SceneLoader::processLightAttenuation(XML_NODE* XMLNode, Ogre::Light *pLight)
{
    // Process attributes
    Ogre::Real range = getAttribReal(XMLNode, "range");
    Ogre::Real constant = getAttribReal(XMLNode, "constant");
    Ogre::Real linear = getAttribReal(XMLNode, "linear");
    Ogre::Real quadratic = getAttribReal(XMLNode, "quadratic");

    // Setup the light attenuation
    pLight->setAttenuation(range, constant, linear, quadratic);
}

Ogre::String SceneLoader::getAttribUnique(XML_NODE* XMLNode, const char *attrib)
{
//    if(XMLNode->first_attribute(attrib))
//        return XMLNode->first_attribute(attrib)->value();
//    else
	{
		mUniqueNameCounter++;
        return String("dsiAutoName") + Ogre::StringConverter::toString(mUniqueNameCounter);
	}
}

Ogre::Vector3 SceneLoader::parseVector3(XML_NODE* XMLNode)
{
    return Ogre::Vector3(
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value())
    );
}

Ogre::Quaternion SceneLoader::parseQuaternion(XML_NODE* XMLNode)
{
    //! @todo Fix this crap!

    Ogre::Quaternion orientation;

    if(XMLNode->first_attribute("qx"))
    {
        orientation.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qx")->value());
        orientation.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qy")->value());
        orientation.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qz")->value());
        orientation.w = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qw")->value());
    }
    if(XMLNode->first_attribute("qw"))
    {
        orientation.w = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qw")->value());
        orientation.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qx")->value());
        orientation.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qy")->value());
        orientation.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qz")->value());
    }
    else if(XMLNode->first_attribute("axisX"))
    {
        Ogre::Vector3 axis;
        axis.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("axisX")->value());
        axis.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("axisY")->value());
        axis.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("axisZ")->value());
        Ogre::Real angle = Ogre::StringConverter::parseReal(XMLNode->first_attribute("angle")->value());;
        orientation.FromAngleAxis(Ogre::Angle(angle), axis);
    }
    else if(XMLNode->first_attribute("angleX"))
    {
		Ogre::Vector3 axisX(1.f, 0.f, 0.f);
		Ogre::Vector3 axisY(0.f, 1.f, 0.f);
		Ogre::Vector3 axisZ(0.f, 0.f, 1.f);

        Ogre::Vector3 axis;
        axis.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("angleX")->value());
        axis.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("angleY")->value());
        axis.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("angleZ")->value());

		Quaternion qx, qy, qz;
		qx.FromAngleAxis(Ogre::Degree(axis.x), axisX);
		qx.FromAngleAxis(Ogre::Degree(axis.y), axisY);
		qx.FromAngleAxis(Ogre::Degree(axis.z), axisZ);

        orientation = qx*qy*qz;
    }
    else if(XMLNode->first_attribute("x"))
    {
        orientation.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value());
        orientation.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value());
        orientation.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value());
        orientation.w = Ogre::StringConverter::parseReal(XMLNode->first_attribute("w")->value());
    }
    else if(XMLNode->first_attribute("w"))
    {
        orientation.w = Ogre::StringConverter::parseReal(XMLNode->first_attribute("w")->value());
        orientation.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value());
        orientation.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value());
        orientation.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value());
    }

    return orientation;
}

Ogre::ColourValue SceneLoader::parseColour(XML_NODE* XMLNode)
{
    return Ogre::ColourValue(
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("r")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("g")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("b")->value()),
        XMLNode->first_attribute("a") != NULL ? Ogre::StringConverter::parseReal(XMLNode->first_attribute("a")->value()) : 1
    );
}

//void SceneLoader::processUserDataReference(XML_NODE* XMLNode, Ogre::Entity *pEntity)
//{
//    Ogre::String str = XMLNode->first_attribute("id")->value();
//    pEntity->setUserAny(Ogre::Any(str));
//}

//---------------

Ogre::String& SceneLoader::_getAttrib(XML_NODE* XMLNode, const char *attrib, const char *def)
{
	const char *res = def;

	XML_ATTRIB *attr = XMLNode->first_attribute(attrib);
   
	if(attr)
        res = attr->value();
    
	mTempStr.assign(res);

	return mTempStr;
}

//---------------
void SceneLoader::processParams(XML_NODE* XMLNode, CGameObject* pObj)
{
	for (XML_NODE *pElem = XMLNode->first_node("param");
		pElem != 0; pElem = pElem->next_sibling("param"))
	{
		if(pElem->first_attribute("name") && pElem->first_attribute("value"))
		{
			String name = getAttrib(pElem, "name");
			String value = getAttrib(pElem, "value");
			pObj->addParams(name, value);
		}
	}

}

void SceneLoader::setDefaultVisibleFlag(Ogre::MovableObject *obj)
{
	if(mDefaultFisibleFlag > 0)
		obj->setVisibilityFlags(mDefaultFisibleFlag);
}

void SceneLoader::processStringsTable(XML_NODE* XMLNode)
{
	String lang = CVirtualScript::getCfgS("language");
	String lang2;
			
	toLogEx("\n IN DSI language: '%s'", lang.c_str());
			
	if(XMLNode->first_attribute("lang2"))
	{
		lang2 = getAttrib(XMLNode, "lang2");
	}

	if (lang.empty() && XMLNode->first_attribute("lang"))
	{
		lang = getAttrib(XMLNode, "lang");
	}
	else
	{
		toLogEx("WARNING: No language assigned in string table\n");
	}

	for(XML_NODE *pNodeElem = XMLNode->first_node("str"); pNodeElem != NULL; pNodeElem = pNodeElem ->next_sibling("str"))
	{
		#ifdef _DEBUG
/*		String who;

		if (pNodeElem->Attribute("who"))
		{
			who.assign(pNodeElem->Attribute("who"));	
		}

		String how;

		if (pNodeElem->Attribute("how"))
		{
			how.assign(pNodeElem->Attribute("how"));	
		}*/
		#endif		
						
		if(pNodeElem->first_attribute("id"))
		{
			String id = getAttrib(pNodeElem, "id");
						
			//languages
			//-------------------------------------------
			Ogre::DisplayString data;
			data = Ogre::DisplayString("String '") + id + Ogre::DisplayString("' not found in '") + lang + Ogre::DisplayString("' language");
			bool hasData = false;

			XML_NODE *pStrNode = pNodeElem->first_node(lang.c_str());
			if(pStrNode && pStrNode->first_attribute("data"))
			{
				data = getAttrib(pStrNode, "data");
				hasData = true;
			}

			if(!hasData && !lang2.empty())
			{
				XML_NODE *pStrNode = pNodeElem->first_node(lang2.c_str());
				if(pStrNode && pStrNode->first_attribute("data"))
				{
					data = getAttrib(pStrNode, "data");
					hasData = true;
				}
			}

			//any language we have
			if(!hasData)
			{
				XML_NODE *pStrNode = pNodeElem->first_node();
				if(pStrNode && pStrNode->first_attribute("data"))
				{
					data = getAttrib(pStrNode, "data");
					hasData = true;
				}
			}

			if(hasData)
				app.getStringTable().add(id, data);
			else
				toLogEx("ERROR: Can't find string '%s' in any languages.\n",id.c_str());
		}
	}

	app.getStringTable().createFontdef();

}

void SceneLoader::processDust(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
	String entName = getAttrib(XMLNode, "name");
	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Dust: " + entName);
	updateProgress();

	bool stat = false;
	Ogre::DustController *dust = NULL;
	try
	{
		Ogre::NameValuePairList params;
		if(XMLNode->first_attribute("count"))
			params["count"] = getAttrib(XMLNode, "count");
		if(XMLNode->first_attribute("width"))
			params["width"] = getAttrib(XMLNode, "width");
		if(XMLNode->first_attribute("height"))
			params["height"] = getAttrib(XMLNode, "height");

		dust = (Ogre::DustController*)mSceneMgr->createMovableObject(Ogre::DustControllerFactory::FACTORY_TYPE_NAME, &params);
		pParent->attachObject(dust);
				
		//--------------
		dust->setCastShadows(false);
		if(XMLNode->first_attribute("renderQueue"))
			dust->setRenderQueueGroup(getAttribInt(XMLNode, "renderQueue"));
		if(XMLNode->first_attribute("visibleFlag"))
			dust->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
		else
			setDefaultVisibleFlag(dust);
		((Ogre::MovableObject*)dust)->setUserAny(Ogre::Any(pObj));
		pObj->mIsSelectable = false;
//		pObj->pEntity = reinterpret_cast<Ogre::Entity*>(dust);

		dust->setQueryFlags(0);
				
		if(XMLNode->first_attribute("material"))
		{
			dust->setMaterial(getAttrib(XMLNode, "material"));
		}
				
		Ogre::RenderSystem* rs = Ogre::Root::getSingleton().getRenderSystem(); 
		const Ogre::Viewport* vp = app.getViewport();

		Real hOffset = rs->getHorizontalTexelOffset() / (0.5f * vp->getActualWidth());
		Real vOffset = rs->getVerticalTexelOffset() / (0.5f * vp->getActualHeight());
				 
		Ogre::AxisAlignedBox aabInf;
		aabInf.setInfinite();
		dust->setBoundingBox(aabInf);
	}
	catch (...)
	{
		if(mShowError)
		    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] ERROR: dust '" + entName + "' failed to load, check the .scene");
	}

}

void SceneLoader::processRect2d(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
//	app.getCamera()->getPosition();
//	Ogre::Quaternion qCamRot = app.getCamera()->getOrientation();

	String entName = getAttrib(XMLNode, "name");
	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Rect2d: " + entName);
	updateProgress();

	bool stat = false;
			
	Ogre::AdvRectangle2D *rect = NULL;
	try
	{
#if JENGINE_MOUSEOLDWORK == TRUE
		if(XMLNode->first_attribute("sincity_mesh"))
		{
			if(!pObj) 
				return;
			String entName	= getAttribUnique(XMLNode, "name");
			Ogre::Entity *pEnt = NULL;
			String meshName = "0x_plashka.mesh";

			Ogre::LogManager::getSingleton().logMessage( "Creating mesh: " + meshName + "Parent name: " + pParent->getName());


			try
			{
				pEnt = mSceneMgr->createEntity(entName, meshName);
			}
			catch (Ogre::Exception ex)
			{
				if(mShowError)
				    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] ERROR: '" + meshName + "' failed to load");
				pEnt = NULL;
			}



			if (pEnt)
			{
				pParent->attachObject((Ogre::MovableObject *)pEnt);
				// update the dotSceneInfo graph
				bool stat = false;
				//--------------
				pEnt->setCastShadows(false);
				if(XMLNode->first_attribute("renderQueue"))
					pEnt->setRenderQueueGroup(getAttribInt(XMLNode, "renderQueue"));
				if(XMLNode->first_attribute("visibleFlag"))
					pEnt->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
				if(XMLNode->first_attribute("material"))
					pEnt->setMaterialName(getAttrib(XMLNode, "material"));

				float x = static_cast<float>(getAttribReal(XMLNode, "px"));
				float y = static_cast<float>(getAttribReal(XMLNode, "py"));
				float sx = static_cast<float>(getAttribReal(XMLNode, "spx"));
				float sy = static_cast<float>(getAttribReal(XMLNode, "spy"));

				Ogre::Vector3 vecUpperLeft(-9.f, 0.f, 7.f);
				//Ogre::Vector3 vecLowerRight(1041.f, 0.f, -781.f);
				Ogre::Vector3 vecLowerRight(1024.f, 0.f, -768.f);
				Ogre::Vector3 vecSub(vecLowerRight - vecUpperLeft);
				Ogre::Vector3 vecPosBase(x, 0.f, -y);
				Ogre::Vector3 vecPosNew(0.f, 0.f, 0.f);
				Ogre::Vector3 vecScaleNew(sx, 0.f, sy);
				Ogre::Vector3 vecMul(1024.f/1007.f, 0.f, 768.f/755.f);

				vecPosNew.x = vecUpperLeft.x + vecPosBase.x;
				vecPosNew.z = vecUpperLeft.z + vecPosBase.z;
				vecPosNew *= vecMul;

				pParent->setPosition(vecPosNew);

				vecScaleNew *= vecMul;
				pParent->setScale(vecScaleNew);

				pObj->pEntity = pEnt;
			}
			else
			{
				if(mShowError)
				    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] ERROR: 2drect's sincity_mesh '" + entName + "' failed to load, check the .scene");
			}
		}
		else
		if(XMLNode->first_attribute("sincity_rect") == NULL)
		{
			Ogre::Camera *pCamera = app.getCamera();
			if (!pCamera) 
				return;

			const char *chNodeName = pParent->getName().c_str();

			int loc = atoi(chNodeName);
			if (loc < 1)
				return;

			int room = atoi(chNodeName + 3);
			if (room < 1)
				return;


			char chCamera[256];
			sprintf(chCamera, "%02d_%02d_Camera01", loc, room);

			Ogre::Camera *pCameraRoom = app.getSceneManager()->getCamera(chCamera);


			Ogre::Vector3 vecCamPos;
			CVirtualScript::getPosition(app.findObjectByName(chCamera), vecCamPos);
			Ogre::Quaternion qCamRot;
			CVirtualScript::getOrientation2(app.findObjectByName(chCamera), qCamRot);

			pCamera->setPosition(vecCamPos);
			pCamera->setOrientation(qCamRot);
			//pCamera->setFOVy(pCameraRoom->getFOVy());
			CVirtualScript::cameraSetFOV(pCameraRoom->getFOVy().valueDegrees());

			Ogre::Ray rayUpperLeft = pCamera->getCameraToViewportRay(0.f, 0.f);
			Ogre::Ray rayUpperRight = pCamera->getCameraToViewportRay(1.f, 0.f);
			Ogre::Ray rayLowerLeft = pCamera->getCameraToViewportRay(0.f, 1.f);
			Ogre::Ray rayLowerRight = pCamera->getCameraToViewportRay(1.f, 1.f);

			Ogre::Vector3 vecUpperLeft = rayUpperLeft.getPoint(100.f);
			Ogre::Vector3 vecUpperRight = rayUpperRight.getPoint(100.f);
			Ogre::Vector3 vecLowerLeft = rayLowerLeft.getPoint(100.f);
			Ogre::Vector3 vecLowerRight = rayLowerRight.getPoint(100.f);

			Ogre::Real fScreenW = vecUpperLeft.distance(vecUpperRight);
			Ogre::Real fScreenH = vecUpperLeft.distance(vecLowerLeft);


			if (!pObj) return;
			String entName	= getAttribUnique(XMLNode, "name");
			Ogre::Entity *pEnt = NULL;
			String meshName = "0x_plashka.mesh";
			try
			{
				pEnt = mSceneMgr->createEntity(entName, meshName);
			}
			catch (Ogre::Exception ex)
			{
				if(mShowError)
				    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] ERROR: '" + meshName + "' failed to load");
				pEnt = NULL;
			}
			if (pEnt)
			{
				pParent->attachObject((Ogre::MovableObject *)pEnt);
				// update the dotSceneInfo graph
				bool stat = false;
				//--------------
				pEnt->setCastShadows(false);
				if(XMLNode->first_attribute("renderQueue"))
					pEnt->setRenderQueueGroup(getAttribInt(XMLNode, "renderQueue"));
				if(XMLNode->first_attribute("visibleFlag"))
					pEnt->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
				if(XMLNode->first_attribute("material"))
					pEnt->setMaterialName(getAttrib(XMLNode, "material"));


				float x = static_cast<float>(getAttribReal(XMLNode, "px"));
				float y = static_cast<float>(getAttribReal(XMLNode, "py"));
				float sx = static_cast<float>(getAttribReal(XMLNode, "spx"));
				float sy = static_cast<float>(getAttribReal(XMLNode, "spy"));

				Ogre::Vector3 vecPosNew(vecUpperLeft + (vecUpperRight - vecUpperLeft)*(x/1024.f) + (vecLowerLeft - vecUpperLeft)*(y/768.f));
				Ogre::Vector3 vecScaleNew(fScreenW*sx/1024.f, 0.f, fScreenH*sy/768.f);

				pParent->setPosition(vecPosNew);
				pParent->setScale(vecScaleNew);

				pParent->setOrientation(qCamRot);
				pParent->rotate(Ogre::Vector3(1.f, 0.f, 0.f), Ogre::Degree(-90.f));

				pObj->pEntity = pEnt;
			}
			else
				if(mShowError)
				    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] ERROR: 2drect's sincity_rect '" + entName + "' failed to load, check the .scene");
		}
		else
#endif // JENGINE_MOUSEOLDWORK
		{
			Ogre::NameValuePairList params;
			params["autoUnload"] = !XMLNode->first_attribute("static");
			rect = (Ogre::AdvRectangle2D*)mSceneMgr->createMovableObject(Ogre::AdvRectangle2DFactory::FACTORY_TYPE_NAME, &params);
			pParent->attachObject((Ogre::MovableObject *)rect);
					
			//--------------
			rect->setCastShadows(false);
			if(XMLNode->first_attribute("renderQueue"))
				rect->setRenderQueueGroup(getAttribInt(XMLNode, "renderQueue"));
			if(XMLNode->first_attribute("visibleFlag"))
				rect->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
			else
				setDefaultVisibleFlag(rect);


			((Ogre::MovableObject*)rect)->setUserAny(Ogre::Any(pObj));
					
			pObj->mIsSelectable = false;
			pObj->pEntity = reinterpret_cast<Ogre::Entity*>(rect);

			pObj->mIsSelectable = false;
					
			if(XMLNode->first_attribute("selectable"))
			{
				if(getAttribBool(XMLNode, "selectable"))
				{
					rect->setQueryFlags(1);
					pObj->mIsSelectable = true;
				}
			}
					
			if(!pObj->mIsSelectable)
				rect->setQueryFlags(0);
					
			if(XMLNode->first_attribute("material"))
			{
				rect->setMaterial(getAttrib(XMLNode, "material"));
			}
					
			Ogre::RenderSystem* rs = Ogre::Root::getSingleton().getRenderSystem(); 
			const Ogre::Viewport* vp = app.getViewport();
			assert(vp && "dotSceneLoaderImpl020::_doRectangle2d viewport is null!");

			Real hOffset = rs->getHorizontalTexelOffset() / (0.5f * vp->getActualWidth());
			Real vOffset = rs->getVerticalTexelOffset() / (0.5f * vp->getActualHeight());
					 
			if(XMLNode->first_attribute("x") && XMLNode->first_attribute("y") && XMLNode->first_attribute("sx") && XMLNode->first_attribute("sy"))
			{
				float x = static_cast<Ogre::Real>(getAttribReal(XMLNode, "x"));
				float y = static_cast<Ogre::Real>(getAttribReal(XMLNode, "y"));
				float sx = static_cast<Ogre::Real>(getAttribReal(XMLNode, "sx"));
				float sy = static_cast<Ogre::Real>(getAttribReal(XMLNode, "sy"));

				x = (x*2.0f)-1.0f;
				y = 1.0f - (y*2.0f);
				sx *= 2.0f;
				sy *= 2.0f;

				rect->setCorners(x + hOffset ,y - vOffset, x + sx + hOffset,y - sy - vOffset);
			}

			if(XMLNode->first_attribute("px") && XMLNode->first_attribute("py") && XMLNode->first_attribute("spx") && XMLNode->first_attribute("spy"))
			{
				float x = static_cast<float>(getAttribReal(XMLNode, "px"));
				float y = static_cast<float>(getAttribReal(XMLNode, "py"));
				float sx = static_cast<float>(getAttribReal(XMLNode, "spx"));
				float sy = static_cast<float>(getAttribReal(XMLNode, "spy"));

				x /= 1024.0;
				y /= 768.0;

				sx /= 1024.0;
				sy /= 768.0;

				x = (x*2.0f)-1.0f;
				y = 1.0f - (y*2.0f);
				sx *= 2.0f;
				sy *= 2.0f;

				rect->setCorners(x + hOffset,y - vOffset,x + sx + hOffset,y - sy - vOffset);
			}
			Ogre::AxisAlignedBox aabInf;
			aabInf.setInfinite();
			rect->setBoundingBox(aabInf);
		}

	}
	catch (Ogre::Exception ex)
	{
		if(mShowError)
		    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] ERROR: rect '" + entName + "' failed to load, check the .scene");
	}
}

void SceneLoader::processRibbonTrail(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
	String psName = getAttrib(XMLNode, "name");
	if(mShowLog)
	    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] Process Ribbon: " + psName);
	updateProgress();

	Ogre::RibbonTrail *pRT = 0;

	//String psScriptName = pNode->Attribute("templateName");

	try
	{
		pRT = mSceneMgr->createRibbonTrail(psName);
	}
	catch(Ogre::Exception ex)
	{
		if(mShowError)
		    Ogre::LogManager::getSingleton().logMessage( "[SceneLoader] ERROR: '" + psName + "' failed to load");
		pRT = NULL;
	}

	if (pRT)
	{
		bool stat = false;
		Ogre::MovableObject *mObj = NULL;
		for (Ogre::SceneNode::ObjectIterator it = pParent->getAttachedObjectIterator(); 
					it.hasMoreElements(); 
					it.getNext())
		{
			Ogre::MovableObject *obj = it.peekNextValue();
			if (obj->getMovableType() == "Entity")
				mObj = obj;
		}

		mSceneMgr->getRootSceneNode()->attachObject((Ogre::MovableObject *)pRT);
		if(XMLNode->first_attribute("attach_bone"))
		{	
			if(mObj)
				pRT->addNode(((Ogre::Entity*)mObj)->getSkeleton()->getBone(getAttrib(XMLNode, "attach_bone")));
		}
		else
		{					
			if (mObj && ((Ogre::Entity*)mObj)->getSkeleton())
			{
				pRT->addNode(((Ogre::Entity*)mObj)->getSkeleton()->getBone(0));
			}
			else
			{
				Ogre::SceneNode *pSceneNode = pParent->createChildSceneNode();
				pRT->addNode(pSceneNode);
			}
		}

		pRT->setVisible(getAttribBool(XMLNode, "show", true));

		if(XMLNode->first_attribute("lenght"))
		{
			pRT->setTrailLength(getAttribReal(XMLNode, "lenght"));
		}

		if(XMLNode->first_attribute("width"))
		{
			pRT->setInitialWidth(0, getAttribReal(XMLNode, "width"));
		}

		if(XMLNode->first_attribute("width_change"))
		{  
			pRT->setWidthChange(0, getAttribReal(XMLNode, "width_change"));
		}

		if(XMLNode->first_attribute("max_elements"))
		{
			pRT->setMaxChainElements(getAttribInt(XMLNode, "max_elements"));
		}

		if(XMLNode->first_attribute("renderQueue"))
		{
			pRT->setRenderQueueGroup(getAttribInt(XMLNode, "renderQueue"));
		}

		if(XMLNode->first_attribute("material"))
		{
			pRT->setMaterialName(getAttrib(XMLNode, "material"));
		}

		if(XMLNode->first_attribute("visibleFlag"))
		{ 
			pRT->setVisibilityFlags(getAttribInt(XMLNode, "visibleFlag"));
		}
		else
			setDefaultVisibleFlag(pRT);

		XML_NODE* pElem = XMLNode->first_node("color_change");
		if (pElem)
		{
			Ogre::ColourValue c = parseColour(pElem);	
			pRT->setColourChange(0, c);
			pRT->setUseVertexColours(true);
		}

		pElem = XMLNode->first_node("color");
		if (pElem)
		{
			Ogre::ColourValue c = parseColour(pElem);	
			pRT->setInitialColour(0, c);
			pRT->setUseVertexColours(true);
		}
	}
}

void SceneLoader::updateProgress()
{
	if(mProgressListener)
		mProgressListener->onProgress();
}

void SceneLoader::processAnimations(XML_NODE* XMLNode, CGameObject* pObj)
{
	for(XML_NODE *pElem = XMLNode->first_node("animation"); pElem != NULL; pElem = pElem->next_sibling("animation"))
	{
		if(pElem->first_attribute("skeleton") && pElem->first_attribute("anim_name"))
		{
			float move_speed = getAttribReal(pElem, "move_speed", 0);									
			float blend_speed = getAttribReal(pElem, "blend_speed", 3.0f);
			float blend_percent = getAttribReal(pElem, "blend_percent", 0);
			int layer = getAttribInt(pElem, "layer", 0);

			((CAnimatableObject*)pObj)->addAnimation(
				getAttrib(pElem, "skeleton"),
				getAttrib(pElem, "anim_name"),
				move_speed,
				blend_percent,
				blend_speed,
				layer);
						
			if(pElem->first_attribute("rot_speed"))
			{
				float rot_speed = getAttribReal(pElem, "rot_speed");
				((CAnimatableObject*)pObj)->setAnimationParam(
					getAttrib(pElem, "anim_name"),
					0, 
					rot_speed);
			}

			if(pElem->first_attribute("blend_frames"))
			{
				float blend_frames = getAttribReal(pElem, "blend_frames");
				((CAnimatableObject*)pObj)->setAnimationParam(
					getAttrib(pElem, "anim_name"), 
					1, 
					blend_frames);
			}
		}
	}

}

void SceneLoader::processOverlay(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
	updateProgress();
	String oName = getAttrib(XMLNode, "name");
	Ogre::Overlay*  m_Overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(oName);
	if (m_Overlay)
		m_Overlay->show();
}


#if USE_BULLET == TRUE
void processRigitBody(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
	CPhysObject *pPhys = NULL;

	if (pObj->getType() == CGameObject::T_PHYS_OBJ)
		pPhys = (CPhysObject*)pObj;
			

	float r = 0.6f;
	float f = 0.6f;
	float m = 1.0f;

	Ogre::SceneNode *pScNode = pParent;

	if (pNode->Attribute("restitution"))
		r = Ogre::StringConverter::parseReal(pNode->Attribute("restitution"));

	if (pNode->Attribute("friction"))
		f = Ogre::StringConverter::parseReal(pNode->Attribute("friction"));

	if (pNode->Attribute("mass"))
		m = Ogre::StringConverter::parseReal(pNode->Attribute("mass"));

	String bone("");
	String obj_name("");
	CGameObject* mGObj = NULL;
	Ogre::Entity *mEntityObj = NULL;

	String nodeName = pParent->getName();
	String rigitBodyName = "body_" + nodeName;
	String shapeName = "shape_" + nodeName;
			
	if (pNode->Attribute("obj"))
	{
		obj_name = pNode->Attribute("obj");

		mGObj = app._findObjectByName(pNode->Attribute("obj"));
		mEntityObj = mGObj->pEntity;
		pScNode = mGObj->pSNode;
	}

	if (pNode->Attribute("bone") && pNode->Attribute("obj"))
	{
		bone = pNode->Attribute("bone");
				
		rigitBodyName = "body_" + bone;
		shapeName = "shape_" + bone;
		nodeName = "dynamic_node_for_" + bone;

		//Ogre::Entity *mObj = NULL;// = pParent->getAttachedObject(0);

	}

	bool disable = false;

	if (pNode->Attribute("disable") && pNode->Attribute("disable") == String("true"))
	{
		disable = true;//body->getBulletRigidBody()->setActivationState(DISABLE_SIMULATION);
	}

	bool st = false;


	short coll_group = 1; 
	short coll_mask = 3;

	if (pNode->Attribute("coll_group"))
		coll_group = Ogre::StringConverter::parseReal(pNode->Attribute("coll_group"));

	if (pNode->Attribute("coll_mask"))
		coll_mask = Ogre::StringConverter::parseReal(pNode->Attribute("coll_mask"));


	OgreBulletDynamics::RigidBody *body =
		new OgreBulletDynamics::RigidBody(rigitBodyName,app.mWorld,coll_group,coll_mask);

	OgreBulletCollisions::CollisionShape *shape = NULL;

	OgreBulletCollisions::CompoundCollisionShape* compound = new OgreBulletCollisions::CompoundCollisionShape();

	for (	TiXmlElement *pElem = pNode->FirstChildElement();
		pElem != 0; pElem = pElem->NextSiblingElement())
	{
		String value = String(pElem->Value());


		if (value == "shape")
		{
			if (pElem->Attribute("type"))
			{
				String type = pElem->Attribute("type");

				float rad = 0;
				float height = 0;
				Ogre::Vector3 size(1.0f,1.0f,1.0f);
				Ogre::Vector3 normal(0,0,1.0f);
				Ogre::Vector3 axis(1.0f,0,0);
				Ogre::Vector3 shift(0,0,0);
				Ogre::Quaternion rot = Ogre::Quaternion::IDENTITY;

				if (pElem->Attribute("radius"))
					rad = Ogre::StringConverter::parseReal(pElem->Attribute("radius"));


				if (pElem->Attribute("height"))
					height = Ogre::StringConverter::parseReal(pElem->Attribute("height"));

				TiXmlElement *posElem = pElem->FirstChildElement("size");
				if (posElem)
					size = xml::readPosition(posElem);

				posElem = pElem->FirstChildElement("shift");
				if (posElem)
					shift = xml::readPosition(posElem);

				posElem = pElem->FirstChildElement("normal");
				if (posElem)
					normal = xml::readPosition(posElem);

				posElem = pElem->FirstChildElement("axis");
				if (posElem)
					axis = xml::readPosition(posElem);

				posElem = pElem->FirstChildElement("rot");
				if (posElem)
					rot = xml::readOrientation(posElem);

						

				if (type == "box")
				{
					shape = new OgreBulletCollisions::BoxCollisionShape(size);

				}
				else if (type == "plane")
				{
					st = true;
					shape = new OgreBulletCollisions::StaticPlaneCollisionShape(normal,rad);

				}
				else if (type == "cylinder")
				{
					shape = new OgreBulletCollisions::CylinderCollisionShape(size, axis);

				}
				else if (type == "sphere")
				{
					shape = new OgreBulletCollisions::SphereCollisionShape(rad);

				}
				else if (type == "capsule")
				{
					shape = new OgreBulletCollisions::CapsuleCollisionShape(rad,height,axis);

				}

				compound->addChildShape(shape, shift, rot);

			}


		}
	}

	//--------------------------------

	if (body && compound)
	{
		if (!mEntityObj)
		{
			if (!st)
			{
				//pParent = pParent->createChildSceneNode();	

				Vector3 pos = pParent->getPosition();
				Quaternion q = pParent->getOrientation();



				body->setShape(pParent, compound, r, f, m, 
					pos, q);

			}
			else
			{
				body->setStaticShape(pParent,shape,r,f,
					pParent->getPosition(), pParent->getOrientation());
			}

			pParent->getUserObjectBindings().
				setUserAny("body",Any(static_cast<OgreBulletDynamics::RigidBody*>(body)));

			pParent->getUserObjectBindings().
				setUserAny("shape",Any(static_cast<OgreBulletCollisions::CollisionShape*>(shape)));

			if (disable)
				body->getBulletRigidBody()->setActivationState(DISABLE_SIMULATION);
		}
		else
			if (mEntityObj->getSkeleton())
			{
				Ogre::Bone *pBone = mEntityObj->getSkeleton()->getBone(bone);
				Ogre::SceneNode *pSN = mpSceneManager->getRootSceneNode()->createChildSceneNode();
				//compound->addChildShape(shape, shift, rot);
				body->setShape(pSN, compound, r, f, m);

				pPhys->AddBody(CPhysObject::CBody(
					pScNode,
					pSN,
					pBone,
					body,
					shape,
					rigitBodyName,
					CPhysObject::CBody::Slave));

			}
	}
}

void processConstraint(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj)
{
	CPhysObject *pPhys = NULL;

	if (pObj->getType() == CGameObject::T_PHYS_OBJ)
		pPhys = (CPhysObject*)pObj;

	if (!pPhys)
		return;

	String rigitA, rigitB;

	if (pNode->Attribute("rigitA"))
		rigitA = pNode->Attribute("rigitA");

	if (pNode->Attribute("rigitB"))
		rigitB = pNode->Attribute("rigitB");

	float low = 0;
	float high = 0;

	if (pNode->Attribute("low"))
		low = Ogre::StringConverter::parseReal(pNode->Attribute("low"));

	if (pNode->Attribute("high"))
		high = Ogre::StringConverter::parseReal(pNode->Attribute("high"));

	Vector3 v1 = pPhys->getRigitBody(rigitA)->getWorldPosition();
	Vector3 v2 = pPhys->getRigitBody(rigitB)->getWorldPosition();
	Quaternion q1 = pPhys->getRigitBody(rigitA)->getWorldOrientation();
	Quaternion q2 = pPhys->getRigitBody(rigitB)->getWorldOrientation();
	Vector3 vv = (v1 + v2)*0.5;

	Vector3 vv1 = q1*Vector3::UNIT_X;
	Vector3 vv2 = q2*Vector3::UNIT_X;
			
			
	OgreBulletDynamics::HingeConstraint *cnst  = 
		new OgreBulletDynamics::HingeConstraint(
			pPhys->getRigitBody(rigitA),
			pPhys->getRigitBody(rigitB),	
			v1-vv, 
			v2-vv,
			vv2,
			vv1);


	((btHingeConstraint*)cnst->getBulletTypedConstraint())->setLimit(low, high);

	if (pNode->Attribute("angular_only"))
		((btHingeConstraint*)cnst->getBulletTypedConstraint())->setAngularOnly(true);

	bool coll = false;

	if (pNode->Attribute("disable_collision"))
		coll = true;

	/*btPoint2PointConstraint *c2 = new btPoint2PointConstraint(
			*pPhys->getRigitBody(rigitA)->getBulletRigidBody(),
			*pPhys->getRigitBody(rigitB)->getBulletRigidBody(),	
			OgreBulletCollisions::OgreBtConverter::to(v1-vv), 
			OgreBulletCollisions::OgreBtConverter::to(v2-vv));*/

			
/*	btTransform localA, localB;
	localA.setIdentity(); localB.setIdentity();
	localA.getBasis().setRotation(OgreBulletCollisions::OgreBtConverter::to(q1)); 
	localA.setOrigin(OgreBulletCollisions::OgreBtConverter::to(v1-vv));
	localB.getBasis().setRotation(OgreBulletCollisions::OgreBtConverter::to(q2)); 
	localB.setOrigin(OgreBulletCollisions::OgreBtConverter::to(v2-vv));


	btConeTwistConstraint *c3 = new btConeTwistConstraint(
			*pPhys->getRigitBody(rigitA)->getBulletRigidBody(),
			*pPhys->getRigitBody(rigitB)->getBulletRigidBody(),
			localA,
			localB);*/

	//((btHingeConstraint*)cnst->getBulletTypedConstraint())->setLimit(			

	//if (low != 0 || high != 0)

/*	OgreBulletDynamics::SixDofConstraint *cnst = 
		new OgreBulletDynamics::SixDofConstraint(
				pPhys->getRigitBody(rigitA),
				pPhys->getRigitBody(rigitB),
				v1,
				q1,
				v2,
				q2);

	cnst->setLimit(0,-0.0001,0.0001);
	cnst->setLimit(1,-0.0001,0.0001);
	cnst->setLimit(2,-0.0001,0.0001);
	cnst->setLimit(3,-0.0001,0.0001);
	cnst->setLimit(4,-0.0001,0.0001);
	cnst->setLimit(5,-0.0001,0.0001);*/
			
	//((btGeneric6DofConstraint*)cnst->getBulletTypedConstraint())->sesetLimit(0,0);

	//app.mWorld->addConstraint(cnst);

	//app.mWorld->getBulletDynamicsWorld()->addConstraint(c3,true);

	app.mWorld->getBulletDynamicsWorld()->addConstraint(cnst->getBulletTypedConstraint(),coll);

}


#endif