#include "stdafx.h"

#ifndef NO_SFX

#include "SfxCore.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	include <mmsystem.h>
#endif
#include <bass.h>

#include "MainApplication.h"
#include "VirtualScript.h"

//---------------------------------------

CSfxCore sfxCore;

void CSfxCore::process()
{
	if (mIsSfxCreated)
	{
		updateListener();
		processEnvSounds();
	}
}
/*
void* CSfxCore::UserOpen(const char* Name)
{
	String s(Name);

	if (!Ogre::ResourceGroupManager::getSingleton().resourceExists("General", s))
	{
		toLogEx("File %s doesn't exist!\n",Name);
		return NULL;
	}

	Ogre::DataStreamPtr pStream = Ogre::ResourceGroupManager::getSingleton().openResource( s );

	//pStream->
	size_t id = findFreeHandle();

    if (id<mSoundHandles.size())
	{
		sprintf(mSoundHandles[id].name,"%s",s.c_str());
		return pStream.getPointer();
	}
	return NULL;
}
*/

CSfxCore::CSfxCore()
{
	mSfxListener = 0;
	mIsSfxCreated = false;

    for (int i = 0; i < ENV_CACHE_SIZE - 1; i++)
        mEnvCache[i] = mError;
    mEnvCache[ENV_CACHE_SIZE - 1] = 0;
}

CSfxCore::~CSfxCore()
{
	//deinitialize();
}

void CSfxCore::initialize()
{
	char buf[256];
	sprintf(buf, "Software\\%s\\%s\\", app.getCompanyName().c_str(), app.getAppFullName().c_str());

	int m_iMusicVolume, m_iSoundVolume;
	m_iSoundVolume = app.getAppState().getValueInt("SoundVolume");
	m_iMusicVolume = app.getAppState().getValueInt("MusicVolume");

	mGroupVolumes[SFX_MUSIC_GROUP] = m_iMusicVolume;
	mGroupVolumes[SFX_EFFECTS_GROUP] = m_iSoundVolume;
	mGroupVolumes[SFX_EFFECTS_GROUP+1] = m_iSoundVolume;
	mGroupVolumes[SFX_EFFECTS_GROUP+2] = m_iSoundVolume;

	mIsSfxCreated = false;
	mCurEnvSound = mError;
	mPrevEnvSound = mError;
	mCurEnvSoundVol = 0;
	mPrevEnvSoundVol = 0;
	mFadeSpeed = 20;

	m_fEnvMaxVolume = 1.0f;
	m_fEnvCurVolume = 1.0f;
	m_fEnvSpeedVolume = 0;

	mSoundHandles.clear();
	mSoundHandles.resize(SFX_MAX_HANDLES);
	for (size_t i = 0; i < mSoundHandles.size(); i++)
		mSoundHandles[i].reset();
	memset(mGrTimes, 0, sizeof(int)*SFX_MAX_GROUPS);
	memset(mEnvCache, -1, sizeof(int)*ENV_CACHE_SIZE);

	mCachedPathsInitialized = false;

	//--------------------------------------------------------
	if (app.soundsEnabled())
	{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
#	ifndef BASS_CONFIG_IOS_MIXAUDIO
#		define BASS_CONFIG_IOS_MIXAUDIO 34
#	endif
		BASS_SetConfig(BASS_CONFIG_IOS_MIXAUDIO, 4);
#endif
		int bass = BASS_Init(-1, 44100, 0, 0 ,NULL);
		if (bass)
		{
			mIsSfxCreated = true;

            loadEnvSounds();
		}
		else
			toLogEx("ERROR: Bass initalization failed. Sound disabled.\n");
	}
}

void CSfxCore::deinitialize()
{
	printf("\n CSfxCore::deinitialize()");
	freeAllSounds();
	if (mIsSfxCreated)
	{
#if (OGRE_PLATFORM != OGRE_PLATFORM_IPHONE) && (OGRE_PLATFORM != OGRE_PLATFORM_APPLE)
		BASS_Stop();
		BASS_Free();
		mIsSfxCreated = false;
#endif
	}
}

void CSfxCore::loadEnvSounds()
{
	if (CVirtualScript::getCfgB("do_not_load_env_sounds"))
		return;

    if (mEnvCache[0] != mError || !mIsSfxCreated)
        return;

    //--------------------------------------------------------
    mEnvCache[0] = loadSound("00_env_01",0);
    mEnvCache[1] = loadSound("00_env_02",0);
    mEnvCache[2] = loadSound("00_env_03",0);
    mEnvCache[3] = loadSound("00_env_04",0);
    mEnvCache[4] = loadSound("00_env_05",0);
    mEnvCache[5] = loadSound("00_env_06",0);
    mEnvCache[6] = loadSound("00_env_07",0);
    mEnvCache[7] = loadSound("00_env_08",0);
    mEnvCache[8] = loadSound("00_env_09",0);
    mEnvCache[9] = loadSound("00_env_10",0);
    mEnvCache[10] = loadSound("00_env_11",0);
    mEnvCache[11] = loadSound("00_env_12",0);
    mEnvCache[12] = loadSound("00_env_13",0);
    mEnvCache[13] = loadSound("00_env_14",0);
    mEnvCache[14] = loadSound("00_env_15",0);
    mEnvCache[15] = loadSound("00_env_16",0);

    //toLogEx("\n env cache = %d", mEnvCache);

    mEnvCache[ENV_CACHE_SIZE-1] = 0;
}

float CSfxCore::getLevel(size_t id, float time)
{
	float result = 0;

	if (id<mSoundHandles.size() && mSoundHandles[id].has_levels)
	{
		float total_time = mSoundHandles[id].levels[mSoundHandles[id].levels.size()-1].time;

		if (mSoundHandles[id].looped)
		{
			while (time > total_time)
				time -= total_time;
		}

		if (time > total_time)
		{
			result = 0;
		}
		else
		{
			for (size_t i=1; i<mSoundHandles[id].levels.size(); i++)
			{
				float t1 = mSoundHandles[id].levels[i-1].time;
				float t2 = mSoundHandles[id].levels[i].time;

				float l1 = mSoundHandles[id].levels[i-1].level;
				float l2 = mSoundHandles[id].levels[i].level;

				if (time >= t1 && time <= t2)
				{
					result = l1 + (l2-l1)*((time - t1)/(t2 - t1));
					break;
				}
			}
		}
	}
	else
		result = 1.0f;

	return result;
}

void CSfxCore::loadDuration(size_t id)
{
	if (id<mSoundHandles.size())
	{
		String name(mSoundHandles[id].name);
		size_t pos = name.find_last_of(".");
		if (pos!=String::npos)
			name.replace(pos, name.length()-pos, ".lvl");

		//if (Ogre::ResourceGroupManager::getSingleton().resourceExists("General", name))
		if(CVirtualScript::resourceExists(name))
		{
			Ogre::DataStreamPtr fp = Ogre::ResourceGroupManager::getSingleton().openResource(name);
			if (!fp.isNull())
			{
				unsigned d = 0;
				if (fp->read(&d, sizeof(unsigned)) == sizeof(unsigned) && d > 0)
				{
					toLogEx("Loaded duration from levels for %s\n", name.c_str());

					mSoundHandles[id].duration = d;
				}

				fp->close();

			}
		}
	}

}

bool CSfxCore::loadLevels(size_t id)
{
	if (id<mSoundHandles.size())
	{
		String name(mSoundHandles[id].name);
		size_t pos = name.find_last_of(".");
		if (pos!=String::npos)
			name.replace(pos, name.length()-pos, ".lvl");

		//if (Ogre::ResourceGroupManager::getSingleton().resourceExists("General", name))
		if(CVirtualScript::resourceExists(name))
		{
			Ogre::DataStreamPtr fp = Ogre::ResourceGroupManager::getSingleton().openResource(name);
			if (!fp.isNull())
			{
				size_t s;
				if (fp->read(&s, sizeof(size_t))==sizeof(size_t) && s>0)
				{
					for(size_t i = 0; i < s; i++)
					{
						float l = 0, t = 0;
						fp->read(&l, sizeof(float));
						fp->read(&t, sizeof(float));
						mSoundHandles[id].levels.push_back(Sample(t,l));
					}
				}

				mSoundHandles[id].duration = static_cast<unsigned int>(mSoundHandles[id].levels[mSoundHandles[id].levels.size()-1].time*1000);

				toLogEx("Loaded levels from %s\n", name.c_str());
				mSoundHandles[id].has_levels = true;
				fp->close();
				return true;
			}
		}
	}
	else
		toLogEx("Warning: SFX_CORE Id '%u' is out of range in loadLevels!\n", id);
	return false;
}

void CSfxCore::saveLevels(size_t id)
{
	if (id<mSoundHandles.size())
	{
		String name(mSoundHandles[id].name);
		size_t pos = name.find_last_of(".");
		if (pos!=String::npos)
			name.replace(pos, name.length()-pos, ".lvl");

		FILE *lvl = fopen(name.c_str(),"wb+");
		if (lvl)
		{
			size_t s = mSoundHandles[id].levels.size();
			fwrite(&s,sizeof(size_t),1,lvl);
			for(size_t i = 0; i < s; i++)
			{
				fwrite(&mSoundHandles[id].levels[i].level,sizeof(float),1,lvl);
				fwrite(&mSoundHandles[id].levels[i].time,sizeof(float),1,lvl);
			}
			toLogEx("Save levels to %s\n", name.c_str());
			fclose(lvl);
		}
	}
	else
		toLogEx("Warning: SFX_CORE Id '%u' is out of range in saveLevels!\n", id);
}

void CSfxCore::updateListener()
{
}

void CSfxCore::assignListener(SFX_OBJECT_HANDLE obj)
{
	mSfxListener = obj;

	updateListener();

	toLogEx("SFX_CORE: Listener %d assigned\n",obj);
}

void CSfxCore::setGroupVolume(int group, int volume)
{
	mGroupVolumes[group] = volume;
	for (size_t i = 1; i < mSoundHandles.size(); i++)
		if (mSoundHandles[i].sample && mSoundHandles[i].group==group)
			setVolume(i, mSoundHandles[i].volume);
}

size_t CSfxCore::findFreeHandle()
{
	for (size_t i = 1; i < mSoundHandles.size(); i++)
		if (!mSoundHandles[i].sample)
			return i;
	return mError;
}

size_t CSfxCore::findByName(const String& name )
{
	for (size_t i = 1; i < mSoundHandles.size(); i++)
	{
		//if (strstr(mSoundHandles[i].name, name.c_str()))
		//if(strcmp(mSoundHandles[i].name,name.c_str()) == 0)
		if(name + String(".") + getFormat() == mSoundHandles[i].name)
		{
			return i;
		}
	}

	return mError;
}

void CSfxCore::initializeCachedPath()
{
	//mCachedPaths

	String groups[] = {"General","Start","Heap"};

	Ogre::String path;


	for(int i = 0; i < 3; i++)
	{
		String gr = groups[i];
	
		const Ogre::ResourceGroupManager::LocationList& list =
			Ogre::ResourceGroupManager::getSingleton().getResourceLocationList(gr);

		for (Ogre::ResourceGroupManager::LocationList::const_iterator iter=list.begin(); iter!=list.end(); ++iter)
		{
			Ogre::Archive* arch = (*iter)->archive;
			Ogre::StringVectorPtr fl = arch->find("*.ogg",false,false);
						
			for (Ogre::StringVector::iterator ni = fl->begin(); ni != fl->end(); ++ni)
			{
				Ogre::String s = (*ni);
				
				mCachedPaths[s] = arch->getName();
			} 
			
		}
	}

	mCachedPathsInitialized = true;
}

Ogre::String CSfxCore::getCachedPath(const Ogre::String &name)
{
	OGRE_HashMap<String, String>::iterator iter = mCachedPaths.find(name);
	if ( iter != mCachedPaths.end() ) 
		return iter->second;

	return Ogre::EmptyString;
}

size_t CSfxCore::loadSound(const String& name, bool memFlag)
{
	if (!mIsSfxCreated) return mError;

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	memFlag = true;
#endif

	//do not load same sound twice
	size_t i = findByName(name);

	if(i != mError)
		return i;

	size_t id = findFreeHandle();

	if (id<mSoundHandles.size())
	{
		if (mEnvCache[ENV_CACHE_SIZE-1] > -1 && name.find("00_env")!=String::npos)
		{
			//00_env_
			int z = atoi(name.c_str()+7);
			if (z > 0)
			{
				toLogEx("SFX_CORE: Use env cache for %s file\n", name.c_str());
				return mEnvCache[z-1];
			}
		}

		char buf[256];
		sprintf(buf,"%s.%s", name.c_str(), getFormat());
		strncpy(mSoundHandles[id].name, buf, sizeof(mSoundHandles[id].name));

		
		if (memFlag)
		{
			if(!CVirtualScript::resourceExists(buf))
			{
				toLogEx("Error: SFX_CORE File %s doesn't exist!\n", buf);
				return mError;
			}

			Ogre::DataStreamPtr pStream = Ogre::ResourceGroupManager::getSingleton().openResource( buf );
			if (!pStream.isNull())
			{
				mSoundHandles[id].sample = (HSAMPLE)BASS_SampleLoad(TRUE,
													(pStream)->getAsString().c_str(),
													0,Ogre::ulong(pStream->size()),1,BASS_SAMPLE_SOFTWARE);
				if (!mSoundHandles[id].sample)
					mSoundHandles[id].pStream = NULL;
				pStream->close();
				pStream.setNull();
			}
		}
		else
		{
			mSoundHandles[id].pStream = &mSoundHandles[id];
			//bool exist = Ogre::ResourceGroupManager::getSingleton().resourceExists("General",buf);

			if(!mCachedPathsInitialized)
				initializeCachedPath();


			//String path = app.getResourcesPath("");

			/*String gr("General");

			//���� ������� ������, �� ����� �� ������
			//if(Ogre::ResourceGroupManager::getSingleton().getResourceLocationList("General").size() < 2)
			//	gr = String("Start");

			bool found = false;

			const Ogre::ResourceGroupManager::LocationList& list =
				Ogre::ResourceGroupManager::getSingleton().getResourceLocationList(gr);

			for (Ogre::ResourceGroupManager::LocationList::const_iterator iter=list.begin(); iter!=list.end(); ++iter)
			{
				Ogre::Archive* arch = (*iter)->archive;
				if (arch && arch->exists(buf))
				{
					path = arch->getName();
					found = true;
					break;
				}
			}

			if(!found)
			{
				gr = String("Start");

				const Ogre::ResourceGroupManager::LocationList& list =
				Ogre::ResourceGroupManager::getSingleton().getResourceLocationList(gr);

				for (Ogre::ResourceGroupManager::LocationList::const_iterator iter=list.begin(); iter!=list.end(); ++iter)
				{
					Ogre::Archive* arch = (*iter)->archive;
					if (arch && arch->exists(buf))
					{
						path = arch->getName();
						found = true;
						break;
					}
				}
			}

            if (!found)
            {
				gr = String("Heap");
				const Ogre::ResourceGroupManager::LocationList& list =
                Ogre::ResourceGroupManager::getSingleton().getResourceLocationList(gr);
				for (Ogre::ResourceGroupManager::LocationList::const_iterator iter=list.begin(); iter!=list.end(); ++iter)
				{
					Ogre::Archive* arch = (*iter)->archive;
					if (arch && arch->exists(buf))
					{
						path = arch->getName();
						//toLogEx("\n arch name: %s", arch->getName().c_str());
						found = true;
						break;
					}
				}
            }

			if(!found)
				toLogEx("ERROR: Sound %s not found in any group!",name.c_str());*/

			String path = getCachedPath(buf);

			if(!path.size())
				toLogEx("ERROR: Sound %s not found in any group!",name.c_str());


//			toLogEx("\n EKK: %s, %s", path.c_str(), buf);

			path += String("/") + buf;

			mSoundHandles[id].sample = BASS_StreamCreateFile(STREAMFILE_NOBUFFER,
				path.c_str(),
				0,
				0,
				0);

			if (mSoundHandles[id].sample)
			{
				setVolume(id,0);
			}
			else
			{
				toLogEx("ERROR: SFX_CORE Couldn't load file %s from path \"%s\"! \n", buf, path.c_str());
				return mError;
			}

		}

		if (name.find("_")==String::npos)
			loadLevels(id);
		else //we no need levels here, but still need sound duration
			loadDuration(id);

		mSoundHandles[id].channel = 0;
		mSoundHandles[id].looped = 0;
		mSoundHandles[id].used = 1;
		mSoundHandles[id].volume = -1;
		mSoundHandles[id].fade = 0;
		mSoundHandles[id].notifyObj = 0;
		mSoundHandles[id].group = -1;
		mSoundHandles[id].type = 0;
		mSoundHandles[id].position[0] = mSoundHandles[id].position[1] = mSoundHandles[id].position[2] = 0;
		mSoundHandles[id].velocity[0] = mSoundHandles[id].velocity[1] = mSoundHandles[id].velocity[2] = 0;

		toLogEx("\nSFX_CORE: Sound %s loaded (id: %d)\n", name.c_str(), id);
	}
	else
		toLogEx("\nWarning: SFX_CORE can't find free handle\n");
	return id;
}

void CSfxCore::playSound(size_t id, int group, bool looped, SFX_OBJECT_HANDLE notifyObj)
{
	if (!mIsSfxCreated) return;
	if (!testGroupTime(group)) return;

	if (id<mSoundHandles.size())
	{
		SFX_HANDLE_TYPE& handle = mSoundHandles[id];
		if (handle.channel)
			stopSound(id);
		if (!handle.pStream)
			handle.channel = BASS_SampleGetChannel(handle.sample, FALSE);
		else
			handle.channel = handle.sample;

		BASS_ChannelPlay(handle.channel, TRUE);
		handle.group = group;
		handle.notifyObj = notifyObj;
		handle.looped = looped;
		setGroupVolume(group, mGroupVolumes[group]);
		if (handle.looped)
			BASS_ChannelFlags(handle.channel, BASS_SAMPLE_LOOP, BASS_SAMPLE_LOOP);
	}
	else
		toLogEx("Warning: SFX_CORE Id '%u' is out of range in playSound!\n", id);
}

void CSfxCore::playSound3D(size_t id, int group, bool looped, Vector3 &pos, SFX_OBJECT_HANDLE notifyObj)
{
}

void CSfxCore::playSoundHandle3D(size_t id, int group, bool looped, SFX_OBJECT_HANDLE source, SFX_OBJECT_HANDLE notifyObj)
{
}

void CSfxCore::stopSound(size_t id)
{
	if (!mIsSfxCreated) return;

	if (id<mSoundHandles.size())
	{
		SFX_HANDLE_TYPE& handle = mSoundHandles[id];
		BASS_SampleStop(handle.sample);
		BASS_ChannelStop(handle.channel);
		handle.channel = 0;
	}
	else
		toLogEx("Warning: SFX_CORE Id '%u' is out of range in stopSound!\n", id);
}

bool CSfxCore::isPlaying(size_t id)
{
	if (id<mSoundHandles.size())
		return (mSoundHandles[id].channel != 0);

	return false;
}

void CSfxCore::freeSound(size_t id)
{
	if (!mIsSfxCreated) return;

	if (id<mSoundHandles.size())
	{
		if (mSoundHandles[id].sample)
		{
			stopSound(id);

			SFX_HANDLE_TYPE& handle = mSoundHandles[id];
			if (handle.pStream)
			{
				BOOL freed = BASS_StreamFree((HSAMPLE)handle.sample);
/*
				handle.stream = 0;
*/
			}
			else
			{
				BASS_SampleFree((HSAMPLE)handle.sample);
			}
/*
			handle.channel = 0;
			handle.sample = 0;
			handle.used = 0;
*/
			handle.reset();

		}
		else
			toLogEx("Warning: SFX_CORE Id is already fried!\n");
	}
	else
		toLogEx("Warning: SFX_CORE Id is out of range in freeSound!\n");
}

void CSfxCore::freeAllSounds()
{
	for(size_t i = 0; i < mSoundHandles.size(); i++)
		if (mSoundHandles[i].sample)
			freeSound(i);
}

void CSfxCore::logLoadedSounds()
{
	toLogEx("***Loaded sounds: %d***\n", mSoundHandles.size());

	for(size_t i = 0; i < mSoundHandles.size(); i++)
	{
		if(mSoundHandles[i].name[0] > 0)
			toLogEx("\n %d: %s",i,mSoundHandles[i].name);
	}
}

int CSfxCore::freeAllSoundsButEnv()
{
	int count = 0;

	for(size_t i = 0; i < mSoundHandles.size(); i++)
	{
		if (mSoundHandles[i].sample && strstr(mSoundHandles[i].name, "00_env") == NULL)
		{
			freeSound(i);
			count++;
		}
	}

	return count;
}


unsigned int CSfxCore::getDuration(size_t id)
{
	if (id < mSoundHandles.size())
	{
		if(mSoundHandles[id].duration)
		{
			return mSoundHandles[id].duration;
		}
		else
		{
			if(mSoundHandles[id].has_levels)
				return static_cast<unsigned int>(mSoundHandles[id].levels[mSoundHandles[id].levels.size()-1].time*1000);
			else
				return static_cast<unsigned int>(BASS_ChannelBytes2Seconds(mSoundHandles[id].channel, BASS_ChannelGetLength(mSoundHandles[id].channel,BASS_POS_BYTE))*1000);
		}
	}
	else
		toLogEx("Warning: SFX_CORE Id is out of range in getDuration!\n");
	return 0;
}

/*
bool CSfxCore::getSoundState(size_t id) const
{
	return (mIsSfxCreated && BASS_ChannelIsActive(mSoundHandles[id].channel) == BASS_ACTIVE_PLAYING);
}*/

void CSfxCore::setSoundLooped(size_t id, bool isLoop)
{
	if (mIsSfxCreated)
	{
		if (id<mSoundHandles.size())
			mSoundHandles[id].looped = isLoop;
		else
			toLogEx("Warning: SFX_CORE Id '%u' is out of range in setSoundLooped!\n", id);
	}
}

int CSfxCore::getVolume(size_t id) const
{
	if (id<mSoundHandles.size())
		return mSoundHandles[id].volume;
	return 0;
}

void CSfxCore::setVolume(size_t id, int vol)
{
	if (mIsSfxCreated)
	{
		if (id<mSoundHandles.size())
		{
			if (mSoundHandles[id].group>=0)
			{
				mSoundHandles[id].volume = vol;
				float v = vol*(mGroupVolumes[mSoundHandles[id].group]/100.f);
				BASS_ChannelSetAttribute(mSoundHandles[id].channel,
										 BASS_ATTRIB_VOL,
										 v/100.f);
			}
		}
    /*
     �����������, ����� �� ����� � ��� �� ������ �����
		else
			toLogEx("Warning: SFX_CORE Id '%u' is out of range in setVolume!\n", id);
     */
	}
}

void CSfxCore::stopGroup(int group)
{
}

void CSfxCore::pauseGroup(int group, int bPause)
{
}

void CSfxCore::pauseAll(bool pause)
{
	if (!mIsSfxCreated) return;

	if (pause)
		BASS_Pause();
	else
		BASS_Start();
}

bool CSfxCore::testGroupTime(int group)
{
	return true;
}

//-----------------------------------------
void CSfxCore::addEnvSound(size_t id, bool looped, SFX_OBJECT_HANDLE notifyObj)
{
	if (mCurEnvSound == id)
		return;

	if (mCurEnvSound != mError && (unsigned)mCurEnvSound > ENV_CACHE_SIZE-1)
	{
		toLogEx("ERROR: CurEnvSound damaged (value=%d)!\n", mCurEnvSound);
		return;
	}

	mPrevEnvSound = mCurEnvSound;
	mCurEnvSound = id;

	SFX_HANDLE_TYPE *pCur = NULL, *pPrev = NULL;
	if (mCurEnvSound != mError && mCurEnvSound<mSoundHandles.size())
		pCur = &mSoundHandles[mCurEnvSound];
	if (mPrevEnvSound != mError && mPrevEnvSound<mSoundHandles.size())
		pPrev = &mSoundHandles[mPrevEnvSound];
	if (pPrev)
	{
		pPrev->fade = -1;
		mPrevEnvSoundVol = mCurEnvSoundVol;
	}

	if (pCur)
	{
		pCur->fade = 1;
		if (BASS_ChannelIsActive(mSoundHandles[id].channel) == BASS_ACTIVE_STOPPED)
		{
			mCurEnvSoundVol = 0;
			playSound(mCurEnvSound, SFX_MUSIC_GROUP, looped,notifyObj);
			setVolume(mCurEnvSound, static_cast<int>(mCurEnvSoundVol));
		}
	}
	toLogEx("SFX_CORE: addEnvSound [%d]\n",id);
//	toLogEx("ERROR: addEnvSound function undefined");
}


void CSfxCore::setEnvLevel(float level)
{
	const float env_max_speed = 0.4f;

	m_fEnvMaxVolume = level;

    if (m_fEnvMaxVolume < m_fEnvCurVolume)
		m_fEnvSpeedVolume = -env_max_speed;
	else
		m_fEnvSpeedVolume = env_max_speed;
}
void CSfxCore::setEnvFadeSpeed(float speed)
{
	mFadeSpeed = speed;
}

void CSfxCore::processEnvSounds()
{
	float inc = ((float)app.getFrameTime())/1000.0f;
	if (inc > 0.03f) inc = 0.03f;

	SFX_HANDLE_TYPE *pCur = 0, *pPrev = 0;
	if (mCurEnvSound != mError && (unsigned)mCurEnvSound > ENV_CACHE_SIZE-1)
	{
		toLogEx("ERROR: CurEnvSound damaged (value=%d)\n", mCurEnvSound);
		mCurEnvSound = mError;
		return;
	}

	if (mCurEnvSound<mSoundHandles.size())
		pCur = &mSoundHandles[mCurEnvSound];
	if (mPrevEnvSound<mSoundHandles.size())
		pPrev = &mSoundHandles[mPrevEnvSound];

	bool cur_update = false;
	bool prev_update = false;
	if (m_fEnvSpeedVolume != 0)
	{
		m_fEnvCurVolume += m_fEnvSpeedVolume*inc;
		if (m_fEnvSpeedVolume < 0 && m_fEnvCurVolume < m_fEnvMaxVolume)
		{
			m_fEnvSpeedVolume = 0;
			m_fEnvCurVolume = m_fEnvMaxVolume;
		}
		if (m_fEnvSpeedVolume > 0 && m_fEnvCurVolume > m_fEnvMaxVolume)
		{
			m_fEnvSpeedVolume = 0;
			m_fEnvCurVolume = m_fEnvMaxVolume;
		}
		cur_update = true;
		prev_update = true;
	}
	if (pCur)
	{
		if (pCur->fade == 1)
		{
			mCurEnvSoundVol += mFadeSpeed*inc;

			if (mCurEnvSoundVol > 100.0f)
			{
				pCur->fade = 0;
				mCurEnvSoundVol = 100.0f;

				for (size_t i = 0; i < ENV_CACHE_SIZE-1; i++)
				{
					if (i != mCurEnvSound)
					{
						stopSound(i);
						mPrevEnvSound = mError;
						mSoundHandles[i].fade = 0;
					}
				}
			}
			cur_update = true;
		}
	}
	if (pPrev)
	{
		mPrevEnvSoundVol -= mFadeSpeed*inc;
		if (mPrevEnvSoundVol < 0)
		{
			pPrev->fade = 0;
			mPrevEnvSoundVol = 0;
			stopSound(mPrevEnvSound);
			mPrevEnvSoundVol = -1.f;
		}
		prev_update = true;
	}
	if (pCur && cur_update)
	{
		setVolume(mCurEnvSound, static_cast<int>(mCurEnvSoundVol*m_fEnvCurVolume));
	}
	if (pPrev && prev_update)
	{
		setVolume(mPrevEnvSound, static_cast<int>(mPrevEnvSoundVol*m_fEnvCurVolume));
	}
}

#endif //#ifndef NO_SFX
