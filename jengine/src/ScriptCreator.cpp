#include "stdafx.h"
#include "ScriptCreator.h"

CScriptCreatorTable mScriptTable;


OGRE_HashMap<String, CScriptCreatorBase*>* CScriptCreatorTable::mScripts = NULL;


CScriptCreatorBase::CScriptCreatorBase(const String& name) 
{
	mName = name;
	std::replace( mName.begin(), mName.end(), '\\', '/' );
	size_t i = mName.find_last_of('/');

	mName = (i == String::npos) ? name : mName.substr(i+1, mName.size() - i - 1);
	std::transform(mName.begin(), mName.end(), mName.begin(), tolower);

	i = mName.find_last_of('.');
	if(i != String::npos)
	{
		mName = mName.substr(0, i);
	}
	mName += ".sc";
	mScriptTable.getScripts()[mName] = this; 	
}

CVirtualScript* CScriptCreatorBase::getScriptObject(const String& name)
{
	String lowername(name);
	Ogre::StringUtil::toLowerCase(lowername);
	if(mScriptTable.getScripts().find(lowername) != mScriptTable.getScripts().end())
	{
		CVirtualScript* script = mScriptTable.getScripts()[lowername]->createScriptObject();
		return script;
	}
	else
	{
		toLogEx("\nERROR: Can't find script '%s'!\n", name.c_str());

		assert(true);
	}
	return NULL;
}




OGRE_HashMap<String, CScriptCreatorBase*>& CScriptCreatorTable::getScripts()
{
	if (mScripts == NULL)
	{
		mScripts = new OGRE_HashMap<String, CScriptCreatorBase*>();
	}
	return *mScripts; 
}
CScriptCreatorTable::~CScriptCreatorTable()
{
	if (mScripts)
		delete mScripts;
}
