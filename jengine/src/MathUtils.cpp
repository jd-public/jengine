#include "stdafx.h"
#include "MathUtils.h"


namespace MathUtils
{

	
Ogre::Vector2 getScreenPoint2dFromPoint3d(const Ogre::Matrix4& projection, const Ogre::Matrix4& view, Ogre::OrientationMode orientation, const Ogre::Vector3& point)
{
	Ogre::Vector2 vp;
	Ogre::Vector3 hcs = projection*(view*point);
	Real x = (hcs.x * 0.5f) + 0.5f;// 0 <= x <= 1 // left := 0,right := 1
	Real y = (hcs.y * 0.5f) + 0.5f;// 0 <= y <= 1 // bottom := 0,top := 1
	switch(orientation)
	{
	case Ogre::OR_PORTRAIT:
		vp.x = x;
		vp.y = 1.f - y;
		break;
	case Ogre::OR_DEGREE_180:
		vp.x = 1.f - x;
		vp.y = y;
		break;
	case Ogre::OR_LANDSCAPELEFT:
		vp.x = 1.f - y;
		vp.y = 1.f - x;
		break;
	case Ogre::OR_LANDSCAPERIGHT:
		vp.x = y;
		vp.y = x;
		break;
	}
	return vp;
}

bool intersectCircle2dTri2d(const Ogre::Vector2& cirCenter, Ogre::Real cirRadius, const Ogre::Vector2& tri1, const Ogre::Vector2& tri2, const Ogre::Vector2& tri3)
{
	// TEST 1: Vertex within circle
	Ogre::Real radiusSqr = cirRadius*cirRadius;

	Ogre::Real c1x = cirCenter.x - tri1.x;
	Ogre::Real c1y = cirCenter.y - tri1.y;		
	Ogre::Real c1sqr = c1x*c1x + c1y*c1y - radiusSqr;
	if (c1sqr <= 0) return true;

	Ogre::Real c2x = cirCenter.x - tri2.x;
	Ogre::Real c2y = cirCenter.y - tri2.y;		
	Ogre::Real c2sqr = c2x*c2x + c2y*c2y - radiusSqr;
	if (c2sqr <= 0) return true;

	Ogre::Real c3x = cirCenter.x - tri3.x;
	Ogre::Real c3y = cirCenter.y - tri3.y;		
	Ogre::Real c3sqr = c3x*c3x + c3y*c3y - radiusSqr;
	if (c3sqr <= 0) return true;

	// TEST 2: Circle centre within triangle

	// Calculate edges
	Ogre::Real e1x = tri2.x - tri1.x;
	Ogre::Real e1y = tri2.y - tri1.y;

	Ogre::Real e2x = tri3.x - tri2.x;
	Ogre::Real e2y = tri3.y - tri2.y;

	Ogre::Real e3x = tri1.x - tri3.x;
	Ogre::Real e3y = tri1.y - tri3.y;

	if (e1y*c1x-e1x*c1y>=0 && e2y*c2x-e2x*c2y>=0 && e3y*c3x-e3x*c3y>=0)
		return true;

	// TEST 3: Circle intersects edge
	// First edge
	Ogre::Real k = c1x*e1x + c1y*e1y;
	if (k > 0)
	{
		Ogre::Real len = e1x*e1x + e1y*e1y;     // squared len
		if (k < len)
			if (c1sqr * len <= k*k)
				return true;
	}

	// Second edge
	k = c2x*e2x + c2y*e2y;
	if (k > 0)
	{
		Ogre::Real len = e2x*e2x + e2y*e2y;
		if (k < len)
			if (c2sqr * len <= k*k)
				return true;
	}

	// Third edge
	k = c3x*e3x + c3y*e3y;
	if (k > 0)
	{
		Ogre::Real len = e3x*e3x + e3y*e3y;
		if (k < len)
			if (c3sqr * len <= k*k)
				return true;
	}
	// We're done, no intersection
	return false;
}

float frame(int frame)
{
	return frame/25.0f*1000;
}

float interp(float x1, float x2, float a)
{
	return x1*(1-a) + x2*a;
}
float interp2(float x1, float x2, int a, int N)
{
	return x1*(N-1-a)/(N-1) + (x2*a)/(N-1);
}

float cubeFuncMirr(float x)
{
	float r = cubeFunc(x);
	if(r > 1)
		r = 2-r;
	return r;
}
Ogre::Vector2 interp(Ogre::Vector2 v1, Ogre::Vector2 v2, float a)
{
	return Ogre::Vector2(interp(v1.x, v2.x, a), interp(v1.y, v2.y, a));
}
Ogre::Vector3 interp(Ogre::Vector3 v1, Ogre::Vector3 v2, float a)
{
	return Ogre::Vector3(interp(v1.x, v2.x, a), interp(v1.y, v2.y, a), interp(v1.z, v2.z, a));
}
float getInterpAlpha(float x1, float x2, float rez)
{
	return (rez-x1)/(x2-x1);
}



//    _
//   / \_
//  /
// /
float cubeFunc(float x)
{
	return 2.9444f*x*x*x - 6.8167f*x*x + 4.8722f*x;
}
//    _
//   /
// _/
float cosFunc(float x)
{
	return 0.5f*(1 - Math::Cos(x*Math::PI));
}

//    _
//   /
// _/
float cos2Func(float x)
{
	return -2*x*x*x + 3*x*x;
}

//    **
//   *  *
// **    ** 
float impFunc(float x)
{
	return 4*x*(1-x);
}
//   x1 x2
//    |_|
//   /   \
//  /     *
float impFunc2(float x, float x1, float x2)
{
	if(x < x1)
		return x/x1;
	if(x < x2)
		return 1;
	return (1 - x)/(1 - x2);
}
	

//   /
//  /
float trivFunc(float x)
{
	return x;
}

//    __
//   /
//  /
float parabFunc(float x)
{
	return x*(2-x);
}

// \  
//  \__
float parab2Func(float x)
{
	return (x-1)*(x-1);
}

//     /
//  __/
float quadFunc(float x)
{
	return x*x*x;
}

float scrX(int x)
{
	return x/(float)SCR_X;
}
float scrY(int y)
{
	return y/(float)SCR_Y;
}
float colorF(int c)
{
	return c/256.f;
}


float len2(Ogre::Vector3 &a, Ogre::Vector3 &b)
{
	float dx = a.x - b.x;
	float dy = a.y - b.y;
	return dx*dx + dy*dy;
}

int mod(int x, int y)
{
	if(x >= 0)
		return x%y;
	else
		return (y-1) - (-1-x)%y;
}

int floor2(float x)
{
	if(x >= 0)
		return (int)x;
	else
		return (int)x-1;
}

unsigned int getFlag(int room)
{
	return 1<<(room-1);
}

Ogre::Vector2 scrVec2(int x, int y)
{
	return Vector2(scrX(x), scrY(y));
}


void rotateVec2(Ogre::Vector2 &vecRev, const Ogre::Degree &fDegrees)
{
	float fAngle = fDegrees.valueRadians();
	float cs = cosf(fAngle);
	float sn = sinf(fAngle);

	vecRev = Ogre::Vector2(vecRev.x*cs - vecRev.y*sn, vecRev.x*sn + vecRev.y*cs);
}


}; // namespace MathUtils
