
#include "RapidXmlFunc.h"


Ogre::String getAttrib(const XmlNode* XMLNode, const char *attrib, const Ogre::String &defaultValue)
{
    if(XMLNode->first_attribute(attrib))
        return XMLNode->first_attribute(attrib)->value();
    else
        return defaultValue;
}

Ogre::Real getAttribReal(const XmlNode* XMLNode, const char *attrib, Ogre::Real defaultValue)
{
    if(XMLNode->first_attribute(attrib))
        return Ogre::StringConverter::parseReal(XMLNode->first_attribute(attrib)->value());
    else
        return defaultValue;
}
int getAttribInt(const XmlNode* XMLNode, const char *attrib, int defaultValue)
{
    if(XMLNode->first_attribute(attrib))
        return Ogre::StringConverter::parseInt(XMLNode->first_attribute(attrib)->value());
    else
        return defaultValue;
}
bool getAttribBool(const XmlNode* XMLNode, const char *attrib, bool defaultValue)
{
    if(!XMLNode->first_attribute(attrib))
        return defaultValue;

    if(Ogre::String(XMLNode->first_attribute(attrib)->value()) == "true")
        return true;

    return false;
}
bool getAttribBool2(const XmlNode* XMLNode, const char *attrib, bool defaultValue)
{
    if(!XMLNode->first_attribute(attrib))
        return defaultValue;

    return Ogre::StringConverter::parseInt(XMLNode->first_attribute(attrib)->value()) != 0;
}


//------------XmlParser-------------------------
XmlParser::XmlParser(const char* text)
{
    mText = strdup(text);
	mDoc.parse<0>(mText);
}
XmlParser::XmlParser(char* text, bool copy)
{
	if(copy)
	{
		mText = strdup(text);
		mDoc.parse<0>(mText);
	}
	else
	{
		mText = NULL;
		mDoc.parse<0>(text);
	}
}
XmlParser::~XmlParser()
{
	if(mText)
		delete mText;
}
const XmlDoc& XmlParser::getDoc()
{
	return mDoc;
}
