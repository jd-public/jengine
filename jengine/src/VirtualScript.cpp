#include "stdafx.h"

#include "VirtualScript.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <shellapi.h>
#include <mmsystem.h>
#include <conio.h>
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
#include "mainIOS.h"
#endif

#include <OgreRingEmitter.h>
#include <OgreFontManager.h>
#include <OgreHardwareVertexBuffer.h>
#include <OgreRenderOperation.h>
#include <OgreOverlaySystem.h>
#include "OgreOverlayContainer.h"
#include <OgreOverlayManager.h>
#include <OgreTextAreaOverlayElement.h>
#include <OgreOverlay.h>

#include "GameObject.h"
#include "AnimObject.h"
#include "HeroObject.h"
#include "AppOISListener.h"
#include "ProgressListener.h"

#include "SfxCorePrerequisites.h"
#ifndef NO_SFX
#	include "SfxCore.h"
#endif

#include "ScriptMsg.h"
#include "FileScript.h"
#include "ScriptCreator.h"
#include "MainApplication.h"
#include <OgrePanelOverlayElement.h>
#include <OgreBorderPanelOverlayElement.h>

std::map<String, String> CVirtualScript::mScriptAliases;

//--------------------
CScriptMsg::CScriptMsg()
{
	pTargetObj = pSourceObj = NULL;
	messageId = MSG_LAST;
	memset(targetName, 0, sizeof(targetName));
	memset(&param, 0, sizeof(param));
};

//----------------------------------------------------
CVirtualScript::CVirtualScript(const String& scrName, const String& /*resGroup*/)
	: mScriptName(scrName),
	mScriptType(eNone),
	mHandle(0)
{
}


void CVirtualScript::findOverlay(const String& overlayName, const String& containerName, const String& innercontainerName, Ogre::Overlay* &overlay, Ogre::OverlayContainer* &container)
{
	overlay = NULL;
	container = NULL;

	if (!overlayName.empty())
	{
		overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(overlayName);
		if (overlay != NULL)
		{
			if (!containerName.empty())
			{
				container = (Ogre::OverlayContainer*)overlay->getChild(containerName);
				if (container && !innercontainerName.empty())
					container = (Ogre::OverlayContainer*)container->getChild(innercontainerName);
			}
		}
	}
}



void CVirtualScript::getMyName(OBJECT_HANDLE handle, String& name)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pSNode)
		name = pObj->pSNode->getName();
}

const String& CVirtualScript::getMyName(OBJECT_HANDLE handle)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pSNode)
		return pObj->pSNode->getName();
	else
		return Ogre::EmptyString;
}

Ogre::ManualObject* CVirtualScript::createManualObject(const String& name, OBJECT_HANDLE attach_to, Ogre::uint8 queue)
{
	Ogre::ManualObject* m = app.getSceneManager()->createManualObject(name);

	CGameObject* pObj = app._findObjectByHandle(attach_to);
	if (pObj && pObj->pSNode)
		pObj->pSNode->attachObject(m);

	m->setRenderQueueGroup(queue);

	return m;
}

const String& CVirtualScript::getMyScriptName(OBJECT_HANDLE handle)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pScript)
		return pObj->pScript->mScriptName;
	else
		return Ogre::EmptyString;
}

CVirtualScript* CVirtualScript::addScript(OBJECT_HANDLE obj, const String &script)
{
	CGameObject* pObj = app._findObjectByHandle(obj);
	if(pObj && pObj->pScript)
	{
		toLogEx("ERROR: object <%s> allready has a script", pObj->getName());
		return NULL;
	}
	else
	{
		pObj->pScript = CScriptCreatorBase::getScriptObject(script);
		
		if(pObj->pScript)
		{
			pObj->pScript->setHandle(obj);

			CScriptMsg m;
			m.messageId = CScriptMsg::MSG_CREATE;
			m.pTargetObj = pObj;
			pObj->pScript->proceedMsg(&m);
		}

		return pObj->pScript;
	}
}


void CVirtualScript::removeStringFromString(String& str, const String& what_remove)
{
	size_t pos = str.find(what_remove);
	size_t len = what_remove.size();

	if (pos != String::npos)
		str.erase(str.begin()+pos, str.begin()+pos+len);
}

void CVirtualScript::intToString(String& str, int i, unsigned short width)
{
	str = Ogre::StringConverter::toString(i, width, '0');
}

String CVirtualScript::intToString(int i, unsigned short width)
{
	return Ogre::StringConverter::toString(i, width, '0');
}

void CVirtualScript::intAddToString(String& str, int i, unsigned short width)
{
	str.append(Ogre::StringConverter::toString(i, width, '0'));
}

void CVirtualScript::floatToString(String& str, float f)
{
	str = Ogre::StringConverter::toString(f);
}

void CVirtualScript::vecToString(String& str, Vector3& v)
{
	str = Ogre::StringConverter::toString(v);
}

bool CVirtualScript::getViewPos(OBJECT_HANDLE handle, Vector2& vp)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pSNode)
	{
		Vector3 objPos = pObj->pSNode->_getDerivedPosition();
		Vector3 hcsPosition = app.getCamera()->getProjectionMatrix() * (app.getCamera()->getViewMatrix() * objPos);

		Real x = (hcsPosition.x * 0.5f) + 0.5f;// 0 <= x <= 1 // left := 0,right := 1
		Real y = (hcsPosition.y * 0.5f) + 0.5f;// 0 <= y <= 1 // bottom := 0,top := 1
#if OGRE_NO_VIEWPORT_ORIENTATIONMODE == 0
		switch(app.getCamera()->getOrientationMode())
		{
			case Ogre::OR_PORTRAIT:
				vp.x = x;
				vp.y = 1.f - y;
				break;
			case Ogre::OR_DEGREE_180:
				vp.x = 1.f - x;
				vp.y = y;
				break;
			case Ogre::OR_LANDSCAPELEFT:
				vp.x = 1.f - y;
				vp.y = 1.f - x;
				break;
			case Ogre::OR_LANDSCAPERIGHT:
				vp.x = y;
				vp.y = x;
				break;
		}
#else
		vp.x = x;
		vp.y = 1.f - y;
#endif
		return true;
	}
	return false;
}

//handle functions
void CVirtualScript::setSceneSelectableFlag(const String& name, unsigned int mask)
{
	for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
		if (CGameObject::mGameObjList[i]->mSceneFileName == name)
			setObjSelectableFlag(CGameObject::mGameObjList[i]->mHandle, mask);
}

void CVirtualScript::sceneRotate(const String& name, const Quaternion &q)
{
	for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
		if (CGameObject::mGameObjList[i]->mSceneFileName == name)
		{
			Quaternion rot = CGameObject::mGameObjList[i]->pSNode->getOrientation();
			Vector3 pos = CGameObject::mGameObjList[i]->pSNode->getPosition();
			CGameObject::mGameObjList[i]->pSNode->setOrientation(q * rot);
			CGameObject::mGameObjList[i]->pSNode->setPosition(q * pos);
		}
}
void CVirtualScript::sceneTranslate(const String& name, const Vector3 &v)
{
	for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
		if (CGameObject::mGameObjList[i]->mSceneFileName == name)
		{
			Vector3 pos = CGameObject::mGameObjList[i]->pSNode->getPosition();
			CGameObject::mGameObjList[i]->pSNode->setPosition(pos + v);
		}
}


void CVirtualScript::addSceneVisibleFlag(const String& name, unsigned int flags, VisibleFlagParam param) //0-add, 1-set, 2-remove
{
	for (size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
		if (CGameObject::mGameObjList[i]->mSceneFileName == name)
			setObjVisibleFlag(CGameObject::mGameObjList[i]->mHandle, flags, param);
}

float CVirtualScript::cameraGetFOV(const String& fov_camera)
{
	if(fov_camera == Ogre::EmptyString)
		return app.getCamera()->getFOVy().valueDegrees();
	else
		return app.getSceneManager()->getCamera(fov_camera)->getFOVy().valueDegrees();
}

void CVirtualScript::cameraSetFOV(const String& fov_camera)
{
	Ogre::Camera *pCam = app.getSceneManager()->getCamera(fov_camera);
	
	if(!pCam)
	{
		toLogEx("\nCamera: there's no cam with name [%s] to change fov",fov_camera.c_str());
		return;
	}

	if(!app.getCamera())
	{
		toLogEx("\nCamera: there's no cam in scene");
		return;
	}

	if (app.getCamera() && pCam)
	{
		cameraSetFOV(pCam->getFOVy().valueDegrees());

		toLogEx("\nCamera: set cam fov from %s %f",fov_camera.c_str(),pCam->getFOVy().valueDegrees());
	}
}

void CVirtualScript::cameraSetOrientation(const Quaternion &q)
{
	if (app.getCamera())
		app.getCamera()->setOrientation(q);
}

void CVirtualScript::cameraSetPosition(const Vector3 &v)
{
	if (app.getCamera())
		app.getCamera()->setPosition(v);
}

void CVirtualScript::cameraSetFOV(float fov)
{
	if (app.getCamera())
	{
		app.setCameraFov(fov);
	}
}
void CVirtualScript::cameraSetAspect(float aspect)
{
	if (app.getCamera())
	{
		app.setCameraAspect(aspect);
	}
}
void CVirtualScript::cameraSetRenderArea(float left, float top, float right, float bottom)
{
	if (app.getCamera())
	{
		Ogre::RealRect rect;
		rect.left = left;
		rect.top = top;
		rect.right = right;
		rect.bottom = bottom;
		app.setRenderArea(rect);
	}
}
void CVirtualScript::cameraGetRenderArea(float &left, float &top, float &right, float &bottom)
{
	Ogre::RealRect rect = app.getRenderArea();
	left = rect.left;
	top = rect.top;
	right = rect.right;
	bottom = rect.bottom;
}
float CVirtualScript::getGameAspect()
{
#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
	return (float)app.getGameResolution().x / app.getGameResolution().y;
#else
	return (float)app.getWindowResolution().x / app.getWindowResolution().y;
#endif
}

void CVirtualScript::nodeSetQueue(OBJECT_HANDLE handle, int queue)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
    Ogre::SceneNode *pNode = pObj->pSNode;
	for (Ogre::SceneNode::ObjectIterator it = pNode->getAttachedObjectIterator(); it.hasMoreElements(); it.getNext())
		it.peekNextValue()->setRenderQueueGroup(queue);
	for (Ogre::SceneNode::ChildNodeIterator it2 = pNode->getChildIterator(); it2.hasMoreElements(); it2.getNext())
	{
		Ogre::SceneNode *pChild = (Ogre::SceneNode *)it2.peekNextValue();
		if (pChild)
			for (Ogre::SceneNode::ObjectIterator it3 = pChild->getAttachedObjectIterator(); it3.hasMoreElements(); it3.getNext())
				it3.peekNextValue()->setRenderQueueGroup(queue);
	}
}

void CVirtualScript::setVisibleMask(unsigned int mask)
{
	app.getViewport()->setVisibilityMask(mask);

	app.mouseMove(false);
}
int CVirtualScript::getVisibleMask()
{
	return app.getViewport()->getVisibilityMask();
}

void CVirtualScript::resetTo(OBJECT_HANDLE handle, const Vector3& pos, const Vector3& rot)
{
	CHeroObject* pObj = app._findHeroByHandle(handle);
	if (pObj) pObj->resetTo(pos,rot);
}

void CVirtualScript::setWaterMode(OBJECT_HANDLE handle, bool mode)
{
	CHeroObject* pObj = app._findHeroByHandle(handle);
	if (pObj) pObj->setWaterMode(mode);
}

void CVirtualScript::stop(OBJECT_HANDLE handle)
{
	CHeroObject* pObj = app._findHeroByHandle(handle);
	if (pObj)
		pObj->stop();
}

void CVirtualScript::assignTo(OBJECT_HANDLE handle, OBJECT_HANDLE walk_plane, const Vector3& pos, const Vector3& rot, bool isRun, bool accuratePos, bool assignRot, OBJECT_HANDLE hTarget)
{
	CHeroObject* pObj = app._findHeroByHandle(handle);
	if (pObj)
		pObj->assignTo(app._findObjectByHandle(walk_plane), pos, rot, isRun, accuratePos, assignRot, app._findObjectByHandle(hTarget));
}

void CVirtualScript::assignToAlt(OBJECT_HANDLE handle, OBJECT_HANDLE walk_plane, const Vector3& pos, const Vector3& rot, bool isRun, bool accuratePos, bool assignRot, OBJECT_HANDLE hTarget)
{
	CHeroObject* pObj = app._findHeroByHandle(handle);
	if (pObj)
		pObj->assignToAlt(app._findObjectByHandle(walk_plane), pos, rot, isRun, accuratePos, assignRot, app._findObjectByHandle(hTarget));
}

void CVirtualScript::statAnim(OBJECT_HANDLE handle, OBJECT_HANDLE target, const String& animName)
{
	CHeroObject* pObj = app._findHeroByHandle(handle);
	if (pObj)
		pObj->statAnim(0, animName);
}

OBJECT_HANDLE CVirtualScript::findObject(const String& name, int index)
{
	String s = name + Ogre::StringConverter::toString(index, 2, '0');
	return findObjectByName(s);
}


void CVirtualScript::setInfiniteBounds(OBJECT_HANDLE hObj)
{
	CGameObject *pObj = app._findObjectByHandle(hObj);
	if (pObj == NULL)
		return;

	Ogre::AxisAlignedBox aabInf;
	aabInf.setInfinite();
	pObj->pEntity->getMesh()->_setBounds(aabInf);
	//pObj->pParent->_updateBounds();
}

bool CVirtualScript::getBounds2d(OBJECT_HANDLE hObj, Vector2 &vecLeftTop, Vector2 &vecRightBottom)
{
	CGameObject *pGameObject = app._findObjectByHandle(hObj);
	if (pGameObject == NULL)
		return false;


	Ogre::AxisAlignedBox bbox = pGameObject->pSNode->getAttachedObject(0)->getBoundingBox();
	const Ogre::Vector3 *pCorners = bbox.getAllCorners();
	Ogre::Vector2 vec2d;

	int i;

	Ogre::Vector3 vecNodePosition = pGameObject->pSNode->getPosition();

	get2dCoordFrom3dAbsolute(vecNodePosition + pCorners[0], vec2d);
	Ogre::FloatRect rcBounds(vec2d.x, vec2d.y, vec2d.x, vec2d.y);

	for (i = 1; i < 8; i++)
	{
		get2dCoordFrom3dAbsolute(vecNodePosition + pCorners[i], vec2d);

		if (vec2d.x < rcBounds.left)
			rcBounds.left = vec2d.x;
		else
		if (vec2d.x > rcBounds.right)
			rcBounds.right = vec2d.x;

		if (vec2d.y < rcBounds.top)
			rcBounds.top = vec2d.y;
		else
		if (vec2d.y > rcBounds.bottom)
			rcBounds.bottom = vec2d.y;
	}

	vecLeftTop.x = rcBounds.left;
	vecLeftTop.y = rcBounds.top;
	vecRightBottom.x = rcBounds.right;
	vecRightBottom.y = rcBounds.bottom;

	return true;
}

void CVirtualScript::heroPlayAnim(OBJECT_HANDLE handle, const String& animName)
{
	CHeroObject* pObj = app._findHeroByHandle(handle);
	if (pObj)
		pObj->playAnim(animName);
}

void CVirtualScript::heroLookToCam(OBJECT_HANDLE handle, OBJECT_HANDLE hTarget)
{
	CHeroObject* pObj = app._findHeroByHandle(handle);
	Vector3 look =  app.getCamera()->getParentNode()->getPosition();
	if (pObj)
		pObj->lookTo(look, app._findObjectByHandle(hTarget));
}

void CVirtualScript::cameraAttachToBone (OBJECT_HANDLE handle, const String& boneName)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	app.attachCamToBone(pObj, boneName);
}

void CVirtualScript::killTimer(OBJECT_HANDLE handle, int timerID)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	if (pObj)
		pObj->killTimer(pObj, timerID);
}

int CVirtualScript::createTimer(OBJECT_HANDLE handle, unsigned int interv, int rcount, OBJECT_HANDLE hTarget)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)	return pObj->createTimer(interv,rcount,app._findObjectByHandle(hTarget));
	else		return 0;
}

void CVirtualScript::setObjRenderQueue(OBJECT_HANDLE handle, int queue)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	Ogre::SceneNode *pNode = pObj->pSNode;
	if (pObj->pEntity)
		pObj->pEntity->setRenderQueueGroup(queue);
	if (pObj->pLight)
		pObj->pLight->setRenderQueueGroup(queue);
	if (pObj->pBillboard)
		pObj->pBillboard->setRenderQueueGroup(queue);
	if (pObj->pParticle)
		pObj->pParticle->setRenderQueueGroup(queue);
}

void CVirtualScript::setObjVisible(const String& name, bool state)
{
	OBJECT_HANDLE obj = app.findObjectByName(name);
	setObjVisible(obj, state);
}

void CVirtualScript::setObjVisible(OBJECT_HANDLE handle, bool state)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		Ogre::SceneNode *pNode = (Ogre::SceneNode*)pObj->pSNode;

		if (pNode)
		{
			for (Ogre::SceneNode::ObjectIterator it = pNode->getAttachedObjectIterator(); it.hasMoreElements(); it.getNext())
			{
				Ogre::MovableObject *pPeekNext = it.peekNextValue();
				if (pPeekNext != NULL)
					pPeekNext->setVisible(state);
			}
		}

		if (pObj->pEntity)	pObj->pEntity->setVisible(state);
		if (pObj->pLight)	pObj->pLight->setVisible(state);
		if (pObj->pBillboard)pObj->pBillboard->setVisible(state);
		if (pObj->pParticle)	pObj->pParticle->setVisible(state);

		//update mouse after updating visibility
		if (pObj->mIsSelectable)
			app.mouseMove(false);
	}
}

void CVirtualScript::setObjSelectableFlag(OBJECT_HANDLE handle, unsigned int flag)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		Ogre::SceneNode *pNode = static_cast<Ogre::SceneNode*>(pObj->pSNode);
		if (pNode)
		{
//			toLogEx("MOUSE: Set query flags %s %x sel:%d",pNode->getName().c_str(),flag,pObj->mIsSelectable);

			for (Ogre::SceneNode::ObjectIterator it = pNode->getAttachedObjectIterator(); it.hasMoreElements(); it.getNext())
				it.peekNextValue()->setQueryFlags(flag);
			for (Ogre::SceneNode::ChildNodeIterator it2 = pNode->getChildIterator(); it2.hasMoreElements(); it2.getNext())
			{
				Ogre::SceneNode *pChild = (Ogre::SceneNode *)it2.peekNextValue();
				if (pChild)
				{
					for (Ogre::SceneNode::ObjectIterator it3 = pChild->getAttachedObjectIterator(); it3.hasMoreElements(); it3.getNext())
						it3.peekNextValue()->setQueryFlags(flag);
				}
			}
		}
	}

	//update mouse after updating visibility
	if (pObj->mIsSelectable)
		app.mouseMove(false);
}

void CVirtualScript::setSceneSelectable(const String& scene, bool v)
{
	for (size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
		if (CGameObject::mGameObjList[i]->mSceneFileName == scene)
			CGameObject::mGameObjList[i]->mIsSelectable = v;
}

bool CVirtualScript::getObjSelectable(OBJECT_HANDLE obj)
{
	CGameObject* pObj = app._findObjectByHandle(obj);
	return pObj->mIsSelectable;
}

void CVirtualScript::setObjSelectable(const String& name, bool v)
{
	CGameObject* pObj = app._findObjectByName(name);
	pObj->mIsSelectable = v;
}

void CVirtualScript::setObjSelectable(OBJECT_HANDLE obj, bool v)
{
	CGameObject* pObj = app._findObjectByHandle(obj);
	pObj->mIsSelectable = v;
}

void CVirtualScript::setObjVisibleFlag(OBJECT_HANDLE handle, unsigned int flags, VisibleFlagParam param)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		Ogre::SceneNode *pNode = (Ogre::SceneNode*)pObj->pSNode;
		if (pNode)
		{
			for (Ogre::SceneNode::ObjectIterator it = pNode->getAttachedObjectIterator(); it.hasMoreElements(); it.getNext())
			{
				switch(param)
				{
					case vfp_remove:
						it.peekNextValue()->removeVisibilityFlags(flags);
						break;
					case vfp_set:
						it.peekNextValue()->setVisibilityFlags(flags);
						break;
					case vfp_add:
						it.peekNextValue()->addVisibilityFlags(flags);
						break;
					default:
						toLogEx("ERROR: CVirtualScript::setVisibleFlags(): unknown operation type: %d\n", param);
						break;
				}
			}
			for (Ogre::SceneNode::ChildNodeIterator it2 = pNode->getChildIterator(); it2.hasMoreElements(); it2.getNext())
			{
				Ogre::SceneNode *pChild = (Ogre::SceneNode *)it2.peekNextValue();
				if (pChild)
				{
					for (Ogre::SceneNode::ObjectIterator it3 = pChild->getAttachedObjectIterator(); it3.hasMoreElements(); it3.getNext())
					{
						switch(param)
						{
						case vfp_add:
							it3.peekNextValue()->addVisibilityFlags(flags);
							break;
						case vfp_set:
							it3.peekNextValue()->setVisibilityFlags(flags);
							break;
						case vfp_remove:
							it3.peekNextValue()->removeVisibilityFlags(flags);
							break;
						default:
							toLogEx("ERROR: CVirtualScript::setVisibleFlags(): unknown operation type: %d\n", param);
							break;
						}
					}
				}
			}
		}
	}
	//update mouse after updating visibility
	if (pObj->mIsSelectable)
		app.mouseMove(false);
}

void CVirtualScript::attachSceneToObject(const String& scene, const String& object)
{
	Ogre::SceneNode *pParentNode = NULL;
	if (object.empty())
		pParentNode = app.getSceneManager()->getRootSceneNode();
	else
	{
		CGameObject* pParentObject= app._findObjectByName(object);
		pParentNode = (Ogre::SceneNode*)pParentObject->pSNode;
	}

	for (size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
	{
		if (CGameObject::mGameObjList[i]->mSceneFileName == scene)
		{
			Ogre::SceneNode *pNode = (Ogre::SceneNode*)CGameObject::mGameObjList[i]->pSNode;
			if (pNode)
			{
				Ogre::SceneNode *pParent = (Ogre::SceneNode*)pNode->getParent();
				if (pParent)
					pParent->removeChild(pNode);
				if (pParentNode)
					pParentNode->addChild(pNode);
			}
		}
	}
}

void CVirtualScript::setSceneVisibleFlagByNames(const String& name)
{
	for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
	{
		if (CGameObject::mGameObjList[i]->mSceneFileName == name)
		{
			int i1, i2;
			getPrefixInt(CGameObject::mGameObjList[i]->mHandle,&i1,&i2);

			//TODO: remove
			//toLogEx("\nsetSceneVisibleFlagByNames %s %d %d",CGameObject::mGameObjList[i]->mSceneFileName.c_str(),i1,i2);


			setObjVisibleFlag(CGameObject::mGameObjList[i]->mHandle, 1 << (i2 - 1), vfp_set);

		}
	}
}

void CVirtualScript::deleteAllTextures()
{
	app.unloadTextures();
}

void CVirtualScript::addNotUnloadableTexture(const Ogre::String &textureName)
{
	app.addNotUnloadableTexture(textureName);
}

void CVirtualScript::toLogLights()
{
	Ogre::MovableObject *pLight = NULL;

	int visible_mask = app.getViewport()->getVisibilityMask();
	toLogEx("Current visible mask %d\nLights list:\nvisible\tflag\tis vis.\tlight name", visible_mask);
	Ogre::SceneManager::MovableObjectIterator loi = app.getSceneManager()->getMovableObjectIterator("Light");
	while(loi.hasMoreElements())
	{
		pLight = loi.getNext();
		int visible = pLight->getVisible();
		int flag = pLight->getVisibilityFlags();
		int is_visible = (visible_mask & flag) && visible;
		toLogEx("%d\t%d\t%d\t%s\n", visible, flag, is_visible, pLight->getName().c_str());
	}

	toLogEx("is visible lights list:\n");
	loi = app.getSceneManager()->getMovableObjectIterator("Light");
	while(loi.hasMoreElements())
	{
		pLight = loi.getNext();
		int visible = pLight->getVisible();
		int flag = pLight->getVisibilityFlags();
		int is_visible = (visible_mask & flag) && visible;
		if (is_visible)
			toLogEx("%s\n", pLight->getName().c_str());
	}
	toLogEx("\n");
}

void CVirtualScript::toLogLoadedTextures()
{
	size_t totalSize = 0;
	Ogre::TextureManager::ResourceMapIterator textureIter = Ogre::TextureManager::getSingleton().getResourceIterator();
	while(textureIter.hasMoreElements())
	{
		Ogre::TexturePtr pTexture = textureIter.getNext().staticCast<Ogre::Texture>();
		if(pTexture->isLoaded())
		{
			toLogEx("\n texture: %d\t%s\t%d", pTexture->getSize(), pTexture->getName().c_str(),pTexture.useCount());
			totalSize += pTexture->getSize();
			pTexture.setNull();
		}
	}
	toLogEx("\n total size: %d", totalSize);
}

void CVirtualScript::setSceneVisible (const String& name, bool state)
{
/*	for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
	{
		Ogre::SceneNode *pNode = (Ogre::SceneNode*)CGameObject::mGameObjList[i]->pSNode;

		if (pNode)
			setObjVisible(pNode->getName(), state);
	}*/

	for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
	{
		if (CGameObject::mGameObjList[i]->mSceneFileName == name)
		{
			Ogre::SceneNode *pNode = (Ogre::SceneNode*)CGameObject::mGameObjList[i]->pSNode;


			for (Ogre::SceneNode::ObjectIterator it = pNode->getAttachedObjectIterator(); it.hasMoreElements(); it.getNext())
			{
				Ogre::MovableObject *pPeekNext = it.peekNextValue();
				if (pPeekNext != NULL)
				{
//					toLogEx("\n [%s] peekNext = %x", pNode->getName().c_str(), pPeekNext);
//					toLogEx(" name = %s", pPeekNext->getName().c_str());
					pPeekNext->setVisible(state);
				}
			}

			for (Ogre::SceneNode::ChildNodeIterator it2 = pNode->getChildIterator(); it2.hasMoreElements(); it2.getNext())
			{
				Ogre::SceneNode *pChild = (Ogre::SceneNode *)it2.peekNextValue();

				if (pChild)
				{
					for (Ogre::SceneNode::ObjectIterator it3 = pChild->getAttachedObjectIterator(); it3.hasMoreElements(); it3.getNext())
						it3.peekNextValue()->setVisible(state);
				}
			}

		}
	}

	//update mouse after updating visibility
	app.mouseMove(false);

}

bool CVirtualScript::getString(OBJECT_HANDLE handle, const String& pname, String& buf)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	std::map<String,String>::iterator iter = pObj->getParams().find(pname);
	if ( iter != pObj->getParams().end() )
	{
		buf = iter->second;
		return true;
	}

	buf.clear();
	return false;
}

int CVirtualScript::getInt(OBJECT_HANDLE handle, const String& pname)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		std::map<String,String>::iterator iter = pObj->getParams().find(pname);
		if ( iter != pObj->getParams().end() )
			return Ogre::StringConverter::parseInt(iter->second);
	}
	return FALSE;
}

bool CVirtualScript::getBool(OBJECT_HANDLE handle, const String& pname, bool &rez)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		std::map<String,String>::iterator iter = pObj->getParams().find(pname);
		if ( iter != pObj->getParams().end() )
		{
			rez = Ogre::StringConverter::parseInt(iter->second) == TRUE;
			return true;
		}
	}
	return false;
}

bool CVirtualScript::getInt(OBJECT_HANDLE handle, const String& pname, int &rez)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	std::map<String,String>::const_iterator iter = pObj->getParams().find(pname);
	if ( iter != pObj->getParams().end() )
	{
		rez = Ogre::StringConverter::parseInt(iter->second);
		return true;
	}
	return false;
}

void CVirtualScript::setInt(OBJECT_HANDLE handle, const String& pname, int value)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	if (pObj != NULL)
		pObj->getParams()[pname] = Ogre::StringConverter::toString(value);
}
void CVirtualScript::setString(OBJECT_HANDLE handle, const String& pname, const String &value)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	if (pObj != NULL)
		pObj->getParams()[pname] = value;
}


int CVirtualScript::getFloat(OBJECT_HANDLE handle, const String& pname, float &rez)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	std::map<String,String>::iterator iter = pObj->getParams().find(pname);
	if ( iter != pObj->getParams().end() ) {

		String str = iter->second;
		rez = (float)atof(iter->second.c_str());
		return 1;
	}
	return 0;
}

bool CVirtualScript::getVisible(OBJECT_HANDLE handle)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	return (pObj && pObj->pEntity) ? pObj->pEntity->getVisible() : false;
}

Vector3 CVirtualScript::getBonePosition (OBJECT_HANDLE handle, const String& name)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pEntity && pObj->pEntity->getSkeleton())
	{
		Ogre::Bone *bone = pObj->pEntity->getSkeleton()->getBone(name);
		if (bone)
			return bone->_getDerivedPosition();
	}
	return Vector3::ZERO;
}

void CVirtualScript::setBonePosition (OBJECT_HANDLE handle, const String& name, const Vector3& pos)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		if (pObj->pEntity && pObj->pEntity->hasSkeleton())
		{
			if (pObj->pEntity->getSkeleton()->hasBone(name))
			{
				Ogre::Bone *bone = pObj->pEntity->getSkeleton()->getBone(name);
				if (bone)
				{
					bone->setManuallyControlled(true);
					bone->setPosition(pos);
				}
			}
		}
		else
			toLogEx("ERROR: Object %s has nigher no mesh or no skeleton!\n", name.c_str());
	}
}

bool CVirtualScript::hasBone(OBJECT_HANDLE handle, const String& name)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	return (pObj && pObj->pEntity && pObj->pEntity->hasSkeleton() && pObj->pEntity->getSkeleton()->hasBone(name));
}

void CVirtualScript::setBoneScale(OBJECT_HANDLE handle, const String& name, const Vector3& scale)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		if (pObj->pEntity && pObj->pEntity->hasSkeleton())
		{
			if (pObj->pEntity->getSkeleton()->hasBone(name))
			{
				Ogre::Bone *bone = pObj->pEntity->getSkeleton()->getBone(name);
				if (bone)
				{
					bone->setManuallyControlled(true);
					bone->setScale(scale);
				}
			}
		}
		else
			toLogEx("ERROR: Object %s has nigher no mesh or no skeleton!\n", name.c_str());
	}
}

void CVirtualScript::interpolateFloat(float *rez, float x1, float x2, float a)
{
	*rez = x1*(1-a) + x2*a;
}

void CVirtualScript::interpolateVec(Vector3& rez, const Vector3& x1, const Vector3& x2, float a)
{
	interpolateFloat(&rez.x, x1.x, x2.x, a);
	interpolateFloat(&rez.y, x1.y, x2.y, a);
	interpolateFloat(&rez.z, x1.z, x2.z, a);
}

void CVirtualScript::shellExecute(const String& str)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	ShellExecute(NULL, "", str.c_str(), NULL, NULL, SW_SHOWNORMAL);
#endif
}

void CVirtualScript::setFullscreen(bool fFullScreen)
{
	app.setFullscreen(fFullScreen);

	for (size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
	{
		CScriptMsg m;
		m.messageId = CScriptMsg::MSG_ACTIVATE;
		m.pTargetObj=CGameObject::mGameObjList[i];
		app.postMsg(m);
	}
}
void CVirtualScript::setFitScreen(bool fFitScreen)
{
	app.setFitScreen(fFitScreen);
}

void CVirtualScript::setInitalBonePosition(OBJECT_HANDLE handle, const String& name, const Vector3& pos)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		if (pObj->pEntity && pObj->pEntity->hasSkeleton())
		{
			if (pObj->pEntity->getSkeleton()->hasBone(name))
			{
				Ogre::Bone *bone = pObj->pEntity->getSkeleton()->getBone(name);
				if (bone)
				{
					bone->setPosition(pos);
					bone->setInitialState();
				}
			}
		}
		else
			toLogEx("ERROR: Object %s has nigher no mesh or no skeleton!\n", name.c_str());
	}
}

void CVirtualScript::setBoneOrientation(OBJECT_HANDLE handle, const String& name, const Vector3& rot)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		if (pObj->pEntity && pObj->pEntity->hasSkeleton())
		{
			if (pObj->pEntity->getSkeleton()->hasBone(name))
			{
				Ogre::Bone *bone = pObj->pEntity->getSkeleton()->getBone(name);
				if (bone)
				{
					bone->setManuallyControlled(true);

					Ogre::Matrix3 m;
					m.FromEulerAnglesXYZ(Ogre::Degree(rot.x),Ogre::Degree(rot.y),Ogre::Degree(rot.z));
					bone->setOrientation(Quaternion(m));
				}
			}
		}
		else
			toLogEx("ERROR: Object %s has nigher no mesh or no skeleton!\n", name.c_str());
	}
}

void CVirtualScript::getBoneOrientation(OBJECT_HANDLE handle, const String& name, Vector3& rot)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		if (pObj->pEntity && pObj->pEntity->hasSkeleton())
		{
			if (pObj->pEntity->getSkeleton()->hasBone(name))
			{
				Ogre::Bone *bone = pObj->pEntity->getSkeleton()->getBone(name);
				if (bone)
				{
					Quaternion q;
					Ogre::Matrix3 m;
					Ogre::Radian r1, r2, r3;
					q = bone->_getDerivedOrientation();
					q.ToRotationMatrix(m);
					m.ToEulerAnglesXYZ(r1, r2, r3);
					rot.x = r1.valueDegrees();
					rot.y = r2.valueDegrees();
					rot.z =	r3.valueDegrees();
				}
			}
		}
		else
			toLogEx("ERROR: Object %s has nigher no mesh or no skeleton!\n", name.c_str());
	}
}

void CVirtualScript::getBoneOrientation2(OBJECT_HANDLE handle, const String& name, Quaternion& q)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		if (pObj->pEntity && pObj->pEntity->hasSkeleton())
		{
			if (pObj->pEntity->getSkeleton()->hasBone(name))
			{
				Ogre::Bone *bone = pObj->pEntity->getSkeleton()->getBone(name);
				if (bone)
				{
					q = bone->_getDerivedOrientation();
				}
			}
		}
		else
			toLogEx("ERROR: Object %s has nigher no mesh or no skeleton!\n", name.c_str());
	}
}

void CVirtualScript::setBoneOrientation2(OBJECT_HANDLE handle, const String& name, const Quaternion& q)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		if (pObj->pEntity && pObj->pEntity->hasSkeleton())
		{
			if (pObj->pEntity->getSkeleton()->hasBone(name))
			{
				Ogre::Bone *bone = pObj->pEntity->getSkeleton()->getBone(name);
				if (bone)
				{
					bone->setManuallyControlled(true);
					bone->setOrientation(q);
				}
			}
		}
		else
			toLogEx("ERROR: Object %s has nigher no mesh or no skeleton!\n", name.c_str());
	}
}

void CVirtualScript::equalPositionRotation (OBJECT_HANDLE hSource, OBJECT_HANDLE hTarget)
{
	CGameObject* pSObj = app._findObjectByHandle(hSource);
	CGameObject* pTObj = app._findObjectByHandle(hTarget);

	pTObj->pSNode->setPosition( pSObj->pSNode->getPosition() );
	pTObj->pSNode->setOrientation( pSObj->pSNode->getOrientation() );
}

void CVirtualScript::dettachObjFromBone(OBJECT_HANDLE hChild)
{
	CGameObject* pCObj = app._findObjectByHandle(hChild);
	if (pCObj->pPearentBone)
	{
		Ogre::MovableObject *pMovObj = pCObj->pEntity;
		if (!pMovObj)
			pMovObj = pCObj->pCamera;
		pCObj->pPearentBone->detachObjectFromBone(pMovObj);
		pCObj->pSNode->attachObject(pMovObj);
		pCObj->pPearentBone = NULL;
	}
}

void attachObjToBoneBase(OBJECT_HANDLE hChild, OBJECT_HANDLE hParent, const String& bone_name, bool is_get_offset_from_node)
{
	CGameObject* pCObj = app._findObjectByHandle(hChild);
	if (pCObj)
	{
		Ogre::MovableObject *pMovObj = pCObj->pEntity;
		if (!pMovObj) pMovObj = pCObj->pCamera;
		if (!pMovObj) pMovObj = pCObj->pLight;
		if (!pMovObj) pMovObj = pCObj->pBillboard;
		if (!pMovObj) pMovObj = pCObj->pParticle;

		if (!bone_name.empty())
		{
//			pCObj->pSNode->detachObject(pMovObj);
			CGameObject* pPObj = app._findObjectByHandle(hParent);
			if (pPObj)
			{
				if (pPObj->pEntity && pPObj->pEntity->getSkeleton() &&
					pPObj->pEntity->getSkeleton()->getBone(bone_name))
				{
					Quaternion offsetOrientation = Quaternion::IDENTITY;
					Vector3 offsetPosition = Vector3::ZERO;
					if (is_get_offset_from_node)
					{
						offsetOrientation = pCObj->pSNode->getOrientation();
						offsetPosition = pCObj->pSNode->getPosition();
					}
					bool hasBone = false;
					Ogre::Entity::ChildObjectListIterator iter=pPObj->pEntity->getAttachedObjectIterator();
					while (iter.hasMoreElements())
					{
						Ogre::MovableObject* obj = iter.getNext();
						if (obj && obj->getName()==pMovObj->getName())
						{
							pPObj->pEntity->detachObjectFromBone(obj);
							break;
						}
					}
					pCObj->pSNode->detachObject(pMovObj);
					pPObj->pEntity->attachObjectToBone(bone_name, pMovObj, offsetOrientation, offsetPosition);
					pCObj->pPearentBone = pPObj->pEntity;
				}
				else
					toLogEx("ERROR: attachObjToBone failed! Can't find bone %s in %s skeleton", bone_name.c_str(), pPObj ? pPObj->getName() : "null");
			}
		}
	}
}

void CVirtualScript::attachObjToBone(OBJECT_HANDLE hChild, OBJECT_HANDLE hParent, const String& bone_name)
{
	attachObjToBoneBase(hChild, hParent, bone_name, false);
}

void CVirtualScript::attachObjToBoneWithNodeOffset(OBJECT_HANDLE hChild, OBJECT_HANDLE hParent, const String& bone_name)
{
	attachObjToBoneBase(hChild, hParent, bone_name, true);
}

void CVirtualScript::getPosition(OBJECT_HANDLE handle, Vector3& pos)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pSNode)
		pos = pObj->pSNode->getPosition();
}

void CVirtualScript::setPosition(OBJECT_HANDLE handle, const Vector3& pos)
{
	if(pos.isNaN())
		return;

	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pSNode)
		pObj->pSNode->setPosition(pos);
}

void CVirtualScript::setScale(OBJECT_HANDLE handle, const Vector3& scale)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pSNode)
		pObj->pSNode->setScale(scale);
}

void CVirtualScript::getScale(OBJECT_HANDLE handle, Vector3& scale)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
		scale = pObj->pSNode->getScale();
}

void CVirtualScript::setPositionRotation(OBJECT_HANDLE handle, float x, float y, float z, float axis_z)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pSNode)
	{
		pObj->pSNode->setPosition(x, y, z);
		Ogre::Matrix3 m;
		m.FromEulerAnglesXYZ(Ogre::Degree(), Ogre::Degree(), Ogre::Degree(axis_z));
		pObj->pSNode->setOrientation(Quaternion(m));
	}
}

void CVirtualScript::lightSetColor(const String& name, int param, float x, float y, float z)
{
	Ogre::Light *light = app.getSceneManager()->getLight(name);
	if (light)
	{
		if (param&1)	light->setDiffuseColour(x,y,z);
		if (param&2)	light->setSpecularColour(x,y,z);
	}
}

void CVirtualScript::interpolateLights(const String& res_light, const String& l1, const String& l2, float a)
{
	Ogre::Light *res = app.getSceneManager()->getLight(res_light);

	Ogre::Light *light1 = app.getSceneManager()->getLight(l1);
	Ogre::Light *light2 = app.getSceneManager()->getLight(l2);

	if (light1 && light2)
	{
		Ogre::Node *n1 = light1->getParentNode();
		Ogre::Node *n2 = light2->getParentNode();

		res->getParentNode()->setPosition(n1->getPosition()*(1-a) + n2->getPosition()*a);
		res->getParentNode()->setOrientation(n1->getOrientation()*(1-a) + n2->getOrientation()*a);

		res->setAttenuation(light1->getAttenuationRange()*(1-a) + light2->getAttenuationRange()*a,
			light1->getAttenuationConstant()*(1-a) + light2->getAttenuationConstant()*a,
			light1->getAttenuationLinear()*(1-a) + light2->getAttenuationLinear()*a,
			light1->getAttenuationQuadric()*(1-a) + light2->getAttenuationQuadric()*a);

		res->setSpotlightFalloff(light1->getSpotlightFalloff()*(1-a) + light2->getSpotlightFalloff()*a);

		res->setSpotlightInnerAngle(light1->getSpotlightInnerAngle()*(1-a) + light2->getSpotlightInnerAngle()*a);

		res->setSpotlightOuterAngle(light1->getSpotlightOuterAngle()*(1-a) + light2->getSpotlightOuterAngle()*a);

		res->setDiffuseColour(light1->getDiffuseColour()*(1-a) + light2->getDiffuseColour()*a);
		res->setSpecularColour(light1->getSpecularColour()*(1-a) + light2->getSpecularColour()*a);
		res->setVisible(true);
	}
}

void CVirtualScript::lightSetAttenuation(const String& name, float r, float c, float l, float q)
{
	Ogre::Light *light = app.getSceneManager()->getLight(name);
	if (light)
		light->setAttenuation(r, c, l,	q);
}

void CVirtualScript::setOrientation (OBJECT_HANDLE handle, const Vector3& rot)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
	{
		Ogre::Matrix3 m;
		m.FromEulerAnglesXYZ(Ogre::Degree(rot.x),Ogre::Degree(rot.y),Ogre::Degree(rot.z));
		pObj->pSNode->setOrientation(Quaternion(m));
	}
}

void CVirtualScript::getOrientation2(OBJECT_HANDLE handle, Quaternion& q)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
		q = pObj->pSNode->getOrientation();
}

void CVirtualScript::setOrientation2(OBJECT_HANDLE handle, const Quaternion& q)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
		pObj->pSNode->setOrientation(q);
}

void CVirtualScript::transformOrientation(Ogre::Quaternion& res, const Ogre::Quaternion& q1, const Ogre::Quaternion& q2)
{
	res = q1*q2;
}

void CVirtualScript::interpOrientation(Ogre::Quaternion& res, const Ogre::Quaternion& q1, const Ogre::Quaternion& q2, float w)
{
	res = Quaternion::nlerp(w,q1,q2,true);
}

void CVirtualScript::initOrientation(Quaternion& q, const Vector3 &ang_rot)
{
	Ogre::Matrix3 m;
	m.FromEulerAnglesXYZ(Ogre::Degree(ang_rot.x),Ogre::Degree(ang_rot.y),Ogre::Degree(ang_rot.z));
	q = Quaternion(m);
}

void CVirtualScript::getOrientation (OBJECT_HANDLE handle, Vector3& rot)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	if (pObj && pObj->pSNode)
	{
		Quaternion q;
		Ogre::Matrix3 m;
		Ogre::Radian r1, r2, r3;

		q = pObj->pSNode->getOrientation();
		q.ToRotationMatrix(m);
		m.ToEulerAnglesXYZ(r1, r2, r3);

		rot.x = r1.valueDegrees();
		rot.y = r2.valueDegrees();
		rot.z = r3.valueDegrees();
	}
	else
		toLogEx("ERROR: getOrientation for NULL object\n");
}

void CVirtualScript::exitApplication ()
{
	app.close();
}

void CVirtualScript::overlaySetTopColour (const String& overlayName, const String& containerName, const String& innercontainerName, float r,float g, float b, float a)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
	{
		Ogre::TextAreaOverlayElement* ta = reinterpret_cast<Ogre::TextAreaOverlayElement*>(container);

		Ogre::ColourValue colour(r,g,b,a);
		
		float h1,s1,b1;
		colour.getHSB(&h1,&s1,&b1);
		colour.setHSB(h1,s1,b1*2.5);

		ta->setColourTop(colour);
	}
}

void CVirtualScript::overlaySetBottomColour (const String& overlayName, const String& containerName, const String& innercontainerName, float r,float g, float b, float a)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
	{
		Ogre::TextAreaOverlayElement* ta = reinterpret_cast<Ogre::TextAreaOverlayElement*>(container);
		Ogre::ColourValue colour(r,g,b,a);

		float h1,s1,b1;
		colour.getHSB(&h1,&s1,&b1);
		colour.setHSB(h1,s1,b1*0.5);

		ta->setColourBottom(colour);
	}
}

void CVirtualScript::overlayShowHide(const String& overlayName, const String& containerName,
									  const String& innercontainerName, bool visible)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;

	findOverlay(overlayName, containerName, innercontainerName, overlay, container);

	if (container)
		if (visible)
			container->show();
		else
			container->hide();
	else
	if (overlay)
		if (visible)
			overlay->show();
		else
			overlay->hide();
}

void CVirtualScript::overlayGetPosition( const String& overlayName, const String& containerName,
										const String& innercontainerName, Vector2& position)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
	{
		position.x = container->getLeft();
		position.y = container->getTop();
	}
}


Vector2 CVirtualScript::roundToPixel(const Vector2 &pos)
{
	MathUtils::TVector2ui resolution = app.getWindowResolution();
	return Vector2(
		Math::Floor(pos.x * resolution.x) / resolution.x,
		Math::Floor(pos.y * resolution.y) / resolution.y);
}

void CVirtualScript::overlaySetPosition( const String& overlayName, const String& containerName,
										 const String& innercontainerName, const Vector2& position)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
	{
		Vector2 roundPos = roundToPixel(position);
		container->setPosition(roundPos.x, roundPos.y);
	}
}

void CVirtualScript::overlaySetPositionNoRound( const String& overlayName, const String& containerName,
										 const String& innercontainerName, const Vector2& position)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
		container->setPosition(position.x, position.y);
}

class CodePointRangeCondition
{
public:
	Ogre::Font::CodePoint cp;
	CodePointRangeCondition(Ogre::Font::CodePoint &CP):cp(CP){}
	bool operator()(Ogre::Font::CodePointRange &cpr){return cpr.first <= cp && cp <= cpr.second;}
};


void CVirtualScript::overlayGetCaptionWidth( const String& overlayName, const String& containerName,
											 const String& innercontainerName, float& width)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
	{
		float spaceWidth = ((Ogre::TextAreaOverlayElement*)container)->getSpaceWidth()/2;
		String font_name = ((Ogre::TextAreaOverlayElement*)container)->getFontName();
		Ogre::FontPtr font = Ogre::FontManager::getSingleton().getByName(font_name).staticCast<Ogre::Font>();

		Ogre::DisplayString str = container->getCaption();
//		std::replace(str.begin(), str.end(), ' ', '_');
		Ogre::Font::CodePointRangeList cprl = font->getCodePointRangeList();
		float char_height = ((Ogre::TextAreaOverlayElement*)container)->getCharHeight();
		width = 0;
		float aspect_mul = ((float)app.getWindowActiveResolution().y)/((float)app.getWindowActiveResolution().x);
		for(size_t i = 0; i < str.size(); i++)
		{
			Ogre::Font::CodePoint cp = str.at(i);
			if(cp == 32 || cp == 160 || cp == 12288)
			{
				width += spaceWidth * aspect_mul;
			}
			else
				if (std::find_if(cprl.begin(), cprl.end(), CodePointRangeCondition(cp)) != cprl.end())
				{
					Ogre::Font::GlyphInfo gi = font->getGlyphInfo(cp);
					width += gi.aspectRatio * char_height * aspect_mul;//*3.f/4.f;
				}
				else
					toLogEx("Warning: overlayGetCaptionWidth() can't find GlyphInfo for CodePoint %d\n", cp);
		}
	}
}

void CVirtualScript::setLoadingWindowAlpha(float alpha)
{
//	CProgressListener::setAlpha(alpha);
}

void CVirtualScript::overlaySetZOrder(const String& overlayName, int zorder)
{
	if (!overlayName.empty())
	{
		Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(overlayName);
		if (overlay)
			overlay->setZOrder(zorder);
	}
}

void CVirtualScript::overlayAddNode(const String& overlayName, const String& sceneNode)
{
	if (!sceneNode.empty() && !overlayName.empty())
	{
		Ogre::SceneNode *node = app.getSceneManager()->getSceneNode(sceneNode);
		if (node)
		{
			Ogre::Node *parent = node->getParent();
			Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(overlayName);
			if (parent && overlay)
			{
				parent->removeChild(node);
				overlay->add3D(node);
			}
		}
	}
}


void CVirtualScript::nodeAttach(const String& nodeName, const String& childName)
{
	Ogre::SceneNode *pPNode = app.getSceneManager()->getSceneNode(childName);
	app.getSceneManager()->getRootSceneNode()->removeChild(pPNode);
	app.getSceneManager()->getSceneNode(nodeName)->addChild(pPNode);
}
void CVirtualScript::cameraAttachOrientation(float x, float y, float z, float w)
{
	app.setCamOrientation(Quaternion(x, y, z, w));
}

void CVirtualScript::nodeDetach(const String& nodeName, const String& childName)
{
	Ogre::SceneNode *pPNode = app.getSceneManager()->getSceneNode(nodeName);
	Ogre::Node *pChNode = pPNode->removeChild(childName);
	app.getSceneManager()->getRootSceneNode()->addChild(pChNode);
}

void CVirtualScript::nodeAttach2(const String& nodeName, const String& childName)// ����������� ����, ���������� �� ����, ��� ��� ���� �� �����
{
	Ogre::SceneNode *node = app.getSceneManager()->getSceneNode(childName);
	if(node->getParent())
		node->getParent()->removeChild(node);
	app.getSceneManager()->getSceneNode(nodeName)->addChild(node);
}
void CVirtualScript::nodeDetachAtAll(const String& nodeName) // ���������� ���� � �������� ������ � �������
{
	Ogre::SceneNode *node = app.getSceneManager()->getSceneNode(nodeName);
	if(node->getParent())
		node->getParent()->removeChild(node);
}


void CVirtualScript::overlayGetSize (const String& overlayName, const String& containerName,
									 const String& innercontainerName, Vector2& size)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
	{
		size.x = container->getWidth();
		size.y = container->getHeight();
	}
}

void CVirtualScript::overlaySetSize( const String& overlayName, const String& containerName,
									 const String& innercontainerName, const Vector2& size)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
	{
		container->setWidth(size.x);
		container->setHeight(size.y);
	}
}

void CVirtualScript::overlaySetUVs(const String& overlayName, const String& containerName,
								   const String& innercontainerName, const Vector2& uv1, const Vector2& uv2)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
		reinterpret_cast<Ogre::PanelOverlayElement*>(container)->setUV(uv1.x,uv1.y,uv2.x,uv2.y);
}

void CVirtualScript::overlaySetRotation(const String& overlayName, float angle)
{
	Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(overlayName);
	if (overlay)
		overlay->setRotate((Ogre::Radian)(angle/180*3.1415926f));
}
void CVirtualScript::overlaySetScale(const String& overlayName, float fScale)
{
	Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(overlayName);
	if (overlay)
		overlay->setScale(fScale, fScale);
}


size_t CVirtualScript::getTextStringLength(int strNum)
{
	return app.getStringTable().getString(strNum).length_Characters();
}

Ogre::DisplayString CVirtualScript::getTextString(int strNum)
{
	return app.getStringTable().getString(strNum);
}
bool CVirtualScript::isTextStringExist(int strNum)
{
	return app.getStringTable().isStringExist(intToString(strNum, 1));
}

void CVirtualScript::overlayGetCaption(const String& overlayName, const String& containerName, const String& innercontainerName, Ogre::DisplayString& txt)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);

	if (container)
		txt = container->getCaption();
}

size_t CVirtualScript::overlayGetCaptionLength(const String& overlayName, const String& containerName, const String& innercontainerName)
{
	Ogre::DisplayString text;
	overlayGetCaption(overlayName, containerName, innercontainerName, text);
	return text.length();
}

void CVirtualScript::_fixMipmapFilterBug(Ogre::OverlayContainer* container)
{
	Ogre::MaterialPtr matTemplate = container->getMaterial();
	Ogre::FilterOptions min = matTemplate->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureFiltering(Ogre::FT_MIN);
	Ogre::FilterOptions mag = matTemplate->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureFiltering(Ogre::FT_MAG);
	Ogre::FilterOptions mip = matTemplate->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureFiltering(Ogre::FT_MIP);

	if(mip != Ogre::FO_NONE)
	{
		//toLogEx("\nERROR: TextArea %s has a mip filter on their material %s!",container->getName().c_str(),matTemplate->getName().c_str());
	}

	matTemplate->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureFiltering(Ogre::FO_LINEAR, Ogre::FO_LINEAR, Ogre::FO_NONE);

}


void CVirtualScript::overlaySetCaption( const String& overlayName, const String& containerName,
										const String& innercontainerName, const Ogre::DisplayString& caption)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);

	if (container)
	{
		Ogre::DisplayString caption_parse = _overlaySetCharColorsFromCaption(overlayName, containerName, innercontainerName, caption, true);
		container->setCaption(caption_parse);

        //((Ogre::TextAreaOverlayElement*)container)->setSpaceWidth(0.01);

		_fixMipmapFilterBug(container);

		_overlaySetCharColorsFromCaption(overlayName, containerName, innercontainerName, caption, false);
	}
}

void CVirtualScript::overlaySetCaptionFromST(const String& overlayName,
											 const String& containerName,
											 const String& innercontainerName,
											 int stringNum)
{
	overlaySetCaptionFromSTBeginEnd(overlayName,containerName,innercontainerName,stringNum,-1,-1, "");
}

int CVirtualScript::overlaySetCaptionFromSTEx(const String& overlayName,
											  const String& containerName,
											  const String& innercontainerName,
											  int stringNum,
											  size_t strLen)
{
	Ogre::DisplayString str = app.getStringTable().getString(stringNum);

//---------------------

	int line_char_num = strLen;//64;
	int line_end = line_char_num;

	int end_str = -1;
	int line_num = 0;


	int real_strlen = str.length_Characters();

	int id = getCfgI("language_id");
	bool asian = false;

	if(id & JENGINE_LANG_ASIAN)
		asian = true;

	//toLogEx("\noverlaySetCaptionFromSTEx");

	//euro and asian punctuation marks
	Ogre::uint16 asian_marks[] = {65292,12290,44,12289,46,33,63,65281,65294,65311,25226,19968,0};

	for(int i = 0; i < real_strlen; i++)
	{
		bool temp_asian = false;

		//if asian, do not transit on punctuation marks and not european symbols
		if(asian && str.at(i) > 1105)
		{
			temp_asian = true;

			for(int k = 0; asian_marks[k] != 0; k++)
			{
				if(str.at(i) == asian_marks[k])
				{
					temp_asian = false;
					break;
				}
			}
		}


		if(str.getChar(i) == L' ' || temp_asian)
		{
			end_str = i;
		}


		//toLogEx("-%d=%u",i,str.at(i));

		if(i == real_strlen-1)
		{
			line_end = i;
		}

		if(str.getChar(i) == L'\n')
		{
			line_num++;

			end_str = -1;
			line_end = i + line_char_num + 1;
		}

		if(i == line_end)
		{
			if(i != real_strlen-1)
			{
				if(end_str != -1)
					i = end_str;

				end_str = -1;

				if(str.getChar(i) == L' ')
				{
				  str.setChar(i,'\n');
				}
				else
				{
				  str.insert(str.begin()+i,'\n');
				}
			}

			line_num++;

			line_end = i++ + line_char_num + 1;
		}
	}

//--------------
/*
	if(overlayName == "")
		return line_num;

	Ogre::Overlay* mOverlay = Ogre::OverlayManager::getSingletonPtr()->getByName(String(overlayName));

	if(mOverlay && containerName != "")
	{
		Ogre::OverlayContainer* mContainer =
			(Ogre::OverlayContainer*)mOverlay->getChild(Ogre::String(containerName));

		if(innercontainerName != "")
			mContainer = (Ogre::OverlayContainer*)mContainer->getChild(Ogre::String(innercontainerName));

		mContainer->setCaption(str);
	}
*/
	overlaySetCaption(overlayName, containerName, innercontainerName, str);
	return line_num;


/*
	int line_num = 0;
	Ogre::DisplayString unconv_str = app.getStringTable().getString(stringNum);
	Ogre::DisplayString str = _overlaySetCharColorsFromCaption(overlayName,containerName,innercontainerName,unconv_str);

	//---------------------
	size_t line_char_num = strLen;
	size_t line_end = line_char_num;

	size_t end_str = String::npos;
	size_t real_strlen = str.length_Characters();
	for (size_t i=0; i<real_strlen; i++)
	{
		if (str.getChar(i) == L' ') end_str = i;
		if (i == real_strlen-1) line_end = i;
		if (str.getChar(i) == L'\n')
		{
			line_num++;
			line_end = i+line_char_num+1;
			end_str = String::npos;
		}

		if (i == line_end)
		{
			if (i != real_strlen-1)
			{
				if (end_str != String::npos)
					i = end_str;
				str.setChar(i,'\n');
			}

			line_num++;
			line_end = i+line_char_num+1;
			end_str = String::npos;
		}
	}

	//--------------
	overlaySetCaption(overlayName, containerName, innercontainerName, str);
	_overlaySetCharColorsFromCaption(overlayName,containerName,innercontainerName,unconv_str,false);
	return line_num;  // �� �������� � ��������� ��������

*/
}


enum SymbolType {stSpace, stPunctuation, stHieroglyph, stPrefixPunctuation};

SymbolType getType(int codePoint)
{
	Ogre::uint16 punctuationSymbolArray[] = {187, 65292, 12290, 44, 12289, 46, 33, 63, 65281, 65294, 65311, 25226, 19968, 8230, 0};

	for(int i = 0; punctuationSymbolArray[i] != 0; i++)
	{
		if(punctuationSymbolArray[i] == codePoint)
			return stPunctuation;
	}

	if(codePoint == 161 || codePoint == 171 || codePoint == 191)
		return stPrefixPunctuation;

	if(codePoint == ' ' || codePoint == 160 || codePoint == 12288)
		return stSpace;

	return stHieroglyph;
}


int CVirtualScript::overlaySetCaptionWithWidth(const String& overlayName, const String& containerName, const String& innercontainerName,
											   const DisplayString& strText, float fMaxWidth, bool rtt)
{
	Ogre::DisplayString strSubj = strText;
	int nRealLen = strSubj.length_Characters();

	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);
	if (container)
	{
		container->_update(); // need getSpaceWidth return correct value

		float space_devide = 2.0f;

#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
        space_devide = 1.0f;
#endif
		float fSpaceWidth = ((Ogre::TextAreaOverlayElement*)container)->getSpaceWidth()/space_devide;
		float fDoubleSpaceWidth = ((Ogre::TextAreaOverlayElement*)container)->getSpaceWidth() * 2.0f * space_devide;
		float fOverlayCharHeight = ((Ogre::TextAreaOverlayElement*)container)->getCharHeight();
		String strFontName = ((Ogre::TextAreaOverlayElement*)container)->getFontName();

		Ogre::FontPtr pFont = Ogre::FontManager::getSingleton().getByName(strFontName).staticCast<Ogre::Font>();
		if (pFont.isNull())
			return 0;

		std::pair<size_t,size_t> pairTexDim = pFont->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureDimensions();

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
		float fResX = (float)app.getWindowResolution().x;
		float fResY = (float)app.getWindowResolution().y;
#else
		float fResX = (float)app.getWindowActiveResolution().x;
		float fResY = (float)app.getWindowActiveResolution().y;
		if(rtt)
		{
			fResX = 1024.f;
			fResY = 768;
		}
#endif
		float fCurStrWidth = 0.f;
		float fCurStrWidthWithoutSpace = 0.f;
		int nLastSpace = -1;
		int nLineNum = 1;
		bool mPrefixPunctuationExist = false;

		//---------
		int id = getCfgI("language_id");

		if(id == 0)
		{
			toLogEx("WARNING: language_id is not set!");
		}

		bool bAsian = false;

		if(id & JENGINE_LANG_ASIAN)
			bAsian = true;

//		toLogEx("\n set caption %s %s %s %d", overlayName.c_str(), containerName.c_str(), innercontainerName.c_str(), bAsian);
		if (fSpaceWidth == 0.f)
		{
			Ogre::Font::CodePoint cp = 'a';
			const Ogre::Font::GlyphInfo &giA = pFont->getGlyphInfo(cp);
			float fCharHeight = pairTexDim.second * (giA.uvRect.bottom - giA.uvRect.top) / fResY;
			fSpaceWidth = (pairTexDim.first * (giA.uvRect.right - giA.uvRect.left)) / fResX * (fOverlayCharHeight / fCharHeight);
			fDoubleSpaceWidth = fSpaceWidth * 4.f;
		}

		for (int i = 0; i < nRealLen; i++)
		{
			Ogre::uint32 nCodePoint = strSubj[i];

			SymbolType currentSymbolType = getType(strSubj[i]);
			SymbolType nextSymbolType = stHieroglyph;
			if(i < nRealLen - 1)
				nextSymbolType = getType(strSubj[i+1]);

			if(nCodePoint == '\n')
			{
//				toLogEx(" find end %d", i);
				nLineNum++;

				nLastSpace = -1;
				fCurStrWidth = 0.f;
				fCurStrWidthWithoutSpace = 0.f;
				continue;
			}

//			toLogEx("\n %d - %d %d %d", i, nCodePoint, currentSymbolType, nextSymbolType);

			float fCharWidth = 0;
			if(currentSymbolType == stSpace)
			{
				fCharWidth = fSpaceWidth;

				if(nCodePoint == 12288)
					fCharWidth = fDoubleSpaceWidth;
			}
			else
			{
				const Ogre::Font::GlyphInfo &giCur = pFont->getGlyphInfo(nCodePoint);
				float fCharHeight = pairTexDim.second * (giCur.uvRect.bottom - giCur.uvRect.top) / fResY;
				fCharWidth = (pairTexDim.first * (giCur.uvRect.right - giCur.uvRect.left)) / fResX * (fOverlayCharHeight / fCharHeight);
			}

			fCurStrWidth += fCharWidth;
			if(currentSymbolType != stSpace)
				fCurStrWidthWithoutSpace = fCurStrWidth;
			if(fCurStrWidthWithoutSpace > fMaxWidth)
			{
				if (nLastSpace != -1)
				{
					mPrefixPunctuationExist = false;
//					toLogEx(" \\ %d", nLastSpace);
					i = nLastSpace;
					nLastSpace = -1;

					if(getType(strSubj[i]) == stSpace)
						strSubj.setChar(i, '\n');
					else
					{
						strSubj.insert(strSubj.begin()+i+1,'\n');
						nRealLen++;
						i++;
					}
				}
				else
				{
					strSubj.insert(strSubj.begin()+i,'\n');
					nRealLen++;
				}

				fCurStrWidth = 0.f;
				fCurStrWidthWithoutSpace = 0.f;
				nLineNum++;
			}
			else
			{
				bool nextIsNewLineSymbol = nextSymbolType == stHieroglyph || nextSymbolType == stPrefixPunctuation;
				if(!mPrefixPunctuationExist && nextIsNewLineSymbol &&
					(currentSymbolType != stHieroglyph ||
					bAsian && currentSymbolType == stHieroglyph))
				{
					if(nextSymbolType == stPrefixPunctuation)
						mPrefixPunctuationExist = true;
					nLastSpace = i;
//					toLogEx(" *");
				}

				if(nextSymbolType == stHieroglyph)
					mPrefixPunctuationExist = false;

			}

		}

		overlaySetCaption(overlayName, containerName, innercontainerName, strSubj);
		return nLineNum;
	}

	return 0;
}

int CVirtualScript::overlaySetCaptionFromSTWithWidth(const String& overlayName, const String& containerName, const String& innercontainerName, int stringNum, float fMaxWidth)
{
	Ogre::DisplayString strText = app.getStringTable().getString(stringNum);

	return overlaySetCaptionWithWidth(overlayName, containerName, innercontainerName, strText, fMaxWidth);
}


void CVirtualScript::overlaySetCaptionFromSTBeginEnd(const String& overlayName,
													 const String& containerName,
													 const String& innercontainerName,
													 int stringNum,
													 int startChar,
													 int len,
													 const String& appe)
{
	Ogre::DisplayString unconv_str = app.getStringTable().getString(stringNum);
	Ogre::DisplayString str = _overlaySetCharColorsFromCaption(overlayName,containerName,innercontainerName,unconv_str);

	if (startChar > -1)
	{
		if (len > -1)
			str = str.substr(startChar, len);
		else
			str = str.substr(startChar);
	}

	str.append(appe);

	overlaySetCaption(overlayName, containerName, innercontainerName, str);
	_overlaySetCharColorsFromCaption(overlayName,containerName,innercontainerName,unconv_str,false);
}

void CVirtualScript::overlaySetCaptionFromSTAppend(const String& overlayName,
												   const String& containerName,
												   const String& innercontainerName,
												   int stringNum,
												   const String& app)
{
	overlaySetCaptionFromSTBeginEnd(overlayName,containerName,innercontainerName,stringNum,-1,-1,app);
}

void CVirtualScript::overlayDeleteCaptionSymbol(const String& overlayName, const String& containerName, const String& innercontainerName)
{
	Ogre::DisplayString text;
	overlayGetCaption(overlayName, containerName, innercontainerName, text);
	text = text.substr(0, text.length()-1);
	overlaySetCaption(overlayName, containerName, innercontainerName, text);
}

void CVirtualScript::overlaySetCaptionFromSTAppend2(const String& overlayName,
													const String& containerName,
													const String& innercontainerName,
													int stringNum)
{
	Ogre::DisplayString str;
	overlayGetCaption(overlayName, containerName, innercontainerName, str);
	Ogre::DisplayString str2 = app.getStringTable().getString(stringNum);
	overlaySetCaption(overlayName, containerName, innercontainerName, str + str2);
}

void CVirtualScript::overlaySetCharHeight (const String& overlayName, const String& containerName,
										   const String& innercontainerName, float height)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);

	if (container)
	{
		((Ogre::TextAreaOverlayElement*)container)->setCharHeight(height);
	}
}

bool CVirtualScript::overlaySetCharHeightByScreen(const String& overlayName, const String& containerName, const String& innercontainerName)
{
	float fCharHeight = overlayGetCharHeightByScreen(overlayName, containerName, innercontainerName);
	if (fCharHeight == 0.f)
		return false;

	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);

//	toLogEx("\n overlaySetCharHeightByScreen: %s %s %s", overlayName.c_str(), containerName.c_str(), innercontainerName.c_str());

	Ogre::TextAreaOverlayElement *pTextArea = (Ogre::TextAreaOverlayElement*)container;

	pTextArea->setCharHeight(fCharHeight);
	return true;
}

float CVirtualScript::overlayGetCharHeightByScreen(const String& overlayName, const String& containerName, const String& innercontainerName)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);

	if (!container)
		return 0.f;

	Ogre::TextAreaOverlayElement *pTextArea = (Ogre::TextAreaOverlayElement*)container;


	return fontGetCharHeightByScreen(pTextArea->getFontName());
}

void CVirtualScript::shiftFontMaterials(float x, float y)
{
	Ogre::ResourceManager::ResourceMapIterator it = Ogre::FontManager::getSingleton().getResourceIterator();
	while(it.hasMoreElements())
	{
		Ogre::Font *f = (Ogre::Font*)&(*it.getNext());
		app.toLogFontInfo(f);
	}


	Ogre::MaterialManager::ResourceMapIterator rmi = Ogre::MaterialManager::getSingleton().getResourceIterator();
	while(rmi.hasMoreElements())
	{
		Ogre::MaterialPtr mMaterial  = rmi.getNext().staticCast<Ogre::Material>();
		if (mMaterial->getNumTechniques())
		{
			Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
			if (mTechnique && mTechnique->getNumPasses())
			{
				Ogre::Pass*  mPass = mTechnique->getPass(0);
				if (mPass && mPass->getNumTextureUnitStates())
				{
					Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(0);
					String textureName = mTUState->getTextureName();
					if (textureName.substr(0, 7) == "00_font")
					{
//						toLogEx("\n shift texture %s", textureName.c_str());
						mTUState->setTextureScroll(x, y);
					}
				}
			}
		}
	}
}

void CVirtualScript::overlaySetFont(const String& overlayName, const String& containerName, const String& innercontainerName, const String& fontName)
{
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);

	if (!container)
		return;

	Ogre::TextAreaOverlayElement *pTextArea = (Ogre::TextAreaOverlayElement*)container;

	pTextArea->setFontName(fontName);
}

float CVirtualScript::fontGetCharHeightByScreen(const String& fontName)
{
	Ogre::FontPtr pFont = Ogre::FontManager::getSingleton().getByName(fontName).staticCast<Ogre::Font>();
	if (pFont.isNull())
		return 0.f;

	if(pFont->getMaterial().isNull())
		pFont->load();

	Ogre::Font::GlyphInfo glyphInfo = pFont->getGlyphInfo('A');
	float fHeightUV = (glyphInfo.uvRect.bottom - glyphInfo.uvRect.top);
	std::pair<size_t, size_t> texSize =
		pFont->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureDimensions();
	float fPixelHeight = texSize.second*fHeightUV;

#if IS_JENGINE_PLATFORM_IOS
	float fResolutionY = (float)app.getWindowResolution().y;
#else
	float fResolutionY = 1024.0f;
#endif

	float fCharHeight = fPixelHeight/fResolutionY;

	return fCharHeight;
}

void CVirtualScript::overlaySetCharColors(const String& overlayName, const String& containerName, const String& innercontainerName, size_t start, size_t end, float r, float g, float b)
{
//#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	Ogre::Overlay* overlay;
	Ogre::OverlayContainer* container;
	findOverlay(overlayName, containerName, innercontainerName, overlay, container);

	if (container)
	{
		Ogre::TextAreaOverlayElement *ta = reinterpret_cast<Ogre::TextAreaOverlayElement*>(container);

		//reset colors
		if (start==String::npos || end==String::npos)
		{
			ta->setColourBottom(ta->getColourBottom());
			ta->setColourTop(ta->getColourTop());
			return;
		}

		//----------------
//		ta->initialise(); //sanya: ������-�� ����� ���� ������� �������� ������ �� ������� �������
		ta->_update();

		Ogre::RGBA colour;
		unsigned short colorBinding = 1;
		Ogre::Root::getSingleton().convertColourValue(Ogre::ColourValue(r,g,b), &colour);

		Ogre::RenderOperation rop;
		ta->getRenderOperation(rop);

		if (!rop.vertexData)
			return;

		Ogre::HardwareVertexBufferSharedPtr vbuf = rop.vertexData->vertexBufferBinding->getBuffer(colorBinding);

		Ogre::RGBA* pDest = static_cast<Ogre::RGBA*>(
			vbuf->lock(Ogre::HardwareBuffer::HBL_DISCARD) );
		size_t mAllocSize = vbuf->getNumVertices();

		for (size_t i = 0; i < mAllocSize; ++i)
		{
			size_t char_num = i/6;
			if (char_num >= start && char_num <= end)
			{
				pDest[i] = colour;
			}
		}
		vbuf->unlock();
	}
//#endif
}


#define IS_UNICODE_EMPTY(x) (x == 0x0085 || x == 0x000D || x == 0x000A || x == 0x0020 || x == 0x0030)

Ogre::DisplayString CVirtualScript::_overlaySetCharColorsFromCaption(const String& o, const String& c, const String& in, const Ogre::DisplayString &str, bool only_parse)
{
	//���������� ���� ����� ������, �.�. �������������� ����� � �� ����, � ���� ������� � ����������� ��������
	if (!only_parse)
	{
		overlaySetCharColors(o, c, in, String::npos, String::npos, 0,0,0);
	}

	if (getCfgI("overlays_dont_parse_text") != 0)
		return str;

	if (str.find('[') == Ogre::DisplayString::npos || str.find(']') == Ogre::DisplayString::npos)
	{
		return str;
	}

	Ogre::DisplayString new_str = str;

	//----------------
	//you want to [c=1.0 0 0]skip[/c] this puzzle

	bool start = false;

	size_t start1 = 0;
	size_t end1 = 0;
	size_t start2 = 0;
	size_t end2 = 0;

	Ogre::ColourValue color;

	for(int i = 0;; i++)
	{
		size_t k = 0;
		if ((k = new_str.find('[')) != Ogre::DisplayString::npos)
		{
			//������ ������
			if (!start)
			{
				start1 = k;
				end1 = new_str.find(']',k);
				Ogre::DisplayString s1 = new_str.substr(start1+1,-1 + end1 - start1);
				size_t eq = s1.find('=');
				Ogre::DisplayString param = s1.substr(eq+1);
				//���� - ������� ��������� �����
				color = Ogre::StringConverter::parseColourValue(param);
				start = true;
				new_str.erase(start1, 1+end1-start1);
				continue;
			}
			else //������� ����� ��������, ������� � �������������
			{
				start2 = k;
				end2 = new_str.find(']',k);
				size_t end_len = 1 + end2 - start2;
				new_str.erase(start2,end_len);
				if (!only_parse)
				{
					//������� ������ ������� �� ���������� (������� � ������)
					int shift1 = 0, shift2 = 1;
					for(size_t z = 0; z < start2; z++)
					{
						if (IS_UNICODE_EMPTY(new_str.getChar(z)))
						{
							if (z < start1)
								shift1++;
							shift2++;
						}
					}
					overlaySetCharColors(o,c,in, start1-shift1, start2-shift2, color.r,color.g,color.b);
				}
				start = false;
			}
		}
		else
			break;
	}
	return new_str;
}

void CVirtualScript::overlayCreateWithTexture(const String& overlayName,
								   const String& containerName,
								   const String& innercontainerName,
								   int zOrder,
								   const String& base_material,
								   const String& material,
								   const String& texture,
								   const Vector2* pos,
								   const Vector2* size)
{
	if(!materialExist(material))
	{
		materialClone(base_material, material);
	}

	materialSetTexture(material,texture);

	overlayCreate(overlayName,containerName,innercontainerName,zOrder,material,pos,size);
}

void CVirtualScript::overlayCreate(const String& overlayName,
								   const String& containerName,
								   const String& innercontainerName,
								   int zOrder,
								   const String& material,
								   const Vector2* pos,
								   const Vector2* size)
{
	if (!overlayName.empty())
	{
		Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(overlayName);
		if (!overlay)
		{
			overlay = Ogre::OverlayManager::getSingletonPtr()->create(overlayName);
			overlay->setZOrder((Ogre::ushort)zOrder);
		}
		if (overlay && !containerName.empty())
		{
			Ogre::OverlayContainer* container = NULL;

			if (!Ogre::OverlayManager::getSingletonPtr()->hasOverlayElement(containerName))
			{
				container = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingletonPtr()->createOverlayElement("Panel", containerName);
				container->setMetricsMode(Ogre::GMM_RELATIVE);
				overlay->add2D(container);
			}
			else
				container = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingletonPtr()->getOverlayElement(containerName);

			if (container)
			{
				if (innercontainerName.empty())
				{
					if (!material.empty())	container->setMaterialName(material);
					if (pos)				container->setPosition(pos->x, pos->y);
					if (size)				container->setDimensions(size->x, size->y);
				}
				else
				{
					Ogre::OverlayElement* innercontainer = NULL;
					if (Ogre::OverlayManager::getSingletonPtr()->hasOverlayElement(innercontainerName))
						innercontainer = Ogre::OverlayManager::getSingletonPtr()->getOverlayElement(innercontainerName);
					else
					{
						innercontainer = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingletonPtr()->createOverlayElement("Panel",innercontainerName);
						container->addChild(innercontainer);
					}

					innercontainer->setMetricsMode(Ogre::GMM_RELATIVE);
					if (!material.empty())	innercontainer->setMaterialName(material);
					if (pos)				innercontainer->setPosition(pos->x, pos->y);
					if (size)				innercontainer->setDimensions(size->x, size->y);
				}
			}
		}
	}
}

void CVirtualScript::overlayCreateFromTemplate(const String& overlayName,
											   const String& containerName,
											   const String& innercontainerName,
											   int zOrder,
											   const String& templateName,
											   const String& typeName,
											   const Vector2* pos,
											   const Vector2* size)
{
	if (!overlayName.empty())
	{
		Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(overlayName);
		if (!overlay)
		{
			overlay = Ogre::OverlayManager::getSingletonPtr()->create(overlayName);
			overlay->setZOrder((Ogre::ushort)zOrder);
		}
		if (overlay && !containerName.empty())
		{
			Ogre::OverlayContainer* container = (Ogre::OverlayContainer*) overlay->getChild(containerName);
			if (!container)
			{
				if (!innercontainerName.empty())
				{
					container = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingletonPtr()->createOverlayElement("Panel",containerName);
				}
				else
				{
					container = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingletonPtr()->createOverlayElementFromTemplate(templateName, typeName, containerName);
					if (pos)	container->setPosition(pos->x, pos->y);
					if (size)	container->setDimensions(size->x, size->y);
				}
				container->setMetricsMode(Ogre::GMM_RELATIVE);
				overlay->add2D(container);
			}

			if (container && !innercontainerName.empty())
			{
				Ogre::OverlayElement* innercontainer = NULL;
				if (Ogre::OverlayManager::getSingletonPtr()->hasOverlayElement(innercontainerName))
					innercontainer = Ogre::OverlayManager::getSingletonPtr()->getOverlayElement(innercontainerName);
				else
				{
					innercontainer = Ogre::OverlayManager::getSingletonPtr()->createOverlayElementFromTemplate(templateName, typeName, innercontainerName);
					container->addChild(innercontainer);
				}
				if (innercontainer)
				{
					innercontainer->setMetricsMode(Ogre::GMM_RELATIVE);
					if (pos)	innercontainer->setPosition(pos->x, pos->y);
					if (size)	innercontainer->setDimensions(size->x, size->y);
				}
			}
		}
	}
}

void CVirtualScript::overlaySetMaterial(const String& overlayName, const String& containerName, const String& innercontainerName, const String& materialName)
{
	Ogre::Overlay* mOverlay;
	Ogre::OverlayContainer* mContainer;

	findOverlay(overlayName, containerName, innercontainerName, mOverlay, mContainer);

	if (mContainer)
	{
		mContainer->setMaterialName(materialName);

		//fix mipmap filter bug here
		if (mContainer->getTypeName() == "TextArea")
		{
			_fixMipmapFilterBug(mContainer);
		}
	}
}
void CVirtualScript::overlaySetBorderMaterial(const String& overlayName, const String& containerName, const String& innercontainerName, const String& materialName)
{
	//	Ogre::BorderPanelOverlayElement *elem;
	//	elem->setBorderMaterialName
	//	any_cast

	Ogre::Overlay* pOverlay;
	Ogre::OverlayContainer* pContainer;


	findOverlay(overlayName, containerName, innercontainerName, pOverlay, pContainer);

	if (pContainer)
	{
		//		mContainer->setMaterialName(materialName);
		Ogre::BorderPanelOverlayElement *pBorderPanel =
			dynamic_cast<Ogre::BorderPanelOverlayElement*>(pContainer);
		if (pBorderPanel)
			pBorderPanel->setBorderMaterialName(materialName);
	}
}

void CVirtualScript::overlaySetBorderSize(const String& overlayName, const String& containerName, const String& innercontainerName, float sides, float topAndBottom)
{
	Ogre::Overlay* pOverlay;
	Ogre::OverlayContainer* pContainer;

	findOverlay(overlayName, containerName, innercontainerName, pOverlay, pContainer);

	if (pContainer)
	{
		Ogre::BorderPanelOverlayElement *pBorderPanel =
			dynamic_cast<Ogre::BorderPanelOverlayElement*>(pContainer);
		if (pBorderPanel)
			pBorderPanel->setBorderSize(sides, topAndBottom);
	}
}

void destroyOverlayElement(Ogre::OverlayElement *elem)
{
	Ogre::OverlayManager::getSingletonPtr()->destroyOverlayElement(elem);
}

void overlayContainerDoChildren(Ogre::OverlayContainer *cont, void (*doSomething)(Ogre::OverlayElement *elem), int &level)
{
	int old_level = level;

	if (cont->getTypeName() == "TextArea")
	{
		doSomething(cont);
		return;
	}

	level++;

	Ogre::OverlayContainer::ChildContainerIterator cont_it = cont->getChildContainerIterator();

	while(cont_it.hasMoreElements())
	{
		Ogre::OverlayContainer *cont = cont_it.peekNextValue();

		overlayContainerDoChildren(cont,doSomething,level);

		cont_it.moveNext();
	}

	Ogre::OverlayContainer::ChildIterator elem_it = cont->getChildIterator();

	std::vector<Ogre::OverlayElement *> v;

	while(elem_it.hasMoreElements())
	{
		Ogre::OverlayElement *elem = elem_it.peekNextValue();
		v.push_back(elem);
		elem_it.moveNext();
	}

	for(size_t i = 0; i < v.size(); i++)
		doSomething(v[i]);

	if (old_level == 0)
		doSomething(cont);
}

void CVirtualScript::overlayDestroy(const String& overlayName,
									const String& containerName,
									const String& innercontainerName)
{
	Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(overlayName);
	if (overlay && containerName.empty() && innercontainerName.empty())
	{
		Ogre::Overlay::Overlay2DElementsIterator vlist = overlay->get2DElementsIterator();

		std::vector<Ogre::OverlayContainer*> conts;
		while(vlist.hasMoreElements())
		{
			Ogre::OverlayContainer *cont = vlist.peekNext();
			if (cont) conts.push_back(cont);
			vlist.moveNext();
		}

		for(size_t i = 0; i < conts.size(); i++)
		{
			int level = 0;
			overlayContainerDoChildren(conts[i], destroyOverlayElement, level);
		}

		Ogre::OverlayManager::getSingletonPtr()->destroy(overlay);
		return;
	}

	if (overlay && !containerName.empty())
	{
		Ogre::OverlayContainer* container =
			(Ogre::OverlayContainer*) overlay->getChild (String( containerName ));

		if (container)
		{
			if (!innercontainerName.empty())
			{
				Ogre::OverlayContainer* container2 = (Ogre::OverlayContainer*)container->getChild(innercontainerName);
				container->removeChild(innercontainerName);
				Ogre::OverlayManager::getSingletonPtr()->destroyOverlayElement(container2);
			}
			else
			{
				overlay->remove2D(container);

				int level = 0;
				overlayContainerDoChildren(container, destroyOverlayElement, level);
			}
		}
	}
}

void CVirtualScript::fontdefexCreateAndLoad(const String &strFontdefex, const String &strFont, int nCharHeight)
{
	Ogre::CFontdefexManager::getSingleton().createAndLoadFont(strFontdefex, strFont, nCharHeight);
}

void CVirtualScript::fontdefexCreateAndLoadRanges(const String &strFontdefex, const String &strFont, int nCharHeight)
{
	Ogre::CFontdefexManager::getSingleton().createAndLoadFont(strFontdefex, strFont, nCharHeight, &app.getStringTable());
}

void CVirtualScript::materialSetScale(const String& materialName, Real movH, Real movV)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(0);
	if (mPass == 0)
		return;

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(0);
	if (mTUState == 0)
		return;

	mTUState->setTextureScale(movH, movV);
}

void CVirtualScript::materialSetFiltering(const String& materialName, int min, int mag, int mip)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(0);
	if (mPass == 0)
		return;

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(0);
	if (mTUState == 0)
		return;

	mTUState->setTextureFiltering((Ogre::FilterOptions)min, (Ogre::FilterOptions)mag, (Ogre::FilterOptions)mip);
}


void CVirtualScript::materialSetAnimatedTextureCurrentFrame(const String& materialName, unsigned short npass, unsigned short unit, unsigned int frame)
{
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();
	if (!material.isNull())
	{
		if (material->getNumTechniques()>0)
		{
			Ogre::Technique* technique = material->getTechnique(0);
			if (technique && technique->getNumPasses()>npass)
			{
				Ogre::Pass* pass = technique->getPass(npass);
				if (pass && pass->getNumTextureUnitStates()>unit)
				{
					Ogre::TextureUnitState* state = pass->getTextureUnitState(unit);
					if (state)
					{
						if (state->getNumFrames() > frame)
						{
							state->setCurrentFrame(frame);
							state->_getAnimController()->setEnabled(false);//���������� ��������
						}
					}
				}
				else
					toLogEx("Warning: NumPasses<=%d in materialSetAnimatedTextureCurrentFrame\n", unit);
			}
			else
				toLogEx("Warning: NumTextureUnitStates<=%d in materialSetAnimatedTextureCurrentFrame\n", npass);
		}
		else
			toLogEx("Warning: NumTechniques==0 in materialSetAnimatedTextureCurrentFrame\n");
	}
	else
		toLogEx("Warning: material '%s' not found in materialSetAnimatedTextureCurrentFrame\n", materialName.c_str());
}

void CVirtualScript::materialSetAnimatedTexturePlayStop(const String& materialName, unsigned short npass, unsigned short unit, bool play_stop)
{
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();
	if (!material.isNull())
	{
		if (material->getNumTechniques()>0)
		{
			Ogre::Technique* technique = material->getTechnique(0);
			if (technique && technique->getNumPasses()>npass)
			{
				Ogre::Pass* pass = technique->getPass(npass);
				if (pass && pass->getNumTextureUnitStates()>unit)
				{
					Ogre::TextureUnitState* state = pass->getTextureUnitState(unit);
					if (state)
					{
//						state->setCurrentFrame(frame);
//						state->_getAnimController()->setEnabled(false);//���������� ��������
						state->_getAnimController()->setEnabled(play_stop);
					}
				}
				else
					toLogEx("Warning: NumPasses<=%d in materialSetAnimatedTextureCurrentFrame\n", unit);
			}
			else
				toLogEx("Warning: NumTextureUnitStates<=%d in materialSetAnimatedTextureCurrentFrame\n", npass);
		}
		else
			toLogEx("Warning: NumTechniques==0 in materialSetAnimatedTextureCurrentFrame\n");
	}
	else
		toLogEx("Warning: material '%s' not found in materialSetAnimatedTextureCurrentFrame\n", materialName.c_str());
}


void CVirtualScript::materialSetScalePassUnit (const String& materialName, unsigned short pass, unsigned short unit, Real movH, Real movV)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(pass);
	if (mPass == 0)
		return;

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(unit);
	if (mTUState == 0)
		return;

	mTUState->setTextureScale(movH, movV);
}

void CVirtualScript::materialSetScroll (const String& materialName, Real movH, Real movV)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(0);
	if (mPass == 0)
		return;

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(0);
	if (mTUState == 0)
		return;

	mTUState->setTextureScroll(movH, movV);
}

void CVirtualScript::materialSetScrollAnim (const String& materialName, Real movH, Real movV)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(0);
	if (mPass == 0)
		return;

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(0);
	if (mTUState == 0)
		return;

	mTUState->setScrollAnimation(movH, movV);
}

void CVirtualScript::materialSetScrollPassUnit(const String& materialName, unsigned short pass, unsigned short unit, Real movH, Real movV)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();
	if (mMaterial->getNumTechniques()>0)
	{
		Ogre::Technique*  mTechnique = mMaterial->getTechnique(0);
		if (mTechnique == NULL) return;

		if (mTechnique->getNumPasses()>pass)
		{
			Ogre::Pass*  mPass = mTechnique->getPass(pass);
			if (mPass == NULL) return;

			if (mPass->getNumTextureUnitStates()>unit)
			{
				Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(unit);
				if (mTUState == NULL) return;

				mTUState->setTextureScroll(movH, movV);
			}
		}
	}
}

void CVirtualScript::materialSetAlpha(const String& materialName, Real alpha)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

//	toLogEx("\n materialSetAlpha1");
	if (mMaterial.isNull())
		return;

//	toLogEx("\n materialSetAlpha1 %p", mMaterial.get());
	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

//	for(int i = 0; i < mTechnique->getNumPasses(); i++)
//		toLogEx("\n %d %p", i, mTechnique->getPass(i));

//	toLogEx("\n materialSetAlpha1 %p", mTechnique);
	Ogre::Pass*  mPass = mTechnique->getPass(0);
	if (mPass == 0)
		return;

//	toLogEx("\n materialSetAlpha1 %p %d", mPass, mPass->getNumTextureUnitStates());
	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(0);
	if (mTUState == 0)
		return;

//	toLogEx("\n materialSetAlpha1");
	mTUState->setAlphaOperation(Ogre::LBX_MODULATE, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, 0.0, alpha, 0.0);
//	toLogEx("\n materialSetAlpha1");
}

void CVirtualScript::materialSetAlphaPassUnit(const String& materialName, unsigned short pass, unsigned short unit, Real alpha)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	if (mMaterial.isNull())
		return;

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(pass);
	if (mPass == 0)
		return;

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(unit);
	if (mTUState == 0)
		return;

	if (alpha < 0 || alpha > 1)
	{
		toLogEx("ERROR: Try to set alpha %f to material %s\n", alpha, materialName.c_str());
		return;
	}

	mTUState->setAlphaOperation (Ogre::LBX_MODULATE, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, 0.0, alpha, 0.0);
}

void CVirtualScript::materialSetBlend (const String& materialName, int blend)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	if (mMaterial.isNull())
		return;

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(0);
	if (mPass == 0)
		return;

	switch(blend)
	{
	case 0: mPass->setSceneBlending(Ogre::SBT_REPLACE);
		mPass->setDepthWriteEnabled(true);
		mPass->setDepthCheckEnabled(true);
		break;
	case 1: mPass->setSceneBlending(Ogre::SBT_ADD);
		mPass->setDepthWriteEnabled(false);
		break;
	case 2: mPass->setSceneBlending(Ogre::SBT_MODULATE);
		mPass->setDepthWriteEnabled(false);
		break;
	case 3: mPass->setSceneBlending(Ogre::SBF_ONE_MINUS_DEST_COLOUR,Ogre::SBF_ONE);
		mPass->setDepthWriteEnabled(false);
		break;

	case 4: mPass->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
		mPass->setDepthWriteEnabled(true);
		mPass->setDepthCheckEnabled(false);
		break;
	}
}

void CVirtualScript::materialSetColor(const String& materialName, int color_op, Real r, Real g, Real b)
{
	materialSetColorPassUnit(materialName, 0, 0, color_op, r, g, b);
}

void CVirtualScript::materialSetAlphaAndColor(const String& materialName, Real a)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	if (mMaterial.isNull())
		return;

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Technique::PassIterator pIt = mTechnique->getPassIterator();

	if (a < 0 || a > 1)
	{
		toLogEx("ERROR: Try to set alpha and color %f to material %s\n", a, materialName.c_str());
		return;
	}

	while(pIt.hasMoreElements())
	{
		Ogre::Pass *p = pIt.getNext();
		if (p)
		{
			Ogre::Pass::TextureUnitStateIterator tuIt = p->getTextureUnitStateIterator();
			if (tuIt.hasMoreElements())
			{
				Ogre::TextureUnitState *tu = tuIt.getNext();
				if (p->getSourceBlendFactor() == Ogre::SBF_SOURCE_ALPHA)
					tu->setAlphaOperation (Ogre::LBX_MODULATE, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, 0.0, a, 0);
				else
					tu->setColourOperationEx(Ogre::LBX_MODULATE, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, Ogre::ColourValue::Black, Ogre::ColourValue(a,a,a,a),0);
			}
		}
	}
}

void CVirtualScript::materialSetColorPass(const String& materialName, unsigned short pass, int color_op, Real r, Real g, Real b)
{
	materialSetColorPassUnit(materialName, pass, 0, color_op, r, g, b);
}

String CVirtualScript::materialGetInfoPassUnit(const String& materialName, unsigned short pass, unsigned short unit)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();
	if (mMaterial.isNull())
		return "";
	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return "";
	Ogre::Pass*  mPass = mTechnique->getPass(pass);
	if (mPass == 0)
		return "";
	int text_unit_num = mPass->getNumTextureUnitStates();
	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(unit);
	if (mTUState == 0)
		return "";

	Ogre::LayerBlendModeEx lbme = mTUState->getAlphaBlendMode();

	char buf[256];
	sprintf(buf, "%s p=%d u=%d op=%d s1=%d s2=%d a1=%f a2=%f",
		materialName.c_str(), pass, unit, (int)lbme.operation, (int)lbme.source1, (int)lbme.source2,
		(float)lbme.alphaArg1, (float)lbme.alphaArg2);

	return buf;
}

void CVirtualScript::materialSetColorPassUnit(const String& materialName, unsigned short pass, int tex_unit, int color_op, Real r, Real g, Real b)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	if (mMaterial.isNull())
		return;

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(pass);
	if (mPass == 0)
		return;

	int text_unit_num = mPass->getNumTextureUnitStates();
	if (tex_unit >= text_unit_num)
	{
		toLogEx("ERROR!!!! materialSetColorPassUnit('%s', %d, %d, ...)\n there is no texture unit %d. There is only %d texture unit ", materialName.c_str(), pass, tex_unit, tex_unit, text_unit_num);
		return;
	}

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(tex_unit);
	if (mTUState == 0)
		return;

	if (r < 0 || r > 1 || g < 0 || g > 1 || b < 0 || b > 1)
	{
		toLogEx("ERROR: Try to set color (%f, %f, %f) to material %s\n", r, g, b, materialName.c_str());
		return;
	}

	if (color_op == 0)
		mTUState->setColourOperationEx(Ogre::LBX_ADD, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, Ogre::ColourValue::Black, Ogre::ColourValue(r,g,b,b),0);
	else if (color_op == 1)
		mTUState->setColourOperationEx(Ogre::LBX_MODULATE, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, Ogre::ColourValue::Black, Ogre::ColourValue(r,g,b,b),0);
	else if (color_op == 2)
		mTUState->setColourOperationEx(Ogre::LBX_SUBTRACT, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, Ogre::ColourValue::Black, Ogre::ColourValue(r,g,b,b),0);
	else if (color_op == 3)
	{
		Ogre::LayerBlendModeEx lbme = mTUState->getColourBlendMode();
		mTUState->setColourOperationEx(lbme.operation, lbme.source1, lbme.source2, Ogre::ColourValue(r,g,b,1), Ogre::ColourValue(r,g,b,1), r);
	}
}

void CVirtualScript::materialSetAmbientPass(const String& materialName, int pass, Real r, Real g, Real b)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();
	if (mMaterial.isNull())
		return;
	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;
	Ogre::Pass*  mPass = mTechnique->getPass(pass);
	if (mPass == 0)
		return;

	mMaterial->setAmbient(r, g, b);
	mMaterial->setLightingEnabled(true);
}

void CVirtualScript::mouseGetPosition(Vector2& pos)
{
	if (app.getOIS())
	{
        pos = app.getOIS()->GetMousePosition() + app.getCursorCastOffset();
	}
	else
		toLogEx("ERROR: mouseGetPosition - OIS not initialize\n");
}

bool CVirtualScript::mouseGetStateLButton()
{
	if (app.getOIS())
		return app.getOIS()->m_bLMouseButton;
	else
		toLogEx("ERROR: mouseGetStateLButton - OIS not initialize\n");
	return false;
}

bool CVirtualScript::mouseGetStateRButton()
{
	if (app.getOIS())
		return app.getOIS()->m_bRMouseButton;
	else
		toLogEx("ERROR: mouseGetStateRButton - OIS not initialize\n");
	return false;
}
int CVirtualScript::mouseGetPinch()
{
	return app.getMousePinch();
}
void CVirtualScript::mouseSetPinch(int pinch)
{
	app.setMousePinch(pinch);
}



Ogre::Vector2& CVirtualScript::mouseGetCursorCastOffset()
{
    return app.getCursorCastOffset();
}

void CVirtualScript::mouseSetCursorCastOffset(const Ogre::Vector2 &vecOffset)
{
    app.setCursorCastOffset(vecOffset);
}

void  CVirtualScript::materialSetTexture(const String& materialName, const String& textureName)
{
	materialSetTexturePassUnit(materialName, 0, 0, textureName);
}

void CVirtualScript::materialSetTexturePass(const String& materialName, int pass, const String& textureName)
{
	materialSetTexturePassUnit(materialName, pass, 0, textureName);
}

void CVirtualScript::materialSetTexturePassUnit(const String& materialName, int pass, int unit, const String& textureName)
{
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();
	if (!material.isNull())
	{
		if (material->getNumTechniques()>0)
		{
			Ogre::Technique*  technique = material->getTechnique (0);
			if (technique)
			{
				if (pass<technique->getNumPasses())
				{
					Ogre::Pass*  tpass = technique->getPass(pass);
					if (tpass && unit<tpass->getNumTextureUnitStates())
					{
						Ogre::TextureUnitState* state = tpass->getTextureUnitState(unit);
						if (state)
						{
							const char* exts[] = {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_ANDROID || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
								"dds",
#endif
#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE || OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
								"pvr",
#endif
								"tga",
								"jpg",
								"png",
								NULL
							};
							String tex(textureName);
							// find in loaded textures
							for (size_t i=0; exts[i]!=NULL && Ogre::TextureManager::getSingleton().getByName(tex, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME).isNull(); i++)
							{
								size_t pos = tex.find_last_of('.');
								if (pos!=String::npos)
									tex.replace(pos+1, strlen(exts[i]), exts[i]);
							}
							if (!Ogre::TextureManager::getSingleton().getByName(tex, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME).isNull())
								state->setTextureName(tex);
							else
							{
								// find in all resource
								for (size_t i=0; exts[i]!=NULL && !Ogre::ResourceGroupManager::getSingleton().resourceExistsInAnyGroup(tex); i++)
								{
									size_t pos = tex.find_last_of('.');
									if (pos!=String::npos)
										tex.replace(pos+1, strlen(exts[i]), exts[i]);
								}
								if (Ogre::ResourceGroupManager::getSingleton().resourceExistsInAnyGroup(tex))
									state->setTextureName(tex);
								else
									toLogEx("Error in materialSetTexturePassUnit(): Can't find texture '%s' in resource\n", textureName.c_str());
							}
						}
					}
				}
			}
			else
				toLogEx("WARNING: can't get technique 0 from material %s\n", materialName.c_str());
		}
	}
	else
		toLogEx("CVirtualScript::materialSetTexturePassUnit(): mMaterial [%s] is empty\n", materialName.c_str());
}

bool CVirtualScript::materialExist(const String& material)
{
	Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().getByName(material).staticCast<Ogre::Material>();

	if(!mat.isNull())
	{
		return true;
	}

	return false;
}

bool CVirtualScript::materialClone(const String& materialTemplate, const String& materialNew)
{
	Ogre::MaterialPtr matNew = Ogre::MaterialManager::getSingleton().getByName(materialNew).staticCast<Ogre::Material>();
	if(!matNew.isNull())
	{
		//toLogEx("\nWARNING: Clone material: %s allreddy exist", materialNew.c_str());
		return false;
	}
	else
	{
		Ogre::MaterialPtr matTemplate = Ogre::MaterialManager::getSingleton().getByName(materialTemplate).staticCast<Ogre::Material>();
		if (matTemplate.isNull())
		{
			toLogEx("\nERROR: Can't find template material %s", materialTemplate.c_str());
			return false;
		}

		matTemplate->clone(materialNew);


		return true;
	}
}

bool CVirtualScript::resourceExists(const String& resName)
{
	return Ogre::ResourceGroupManager::getSingleton().resourceExistsInAnyGroup(resName);
}

bool CVirtualScript::textureExists(const String& resName)
{
	return !Ogre::TextureManager::getSingleton().getByName(resName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME).isNull();
}

void  CVirtualScript::materialSetRotate (const String& materialName, int tex_unit, float rot)
{
	Ogre::MaterialPtr mMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(0);
	if (mPass == 0)
		return;

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(tex_unit);
	if (mTUState == 0)
		return;

	mTUState->setTextureRotate(Ogre::Degree(rot));
}

void clearInterpolateAnimationState(CGameObject* pObj)
{
	if (pObj->pAnimState)
	{
		pObj->pAnimState = 0;
	}

	pObj->pEntity->getAllAnimationStates()->removeAllAnimationStates();

	Ogre::Skeleton *pSkel = pObj->pEntity->getSkeleton();
	if (pSkel->getNumAnimations() > 0)
	{
		if (pSkel->getAnimation("InterpolateAnimation"))
			pSkel->removeAnimation("InterpolateAnimation");

		pSkel->removeAllLinkedSkeletonAnimationSources();
	}
}

Quaternion getRotToKeepInView(Vector3& pointToKeepInView, Vector3& p, Quaternion& q)
{
	Quaternion rotToKeepInView;
	Vector3 dirToKeepInView = p - pointToKeepInView;
	dirToKeepInView.normalise();

	Vector3 dirCurrentCam = q*Vector3::UNIT_Z;
	dirCurrentCam.normalise();

	rotToKeepInView = CAnimatableObject::_getRotationTo(dirCurrentCam, dirToKeepInView, Vector3::UNIT_Z)*q;
	rotToKeepInView.normalise();

	return rotToKeepInView;
}

void setCameraMoveNearPlaneAnimationTrack(Ogre::NodeAnimationTrack *pTrack, float len, Vector3& p1, Vector3& p2, Quaternion& q1, Quaternion& q2, Vector3& pointToKeepInView, bool isCamOut)
{
	Ogre::TransformKeyFrame* key;

	Real zoom_interval = 100.f;
	Real switch_time = len*0.05f;

	Real zoom_interval1;
	Real zoom_interval2;

	Quaternion rotToKeepInView;
	Vector3 switchPoint1;
	Vector3 switchPoint2;

	Quaternion switchRot1;
	Quaternion switchRot2;

	switchRot2 = q2;

	if (isCamOut)
	{
		rotToKeepInView = getRotToKeepInView(pointToKeepInView, p2, q2);

		switchRot1 = q1;

		zoom_interval1 = zoom_interval*.3f;
		zoom_interval2 = zoom_interval*1.f;

		switchPoint1 = p1 + (switchRot1*Vector3::UNIT_Z)*zoom_interval1;
		switchPoint2 = p2 - (switchRot2*Vector3::UNIT_Z)*zoom_interval2;
	}
	else
	{
		rotToKeepInView = getRotToKeepInView(pointToKeepInView, p1, q1);

		switchRot1 = rotToKeepInView;

		zoom_interval1 = zoom_interval*1.f;
		zoom_interval2 = zoom_interval*.3f;

		switchPoint1 = p1 - (switchRot1*Vector3::UNIT_Z)*zoom_interval1;
		switchPoint2 = p2 + (switchRot2*Vector3::UNIT_Z)*zoom_interval2;
	}

	key = pTrack->createNodeKeyFrame(0.);
	key->setRotation(q1);
	key->setTranslate(p1);

	key = pTrack->createNodeKeyFrame(switch_time);
	key->setRotation(switchRot1);
	key->setTranslate(switchPoint1);

	key = pTrack->createNodeKeyFrame(len - switch_time);
	key->setRotation(switchRot2);
	key->setTranslate(switchPoint2);

	key = pTrack->createNodeKeyFrame(len);
	key->setRotation(q2);
	key->setTranslate(p2);
}

int CVirtualScript::startCameraMoveNearPlaneAnimation(OBJECT_HANDLE handle,  OBJECT_HANDLE start_pt, OBJECT_HANDLE end_pt, float len, int ntimes, OBJECT_HANDLE thandle, OBJECT_HANDLE vhandle)
{
	// ���������� ���� len ��� ���� ����������� ��������:
	// -1 - ������ �� �������� �����
	//  1 - ����� �� ������� ����
	bool is_cam_out = len < 0;
	len = len < 0 ? -len : len; // ��

	CGameObject* pObj = app._findObjectByHandle(handle);
	CGameObject* pObjKeepInView = app._findObjectByHandle(vhandle);

	CGameObject* pObjStart = app._findObjectByHandle(start_pt);
	CGameObject* pObjFinish = app._findObjectByHandle(end_pt);


	clearInterpolateAnimationState(pObj);

	Vector3 point_to_keep_in_view = pObjKeepInView->pSNode->getPosition();

	Vector3 p1 = pObjStart->pSNode->getPosition();
	Vector3 p2 = pObjFinish->pSNode->getPosition();
	Quaternion q1 = pObjStart->pSNode->getOrientation();
	Quaternion q2 = pObjFinish->pSNode->getOrientation();

	//----------------------
	Vector3 px1 = q1*Vector3::UNIT_X;
	Vector3 px2 = q2*Vector3::UNIT_X;

	float dot = px1.dotProduct(px2);

	float coef = 1.0f - dot/2.0f;
	len *= coef;

	Ogre::Skeleton *pSkel = pObj->pEntity->getSkeleton();
	Ogre::Animation *pAnim = pSkel->createAnimation("InterpolateAnimation",len);

	Ogre::Bone *pBone = pSkel->getBone(0);
	pBone->setPosition(Vector3::ZERO);
	pBone->setOrientation(Quaternion::IDENTITY);
	pBone->setInitialState();

	Ogre::NodeAnimationTrack *pTrack = pAnim->createNodeTrack(0,pBone);

    setCameraMoveNearPlaneAnimationTrack(pTrack, len, p1, p2, q1, q2, point_to_keep_in_view, is_cam_out);

	pBone->setPosition(p1);
	pBone->setOrientation(q1);

	pAnim->setInterpolationMode(Ogre::Animation::IM_LINEAR);
	pAnim->setRotationInterpolationMode(Ogre::Animation::RIM_LINEAR);

	pObj->pEntity->refreshAvailableAnimationState();

	return startAnimation(handle, "InterpolateAnimation",ntimes,thandle);
}

int CVirtualScript::startInterpolateAnimation(OBJECT_HANDLE handle,  OBJECT_HANDLE start_pt, OBJECT_HANDLE end_pt, float len, int ntimes, OBJECT_HANDLE thandle)
{
	if(!start_pt || !end_pt)
	{
		toLogEx("ERROR: startInterpolateAnimation has bad parameters (pObjStart:%u, pObjFinish:%u)!",start_pt,end_pt);
		return 0;
	}

	CGameObject* pObjStart = app._findObjectByHandle(start_pt);
	CGameObject* pObjFinish = app._findObjectByHandle(end_pt);


	Vector3 p1 = pObjStart->pSNode->getPosition();
	Quaternion q1 = pObjStart->pSNode->getOrientation();


	Vector3 p2 = pObjFinish->pSNode->getPosition();
	Quaternion q2 = pObjFinish->pSNode->getOrientation();

	return startInterpolateAnimation2(handle, p1, q1, p2, q2, len, ntimes, thandle);
}

int CVirtualScript::startInterpolateAnimation2(OBJECT_HANDLE handle,  const Vector3 &p1, const Quaternion &q1, const Vector3 &p2, const Quaternion &q2, float len, int ntimes, OBJECT_HANDLE thandle)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pEntity)
	{
		if (pObj->pAnimState)
			pObj->pAnimState = NULL;
		if (pObj->pEntity->getAllAnimationStates())
			pObj->pEntity->getAllAnimationStates()->removeAllAnimationStates();

		Ogre::Skeleton *pSkel = pObj->pEntity->getSkeleton();
		if (pSkel)
		{
			if (pSkel->getNumAnimations() > 0)
			{
				if (pSkel->getAnimation("InterpolateAnimation"))
					pSkel->removeAnimation("InterpolateAnimation");

				pSkel->removeAllLinkedSkeletonAnimationSources();
			}

			Vector3 px1 = q1*Vector3::UNIT_X;
			Vector3 px2 = q2*Vector3::UNIT_X;

			float dot = px1.dotProduct(px2);
			float coef = 1.0f - dot/2.0f;
			len *= coef;

			//----------------------
			Ogre::Animation *pAnim = pSkel->createAnimation("InterpolateAnimation",len);
			if (pAnim)
			{
				Ogre::Bone *pBone = pSkel->getBone(0);
				if (pBone)
				{
					pBone->setPosition(Vector3::ZERO);
					pBone->setOrientation(Quaternion::IDENTITY);
					pBone->setInitialState();
				}

				//-----------------------------------
				Ogre::NodeAnimationTrack *pTrack = pAnim->createNodeTrack(0,pBone);
				if (pTrack)
				{
					Ogre::TransformKeyFrame* key = NULL;
					const int keys_num = 32;
					float key_len = 1.f/keys_num;
					for (int i = 0; i < keys_num; i++)
					{
						float key_time = (float)i*key_len;
						float coef = MathUtils::cosFunc(key_time);
						key = pTrack->createNodeKeyFrame(len*key_time);
						if (key)
						{
							Quaternion q = Quaternion::Slerp(coef, q1, q2, true);
							key->setRotation(q);
							key->setTranslate(MathUtils::interp(p1, p2, coef));
						}
					}

					key = pTrack->createNodeKeyFrame(len);
					if (key)
					{
						key->setTranslate(p2);
						key->setRotation(q2);
					}
				}

				//-------
				pBone->setPosition(p1);
				pBone->setOrientation(q1);

				pAnim->setInterpolationMode(Ogre::Animation::IM_LINEAR);
				pAnim->setRotationInterpolationMode(Ogre::Animation::RIM_LINEAR);

				pObj->pEntity->refreshAvailableAnimationState();
				pObj->resetAnimTimeScaler();
			}
		}
	}
	return startAnimation(handle,"InterpolateAnimation",ntimes,thandle);
}

int CVirtualScript::startAnimation(OBJECT_HANDLE handle, const String& animName, int ntimes, OBJECT_HANDLE thandle)
{
	return startAnimation(handle, animName, ntimes, thandle, 0, 1);
}

int CVirtualScript::startAnimation(OBJECT_HANDLE handle, const String& animName, int ntimes, OBJECT_HANDLE thandle, float begin, float end)
{
	toLogEx("Play linked anim: skel:[%s]\n", animName.c_str());
	stopAnimation(handle);

	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pEntity)
	{
		pObj->pAnimState = pObj->pEntity->getAnimationState(animName);
		pObj->resetAnimTimeScaler();
		pObj->setAnimationBeginEnd(begin, end);
		if (pObj->pAnimState)
		{
			pObj->pAnimState->setEnabled(true);
			pObj->pAnimState->setLoop(false);
			pObj->pAnimState->setWeight(1.0f);
			pObj->updateAnimTimePosition();
		}
		pObj->mTimes = ntimes;
		pObj->pTarget = app._findObjectByHandle(thandle);
		return (int)pObj->pAnimState;
	}
	return 0;
}

void CVirtualScript::animSetLoop(OBJECT_HANDLE handle, bool set)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animSetLoop for not animatable object");
		((CAnimatableObject*)obj)->setLooped(set);
	}
}

void CVirtualScript::animSkip(OBJECT_HANDLE handle)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animSkip for not animatable object");
		((CAnimatableObject*)obj)->skip();
	}
}

void CVirtualScript::animSkipAll(OBJECT_HANDLE handle)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animSkipAll for not animatable object");
		((CAnimatableObject*)obj)->skip(true);
	}
}

void CVirtualScript::animStart(OBJECT_HANDLE handle)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animStart for not animatable object");
		((CAnimatableObject*)obj)->start();
	}
}

void CVirtualScript::animStop(OBJECT_HANDLE handle)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animStop for not animatable object");
		((CAnimatableObject*)obj)->stop();
	}
}

void CVirtualScript::animClear(OBJECT_HANDLE handle)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animClear for not animatable object");
		((CAnimatableObject*)obj)->clear();
	}
}

void CVirtualScript::animAddAnim(OBJECT_HANDLE handle, const String& anim, int ntimes, OBJECT_HANDLE target, int param)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animAddAnim for not animatable object");
		((CAnimatableObject*)obj)->addPoint(anim, ntimes, 1000, target, param);
	}
}

void CVirtualScript::animAddAnim2(OBJECT_HANDLE handle, const String& anim, int ntimes,
								 const String& anim2, size_t snd_id,
								 OBJECT_HANDLE target, int param)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animAddAnim2 for not animatable object");
		((CAnimatableObject*)obj)->addPoint(anim, ntimes, 1000, anim2, snd_id, target, param);
	}
}

void CVirtualScript::animAddMove(OBJECT_HANDLE handle, const String& anim, const Vector3& pos, const Vector3& rot, bool carePos, bool careRot, OBJECT_HANDLE target, int param)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animAddMove for not animatable object");

		Ogre::Matrix3 m;
		m.FromEulerAnglesXYZ(Ogre::Degree(rot.x),Ogre::Degree(rot.y),Ogre::Degree(rot.z));
		((CAnimatableObject*)obj)->addPoint(anim, pos, Quaternion(m), carePos, careRot, target, param); // (Mac OS X specific) don't use anim and m instead animString and q because XCode will gone mad
	}
}

void CVirtualScript::animAddMove2(OBJECT_HANDLE handle, const String& anim, const Vector3& pos, const Vector3& rot, bool carePos, bool careRot, const String& anim2, size_t snd_id, OBJECT_HANDLE target, int param)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animAddMove2 for not animatable object");

		Ogre::Matrix3 m;
		m.FromEulerAnglesXYZ(Ogre::Degree(rot.x),Ogre::Degree(rot.y),Ogre::Degree(rot.z));
		((CAnimatableObject*)obj)->addPoint(anim, pos, Quaternion(m), carePos, careRot, anim2, snd_id, target, param);
	}
}

void CVirtualScript::animSetRotSpeed(OBJECT_HANDLE handle, const String& anim, float rot_speed)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animSetRotSpeed for not animatable object");
		((CAnimatableObject*)obj)->setAnimationParam(anim, 0, rot_speed);
	}
}

// ���������� 1000 * �� �������� �������� ����������� ������ ��� �������� �������� (��. animAddMove)
// int � ��������� �� 1000 - ������ ��� � ���������� float � �������� ��������� ��������
int CVirtualScript::animGetRotSpeed(OBJECT_HANDLE handle, const String& anim)
{
	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animGetRotSpeed for not animatable object");
		return static_cast<int>(((CAnimatableObject*)obj)->getAnimationRotSpeed(anim)*1000);
	}
	return 0;
}

int CVirtualScript::animGetCurrentLength(OBJECT_HANDLE handle, const String& animationName)
{
	float result = 0;

	CGameObject* obj = app._findObjectByHandle(handle);
	if (obj)
	{
		assert(obj->getType()==CGameObject::T_ANIM_OBJ && "using animGetCurrentLength for not animatable object");
		int anim_idx = ((CAnimatableObject*)obj)->findAnim(animationName);
		if (anim_idx >= 0)
		{
			Ogre::AnimationState *anim_state = ((CAnimatableObject*)obj)->mAnimsList[anim_idx].pAnimState;
			result = anim_state->getLength();
		}
		else
			toLogEx("ERROR: CVirtualScript::animGetCurrentLength(): can't find animation %s for CAnimatableObject %s\n", animationName.c_str(), obj->getName());
	}
	else
		toLogEx("ERROR: CVirtualScript::animGetCurrentLength(): can't find CAnimatableObject by handle %d\n", handle);

	return static_cast<int>(result*1000);
}

float CVirtualScript::getAnimationPosition(OBJECT_HANDLE handle)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
		return pObj->getAnimationPosition();
	return 0;
}

void CVirtualScript::setAnimationPosition(OBJECT_HANDLE handle, float position)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
		pObj->setAnimationPosition(position);
}

int CVirtualScript::startLinkedAnimation (OBJECT_HANDLE handle, const String& skeletonName, const String& animName, int ntimes, OBJECT_HANDLE thandle)
{
	return startLinkedAnimation(handle, skeletonName, animName, ntimes, thandle, 0, 1);
}

int CVirtualScript::startLinkedAnimation (OBJECT_HANDLE handle, const String& skeletonName, const String& animName, int ntimes, OBJECT_HANDLE thandle, float begin, float end)
{
	stopAnimation(handle);

	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pEntity)
	{
		if (!pObj->pEntity->hasSkeleton())
		{
			toLogEx("ERROR: startLinkedAnimation failed! Ogre::Entity %s has no skeleton!\n", pObj->pSNode->getName().c_str());
			return 0;
		}

		Ogre::SkeletonInstance *pBaseSkel = pObj->pEntity->getSkeleton();
		pObj->pEntity->getAllAnimationStates()->removeAllAnimationStates();
		pBaseSkel->removeAllLinkedSkeletonAnimationSources();

		if (pBaseSkel->getName() == skeletonName)
		{
			toLogEx("ERROR: skeleton names are equal\n");
			return 0;
		}

		pBaseSkel->addLinkedSkeletonAnimationSource(skeletonName);
		pObj->pEntity->refreshAvailableAnimationState();
		Ogre::Skeleton::LinkedSkeletonAnimSourceIterator linkIter = pBaseSkel->getLinkedSkeletonAnimationSourceIterator();

		Ogre::LinkedSkeletonAnimationSource animSrc = linkIter.getNext();
		CGameObject::equalSkeletons (pBaseSkel, animSrc.pSkeleton);
		return startAnimation(handle, animName, ntimes, thandle, begin, end);
	}
	return 0;
}

int CVirtualScript::startBlendAnimation(OBJECT_HANDLE handle, const String& skeletonName, const String& animName, int ntimes, int blend_time, OBJECT_HANDLE thandle)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	if (!pObj)
	{
		toLogEx("ERROR: Animated object %s not found!\n", skeletonName.c_str());
		return 0;
	}

	if (!pObj->_pAnimState[0])
		blend_time = 0;

	pObj->startBlendTime = app.getTime();
	pObj->endBlendTime = pObj->startBlendTime + blend_time;

	if (!pObj->pEntity)
	{
		toLogEx("ERROR: No entity at %s animation!\n", animName.c_str());
		return 0;
	}

	Ogre::SkeletonInstance *pBaseSkel = pObj->pEntity->getSkeleton();

	if (pBaseSkel->getName()==skeletonName)
	{
		toLogEx("ERROR: Skeletons names(%s) are equal!\n", skeletonName.c_str());
		return 0;
	}

	pBaseSkel->addLinkedSkeletonAnimationSource(skeletonName);
	pObj->pEntity->refreshAvailableAnimationState();

	pObj->_pAnimState[1] = pObj->pEntity->getAnimationState(animName);
	pObj->_pAnimState[1]->setEnabled(true);
	pObj->_pAnimState[1]->setLoop(false);
	pObj->_pAnimState[1]->setWeight(0);
	pObj->_pAnimState[1]->setTimePosition(0);
	pObj->_mTimes[1] = ntimes;
	pObj->_pTarget[1] = app._findObjectByHandle(thandle);

	pObj->blendSkeletons();

	pBaseSkel->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);

	return (int)pObj->_pAnimState[1];
}

void CVirtualScript::stopAnimation (OBJECT_HANDLE handle)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pAnimState)
	{
		pObj->pAnimState->setEnabled(false);
		pObj->pAnimState->setLoop(false);
		pObj->pAnimState = NULL;
	}
}


void CVirtualScript::ribbonVisible(const String& ribbonName, bool state)
{
	Ogre::RibbonTrail* rt = app.getSceneManager()->getRibbonTrail(ribbonName);
	if (rt)
		rt->setVisible(state);
}

void CVirtualScript::particleShowHide(const String& particleName, bool state)
{
	if (!particleName.empty())
	{
		Ogre::ParticleSystem* ps = app.getSceneManager()->getParticleSystem(particleName);
		if (ps) ps->setVisible(state);
	}
	else
		toLogEx("ERROR: particleShowHide  empty string\n");
}

void CVirtualScript::particlePlayStop (const String& particleName, bool state)
{
	if (particleName.empty())
		toLogEx("ERROR: particlePlayStop  empty string\n");
	Ogre::ParticleSystem* PS = app.getSceneManager()->getParticleSystem(particleName);
	for (unsigned short i=0; i < PS->getNumEmitters(); i++)
	{
		Ogre::ParticleEmitter* PE = PS->getEmitter(i);
		PE->setEnabled(state);
	}
}

// ����� ������� ��������
void CVirtualScript::particleSetEmitterSize(const String& particleName, int emitter_idx, Real size_x, Real size_y, Real size_z)
{
	Ogre::ParticleSystem* ps = app.getSceneManager()->getParticleSystem(particleName);
	if (ps && ps->getNumEmitters()>emitter_idx)
	{
		Ogre::ParticleEmitter* emitter = ps->getEmitter(emitter_idx);
		if (emitter && emitter->getType()=="Ring")
		{
			Ogre::RingEmitter *ring_emitter = (Ogre::RingEmitter *)emitter;
			ring_emitter->setSize(size_x, size_y, size_z);
		}
	}
}

void CVirtualScript::particleSetEmitterColor(const String& particleName, int emitter_idx, Real color)
{
	Ogre::ParticleSystem* ps = app.getSceneManager()->getParticleSystem(particleName);
	if (ps && ps->getNumEmitters() > emitter_idx)
	{
		Ogre::ParticleEmitter *emitter = ps->getEmitter(emitter_idx);
		if (emitter)
			emitter->setColour(Ogre::ColourValue(color,color,color));
	}
}

// ����� �������� �������� ���������� ������
void CVirtualScript::particleSetEmitterRate (const String& particleName, int emitter_idx, Real rate)
{
	Ogre::ParticleSystem* ps = app.getSceneManager()->getParticleSystem(particleName);
	if (ps && ps->getNumEmitters()>emitter_idx)
	{
		Ogre::ParticleEmitter *emitter = ps->getEmitter(emitter_idx);
		if (emitter)
			emitter->setEmissionRate(rate);
	}
}

void CVirtualScript::particleSetStartTime (const String& particleName, Real startTime)
{
	Ogre::ParticleSystem* ps = app.getSceneManager()->getParticleSystem(particleName);
	if (ps)
	{
		ps->clear();
		ps->fastForward (startTime, .1f);
	}
}

void CVirtualScript::particleClear(const String& particleName)
{
	Ogre::ParticleSystem* ps = app.getSceneManager()->getParticleSystem(particleName);
	ps->clear();
}


void CVirtualScript::setSubmeshVisible(OBJECT_HANDLE handle, const String& materialName, bool state)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	if (!pObj->pEntity)
	{
		toLogEx("ERROR: submeshSetVisible(): There is not entity on %s\n", pObj->pSNode->getName().c_str());
		return;
	}

	for(unsigned int i = 0; i < pObj->pEntity->getNumSubEntities(); i++)
	{
		Ogre::SubEntity* SubE = pObj->pEntity->getSubEntity(i);

		if (!SubE)
			return;

		if (SubE->getMaterialName() == materialName)
		{
			SubE->setVisible(state);
            //toLogEx("\n set SubE visible: %s %d", materialName.c_str(), state);
		}
	}
}

void CVirtualScript::setMaterial (OBJECT_HANDLE handle, int subEntity, const String& materialName)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	if (!pObj || !pObj->pSNode)
	{
		toLogEx("ERROR: setMaterial(): There is no object(0x%x) or no node(0x%x)\n", pObj, pObj ? NULL : pObj->pSNode);
		return;
	}

	Ogre::Entity* pEntity = pObj->pEntity;

	if (pEntity == 0)
	{
		toLogEx("ERROR: CVirtualScript::setMaterial(): There is not entity on %s (material: %s)\n", pObj->pSNode->getName().c_str(), materialName.c_str());
		return;
	}

	if (subEntity != -1)
	{
		Ogre::SubEntity* SubE = pEntity->getSubEntity(subEntity);
		if (SubE == 0)
		{
			toLogEx("ERROR: setMaterial(): Can not get subentity %d in node %s\n", subEntity, pObj->pSNode->getName().c_str());
			return;
		}
		SubE->setMaterialName(materialName);
	}
	else
	{
		// ���������� �������� ��� ���� ��������
		for(unsigned int i = 0; i < pEntity->getNumSubEntities(); i++)
		{
			Ogre::SubEntity* SubE = pEntity->getSubEntity(i);
			if (!SubE)
			{
				toLogEx("ERROR: setMaterial(): What are f...!! Can not get subentity %d in node %s\n", i, pObj->pSNode->getName().c_str());
				return;
			}
			SubE->setMaterialName(materialName);
		}
	}
}

void CVirtualScript::setAnimationBeginEnd(OBJECT_HANDLE handle, float begin, float end)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
		pObj->setAnimationBeginEnd(begin, end);
}

void CVirtualScript::setAnimationSpeed (OBJECT_HANDLE handle, int svalue)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj)
		pObj->setAnimSpeed((float)svalue/1000.0f);
}

void CVirtualScript::setAnimationSpeedPolynom(OBJECT_HANDLE handle, int coeff0, int coeff1, int coeff2, int coeff3)
{
	CGameObject* pObj = app._findObjectByHandle(handle);

	pObj->setAnimSpeedPolynom((float)coeff0/1000.0f, (float)coeff1/1000.0f, (float)coeff2/1000.0f, (float)coeff3/1000.0f);
}

void CVirtualScript::texturesUnloadLocRoom()
{

	const bool bLog = false;

	if (bLog)
		toLogEx("\n Unload Loc Room:");

	Ogre::MaterialManager::ResourceMapIterator matIter = Ogre::MaterialManager::getSingleton().getResourceIterator();
	while (matIter.hasMoreElements())
	{
		Ogre::MaterialPtr pMaterial = matIter.getNext().staticCast<Ogre::Material>();

		Ogre::Material::TechniqueIterator techIter = pMaterial->getTechniqueIterator();
		while (techIter.hasMoreElements())
		{
			Ogre::Technique *pTechniique = techIter.getNext();

			Ogre::Technique::PassIterator passIter = pTechniique->getPassIterator();

			// ������� ���-�� �����
			int nPassCount = 0;
			while (passIter.hasMoreElements())
			{
				passIter.getNext();
				nPassCount++;
			}

			if (nPassCount != 1)
				break;

			passIter = pTechniique->getPassIterator();

			while (passIter.hasMoreElements())
			{
				Ogre::Pass *pPass = passIter.getNext();
				//				pPass->
				Ogre::Pass::TextureUnitStateIterator textureUnitStateIter = pPass->getTextureUnitStateIterator();

				// ������� ���-�� ���������� ������
				int nTextureUnitCount = 0;
				while (textureUnitStateIter.hasMoreElements())
				{
					textureUnitStateIter.getNext();
					nTextureUnitCount++;
				}
				if (nTextureUnitCount != 1)
					break;

				textureUnitStateIter = pPass->getTextureUnitStateIterator();


				while (textureUnitStateIter.hasMoreElements())
				{
					Ogre::TextureUnitState *pTextureUnitState = textureUnitStateIter.getNext();
//					pTextureUnitState->get

					if (pTextureUnitState->getNumFrames() != 1)
						continue;

					Ogre::String strTexName = pTextureUnitState->getTextureName();

					if (strTexName.length() < 5)
						continue;

					if (bLog)
						toLogEx("\n     texture \"%s\"", strTexName.c_str());

					int iddLoc = atoi(strTexName.c_str());
					int iddRoom = atoi(strTexName.c_str() + 3);


					if (iddLoc >= 1 && iddLoc <= 5 && iddRoom >= 1 && iddRoom <= 31)
					{
						pTextureUnitState->setTextureName("00_white.dds");
						deleteTexture(strTexName);
						if (bLog)
							toLogEx(" unloaded");
					}
					else
					{
						if (bLog)
							toLogEx(" not unloaded");
					}

				}
			}
		}
	}
/*
	Ogre::TextureManager::ResourceMapIterator textureIter = Ogre::TextureManager::getSingleton().getResourceIterator();
	while(textureIter.hasMoreElements())
	{
		Ogre::TexturePtr pTexture = textureIter.getNext();

		//		if (!pTexture)
		//			continue;
		const String &strTexName = pTexture->getName();

		if (strTexName.length() < 5)
			continue;
		int iddLoc = atoi(strTexName.c_str());
		int iddRoom = atoi(strTexName.c_str() + 3);
		if (iddLoc >= 1 && iddLoc <= 5 && iddRoom >= 1 && iddRoom <= 31)
		{
			Ogre::TextureManager::getSingleton().remove(strTexName);
			Ogre::TextureManager::getSingleton().unload(strTexName);
		}

		//		bool loaded = pMaterial->isLoaded();
		//if (loaded)
		//pMaterial->unload()
	}
*/
//	Ogre::TextureManager::getSingleton().unloadUnreferencedResources(false);


	Ogre::TextureManager::getSingleton().removeUnreferencedResources(false);
}

bool CVirtualScript::texturesChangeToAtlas(const String &matName)
{
	return Ogre::CAtlasManager::getSingleton().changeMaterialTexturesToAtlas(matName);
}
bool CVirtualScript::texturesChangeToAtlas(const String &matName, const String &texName, bool bSingleState)
{
	return Ogre::CAtlasManager::getSingleton().changeMaterialTexturesToAtlas(matName, texName, bSingleState);
}

void CVirtualScript::loadTexture(const String& textureName)
{
	Ogre::ResourcePtr rp = Ogre::TextureManager::getSingleton().getByName(textureName);
	if (rp.isNull())
	{
		Ogre::TextureManager::getSingleton().load(textureName, "General");
	}
	rp = Ogre::TextureManager::getSingleton().getByName(textureName).staticCast<Ogre::Texture>();
}

void CVirtualScript::deleteTexture(const String& textureName)
{
	Ogre::ResourcePtr rp = Ogre::TextureManager::getSingleton().getByName(textureName);
	if (!rp.isNull())
	{
		Ogre::TextureManager::getSingleton().getByName(textureName).staticCast<Ogre::Texture>()->unload();
//		Ogre::TextureManager::getSingleton().remove(rp); //do no work
//		rp.setNull();

		//toLogEx("\n VirtualScript texture %s removed!", textureName.c_str());
	}
}

void CVirtualScript::materialDeleteTexture(const String& materialName)
{
	Ogre::MaterialPtr mat = (Ogre::MaterialPtr)Ogre::MaterialManager::getSingleton().getByName(materialName).staticCast<Ogre::Material>();

	Ogre::Technique*  technique = mat->getTechnique(0);
	if (technique == 0)
		return;

	Ogre::Technique::PassIterator passIter = technique->getPassIterator();
	while (passIter.hasMoreElements())
	{
		Ogre::Pass *pPass = passIter.getNext();
		Ogre::Pass::TextureUnitStateIterator textureUnitStateIter = pPass->getTextureUnitStateIterator();
		while (textureUnitStateIter.hasMoreElements())
		{
			Ogre::TextureUnitState *tu = textureUnitStateIter.getNext();
			String texture = tu->getTextureName();
//			toLogEx("\n try delete texture <%s>", texture.c_str());
			deleteTexture(texture);
		}
	}
}

void CVirtualScript::sceneDeleteTextures(const String& name)
{
	for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
		if (CGameObject::mGameObjList[i]->mSceneFileName == name)
		{
			Ogre::Entity *pEnpity = CGameObject::mGameObjList[i]->pEntity;
			if(pEnpity)
				for(unsigned int i = 0; i < pEnpity->getNumSubEntities(); i++)
				{
					Ogre::SubEntity* SubE = pEnpity->getSubEntity(i);
//					toLogEx("\n delete material texture <%s>", SubE->getMaterialName().c_str());
					materialDeleteTexture(SubE->getMaterialName());
				}
		}
}

void CVirtualScript::billboardSetSize(const String& name, Vector3 &size)
{
	Ogre::BillboardSet *bbset = app.getSceneManager()->getBillboardSet(name);
	if (bbset)
	{
		Ogre::Billboard *bb = bbset->getBillboard(0);
		if (bb)
			bb->setDimensions(size.x,size.y);
	}
}

void CVirtualScript::billboardVisible(const String& name, bool visible)
{
	if (app.getSceneManager()->hasBillboardSet(name))
	{
		Ogre::BillboardSet *bbset = app.getSceneManager()->getBillboardSet(name);
		if (bbset)
			bbset->setVisible(visible);
	}
	else
		toLogEx("ERROR: billboard '%s' not found in CVirtualScript::billboardVisible\n", name.c_str());
}


void CVirtualScript::loadCfgValue(const String& name, bool &save)
{
	//if (app.getAppState().getValue(name).saveToReg)
	//	app.getAppState().configureLoadValue(name);
	app.getAppState().configureLoad();

	save = app.getAppState().getValueBool(name);
}

void CVirtualScript::loadCfgValue(const String& name, int &save)
{
	//if (app.getAppState().getValue(name).saveToReg)
	//	app.getAppState().configureLoadValue(name);

	app.getAppState().configureLoad();

	save = app.getAppState().getValueInt(name);
}
void CVirtualScript::loadCfgValue(const String& name, String &save)
{
	app.getAppState().configureLoad();
	save = app.getAppState().getValueText(name);
}

bool CVirtualScript::getCfgB(const String& name)
{
	if (app.getAppState().hasValue(name))
		return app.getAppState().getValueBool(name);
	return false;
}

int CVirtualScript::getCfgI(const String& name)
{
	if (app.getAppState().hasValue(name))
		return app.getAppState().getValueInt(name);
	return 0;
}

void CVirtualScript::getCfgF(const String& name, float &value)
{
	if (app.getAppState().hasValue(name))
		value = app.getAppState().getValueFloat(name);
}

String CVirtualScript::getCfgS(const String& name)
{
	if (app.getAppState().hasValue(name))
		return app.getAppState().getValueText(name);
	return Ogre::EmptyString;
}

void CVirtualScript::setVisibleVariables()
{
	setVisibleVariablesR(app.getSceneManager()->getRootSceneNode());
}

void CVirtualScript::setVisibleVariablesR(Ogre::SceneNode *pRoot)
{
	Ogre::Node::ChildNodeIterator it = pRoot->getChildIterator();
	while(it.hasMoreElements())
	{
		//toLogEx("\n itNext");
		String key = it.peekNextKey();
		//toLogEx("\n setVisibleVariablesR: %s", key.c_str());
		Ogre::SceneNode *pNode = (Ogre::SceneNode *)it.peekNextValue();

		CGameObject* pObj = app._findObjectByName(key);
		if (pObj && pObj->pEntity && (strstr(pObj->mSceneFileName.c_str(),"dyn.scene") || strstr(pObj->mSceneFileName.c_str(),"az.scene") || strstr(pObj->mSceneFileName.c_str(),"dyn2.scene")))
		{
			String param_v = pNode->getName() + "@visible";
			String param_s = pNode->getName() + "@selectable";
			app.getAppState().mSave.curStage[param_v] = Ogre::StringConverter::toString((int)pObj->pEntity->getVisible());
			app.getAppState().mSave.curStage[param_s] = Ogre::StringConverter::toString((int)pObj->mIsSelectable);
			setVisibleVariablesR(pNode);
		}
		it.moveNext();
	}
}

void CVirtualScript::getVisibleVariables()
{
	getVisibleVariablesR(app.getSceneManager()->getRootSceneNode());
}

void CVirtualScript::getVisibleVariablesR(Ogre::SceneNode *pRoot)
{
	Ogre::Node::ChildNodeIterator it = pRoot->getChildIterator();
	while(it.hasMoreElements())
	{
		String key = it.peekNextKey();
		Ogre::SceneNode *pNode = (Ogre::SceneNode *)it.peekNextValue();
		CGameObject* pObj = app._findObjectByName(key);
		if (pObj && pObj->pEntity && (strstr(pObj->mSceneFileName.c_str(),"dyn.scene") || strstr(pObj->mSceneFileName.c_str(),"az.scene") || strstr(pObj->mSceneFileName.c_str(),"dyn2.scene")))
		{
			String param_v = pNode->getName() + "@visible";
			String param_s = pNode->getName() + "@selectable";
			StageIterator iter = app.getAppState().mSave.curStage.find(param_v);
			if (iter != app.getAppState().mSave.curStage.end())
			{
				String v = iter->second;
				int vi = Ogre::StringConverter::parseInt(v);
				pObj->pEntity->setVisible(vi!=FALSE);
			}
			else
			{
				toLogEx("Warning: Visible of %s is unknown\n", pNode->getName().c_str());
			}

			iter = app.getAppState().mSave.curStage.find(param_s);
			if (iter != app.getAppState().mSave.curStage.end())
			{
				String s = iter->second;
				int si = Ogre::StringConverter::parseInt(s);
				pObj->mIsSelectable = si!=FALSE;
			}
			else
			{
				toLogEx("Warning: Visible of %s is unknown\n", pNode->getName().c_str());
			}
			getVisibleVariablesR(pNode);
		}
		it.moveNext();
	}
}

// from emerald
void CVirtualScript::clearVars(int*var, int &game_end, const int count)
{
	for(int i = 0; i < count; i++)
		var[i] = 0;
	game_end = 0;
}

String CVirtualScript::getVarName(const int loc, const int room, const int number)
{
	if(number == -1)
		return Ogre::StringConverter::toString(loc)+"_"+Ogre::StringConverter::toString(room)+"_variable_game_end";
	else
		return Ogre::StringConverter::toString(loc)+"_"+Ogre::StringConverter::toString(room)+"_variable_"+Ogre::StringConverter::toString(number);
}

int CVirtualScript::existVars(int loc, int game)
{
	return app.getAppState().hasValue(getVarName(loc, game, -1)) ? 1 : 0;
}

void CVirtualScript::loadVars(const int loc, const int room, int*var, int &game_end, const int count)
{
	for(int i = 0; i < count; i++)
	{
		const String& name = getVarName(loc, room, i);
		if (app.getAppState().hasValue(name))
			var[i] = app.getAppState().getValueInt(name);
		else
			var[i] = 0;
	}
	game_end = app.getAppState().getValueInt(getVarName(loc, room, -1));
}

void CVirtualScript::saveVars(const int loc, const int room, int*var, int &game_end, const int count)
{
	for(int i = 0; i < count; i++)
		app.getAppState().setValue(getVarName(loc, room, i), var[i]);
	app.getAppState().setValue(getVarName(loc, room, -1), game_end);
}

// 3� ���������� �������� ������������ ������
void CVirtualScript::get2dCoordFrom3d(const Vector3& vec3d, Vector2& vec2d)
{
	if (app.getCamera())
	{
		Vector3 hcsPosition = app.getCamera()->getProjectionMatrix() * vec3d;
		vec2d.x = ((hcsPosition.x * 0.5f) + 0.5f);// 0 <= x <= 1 // left := 0,right := 1
		vec2d.y = 1.0f - ((hcsPosition.y * 0.5f) + 0.5f);// 0 <= y <= 1 // bottom := 0,top := 1
	}
	else
		toLogEx("ERROR: null in get2dCoordFrom3d\n");
}
// 3� ���������� �������� � ���������� �����������
void CVirtualScript::get2dCoordFrom3dAbsolute(const Vector3& vec3d, Vector2& vec2d)
{
	if (app.getCamera())
	{
		Vector3 hcsPosition = app.getCamera()->getProjectionMatrix() * app.getCamera()->getViewMatrix() * vec3d;
		vec2d.x = ((hcsPosition.x * 0.5f) + 0.5f);// 0 <= x <= 1 // left := 0,right := 1
		vec2d.y = 1.0f - ((hcsPosition.y * 0.5f) + 0.5f);// 0 <= y <= 1 // bottom := 0,top := 1
	}
	else
		toLogEx("ERROR: null in get2dCoordFrom3d\n");
}

// ������� ����������� �� �������� ��������� � ��������������� (�� ��������� ��������� �� ������ �� ������� Z)
void CVirtualScript::get3dCoordFrom2d(const Vector2& vec2d, float z, Vector3& vec3d)
{
	if (app.getViewport() && app.getViewport()->getCamera())
	{
		const Ogre::Matrix4& projection = app.getViewport()->getCamera()->getProjectionMatrix();

		Vector3 v1(0, 0, -z);
		Vector3 v2 = projection * v1;

		Real x = vec2d.x*2;
		Real y = vec2d.y*2;
#if OGRE_NO_VIEWPORT_ORIENTATIONMODE == 0
		switch(app.getViewport()->getCamera()->getOrientationMode())
		{
			case Ogre::OR_PORTRAIT:
				v2.x = x - 1;
				v2.y = 1 - y;
				break;
			case Ogre::OR_DEGREE_180:
				v2.x = 1 - x;
				v2.y = y - 1;
				break;
			case Ogre::OR_LANDSCAPELEFT:
				v2.x = 1 - y;
				v2.y = 1 - x;
				break;
			case Ogre::OR_LANDSCAPERIGHT:
				v2.x = y - 1;
				v2.y = x - 1;
				break;
		}
#else
		v2.x = x - 1;
		v2.y = 1 - y;
#endif
		vec3d = projection.inverse() * v2;
	}
	else
		toLogEx("ERROR: null in get3dCoordFrom2d\n");
}

Ogre::Ray CVirtualScript::getViewportRay(float x, float y)
{
	return app.getViewport()->getCamera()->getCameraToViewportRay(x,y);
}

bool CVirtualScript::objectRaycast(const Vector2 &vecScr, OBJECT_HANDLE hObject, Vector3 &vecResult)
{
	CGameObject *pGameObject = app._findObjectByHandle(hObject);
	if (pGameObject == NULL)
		return false;

	Ogre::Ray ray = getViewportRay(vecScr.x, vecScr.y);

	return app.polygonRaycast(ray, pGameObject, vecResult);
}

void CVirtualScript::get3dCoordFrom2dAbsolute(const Vector3& vec2d, Vector3& vec3d)
{
	Ogre::Matrix4 inverseVP = (app.getViewport()->getCamera()->getProjectionMatrix() * app.getViewport()->getCamera()->getViewMatrix(true)).inverse();

	Vector3 nearPoint(0, 0, 1.f);
	Vector3 midPoint (0, 0, 0);

	Vector3 rayOrigin, rayTarget;

	rayOrigin = inverseVP * nearPoint;
	rayTarget = inverseVP * midPoint;

	Vector3 rayDirection = rayOrigin - rayTarget;
	rayDirection.normalise();
	Ogre::Node*n = app.getViewport()->getCamera()->getParentNode();
	if (n)
	{
		Vector3 camPos = n->_getDerivedPosition();
		Real camLen = rayDirection.dotProduct(camPos);

		Ogre::Plane plane(rayDirection, vec2d.z + camLen);
		Ogre::Ray ray = app.getViewport()->getCamera()->getCameraToViewportRay(vec2d.x, vec2d.y);

		std::pair<bool, Real> pair = Ogre::Math::intersects(ray, plane);
		vec3d = ray.getPoint(pair.second);
	}
	else
	{
		vec3d.x = 0;
		vec3d.y = 0;
		toLogEx("ERROR: No parent node of camera\n");
	}
}

void CVirtualScript::billboardSetPos(const String& name, Vector3 &pos)
{
	Ogre::BillboardSet *bbset = app.getSceneManager()->getBillboardSet(name);
	if (bbset)
	{
		Ogre::Billboard *bb = bbset->getBillboard(0);
		if (bb)
			bb->mPosition = pos;
	}
}
void CVirtualScript::billboardSetRot(const String& name, float angle)
{
	Ogre::BillboardSet *bbset = app.getSceneManager()->getBillboardSet(name);
	if (bbset)
	{
		Ogre::Billboard *bb = bbset->getBillboard(0);
		if (bb)
			bb->setRotation(Ogre::Degree(angle));
	}
}

void CVirtualScript::billboardGetSize(const String& name, Vector3 &size)
{
	Ogre::BillboardSet *bbset = app.getSceneManager()->getBillboardSet(name);
	Ogre::Billboard *bb = bbset->getBillboard(0);
	size.x = bb->getOwnWidth();
	size.y = bb->getOwnHeight();
}

void CVirtualScript::billboardGetPos(const String& name, Vector3 &pos)
{
	Ogre::BillboardSet *bbset = app.getSceneManager()->getBillboardSet(name);
	Ogre::Billboard *bb = bbset->getBillboard(0);
	pos = bb->mPosition;
}

void CVirtualScript::billboardSetAlpha(const String& name, Real alpha)
{
	Ogre::BillboardSet *bbset = app.getSceneManager()->getBillboardSet(name);

	Ogre::MaterialPtr mMaterial = bbset->getMaterial();

	Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
	if (mTechnique == 0)
		return;

	Ogre::Pass*  mPass = mTechnique->getPass(0);
	if (mPass == 0)
		return;

	Ogre::TextureUnitState* mTUState = mPass->getTextureUnitState(0);
	if (mTUState == 0)
		return;

	mTUState->setAlphaOperation (Ogre::LBX_MODULATE, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, 0.0, alpha, 0.0);
}

void CVirtualScript::billboardSetColor(const String& name, int index, float r, float g, float b, float a)
{
	Ogre::BillboardSet *bbset = app.getSceneManager()->getBillboardSet(name);
	Ogre::Billboard *pBillboard = bbset->getBillboard(index);
	if (!pBillboard)
		return;

	pBillboard->setColour(Ogre::ColourValue(r, g, b, a));
}

bool CVirtualScript::isShowCursor()
{
	return app.isShowCursor();
}

void CVirtualScript::showCursor(bool fshow)
{
	app.showCursor(fshow);
	app.updateCursor();
	//FIXME: �� �������, ����� ��� ��� ���
}

void CVirtualScript::setCursor(int id)
{
	app.setCursorCurrentId(id);
	app.updateCursor();
}
void CVirtualScript::setCursorStandard(bool state)
{
	app.setCursorStandard(state);
}
void CVirtualScript::setCursorUseTextures(bool state)
{
	app.setCursorUseTextures(state);
}

//-------------------------------------
void CVirtualScript::sendCMsgNow(const String& name, const CMsg& m)
{
    OBJECT_HANDLE hObject = app.findObjectByName(name);
    if (hObject)
        sendCMsgNowHandle(hObject, m);
}

void CVirtualScript::sendCMsgNowHandle(OBJECT_HANDLE h, const CMsg& msg)
{
	::CScriptMsg m;
	m.pTargetObj = app._findObjectByHandle(h);
	m.param.lparam = msg.lparam;
	m.param.rparam = msg.rparam;
	m.messageId =  CScriptMsg::MSG_NOTIFY;
	m.pSourceObj = app._findObjectByHandle(msg.sender);
	m.pTargetObj->ProcessScriptMsg(&m);
}

void CVirtualScript::sendCMsg(const String& name, const CMsg& m)
{
	sendCMsgHandle(app.findObjectByName(name), m);
}

void CVirtualScript::sendCMsgHandle(OBJECT_HANDLE h, const CMsg& msg)
{
	::CScriptMsg m;
	m.pTargetObj = app._findObjectByHandle(h);
	m.param.lparam = msg.lparam;
	m.param.rparam = msg.rparam;
	m.messageId =  CScriptMsg::MSG_NOTIFY;
	m.pSourceObj = app._findObjectByHandle(msg.sender);

	if (m.pTargetObj && m.pTargetObj->pSNode && m.pTargetObj->pSNode->getName() == "00_cam_move")
		m.pTargetObj->ProcessScriptMsg(&m);
	else
		app.postMsg(m);
}

void CVirtualScript::answerCMsg(const CMsg& m, OBJECT_HANDLE sender)
{
	if (m.sender)
		sendCMsgHandle(m.sender, CMsg(m.lparam, m.rparam, sender));
}

void CVirtualScript::answerCMsgNow(const CMsg& m, OBJECT_HANDLE sender)
{
	if (m.sender)
		sendCMsgNowHandle(m.sender, CMsg(m.lparam, m.rparam, sender));
}

int CVirtualScript::getPostfixInt(OBJECT_HANDLE h, const String& prefix)
{
	CGameObject* pObj = app._findObjectByHandle(h);
	String name = pObj->pSNode->getName();
	return atoi(name.c_str() + prefix.length());
}

void CVirtualScript::_getPrefixInt(String& name, int *i1, int *i2)
{
	if (i1)
	{
		*i1 = 0;
	}

	if (i2)
	{
		*i2 = 0;
	}

	char num[3];
	num[2]='\0';

	if (i1)
	{
		if (name.length() > 1)
		{
			num[0] = name[0];
			num[1] = name[1];
			*i1 = atoi(num);
		}
	}
	//NN_MM_
	if (i2)
	{
		if (name.length() > 5)
		{
			num[0] = name[3];
			num[1] = name[4];
			*i2 = atoi(num);
		}
	}
}

void CVirtualScript::getPrefixInt(OBJECT_HANDLE h, int *i1, int *i2)
{
	CGameObject* pObj = app._findObjectByHandle(h);
	if (pObj)
	{
		String name = pObj->pSNode->getName();
		_getPrefixInt(name, i1, i2);
	}
}

float CVirtualScript::getDepth(OBJECT_HANDLE handle)
{
	CGameObject* pObj = app._findObjectByHandle(handle);
	if (pObj && pObj->pSNode)
		return Ogre::Math::Sqrt(pObj->pSNode->getSquaredViewDepth(app.getCamera()));
	return 0;
}

void CVirtualScript::updateFocusedObj()
{
	app.updateFocusedObj();
}

OBJECT_HANDLE CVirtualScript::getFocusedObj()
{
	const CGameObject *pGameObject = app.getFocusedObj();

	return (pGameObject ? pGameObject->getHandle() : 0);
}

void CVirtualScript::updateInventory ()
{
	CScriptMsg m;
	sprintf(m.targetName,"01_inventory");
	m.pTargetObj = NULL;
	m.param.lparam = 0;
	m.messageId = CScriptMsg::MSG_NOTIFY;
	app.postMsg(m);
}

void CVirtualScript::sqrt(float *res)
{
	*res = Ogre::Math::Sqrt(*res);
}

void CVirtualScript::sin(Vector3& xyz)
{
	xyz.y = Ogre::Math::Sin(xyz.x);
	xyz.z = Ogre::Math::Cos(xyz.x);
}

void CVirtualScript::acos(float *res)
{
	*res = Ogre::Math::ACos(*res).valueDegrees();
}

int CVirtualScript::rand ()
{
	return (int)(Ogre::Math::RangeRandom(0,1e+6));
}

bool CVirtualScript::keyGetStatus(int key)
{
	if (app.getOIS())
		return key < 256 ? app.getOIS()->mKeys[key] : false;
	else
		toLogEx("ERROR: keyGetStatus - OIS not initialize\n");
	return false;
}

int CVirtualScript::getSS(const String& name, String& value)
{
	StageIterator iter = app.getAppState().mSave.curStage.find(name);
	if (iter != app.getAppState().mSave.curStage.end())
		value = iter->second;
	else
		return 0;
	return 1;
}

const String& CVirtualScript::getSS(const String& name)
{
	StageIteratorConst iter = app.getAppState().mSave.curStage.find(name);
	if(iter != app.getAppState().mSave.curStage.end())
		return iter->second;
	return Ogre::EmptyString;
}

int CVirtualScript::keyExist(const String& str)
{
	std::map<String,String>::iterator iter = app.getAppState().mSave.curStage.find(str);
	if (iter != app.getAppState().mSave.curStage.end())
		return 1;
	else
		return 0;
}

bool CVirtualScript::getSB(const String& name)
{
	std::map<String,String>::iterator iter = app.getAppState().mSave.curStage.find(name);
	if (iter != app.getAppState().mSave.curStage.end())
	{
		if (Ogre::StringConverter::parseInt(iter->second) == 0)
			return false;
		else
			return true;
	}
	else
		return false;
}

int CVirtualScript::getSI(const String& name)
{
	std::map<String,String>::iterator iter = app.getAppState().mSave.curStage.find(name);
	if (iter != app.getAppState().mSave.curStage.end())
		return Ogre::StringConverter::parseInt(iter->second);
	else
		return 0;
}

float CVirtualScript::getSF(const String& name)
{
	std::map<String,String>::iterator iter = app.getAppState().mSave.curStage.find(name);
	if (iter != app.getAppState().mSave.curStage.end())
		return Ogre::StringConverter::parseReal(iter->second);
	else
		return 0;
}

void CVirtualScript::setSS(const String& name, const String& value)
{
	app.getAppState().mSave.curStage[name] = value;
}

void CVirtualScript::setSB(const String& name, bool value)
{
	String a;
	if (value)
		a = "1";
	else
		a = "0";
	app.getAppState().mSave.curStage[name] = a;
}

void CVirtualScript::setSI(const String& name, int value)
{
	String a = Ogre::StringConverter::toString(value);
	app.getAppState().mSave.curStage[name] = a;
}

void CVirtualScript::setSF(const String& name, float value)
{
	String a = Ogre::StringConverter::toString(value);
	app.getAppState().mSave.curStage[name] = a;
}

void CVirtualScript::variableSave(const String&  name)
{
	app.getAppState().variableSave(name);
}

int CVirtualScript::variableLoad(const String&  name)
{
	return app.getAppState().variableLoad(name);
}

void CVirtualScript::variableAddStage(size_t number, const String& name)
{
	size_t sz = app.getAppState().mSave.stage.size();
	while (app.getAppState().mSave.stage.size() < number + 1)
	{
		Stage stage;
		stage["stage_name"] = "empty";
		app.getAppState().mSave.stage.push_back(stage);
	}
	app.getAppState().mSave.stage[number] = app.getAppState().mSave.curStage;
	app.getAppState().mSave.stage[number]["stage_name"] = name;

}
int CVirtualScript::variableGetNum()
{
	return app.getAppState().mSave.stage.size();
}

int CVirtualScript::variableGetStageName(int i)
{
	return Ogre::StringConverter::parseInt(app.getAppState().mSave.stage[i]["stage_name"]);
}

void CVirtualScript::variableSelectStage(int i)
{
	app.getAppState().mSave.curStage = app.getAppState().mSave.stage[i];
}

void CVirtualScript::initGame()
{
}

bool CVirtualScript::getSaveStatus(const String& filename)
{
	return app.getAppState().getMapStatus(filename);
}

String CVirtualScript::getNameCurrent()
{
	return app.getAppState().mSave.name;
}

int CVirtualScript::getNameFromSave(const String& file, String& name)
{
	return app.getAppState().getNameFromSave(file, name);
}

void CVirtualScript::setNameCurrent(const String& name)
{
	app.getAppState().mSave.name = name;
}

int CVirtualScript::setNameToSave(const String& file, const String& new_name)
{
	return app.getAppState().setNameToSave(file, new_name);
}

void CVirtualScript::deleteSave(const String& name)
{
	app.getAppState().deleteVariableFile(name);
}

void CVirtualScript::variableInit(const String& name)
{
	app.getAppState().variableInit(name);
}

int CVirtualScript::getII(int num)
{
	String nums = Ogre::StringConverter::toString(num);
	return Ogre::StringConverter::parseInt(app.getAppState().mSave.curStage[nums]);
}

void CVirtualScript::setII(int num, int value)
{
	String nums = Ogre::StringConverter::toString(num);
	app.getAppState().mSave.curStage[nums] = Ogre::StringConverter::toString(value);
}

void CVirtualScript::createRTT(const String& text_name, const String& camera, int sx, int sy, int params, unsigned int visible_mask)
{
    toLogEx("\n create rtt: %s %d/%d", text_name.c_str(), sx, sy);
	createRTT2(text_name, camera, sx, sy, params, visible_mask, 0, 0, 1, 1);
}

int rnd_2_pwr2(int n)
{
	int pwr2 = 1;
	for(; pwr2 <= n; pwr2 <<= 1);
	return pwr2 >> 1;
}

void CVirtualScript::createRTT2(
	const String& text_name,
	const String& camera,
	int sx, int sy,
	int params,
	unsigned int visible_mask,
	float left,
	float top,
	float width,
	float height)
{
	_createRTT(text_name, camera, sx, sy, params, visible_mask, left, top, width, height, 24);
}

void CVirtualScript::_createRTT(
	const String& text_name,
	const String& camera,
	int sx, int sy,
	int params,
	unsigned int visible_mask,
	float left,
	float top,
	float width,
	float height, int bpp)
{
	if (!Ogre::Root::getSingleton().getRenderSystem()->getCapabilities()->hasCapability(Ogre::RSC_NON_POWER_OF_2_TEXTURES))
	{
		sx = rnd_2_pwr2(sx);
		sy = rnd_2_pwr2(sy);
	}

	Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(text_name, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME).staticCast<Ogre::Texture>();
	if (tex.isNull())
	{
		Ogre::PixelFormat pf;
		switch(bpp)
		{
/*		case 16:
			pf = Ogre::PF_L8;
			break;*/
		case 24:
			pf = Ogre::PF_R8G8B8;
			break;
		case 32:
			pf = Ogre::PF_R8G8B8A8;
			break;
		default:
			toLogEx("ERROR: _createRTT()  unabled bpp parameter (bpp = %d)", bpp);
			return;
		}
		tex = Ogre::TextureManager::getSingleton().createManual(
			text_name,
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			Ogre::TEX_TYPE_2D,
			sx, sy,
			0,
			pf,
			Ogre::TU_RENDERTARGET );

		Ogre::RenderTarget* rtt = tex->getBuffer()->getRenderTarget();
		if (rtt)
		{
		    toLogEx("\n create rtt: %s %dx%d", text_name.c_str(), sx, sy);

			Ogre::Camera* pcamera = app.getSceneManager()->getCamera(camera);
			if (pcamera)
			{
				Ogre::Viewport *v = rtt->addViewport(pcamera);
				if (v)
				{
					v->setBackgroundColour(Ogre::ColourValue(0.f, 0.f, 0.f, 0.f));
					v->setOverlaysEnabled((params&0x02)!=0);
					v->setVisibilityMask(visible_mask);
				}
				rtt->setAutoUpdated((params & 0x01));
			}
		}
	}
	else
		toLogEx("ERROR: Can't create RTT %s, texture name already in use\n", text_name.c_str());
}

void CVirtualScript::updateRTT(const String& text_name)
{
	Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(text_name).staticCast<Ogre::Texture>();
	if (!tex.isNull())
	{
		if (!tex->getBuffer().isNull())
		{
			Ogre::RenderTarget* rtt = tex->getBuffer()->getRenderTarget();
			if (rtt)
				rtt->update();
		}
	}
	else
		toLogEx("ERROR: RTT %s not found\n", text_name.c_str());
}

void CVirtualScript::setRTTOverlayEnable(const String& text_name, bool enable)
{
	Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(text_name).staticCast<Ogre::Texture>();
	if(!tex.isNull() && !tex->getBuffer().isNull())
	{
		Ogre::RenderTarget* rtt = tex->getBuffer()->getRenderTarget();
		if (rtt && rtt->getNumViewports()>0)
			rtt->getViewport(0)->setOverlaysEnabled(enable);
	}
	else
		toLogEx("ERROR: RTT %s not found\n", text_name.c_str());
}

void CVirtualScript::setRTTDimension(const String& text_name, float left, float top, float width, float height)
{
	Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(text_name).staticCast<Ogre::Texture>();
	if (!tex.isNull() && !tex->getBuffer().isNull())
	{
		Ogre::RenderTarget* rtt = tex->getBuffer()->getRenderTarget();
		if (rtt && rtt->getNumViewports()>0)
			rtt->getViewport(0)->setDimensions(left, top, width, height);
	}
	else
		toLogEx("ERROR: RTT %s not found\n", text_name.c_str());
}

bool CVirtualScript::cheats()
{
	return app.cheatsEnabled();
}

void CVirtualScript::setCheats(bool value)
{
	return app.enableCheats(value);
}

void CVirtualScript::unlinkFile(const String& file_name)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	SetFileAttributes(app.getAppState().getFullName(file_name).c_str(), 0);
#else
#endif
	unlink(app.getAppState().getFullName(file_name).c_str());
}

void CVirtualScript::saveScreenShot(const String& file_name, const String& texture_name, int visible_mask, bool overlay_enable)
{
	Ogre::TexturePtr texturePtr = Ogre::TextureManager::getSingleton().getByName(texture_name).staticCast<Ogre::Texture>();


	Ogre::RenderTarget* rtt = texturePtr->getBuffer()->getRenderTarget();
	if (rtt)
	{
		rtt->removeAllViewports();
		Ogre::Viewport* v = rtt->addViewport(app.getCamera(), 0, 0, 0, 1.0f, 0.75f);
		v->setOverlaysEnabled(overlay_enable);
		v->setVisibilityMask(visible_mask);

		rtt->update();
		app.setScreenshot(true);
		if (app.getRoot())
			app.getRoot()->renderOneFrame();

		rtt->removeAllViewports();
		v = rtt->addViewport(app.getCamera(), 0, 0, 0, 1.0f, 1.0f);
		v->setOverlaysEnabled(false);
		rtt->writeContentsToFile(app.getAppState().getFullName(file_name));
	}
}



void CVirtualScript::saveScreenShot2(const String &file_name, const String &texture_name, int visible_mask, bool overlay_enable)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
//	app.mOgreOISListener->SetCursorStandartArrow(true);
	app.getOIS()->updateCursorUseTextures(false);
#endif

	Ogre::TexturePtr texturePtr = Ogre::TextureManager::getSingleton().getByName(texture_name).staticCast<Ogre::Texture>();

	if(texturePtr.isNull())
	{
		toLogEx("\n writeContentsToFile ERROR");
		return;
	}

	Ogre::RenderTarget* rtt = texturePtr->getBuffer()->getRenderTarget();
	rtt->getViewport(0)->setOverlaysEnabled(overlay_enable);
	rtt->getViewport(0)->setVisibilityMask(visible_mask);
	rtt->update();

	app.setScreenshot(true);
	app.getWindow()->setAutoUpdated(false);
	app.getRoot()->renderOneFrame();
	app.getWindow()->setAutoUpdated(true);

//	toLogEx("\n rtt begin write contents to file");
	rtt->writeContentsToFile(app.getAppState().getFullName(file_name));
//	toLogEx("\n rtt write contents to file done");

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
//	app.mOgreOISListener->SetCursorStandartArrow(app.mOgreOISListener->curStandartArrow);
	app.getOIS()->updateCursorUseTextures(true);
#endif

	toLogEx("\n writeContentsToFile %s", app.getAppState().getFullName(file_name).c_str());
}

void CVirtualScript::saveRTTtoFile(const String& texture_name, const String& file_name)
{
	Ogre::TexturePtr texturePtr = Ogre::TextureManager::getSingleton().getByName(texture_name).staticCast<Ogre::Texture>();

	if (texturePtr.isNull())
		return;

	Ogre::RenderTarget* rtt = texturePtr->getBuffer()->getRenderTarget();

	if (rtt)
	{
		rtt->writeContentsToFile(app.getAppState().getFullName(file_name));
	}
}

void CVirtualScript::setViewportOverlayEnable(bool enable)
{
	app.getViewport()->setOverlaysEnabled(enable);
}

void CVirtualScript::setRTTAutoUpdate(const String& text_name, bool fenable)
{
	Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(text_name).staticCast<Ogre::Texture>();
	if (!tex.isNull())
	{
		if (!tex->getBuffer().isNull())
		{
			Ogre::RenderTarget* rtt = tex->getBuffer()->getRenderTarget();
			if (rtt)
				rtt->setAutoUpdated(fenable);
		}
	}
	else
		toLogEx("ERROR: RTT %s not found\n", text_name.c_str());
}

void CVirtualScript::setRTTVisibleMask(const String& text_name, int param)
{
	Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(text_name).staticCast<Ogre::Texture>();
	if (!tex.isNull())
	{
		if (!tex->getBuffer().isNull() && tex->getBuffer()->getRenderTarget())
		{
			Ogre::Viewport *v = tex->getBuffer()->getRenderTarget()->getViewport(0);
			if (v)
				v->setVisibilityMask(param);
		}
	}
	else
		toLogEx("ERROR: RTT %s not found\n", text_name.c_str());
}

int CVirtualScript::loadSound(const char* sound_id)
{
#ifndef NO_SFX
	return sfxCore.loadSound(sound_id, false);
#else
	return -1;
#endif
}

void CVirtualScript::playSound(int id, int group, bool looped, SFX_OBJECT_HANDLE notifyObj)
{
#ifndef NO_SFX
	sfxCore.playSound(id, group, looped, notifyObj);
#endif
}

void CVirtualScript::stopSound(int id)
{
#ifndef NO_SFX
	sfxCore.stopSound(id);
#endif
}

void CVirtualScript::freeSound(int id)
{
#ifndef NO_SFX
	sfxCore.freeSound(id);
#endif
}

unsigned CVirtualScript::getSoundDuration(int id)
{
#ifndef NO_SFX
	return sfxCore.getDuration(id);
#else
	return 1;
#endif
}

int CVirtualScript::getVolume(int id)
{
#ifndef NO_SFX
	return sfxCore.getVolume(id);
#endif
}

bool CVirtualScript::isPlaying(int id)
{
#ifndef NO_SFX
	return sfxCore.isPlaying(id);
#endif
}

void CVirtualScript::setVolume(int id, int volume)
{
#ifndef NO_SFX
	sfxCore.setVolume(id, volume);
#endif
}

void CVirtualScript::setEnvLevel(float level)
{
#ifndef NO_SFX
	sfxCore.setEnvLevel(level);
#endif
}

void CVirtualScript::addEnvSound(int id, bool looped, SFX_OBJECT_HANDLE notifyObj)
{
#ifndef NO_SFX
	sfxCore.addEnvSound(id, looped, notifyObj);
#endif
}
void CVirtualScript::setEnvFadeSpeed(float speed)
{
#ifndef NO_SFX
	sfxCore.setEnvFadeSpeed(speed);
#endif
}

void CVirtualScript::setGroupVolume(int group, int volume)
{
#ifndef NO_SFX
	sfxCore.setGroupVolume(group, volume);
#endif
}

void CVirtualScript::loadEnvSounds()
{
#ifndef NO_SFX
	sfxCore.loadEnvSounds();
#endif
}

OBJECT_HANDLE CVirtualScript::findObjectByName(const String& name)
{
	OBJECT_HANDLE obj = app.findObjectByName(name);
	if (!obj)
		toLogEx("ERROR: Can't find object [%s] by name!\n", name.c_str());
	return obj;
}

OBJECT_HANDLE CVirtualScript::findObjectByNameIf(const String& name)
{
	return app.findObjectByName(name);
}

int CVirtualScript::getFrameTimeMS()
{
	return app.getFrameTime();
}
float CVirtualScript::getFrameTimeS()
{
	return app.getFrameTime()/1000.f;
}

int CVirtualScript::getTimeMS()
{
	return app.getTime();
}

Ogre::ulong CVirtualScript::getSystemTimeMS()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	return timeGetTime();
#else
	return 0;
#endif

}

void CVirtualScript::setSelectableMask(unsigned long mask)
{
	app.setQueryMask(mask);
}

void CVirtualScript::deleteScene()
{
	app.deleteScene();
}

void CVirtualScript::loadScene(const String& sceneName, bool useLoader)
{
	app.loadScene(sceneName,useLoader);
}

void CVirtualScript::setViewCamera(const String& name)
{
	app.setViewCamera(name);
}

void CVirtualScript::setCameraAspect(const String& name, float aspect)
{
	if(name.empty())
	{
		Ogre::Camera *pCamera = app.getCamera();
		if (pCamera)
		{
			pCamera->setAspectRatio(aspect);
		}
	}
	else
	{
		toLogEx("\nERROR: setCameraAspect for non-empty name not implemented!");
	}

}

OBJECT_HANDLE CVirtualScript::getSourceHandle(const CScriptMsg * pMsg)
{
	if(pMsg->pSourceObj)
		return pMsg->pSourceObj->mHandle;
	else
		return 0;
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
void CVirtualScript::setGameIcon(WORD IDR_MAINFRAME)
{


	size_t windowHnd = 0;
	app.getWindow()->getCustomAttribute("WINDOW", &windowHnd);
	HICON hIcon = ::LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDR_MAINFRAME));
	if (hIcon)
	{
		::SendMessage((HWND)windowHnd, WM_SETICON, 1, (LPARAM)hIcon);
		::SendMessage((HWND)windowHnd, WM_SETICON, 0, (LPARAM)hIcon);
	}

}
#else
void CVirtualScript::setGameIcon(unsigned short s) {}
#endif

String CVirtualScript::getResourceAsString(const String &filename)
{
	if(!Ogre::ResourceGroupManager::getSingleton().resourceExistsInAnyGroup(filename))
	{
		toLogEx("\nERROR: File %s doesn't exist!", filename.c_str());
		return "";
	}
	Ogre::DataStreamPtr pStream = Ogre::ResourceGroupManager::getSingleton().openResource(filename);
	return pStream->getAsString();
}
String CVirtualScript::getResourceAsString(const String &filename, const String &group)
{
	if(!Ogre::ResourceGroupManager::getSingleton().resourceExists(group, filename))
	{
		toLogEx("\nERROR: File %s doesn't exist!", filename.c_str());
		return "";
	}
	Ogre::DataStreamPtr pStream = Ogre::ResourceGroupManager::getSingleton().openResource(filename, group);
	return pStream->getAsString();
}

void CVirtualScript::initialiseResourceGroup(const String &resourceGroup)
{
    Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup(resourceGroup);
}

void CVirtualScript::setQueryMask(unsigned long mask)
{
	 app.setQueryMask(mask);
}

#if USING_THEORA

#if !defined(NEW_THEORA_LIB)
class MyVideoManager : public Ogre::OgreVideoManager
{
public:
	const ClipList& getClipList() { return mClips; }

	int getNumReadyFrames() { if(getClipList().size() > 0) return getClipList()[0]->getNumReadyFrames(); else return -1;}

	//TODO: remove it!
	//Ogre::TexturePtr getTexture() { return mTextures.begin()->second; }
};

#endif



float CVirtualScript::startVideoClip(const String &materialName, const String &videoName)
{
	Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(videoName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME).staticCast<Ogre::Texture>();
	toLogEx("\nstartVideoClip: texture %d", !tex.isNull());

	if(!app.getVideoManager()->getVideoClipByName(videoName))
	{
		toLogEx("\nstartVideoClip: creating, materialName: %s videoName: %s", materialName.c_str(), videoName.c_str());

		app.getVideoManager()->setInputName(videoName);

		toLogEx("\nstartVideoClip: setInputName");

		app.getVideoManager()->createDefinedTexture(materialName);

		//MyVideoManager *m = (MyVideoManager *)app.getVideoManager();

		//size_t w = m->getTexture()->getWidth();
		//size_t h = m->getTexture()->getHeight();
		//String s = m->getTexture()->getName();

		//toLogEx("\n Video texture size %ux%u Name:%s", w, h, s.c_str());
	}
	else
	{
		toLogEx("\n startVideoClip, exists, materialName: %s videoName: %s", materialName.c_str(), videoName.c_str());
	}

#if !defined(NEW_THEORA_LIB)
		app.getVideoManager()->getVideoClipByName(videoName)->play();
		return app.getVideoManager()->getVideoClipByName(videoName)->getDuration();
#else
		app.getVideoManager()->play(videoName);
		return app.getVideoManager()->getDuration(videoName);
#endif

	 
}


void CVirtualScript::stopVideoClip(const String &videoName)
{
	if(app.getVideoManager()->getVideoClipByName(videoName))
	{
#if !defined(NEW_THEORA_LIB)
		app.getVideoManager()->getVideoClipByName(videoName)->stop();
#else
		app.getVideoManager()->stop(videoName);
#endif


	}
}

void CVirtualScript::destroyVideoClips()
{
	//????
}

int CVirtualScript::getNumReadyFrames()
{
#if !defined(NEW_THEORA_LIB)
		MyVideoManager *m = (MyVideoManager *)app.getVideoManager();

		return m->getNumReadyFrames();
#else
		return app.getVideoManager()->getNumReadyFrames();
#endif

}

void CVirtualScript::destroyVideoClip(const String &materialName, const String &videoName)
{
	if(app.getVideoManager()->getVideoClipByName(videoName))
	{
		//app.getVideoManager()->getVideoClipByName(videoName)->stop();
		stopVideoClip(videoName);

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE ||  OGRE_PLATFORM == OGRE_PLATFORM_LINUX ||  OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		app.getVideoManager()->destroyAdvancedTexture(materialName, videoName);
		
#if !defined(NEW_THEORA_LIB)		
		app.getVideoManager()->destroyVideoClip(app.getVideoManager()->getVideoClipByName(videoName));
#else
		app.getVideoManager()->destroyVideoClip(videoName);
#endif

		toLogEx("\n getVideoManager");

#if !defined(NEW_THEORA_LIB)
		MyVideoManager *m = (MyVideoManager *)app.getVideoManager();
				
		toLogEx("\n destroyVideoClip %s Total clips: %d",videoName.c_str(),m->getClipList().size());
#endif

#endif
		app.getVideoManager()->setInputName("None");

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
//		materialSetTexture(materialName, "00_black.tga");
//		deleteTexture(videoName);
#endif
	}
}
void CVirtualScript::setVideoClipLoop(const String &videoName, bool loop)
{
#if !defined(NEW_THEORA_LIB)
	app.getVideoManager()->getVideoClipByName(videoName)->setAutoRestart(loop);
#else
	app.getVideoManager()->setAutoRestart(videoName,loop);
#endif
}

int CVirtualScript::getVideoClipWorkerThreads() // Sanya: �������� �������, ������ 1 ����������
{
	return app.getVideoManager()->getNumWorkerThreads();
}
#else
float CVirtualScript::startVideoClip(const String &materialName, const String &videoName)
{
//	return 1.f;
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
    char chFileName[256];
    strcpy(chFileName, videoName.c_str());
    char *pDot = strstr(chFileName, ".");
    if (pDot)
        pDot[0] = '\0';

    float len = applicationPlayVideo(chFileName);
//	float len = 30.f;

    toLogEx("\n play video returned length: %f", len);

    return len;
#else
	return 1.f;
#endif
}
void CVirtualScript::stopVideoClip(const String &videoName)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
    applicationStopVideo();
#endif
}
void CVirtualScript::destroyVideoClip(const String &materialName, const String &videoName)
{
}
void CVirtualScript::setVideoClipLoop(const String &videoName, bool loop)
{
}

int CVirtualScript::getVideoClipWorkerThreads() // Sanya: �������� �������, ������ 1 ����������
{
	return 1;
}
#endif

//app initialization

void CVirtualScript::addCursor(int id, const String& curCustom, int curStandard, const String &material)
{
	#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

		char *_curStd = 0;
		switch(curStandard)
		{
			case J_STD_CURSOR_ARROW: _curStd = IDC_ARROW; break;
			default: _curStd = IDC_ARROW;
		};

		app.addCursor( id, curCustom, _curStd, material);

	#endif
}


void CVirtualScript::addResourcePath(const String& path, const String& group)
{
	app.addResourcePath(path,"FileSystem",group);
}

void CVirtualScript::appSetup(const String &company, const String &appname, const String &shortname, SetupScriptFunc func)
{
	app.setCompanyName(company);
	app.setAppFullName(appname);
	app.setAppShortName(shortname);
	app.setSetupScriptFunc(func);
}

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
void CVirtualScript::addObbLocation()
{
	if(!app.getOBBPath().empty())
	{
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(app.getOBBPath(), "Zip", "Heap", true);
	}
}
#endif


//----------------------
#if USE_BULLET == TRUE
#include "OgreBulletDynamicsRigidBody.h"
#include "PhysObject.h"

void CVirtualScript::attachBoneToBone(OBJECT_HANDLE h, const String& parent_bone, const String& child_bone)
{
	CGameObject* pObj = app._findObjectByHandle(h);

	Ogre::Entity *pMovObj = pObj->pEntity;

	pMovObj->_updateAnimation();

	Ogre::Bone *pPBone = pMovObj->getSkeleton()->getBone(parent_bone);
	Ogre::Bone *pCBone = pMovObj->getSkeleton()->getBone(child_bone);

	Ogre::Bone *pOldParent = (Ogre::Bone*)pCBone->getParent();

	if (pOldParent == pPBone)
		return;


	Vector3 v1 = pCBone->_getDerivedPosition();
	Quaternion q1 = pCBone->_getDerivedOrientation();

	pOldParent->removeChild(pCBone);

	pCBone->setManuallyControlled(true);

	Vector3 new_rel_pos = pOldParent->_getDerivedPosition() - pPBone->_getDerivedPosition() ;
	Quaternion new_rel_q = pOldParent->_getDerivedOrientation()*pPBone->_getDerivedOrientation().Inverse();

	pPBone->addChild(pCBone);

	pCBone->setOrientation(pCBone->getOrientation()*new_rel_q);
	pCBone->setPosition(pCBone->getPosition() + new_rel_pos);

	Vector3 v2 = pCBone->_getDerivedPosition();
	Quaternion q2 = pCBone->_getDerivedOrientation();

	pCBone->_setDerivedPosition(v1);
	Vector3 v3 = pCBone->_getDerivedPosition();
}

void CVirtualScript::setPhysicsProp(OBJECT_HANDLE h, int prop, Vector3 &val, int ivalue)
{
	CGameObject* pObj = app._findObjectByHandle(h);
	CPhysObject *pPhys = 0;

	OgreBulletDynamics::RigidBody *body = 0;

	if (pObj->getType() != CGameObject::T_PHYS_OBJ)
	{
		body = Ogre::any_cast<OgreBulletDynamics::RigidBody*>(pObj->pSNode->getUserObjectBindings().getUserAny("body"));
		if (!body)
		{
			toLogEx("ERROR: Rigit body in %s not found",pObj->getName());
		}
	}
	else
	{
		pPhys = (CPhysObject*)pObj;
	}

	switch(prop)
	{
	//pos
	case 0:
		body->setPosition(val);
		break;

	//lin vel
	case 1:
		body->setLinearVelocity(val);
		break;

	//ang rot
	case 2: break;

	//change state for all object
	case 3:
		pPhys->ChangeBodyState(-1,ivalue);
		break;

	}
}

#endif

void CVirtualScript::get3dFrom2dEx(const Vector2 &pos2d, float zdepth, Vector3 &res)
{
	//camera node position
	if(!app.getCamera() || !app.getCamera()->getParentSceneNode())
	{
		res.x = res.y = res.z = -99999.99f;
		return;
	}

	Ogre::Vector3 p = app.getCamera()->getParentSceneNode()->getPosition();
	Ogre::Quaternion q = app.getCamera()->getParentSceneNode()->getOrientation();

	//get world ray
	Ogre::Ray ray = CVirtualScript::getViewportRay(pos2d.x,pos2d.y);

	//get local plane
	Ogre::Plane plane(Vector3(0,0,-1), Vector3(0,0, zdepth));

	//transform ray into camera space
	Ogre::Vector3 _or = ray.getOrigin() - p;
	Ogre::Vector3 v = q.Inverse()*ray.getDirection();

	ray.setDirection(v);
	ray.setOrigin(_or);

	//intersect ray with plane
	std::pair<bool, Real> pair = Ogre::Math::intersects(ray, plane);
	res = ray.getPoint(pair.second);
}

void CVirtualScript::fontSwitchFiltering(bool enable)
{
	Ogre::MaterialManager::ResourceMapIterator rmi = Ogre::MaterialManager::getSingleton().getResourceIterator();
	while(rmi.hasMoreElements())
	{
		Ogre::MaterialPtr mMaterial  = rmi.getNext().staticCast<Ogre::Material>();
		if (mMaterial->getNumTechniques())
		{
			Ogre::Technique*  mTechnique = mMaterial->getTechnique (0);
			if (mTechnique && mTechnique->getNumPasses())
			{
				Ogre::Technique::PassIterator passIterator = mTechnique->getPassIterator();
				while(passIterator.hasMoreElements())
				{
					Ogre::Pass* mPass = passIterator.getNext();
					Ogre::Pass::TextureUnitStateIterator unitIterator = mPass->getTextureUnitStateIterator();
					while(unitIterator.hasMoreElements())
					{
						Ogre::TextureUnitState* mTUState = unitIterator.getNext();
						String textureName = mTUState->getTextureName();
						if (Ogre::StringUtil::startsWith(textureName, "00_font"))
						{
							if(enable)
								mTUState->setTextureFiltering(Ogre::TFO_TRILINEAR);
							else
								mTUState->setTextureFiltering(Ogre::TFO_NONE);
						}

					}
				}
			}
		}
	}
}

Ogre::Camera *CVirtualScript::getCamera()
{
	return app.getCamera();
}

Ogre::SceneManager *CVirtualScript::getSceneManager()
{
	return app.getSceneManager();
}

/*CGameObject*  CVirtualScript::_findObjectByHandle(OBJECT_HANDLE obj)
{
	return app._findObjectByHandle(obj);
}*/

Vector2 CVirtualScript::getWindowResolution()
{
	return Vector2((Ogre::Real)app.getWindowResolution().x,(Ogre::Real)app.getWindowResolution().y);
}

Vector2 CVirtualScript::findResolution(bool fs)
{
	return Vector2((Ogre::Real)app.findResolution(fs).x,(Ogre::Real)app.findResolution(fs).y);
}

void CVirtualScript::disableObjectsFocus(bool state)
{
	app.disableObjectsFocus(state);
}

void CVirtualScript::_setCfgValue(const String& name, const int& value)
{
	app.getAppState().setValue(name, value);
}

void CVirtualScript::_setCfgValue(const String& name, const bool& value)
{
	app.getAppState().setValue(name, value);
}

void CVirtualScript::_setCfgValue(const String& name, const float& value)
{
	app.getAppState().setValue<float>(name, value);
}

void CVirtualScript::_setCfgValue(const String& name, const String& value)
{
	app.getAppState().setValue(name, value);
}

void CVirtualScript::_setCfgValue(const String& name, const CValue& value)
{
	app.getAppState().setValue(name, value);
}

void CVirtualScript::_saveCfgValue(const String& name, const int& value)
{
	//app.getAppState().setValue(name, value);
	_setCfgValue(name,value);
	app.getAppState().configureSave();
}

void CVirtualScript::_saveCfgValue(const String& name, const bool& value)
{
	//app.getAppState().setValue(name, value);
	_setCfgValue(name,value);
	app.getAppState().configureSave();
}

void CVirtualScript::_saveCfgValue(const String& name, const String& value)
{
	//app.getAppState().setValue(name, value);
	_setCfgValue(name,value);
	app.getAppState().configureSave();
}

void CVirtualScript::_saveCfgValue(const String& name, const CValue& value)
{
	//app.getAppState().setValue(name, value);
	_setCfgValue(name,value);
	app.getAppState().configureSave();
}

bool CVirtualScript::isMouseLButton()
{
	return app.isMouseLButton();
}

bool CVirtualScript::isMouseRButton()
{
	return app.isMouseRButton();
}

std::string CVirtualScript::getKeyboardText()
{
#if  OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	return app.getKeyboardText();
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
	return ::getKeyboardText();
#endif

	return std::string();
}

void CVirtualScript::enterBackground()
{
	app.enterBackground();
}

void CVirtualScript::showKeyboard(bool show)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
	::showKeyboard(show);
#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	app.showKeyboard(show);
#endif
}

void CVirtualScript::setKeyboardText(const std::string& text)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
	::setKeyboardText(text);
#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	app.setKeyboardText(text);
#endif
}

CVirtualScript* CVirtualScript::getVirtualScript(OBJECT_HANDLE obj)
{
	CGameObject* pObj = app._findObjectByHandle(obj);
	if(pObj)
		return pObj->pScript;
	else
		return NULL;
}

/*
using:

std::vector<std::pair<String,RealRect>> rects;
CVirtualScript::loadRectArrayFromCfg(CMainApplication::getCfgPath("ipad_4x_zh2.txt"),rects);

for(int i = 0; i < rects.size(); i++)
{
	toLogEx("%s %f %f %f %f",rects[i].first.c_str(),rects[i].second.left,rects[i].second.top,rects[i].second.right,rects[i].second.bottom);
}

*/
void CVirtualScript::loadRectArrayFromCfg(const String fname, std::vector<std::pair<Ogre::String,Ogre::RealRect> > &rects)
{
	Ogre::ConfigFile cf;

	try
	{
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
		cf.load(fname);
#else
		cf.load(app.getAppState().openAPKFile(fname));
#endif
	}
	catch (Ogre::Exception ex)
	{
		toLogEx("\nERROR: File %s not found!\n", fname.c_str());
		return;
	}

	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	String secName, param, value;
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			param = i->first;
			value = i->second;

			Ogre::StringVector sv = Ogre::StringConverter::parseStringVector(value);

			Real f[4];

			for(int i = 0; i < 4; i++)
			{
				f[i] = Ogre::StringConverter::parseReal(sv[i]);
			}

			Ogre::RealRect rr(f[0],f[1],f[2],f[3]);

			rects.push_back(std::make_pair(param,rr));
		}
	}
}
