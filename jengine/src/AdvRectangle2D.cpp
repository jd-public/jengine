#include "stdafx.h"
#include "AdvRectangle2D.h"
#include <OgreViewport.h>
#include <OgreMaterialManager.h>
#include <OgreTextureManager.h>
#include <OgreHardwareBufferManager.h>
#include "AtlasManager.h"
#include "MathUtils.h"

namespace Ogre {

const Ogre::Matrix4 AdvRectangle2D::MAT_DEGREE_90 = Ogre::Quaternion(Ogre::Radian(Ogre::Math::HALF_PI), Ogre::Vector3(0,0,1.f));
const Ogre::Matrix4 AdvRectangle2D::MAT_DEGREE_270 = Ogre::Quaternion(Ogre::Radian(3*Ogre::Math::HALF_PI), Ogre::Vector3(0,0,1.f));

AdvRectangle2D::AdvRectangle2D(bool fautounload)
	: Rectangle2D(true), fAutoUnload(fautounload)
{
	fOldVisible = false;
}

bool AdvRectangle2D::isVisible() const
{
//	toLogEx("\nVISIBLE");

	bool curVisible = Rectangle2D::isVisible();
	if (!fOldVisible && curVisible)
	{
		LoadMaterial();
	}
//#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
	else if (fOldVisible && !curVisible && fAutoUnload)
	{
//		toLogEx("\nUNLOAD MATERIAL");

		UnloadMaterial();
	}
//#endif
	fOldVisible = curVisible;
	return curVisible;
}

void AdvRectangle2D::LoadMaterial() const
{
	return;

	if (!mMaterial.isNull())
	{
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByHandle(mMaterial->getHandle()).staticCast<Material>();
		if (material.isNull())
		{
			OGRE_EXCEPT( Exception::ERR_ITEM_NOT_FOUND, "Could not find material " + mMatName,
				"SimpleRenderable::setMaterial" );
		}
		else
		{
			if (!material->isLoaded())
			{
				material->load();
			}
		}
	}
}

void AdvRectangle2D::UnloadMaterial() const
{
	return;

	if (!mMaterial.isNull())
	{
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByHandle(mMaterial->getHandle()).staticCast<Material>();
		if (material.isNull())
		{
			OGRE_EXCEPT( Exception::ERR_ITEM_NOT_FOUND, String("Could not find material ") + mMatName,
				"SimpleRenderable::setMaterial" );
		}
		else
		{
			if (material->isLoaded())
			{
				material->unload();
				Ogre::TextureManager::getSingleton().unloadUnreferencedResources(false);
			}
		}
	}
}

void AdvRectangle2D::setMaterial( const String& matName )
{
	mMatName = matName;
	mMaterial = Ogre::MaterialManager::getSingleton().getByName(mMatName).staticCast<Material>();

	if (mMaterial.isNull())
	{
		OGRE_EXCEPT( Exception::ERR_ITEM_NOT_FOUND, String("Could not find material ")+mMatName,
			"AdvRectangle2D::setMaterial" );
	}


//	mMaterial = Ogre::MaterialManager::getSingleton().getByName(m_strMatName);
//	mMaterial.isNull();
//	CAtlasManager::getSingleton().changeMaterialTexturesToAtlas(m_strMatName);
}

void AdvRectangle2D::setUVs(const RealRect& rect, bool frotate)
{
	setUVs(rect.left, rect.top, rect.right, rect.bottom, frotate);
}

void AdvRectangle2D::setUVs(Real left, Real top, Real right, Real bottom, bool frotate)
{
	if (mRenderOp.vertexData)
	{
		VertexDeclaration* decl = mRenderOp.vertexData->vertexDeclaration;
		VertexBufferBinding* bind = mRenderOp.vertexData->vertexBufferBinding;

		decl->addElement(TEXCOORD_BINDING, 0, VET_FLOAT2, VES_TEXTURE_COORDINATES);

		HardwareVertexBufferSharedPtr tvbuf = 
			HardwareBufferManager::getSingleton().createVertexBuffer(
			decl->getVertexSize(TEXCOORD_BINDING),
			mRenderOp.vertexData->vertexCount,
			HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY);

		// Bind buffer
		bind->setBinding(TEXCOORD_BINDING, tvbuf);

		// Set up basic tex coordinates
		float* pTex = static_cast<float*>(tvbuf->lock(HardwareBuffer::HBL_DISCARD));
		if (!frotate)
		{
			*pTex++ = left;
			*pTex++ = top;
			*pTex++ = left;
			*pTex++ = bottom;
			*pTex++ = right;
			*pTex++ = top;
			*pTex++ = right;
			*pTex++ = bottom;
		}
		else
		{
			*pTex++ = right;
			*pTex++ = top;
			*pTex++ = left;
			*pTex++ = top;
			*pTex++ = right;
			*pTex++ = bottom;
			*pTex++ = left;
			*pTex++ = bottom;
		}
		tvbuf->unlock();
	}
	else
		OGRE_EXCEPT( Exception::ERR_INTERNAL_ERROR, String("mRenderOp.vertexData==NULL - how!!? material=") + mMatName, "Rect2D::setUVs" );
}

void AdvRectangle2D::getWorldTransforms(Ogre::Matrix4* xform) const
{
#if OGRE_NO_VIEWPORT_ORIENTATIONMODE == 0
	switch (Ogre::Viewport::getDefaultOrientationMode())
	{
		case Ogre::OR_DEGREE_90:
			*xform = MAT_DEGREE_90;
			break;
		case Ogre::OR_DEGREE_270:
			*xform = MAT_DEGREE_270;
			break;
		default:
			Rectangle2D::getWorldTransforms(xform);
	}			
#else
	Rectangle2D::getWorldTransforms(xform);
#endif
}

//-----------------------------------------------------------------------
const String AdvRectangle2DFactory::FACTORY_TYPE_NAME = "AdvRectangle2D";
//-----------------------------------------------------------------------
const String& AdvRectangle2DFactory::getType(void) const
{
	return FACTORY_TYPE_NAME;
}
//-----------------------------------------------------------------------
MovableObject* AdvRectangle2DFactory::createInstanceImpl( const String& name,
														 const NameValuePairList* params)
{
	bool autounload = true;
	if (params != 0)
	{
		NameValuePairList::const_iterator ni = params->find("autoUnload");
		if (ni != params->end())
			autounload = StringConverter::parseBool(ni->second);
	}

	return OGRE_NEW AdvRectangle2D(autounload);
}
//-----------------------------------------------------------------------
void AdvRectangle2DFactory::destroyInstance( MovableObject* obj)
{
	OGRE_DELETE obj;
}
void AdvRectangle2D::setUserAny(const Any& anything)
{
	Ogre::MovableObject::setUserAny(anything); 
}
AdvRectangle2DFactory::AdvRectangle2DFactory()
{}
AdvRectangle2DFactory::~AdvRectangle2DFactory()
{}


}

