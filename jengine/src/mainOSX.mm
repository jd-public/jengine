#include "stdafx.h"


#include <Cocoa/Cocoa.h>


#ifdef USE_XPROMO
#	import "OgreOSXCocoaView.h"
#	import "gamekit/GKGameCenterViewController.h"
#	import "gamekit/GKDialogController.h"
#	import "alertInterface.h"
#	include "xpromo.h"
#	include "FacebookIntegrator.h"
#endif

//#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS
//#	include "OdinString.h"
//#endif

NSAutoreleasePool *pool;


void applicationDoLog(const char *s)
{
	NSString *nsstr = [NSString stringWithUTF8String:s];
	NSLog(@"%@",nsstr);
}

class MyLog : public Ogre::LogListener
{
	virtual void messageLogged( const String& message, Ogre::LogMessageLevel lml, bool maskDebug, const String &logName, bool& skipThisMessage )
	{
		applicationDoLog(message.c_str());
	}

};

#define MAX_CURSORS 10
@interface OgreController : NSObject<NSApplicationDelegate>
{
@public;
	bool need_switch_fullscreen;
	NSMenuItem *switch_fs;
	NSImageView *mPauseImageView;
	NSTextField *mPauseImageViewLabel;
	
	
	NSCursor *cursors[MAX_CURSORS];
	int cursor_num;
}

-(void)enableFullscreenMenuItem:(BOOL)e;

@end

OgreController *delegate = 0;

@implementation OgreController

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

-(void) applicationWillTerminate:(NSNotification *)notification
{
	NSLog(@"applicationWillTerminate!");
	
#ifdef USE_XPROMO
	//TODO: uncomment me!!!
	//xpromo::Shutdown();
#endif
	
	app.deinitialize();
	
	
	
	//[pool release];
}

-(void)switchWindowedAndFullscreen:(NSNotification *)notification
{
	//app.setFullscreen(!app.getFullscreen());
	need_switch_fullscreen = true;
}

-(void)enableFullscreenMenuItem:(BOOL)e
{
	[switch_fs setHidden:!e];
}

- (void)createAppMenu
{
	NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
	
	
	id menubar = [[NSMenu new] autorelease];
	id appMenuItem = [[NSMenuItem new] autorelease];
	[menubar addItem:appMenuItem];
	[NSApp setMainMenu:menubar];
	
	
	id appMenu = [[NSMenu new] autorelease];
	//id appName = [[NSProcessInfo processInfo] processName];
	id quitTitle = [@"Quit " stringByAppendingString:appName];
	id quitMenuItem = [[[NSMenuItem alloc] initWithTitle:quitTitle action:@selector(terminate:) keyEquivalent:@"q"] autorelease];
	
	id about = [[[NSMenuItem alloc] initWithTitle:[@"About " stringByAppendingString: appName] action:@selector(orderFrontStandardAboutPanel:) keyEquivalent:@""] autorelease];
	
	id hide = [[[NSMenuItem alloc] initWithTitle:[@"Hide " stringByAppendingString: appName] action:@selector(hide:) keyEquivalent:@"h"] autorelease];
	
	NSMenuItem *hide_all = [[[NSMenuItem alloc] initWithTitle:@"Hide All "action:@selector(hideOtherApplications:) keyEquivalent:@"h"] autorelease] ;
	
	id show_all = [[[NSMenuItem alloc] initWithTitle:@"Show All" action:@selector(unhideAllApplications:) keyEquivalent:@""] autorelease];
	
	[hide_all setKeyEquivalentModifierMask:NSCommandKeyMask | NSAlternateKeyMask];
	
	switch_fs = [[[NSMenuItem alloc] initWithTitle:@"Switch to full screen mode"action:@selector(switchWindowedAndFullscreen:) keyEquivalent:@"f"] autorelease] ;
	[switch_fs setKeyEquivalentModifierMask:NSCommandKeyMask | NSAlternateKeyMask];
	//[switch_fs setHidden:TRUE];
	
	[self enableFullscreenMenuItem:FALSE];
	
	[appMenu addItem:about];
	[appMenu addItem:[NSMenuItem separatorItem]];
	[appMenu addItem:hide];
	[appMenu addItem:hide_all];
	[appMenu addItem:show_all];
	[appMenu addItem:switch_fs];
	[appMenu addItem:[NSMenuItem separatorItem]];
	[appMenu addItem:quitMenuItem];
	
	[appMenuItem setSubmenu:appMenu];
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
	mPauseImageView = nil;        
	mPauseImageViewLabel = nil;

	NSLog(@"applicationDidFinishLaunching");
	
	Ogre::LogManager *log = new Ogre::LogManager;
	log->createLog("(null)",true,true,true);
	Ogre::LogManager::getSingleton().getDefaultLog()->addListener(new MyLog());
	
	cursor_num = 0;
	for(int i = 0; i < MAX_CURSORS;i++)
		cursors[i] = nil;
	
	try
	{
		app.go();
	}
	catch( Ogre::Exception& e )
	{
		
	}
	
	[self initCursor:true];
	
	NSLog(@"application initialized okay");
	
	[self createAppMenu];
	
	need_switch_fullscreen = false;
	

	
	//Hide jetdogs logo at the start up
	CVirtualScript::setSceneVisible("00_logo_start.scene", false);

	//----

	
	NSTimer *renderTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(renderFrame) userInfo:NULL repeats:YES];
	[[NSRunLoop currentRunLoop] addTimer:renderTimer forMode:NSEventTrackingRunLoopMode];
	
	//----
	
	NSWindow *twindow = [self myWindow];
	
	//ShowReviewGameAlert();
	
	NSLog(@"application window %@",twindow);

}

- (void)renderFrame
{
	//NSLog(@"active %d",Ogre::Root::getSingleton().getAutoCreatedWindow()->isActive());
	//Ogre::Root::getSingleton().getAutoCreatedWindow()->setActive(true);
	///return;
	//NSLog(@"renderFrame");
	
	//applicationSetCursor(0);
	
	if(need_switch_fullscreen)
	{
		CVirtualScript::sendCMsgNow("interface", 20015, 0);
		CVirtualScript::setFullscreen(!app.getFullscreen());
		
		need_switch_fullscreen = false;
	}
	
	if(!Ogre::Root::getSingleton().renderOneFrame())
	{
		NSLog(@"renderOneFrame terminate");
		
		[self terminate];
	}
}

-(void)terminate
{
	[NSApp terminate:self];
}

- (void) showPausePicture
{
	if (mPauseImageView != nil)
		[self hidePausePicture];
	
	NSView *twindow = [self myView];
	
	Ogre::Root::getSingleton().getAutoCreatedWindow()->setActive(false);
	
	
#ifdef IOS_FREEMIUM
	bool bIsInPurchaseNow = false;//gIsInPurchaseNow;
#else
	bool bIsInPurchaseNow = false;
#endif
	
	if (twindow)
	{
		CGRect rectBounds = NSRectToCGRect([twindow bounds]);
		
		CGSize borderSize = CGSizeMake(rectBounds.size.width, rectBounds.size.height);
		//CGPoint topLeft = CGPointMake(((float)rectBounds.size.width - borderSize.width)/2.f, ((float)rectBounds.size.height - borderSize.height)/2.f);
		//CGRect imageRect = CGRectMake(topLeft.x, topLeft.y, borderSize.width, borderSize.height);
		
		
		
		mPauseImageView = [[NSImageView alloc] initWithFrame:[twindow bounds]];
		
		NSLog(@"mPauseImageView: %@",mPauseImageView);
		
		NSString *strImage = @"ios-labelcap.tga";
		
		NSImage *tmpImage = [NSImage imageNamed:strImage];
		
		NSLog(@"Image: %@",tmpImage);
		
		[tmpImage setSize:NSMakeSize(rectBounds.size.width, rectBounds.size.height)];
		
		[mPauseImageView setImage:tmpImage];
		
		
		//[mPauseImageView setImageScaling:NSOnState];
		
		//mPauseImageView.opaque = NO; // explicitly opaque for performance
		//mPauseImageView.backgroundColor = [[UIColor alloc] initWithWhite:0.f alpha:0.f];
		
		NSLog(@"2");
		
		Ogre::UTFString ogreStrText = CVirtualScript::getTextString(bIsInPurchaseNow ? 464 : 68); // "accessing iTunes..." : "tap anywhere to continue..."
		NSString *caption = [NSString stringWithCString:ogreStrText.asUTF8_c_str() encoding:NSUTF8StringEncoding];
		
		NSLog(@"Caption: %@",caption);
		
		CGRect myImageLabelRect = CGRectMake(0.0f, 0, rectBounds.size.width, rectBounds.size.height/2);
		
		mPauseImageViewLabel = [[NSTextField alloc] initWithFrame:NSRectFromCGRect(myImageLabelRect)];
		
		[mPauseImageViewLabel setAlignment:NSCenterTextAlignment];
		
		[mPauseImageViewLabel setStringValue:caption];
		
		mPauseImageViewLabel.backgroundColor = [NSColor clearColor];
		
		mPauseImageViewLabel.textColor = [NSColor cyanColor];
		
		mPauseImageViewLabel.font = [NSFont fontWithName:@"Verdana" size:32];

		
		mPauseImageViewLabel.bezeled         = NO;
		mPauseImageViewLabel.editable        = NO;
		mPauseImageViewLabel.drawsBackground = NO;
		
		//[mPauseImageViewLabel setEditable: false];
		//[mPauseImageViewLabel setDrawsBackground: false];
		
		//mPauseImageViewLabel.shadowColor = [[UIColor alloc] initWithRed:0.f green:0.f blue:0.f alpha:1.f];
		//mPauseImageViewLabel.shadowOffset = CGSizeMake(1, 1);
		mPauseImageViewLabel.hidden = NO;
		
		/*if (bIsInPurchaseNow)
		{
			ogreStrText = CVirtualScript::getTextString(463); // "continue game"
			caption = [NSString stringWithCString:ogreStrText.asUTF8_c_str() encoding:NSUTF8StringEncoding];
			
			mPauseImageViewButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
			[mPauseImageViewButton setTitle:caption forState:UIControlStateNormal];
			[[mPauseImageViewButton titleLabel] setFont: [UIFont boldSystemFontOfSize:14]];
			[mPauseImageViewButton sizeToFit];
			mPauseImageViewButton.center = CGPointMake(topLeft.x + borderSize.width/2, topLeft.y + borderSize.height/2 + 40);
			//button.tag = 777;
			[mPauseImageViewButton addTarget:self action:@selector(pausePictureButtonPressed) forControlEvents:UIControlEventTouchUpInside];
		}*/
		
		[mPauseImageView setWantsLayer:YES];
		
		[mPauseImageView setAlphaValue:0.99f];
		
		[twindow addSubview:mPauseImageView];
		
		[mPauseImageViewLabel setWantsLayer:YES];
		
		[twindow addSubview:mPauseImageViewLabel];
		
	
		/*if (bIsInPurchaseNow)
		{
			[twindow addSubview:mPauseImageViewButton];
			mPauseImageViewButton.hidden = YES;
			
			mTimerButtonShowEnable = true;
			mTimerButtonShow = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)(30.0f)
																target:self
															  selector:@selector(showPausePictureButton:)
															  userInfo:nil
															   repeats:NO];
		}*/
		
		[mPauseImageView release];
		[mPauseImageViewLabel release];
	}
}

- (void) hidePausePicture
{
	NSLog(@"\n hidePauseMenu1");
	/*toLogEx("\n hidePauseMenu1");
	
	if (mTimerButtonShow != nil)
	{
		toLogEx("\n hidePauseMenu1 %@",mTimerButtonShow);
		
		//		[mTimerButtonShow invalidate];
		mTimerButtonShow = nil;
	}
	mTimerButtonShowEnable = false;
	
	if (!mPauseImageView)
		return;
	
	NSArray *windows = [UIApplication sharedApplication].windows;
	UIWindow *twindow = [windows objectAtIndex:0];
	
	if (twindow)
	{
		[mPauseImageView willRemoveSubview:mPauseImageViewLabel];
		[mPauseImageViewLabel removeFromSuperview];
		mPauseImageViewLabel = nil;
		
		[twindow willRemoveSubview:mPauseImageView];
		[mPauseImageView removeFromSuperview];
		mPauseImageView = nil;
		
		if (mPauseImageViewButton != nil)
		{
			[twindow willRemoveSubview:mPauseImageViewButton];
			[mPauseImageViewButton removeFromSuperview];
			mPauseImageViewButton = nil;
		}
	}*/
	
	if(mPauseImageView == nil)
		return;
		
	NSLog(@"\n hidePauseMenu2 %@ %@",mPauseImageView,mPauseImageViewLabel);
	
	NSView *twindow = [self myView];
	
	if (mPauseImageView != nil)
	{
		[twindow willRemoveSubview:mPauseImageView];
		[mPauseImageView removeFromSuperview];
		mPauseImageView = nil;
	}
	
	if (mPauseImageViewLabel != nil)
	{
		[twindow willRemoveSubview:mPauseImageViewLabel];
		[mPauseImageViewLabel removeFromSuperview];
		mPauseImageViewLabel = nil;
	}
}

//!!!!
//- (void)applicationWillResignActive:(NSNotification *)application
-(void)active
{
	NSLog(@"applicationWillResignActive");
    
    // send message to ginterface to save game
    // and message to interface to suspend xpromo
    if (CVirtualScript::getCfgB("msg_to_gi_on_home_btn"))
    {
        CMsg msgSaveGame;
        msgSaveGame.lparam = 29;
        msgSaveGame.rparam = 0;
        CVirtualScript::sendCMsgNow("00_game_interface", msgSaveGame);
		
        CMsg msgResignActive;
        msgResignActive.lparam = 20010;
        msgResignActive.rparam = 0;
        CVirtualScript::sendCMsgNow("interface", msgResignActive);
    }
	
	
	Ogre::Root::getSingleton().getAutoCreatedWindow()->setActive(false);
		
    // Pause FrameListeners and rendering
	app.enterBackground();
    Ogre::Root::getSingleton().saveConfig();
	
	
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
	
	if (CVirtualScript::getCfgB("ios_pause_enabled") && (CVirtualScript::getCfgI("game_state") == 100 || CVirtualScript::getCfgB("pause_on_deactive")))
	{
		//applicationPauseVideo();
		//applicationShowPausePicture();
	}
	//applicationRememberCameraAspect();
	
	[self showPausePicture];
	
	NSLog(@"applicationWillResignActive end");
}

//- (void)applicationDidBecomeActive:(NSNotification *)application
- (void)deactive
{
	NSLog(@"applicationDidBecomeActive");
	
	
    // send message to interface to resume xpromo
	
    if (CVirtualScript::getCfgB("msg_to_gi_on_home_btn"))
    {
        CMsg msgBecomeActive;
        msgBecomeActive.lparam = 20011;
        msgBecomeActive.rparam = 0;
        CVirtualScript::sendCMsgNow("interface", msgBecomeActive);
    }
	
	Ogre::Root::getSingleton().getAutoCreatedWindow()->setActive(true);
	app.enterForeground();
	
	if (!CVirtualScript::getCfgB("ios_pause_enabled") || (CVirtualScript::getCfgI("game_state") != 100 && !CVirtualScript::getCfgB("pause_on_deactive")))
	{
		
		
		//		toLogEx("\n applicationDidBecomeActive %d %d %d", CVirtualScript::getCfgB("ios_pause_enabled"), CVirtualScript::getCfgI("game_state"), CVirtualScript::getCfgB("pause_on_deactive"));
		if (CVirtualScript::getCfgB("ios_pause_enabled"))
		{
			//applicationHidePausePicture();
		}
	}
	
	[self hidePausePicture];
	
	
	//applicationReleaseFacebook();

	//mLastFrameTime = CFAbsoluteTimeGetCurrent();
	
	//applicationUpdateCameraAspect();
	//CVirtualScript::setSI("iphone_need_aspect", 1);
	
    // Resume FrameListeners and rendering
    //[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:)
    //                                             name:UIDeviceOrientationDidChangeNotification object:nil];
	
}

-(void)activeWindow:(bool)a
{
	if(a)
		[self active];
	else
		[self deactive];
}

-(NSWindow*)myWindow
{
	size_t v;
	Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("WINDOW", &v);
	return (NSWindow *)v;
}

-(NSView*)myView
{
	size_t v;
	Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("VIEW", &v);
	return (NSView *)v;
}

- (void)initCursor:(bool)enable
{
	NSWindow *win = [self myWindow];
	
	if(enable)
	{

		
		[win disableCursorRects];
	}
	else
	{
		[win enableCursorRects];
	}
	
	
}

- (int)loadCursor:(const char *)cur
{
	//NSView *win = [self myView];
	
	
	NSString *nsstr = [NSString stringWithUTF8String:cur];
	NSImage *tmpImage = [NSImage imageNamed:nsstr];
	
	
	[tmpImage setSize:NSMakeSize(40, 40)];
	cursors[cursor_num] = [[NSCursor alloc] initWithImage:tmpImage hotSpot:NSMakePoint(0.0, 0.0)];
	cursor_num++;
	
	 int ret = cursor_num-1;
	 
	NSLog(@"load cursor %d %@", ret, tmpImage);
	//[win addCursorRect:[win bounds] cursor:cursors[cursor_num]];
	
	return ret;
}

- (void)setCursor:(int)cur
{
	toLogEx("set cursor %d",cur);
	
	if(cur > MAX_CURSORS)
	{
		NSLog(@"cursor error %d %@",cur, cursors[cur]);
		return;
	}
	
	
	NSLog(@"set cursor %@",cursors[cur]);

	if(cur == -1)
	{
		[[NSCursor arrowCursor] set];
	}
	else
	{
		[cursors[cur] set];
	}
}

@end


Ogre::Vector2 applicationGetWindowPos()
{
	size_t v;
	Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("WINDOW", &v);
	NSWindow *win = (NSWindow *)v;
	
	NSRect r = [win frame];
	
	return Ogre::Vector2(r.origin.x,r.origin.y);
}

void applicationEnableFullscreenSwitch(bool e)
{
	[delegate enableFullscreenMenuItem:(BOOL)e];
}

void applicationSetCursor(const char *cur)
{

	
	NSString *imageString = [[NSBundle mainBundle] pathForResource:@"00_cursor_01" ofType:@"png"];
	NSImage *tmpImage = [NSImage imageNamed:@"00_cursor_01.png"];
	NSLog(@"----%@ str: %@ bundle %@",tmpImage, imageString,[NSBundle mainBundle]);
	NSCursor *pointer = [[NSCursor alloc] initWithImage:tmpImage hotSpot:NSMakePoint(10.0, 10.0)];
	[pointer set];
	
	
}

int applicationLoadCursorFromFile(const char *cur)
{
	int k = [delegate loadCursor:cur];
	
	NSLog(@"applicationLoadCursorFromFile %d",k);
	return k;
}

void applicationSetCursor(int i)
{
	[delegate setCursor:i];
}

void appicationWindowActive(bool active)
{
	//[delegate activeWindow:active];
}

#ifdef USE_XPROMO

void applicationShowGameCenter()
{
	
	NSLog(@"applicationShowGameCenter");
	 
	 GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
	 if (gameCenterController != nil)
	 {
		 size_t v;
		 Ogre::Root::getSingleton().getAutoCreatedWindow()->getCustomAttribute("WINDOW", &v);
		 NSWindow *win = (NSWindow *)v;
		 
		 
		 GKDialogController *sdc = [GKDialogController sharedDialogController];
		 sdc.parentWindow = win;
		
		 [sdc presentViewController: gameCenterController];
	 }
}
#endif

//OgreController *delegate = 0;

GAME_API int StartApplication(const char* lpCmdLine)
{
	app.processCommandLine(lpCmdLine);
	
	pool = [[NSAutoreleasePool alloc] init];
	
	delegate = [[OgreController alloc] init];
	[NSApplication sharedApplication];
	[NSApp setDelegate: delegate];
	
	[NSApp run];
	
	return 0;
}



