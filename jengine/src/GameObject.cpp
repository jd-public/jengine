#include "stdafx.h"

#include "GameObject.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	include <mmsystem.h>
#endif

#include "MainApplication.h"
#include "VirtualScript.h"
#include "ScriptCreator.h"
#include "ScriptMsg.h"
#include "CMsg.h"

//-----------------------------------------------------------
/*CGameObject::CObjectCollector::CObjectCollector()
{
	lastNum = 0;
	memset(mObj,0,MAX_OBJECT_NUM*sizeof(LPGAMEOBJECT));
}

inline int CGameObject::CObjectCollector::size()
{ 
	return lastNum; 
}

inline LPGAMEOBJECT CGameObject::CObjectCollector::operator[] (int z)
{
	return mObj[z];
}

void CGameObject::CObjectCollector::removeAll()
{
	for(int i = 0; i < lastNum; i++)	
		SAFE_DEL(mObj[i]);

	lastNum = 0;
}

OBJECT_HANDLE CGameObject::CObjectCollector::push_back(LPGAMEOBJECT obj, const CGameObject::Type &t)
{
	for(int i = 0; i < lastNum+1; i++)
	{
		if (mObj[i] == NULL)
		{
			mObj[i] = obj;

			if (i >= lastNum)
				lastNum = i+1;

			if (i >= MAX_OBJECT_NUM)
			{
                toLogEx("ERROR (CGameObject::CObjectCollector::push_back): What tha F! We have no room for objects!\n");
			}

			return MAKELONG(i,t);
		}
	}

	return 0;
}

LPGAMEOBJECT CGameObject::CObjectCollector::findByHandle( const OBJECT_HANDLE &h )
{
#ifdef _DEBUG
	assert(HIWORD(h) < CGameObject::T_LAST);
	assert(LOWORD(h) < lastNum);
#endif

	return h ? mObj[LOWORD(h)] : NULL;
}

void CGameObject::CObjectCollector::eraseByHandle( const OBJECT_HANDLE &h )
{
	if (h)
	{
		int pos = LOWORD(h);

		mObj[pos] = 0;

		int i = lastNum;
		for(; i > 0; i--)
		{
			if (i > 0 && mObj[i-1] != 0)
			{
				break;
			}
		}

		lastNum = i;
	}
}*/

CObjectCollectorTempl<CGameObject*,CGameObject::Type> CGameObject::mGameObjList;
std::vector<CGameObject*> CGameObject::mMouseEventListenerList;

//----------------------------------------------------------
CGameObject::CGameObject()
{
	unical_timer_counter = 100;
	pEntity = NULL;
	pCamera = NULL;
	pParticle = NULL;
	pBillboard = NULL;
	pLight = NULL;

	pScript = NULL;
	pSNode	= NULL;
	pPearentBone = NULL;
	mIsSelectable = false;

	_pAnimState[0] =0;
	_pAnimState[1] =0;

	_animSpeed[0] = 1.0f;
	_animSpeed[1] = 1.0f;

	activeAnim = 0;
	startBlendTime = 0;
	endBlendTime = 0;

	pAnimState = 0;
	setAnimSpeed(1.0f);

	mHandle = mGameObjList.push_back(this,T_BASIC);
	mTimerList.resize(16);
	
	mSoftwareAnim = false;
	
}

CGameObject::CGameObject(const String& scrName)
{
	unical_timer_counter = 100;
	pScript = NULL;
	pEntity = NULL;
	pSNode	= NULL;
	pCamera = NULL;
	pBillboard = NULL;
	pLight = NULL;
	pParticle = NULL;
	pPearentBone = NULL;

	_pAnimState[0] =0;
	_pAnimState[1] =0;

	_animSpeed[0] = 1.0f;
	_animSpeed[1] = 1.0f;

	activeAnim = 0;
	startBlendTime = 0;
	endBlendTime = 0;

	pAnimState = 0;
	setAnimSpeed(1.0f);

	mIsSelectable = false;
	
	mSoftwareAnim = false;
	
	
	String name = scrName;
	std::map<String, String>::iterator iter;
	for( iter = CVirtualScript::mScriptAliases.begin(); iter != CVirtualScript::mScriptAliases.end(); ++iter ) {
		if (Ogre::StringUtil::match(scrName,iter->first,false))
		{
			name = iter->second;
			break;
		}
	}
	mHandle = mGameObjList.push_back(this,T_BASIC);
	pScript = CScriptCreatorBase::getScriptObject(name);
	if (pScript)
	{
		pScript->setHandle(mHandle);
		if(pScript->isMouseEventListener())
			mMouseEventListenerList.push_back(this);
	}
}

CGameObject::~CGameObject()
{
	mTimerList.clear();
	mGameObjList.eraseByHandle(mHandle);
	std::vector<CGameObject*>::iterator i = std::find(mMouseEventListenerList.begin(), mMouseEventListenerList.end(), this);
	if(i != mMouseEventListenerList.end())
		mMouseEventListenerList.erase(i);
	SAFE_DEL(pScript);
}

void CGameObject::equalSkeletons(Ogre::SkeletonInstance* ptargetS, Ogre::SkeletonPtr pmaterialS)
{
	Ogre::Skeleton::BoneIterator boneIter = ptargetS->getBoneIterator();

	while(boneIter.hasMoreElements())
	{
		Ogre::Bone *oldBone = boneIter.getNext();

		Ogre::Bone *newBone = 0;

		if(pmaterialS->hasBone(oldBone->getName()))
		{
			newBone = pmaterialS->getBone(oldBone->getName());
		}
		else
		{
			toLogEx("\nWARNING: equalSkeletons couldn't find bone %s in skeleton\nTry to use bone hadle.\n",oldBone->getName().c_str());
			newBone = pmaterialS->getBone(oldBone->getHandle());
		}

		oldBone->setOrientation(newBone->getInitialOrientation());
		oldBone->setPosition(newBone->getInitialPosition());
		oldBone->setScale(newBone->getInitialScale());
		oldBone->setInitialState();
	}
}

void CGameObject::frameStarted( const Ogre::FrameEvent& evt )
{
	for(size_t i = 0; i < CGameObject::mGameObjList.size(); i++)
		CGameObject::mGameObjList[i]->processFrame(evt);
}

void CGameObject::blendSkeletons()
{
#ifndef OGRE_DX7
	Ogre::SkeletonInstance *pBaseSkel = pEntity->getSkeleton();
	Ogre::Skeleton::LinkedSkeletonAnimSourceIterator linkIter = pBaseSkel->getLinkedSkeletonAnimationSourceIterator();

	int animNum = 0;

	while(linkIter.hasMoreElements())
	{
		animNum++;

		Ogre::LinkedSkeletonAnimationSource animSrc = linkIter.getNext();

		Ogre::Skeleton::BoneIterator boneIter = pBaseSkel->getBoneIterator();

		Ogre::Animation *pAni = animSrc.pSkeleton->getAnimation(0);
		Ogre::AnimationState* pCurState = 0;

		if (_pAnimState[0])
			pCurState = _pAnimState[0]->getParent()->getAnimationState(pAni->getName());
		else
			pCurState = _pAnimState[1]->getParent()->getAnimationState(pAni->getName());

		if (!pCurState)
		{
			toLogEx("Can`t find anim state for %s\n",animSrc.skeletonName.c_str());
			continue;
		}

		Real animWeight = pCurState->getWeight();

		if (animWeight == 0 || !pCurState->getEnabled()) continue;

		while(boneIter.hasMoreElements())
		{
			Ogre::Bone *skelBone = boneIter.getNext();
			Ogre::Bone *animBone = animSrc.pSkeleton->getBone(skelBone->getHandle());

			Vector3 v1 = skelBone->getInitialPosition();
			Vector3 v2 = animBone->getInitialPosition();

			if (animNum == 1)
				v1 = v2;
			else
				v1 += animWeight*(v2 - v1);

			skelBone->setPosition(v1);		

			Quaternion q1 = skelBone->getInitialOrientation();
			Quaternion q2 = animBone->getInitialOrientation();

			if (animNum == 1)
				q1 = q2;
			else
				q1 = Quaternion::Slerp(animWeight,q1,q2,true);

			skelBone->setOrientation(q1);

			skelBone->setInitialState();
		}
	}

#endif
}

void CGameObject::setAnimationPosition(Real position)
{
	if (pAnimState)
	{
		Real pos;

		if (position >= 0.0)
		{
			pos = position;
		}
		else if (position == -2)
		{
			pos = pAnimState->getLength();
		}
		else if (position == -1)
		{
			pos = pAnimState->getLength() - mAnimTimeScaler.getRealTime();
		}
		else
			return;

		mAnimTimeScaler.setRealTime(pos);
	}
}

float CGameObject::getAnimationPosition() const
{
	if (pAnimState)
		return mAnimTimeScaler.getRealTime();
	return 0;
}

void CGameObject::updateAnimTimePosition()
{
	mAnimTimeScaler.setAnimTimePosition(pAnimState);
}

void CGameObject::processFrame( const Ogre::FrameEvent& evt)
{
	if (pAnimState && pAnimState->getEnabled() && mAnimTimeScaler.getAnimSpeed() > 0)
	{
		Real inc = evt.timeSinceLastFrame;
		mAnimTimeScaler.addRealTime(inc);
		if (mAnimTimeScaler.getRealTime() < mAnimTimeScaler.getFragmentLenght(pAnimState->getLength()))
		{
//			mAnimTimeScaler.setAnimTimePosition(pAnimState);
			updateAnimTimePosition();
		}
		else
		{
			mTimes--;
			
			if (mTimes == 0)
			{
				mAnimTimeScaler.setRealTime(pAnimState->getLength());
				mAnimTimeScaler.setAnimTimePosition(pAnimState);

				CScriptMsg m;
				m.messageId = CScriptMsg::MSG_NOTIFY;
				m.pTargetObj = pTarget;
				m.param.lparam = (int)pAnimState;

				m.param.rparam = MT_STOP_ANIMATION;
				m.pSourceObj = this;
				app.postMsg(m);

				pAnimState = 0;
				mAnimTimeScaler.reset();
			}
			else
			{
				float d = mAnimTimeScaler.getRealTime() - mAnimTimeScaler.getFragmentLenght(pAnimState->getLength()); 
				mAnimTimeScaler.setRealTime(d);
//				mAnimTimeScaler.setAnimTimePosition(pAnimState);
				updateAnimTimePosition();
			}
		}
	}

	processTimer();
}

void CGameObject::processTimer()
{
	bool hasTimersToKill = false;
	for(int i = 0; i < mTimerList.size(); i++)
	{
		ObjTimer timer = mTimerList[i];
		if (app.getTime() - timer.start_time > timer.interval)
		{
			//send create message
			CScriptMsg m;
			m.messageId = CScriptMsg::MSG_TIME;
			m.param.rparam = timer.id;
			m.pTargetObj = timer.objTarget;
			app.postMsg(m);

			mTimerList[i].repeatNum--;

			if (mTimerList[i].repeatNum == 0)
			{
				hasTimersToKill = true;
				mTimerList[i].unused = 1;
			}
			else
			{
				mTimerList[i].unused = 0;
				mTimerList[i].start_time = app.getTime();
			}
		}
	}

	if (hasTimersToKill)
		for(int i = 0; i < mTimerList.size();i++)
			if (mTimerList[i].unused)
			{
				mTimerList.erase( mTimerList.begin() + i);
				i--;
			}
}

void CGameObject::killTimer(CGameObject* obj, int timerID)
{
	int size = mTimerList.size();
	for(int i = 0; i < size; i++)
	{
		ObjTimer timer = obj->mTimerList[i];

		if (timer.id == timerID)
		{
			toLogEx("killTimer (%d,%d) id=0x%x object [%s]\n", i, size, timerID, obj->getName());
			mTimerList.erase(mTimerList.begin() + i);
		}
	}
}

int CGameObject::createTimer(unsigned int _int, int _rep, CGameObject* obj)
{
	ObjTimer timer;
	timer.interval = _int;
	timer.repeatNum = _rep;
	timer.unused = 0;
	timer.start_time = app.getTime();

	if (obj == NULL)
		timer.objTarget = this;
	else
		timer.objTarget = obj;

	int i = mTimerList.push_back(timer);
	int id = ++unical_timer_counter;
	mTimerList[i].id = id;
	toLogEx("\ncreateTimer (%d,%d) id=0x%x object [%s] (%s)\n",i ,mTimerList.size(), id, timer.objTarget->getName(), timer.objTarget->pScript->mScriptName.c_str());

	return id;
}

void CGameObject::createHandle( Type t )
{
	size_t size = mGameObjList.size();

	int id = 1;
	for(;; id++)
	{
		bool hasId = false;
		for(size_t i = 0; i < size; i++)
		{
			if (mGameObjList[i]->getUid() == id)
			{
				hasId = true;
				break;
			}
		}

		if (!hasId)
			break;
	}
	mHandle = MAKELONG(id,t);
}

short CGameObject::getType() const
{
	return HIWORD(mHandle); 
}

short CGameObject::getUid() const
{
	return LOWORD(mHandle);
}

void CGameObject::setType( Type t ) 
{ 
	short id = getUid(); 
	mHandle = MAKELONG(id,t);

	if (pScript)
		pScript->setHandle(mHandle);
}

long CGameObject::getTypeID()
{
	return 1;
}

String CGameObject::getTypeName() const
{
	return "GameObject";
}

void CGameObject::ProcessScriptMsg(const CScriptMsg *m)
{
	if (pScript) 
		pScript->proceedMsg(m);
}

//listener
void CGameObject::nodeUpdated(const Ogre::Node* node)
{

}

/** Ogre::Node is being destroyed */
void CGameObject::nodeDestroyed(const Ogre::Node* node)
{
//	delete this;
}

/** Ogre::Node has been attached to a parent */
void CGameObject::nodeAttached(const Ogre::Node* node)
{

}

/** Ogre::Node has been detached from a parent */
void CGameObject::nodeDetached(const Ogre::Node* node)
{

}
