#include "stdafx.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	include <conio.h>
#endif
#include <OgreLogManager.h>
#include "log.h"

bool logInit = false;

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	void applicationDoLog(const char *s);
#endif


GAME_API void toLogEx(const char* str, ...)
{
	//return;

	va_list marker;
	char s[4096];
	va_start( marker, str );
	vsprintf( s, str, marker );
	va_end( marker );
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	cprintf("%s", s);
	OutputDebugString(s);
#endif

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE
	if(logInit)// not to logMessage after Root has deleted 
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL, s);
#else
	applicationDoLog(s);
#endif
}

