#include "stdafx.h"

#include "RapidXmlFunc.h"

#include "AppState.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	include <shlobj.h>
#endif

#include <stdio.h>
#include <zlib.h>
#include <sys/stat.h>
#include <OgreNode.h>

#include "MainApplication.h"
#include "HeroObject.h"

#include "CMsg.h"

#if (OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS) && USE_XPROMO
	const char* ODINCStr();
#endif

//CValue

/*void CValue::_init(int s, int r)
{
	text = Ogre::StringConverter::toString(s);
	saveToReg = r;
}

void CValue::_init(bool s, int r)
{
	text = Ogre::StringConverter::toString(s);
	saveToReg = r;
}

void CValue::_init(float s, int r)
{
	text = Ogre::StringConverter::toString(s);
	saveToReg = r;
}*/

//----------

// save xml
#define MAPXML_ROOT			"JEngineSave"
#define MAPXML_NAME			"name"
#define MAPXML_CURSTAGE		"curstage"
#define MAPXML_STAGES		"stages"
#define MAPXML_STAGE		"stage"
#define MAPXML_ITEM			"item"
#define MAPXML_VALUE		"value"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
const String DIR_DELIMITER("\\");
#else
const String DIR_DELIMITER("/");
#endif

CValue CAppState::mNullValue;

CAppState::CAppState()
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
: mAssetMgr(NULL)
#endif
{
}

CAppState::~CAppState()
{
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
bool CAppState::getDirectXVersion(int &major, int &minor)
{
	HKEY h;
	major = 0;
	minor = 0;
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		"Software\\Microsoft\\DirectX",
		0,
		KEY_QUERY_VALUE,
		&h) == ERROR_SUCCESS)
	{
		Ogre::ulong type=1;
		Ogre::ulong size=64;
		Ogre::uchar str[64];
		if (RegQueryValueEx(h, "Version", NULL , &type, str, &size)==ERROR_SUCCESS)
		{
			char* p1 = strstr((char*)str, ".");
			char* p2 = strstr((char*)p1+1, ".");
			char* p3 = strstr((char*)p2+1, ".");
			*p2='\0';
			*p3='\0';
			major = atoi(p1+1);
			minor = atoi(p2+1);
		}
		else
		{
			RegCloseKey(h);
			return false;
		}

		RegCloseKey(h);
		return true;
	}

	return false;
}
#endif

bool CAppState::hasValue(const String& name) const
{
	if (mConfigureMap.find(name) != mConfigureMap.end())
		return true;
	return false;
}

const CValue& CAppState::getValue(const Ogre::String& name) const
{
	ConfigureMapIteratorConst iter = mConfigureMap.find(name);
	if (iter != mConfigureMap.end())
		return iter->second;
	return mNullValue;
}

bool CAppState::getValueBool(const Ogre::String& name) const
{
	return Ogre::StringConverter::parseBool(getValueText(name));
}

int CAppState::getValueInt(const Ogre::String& name) const
{
	return Ogre::StringConverter::parseInt(getValueText(name));
}

unsigned int CAppState::getValueUint(const Ogre::String& name) const
{
	return Ogre::StringConverter::parseUnsignedInt(getValueText(name));
}

float CAppState::getValueFloat(const Ogre::String& name) const
{
	return Ogre::StringConverter::parseReal(getValueText(name));
}

const String& CAppState::getValueText(const Ogre::String& name) const
{
	return getValue(name).text;
}

void CAppState::setValue(const Ogre::String& name, const CValue& value)
{
	mConfigureMap[name] = value;
}

void CAppState::configureInit()
{
	mConfigureMap["Rendering Device"]			=	CValue("Auto", 1);
	mConfigureMap["VSync"]						=	CValue(true, 1);
	mConfigureMap["Video Mode"]					=	CValue("1024 x 768 @ 32-bit colour", 1);
	mConfigureMap["SoundVolume"]				=	CValue(50, 1);
	mConfigureMap["MusicVolume"]				=	CValue(50, 1);
	mConfigureMap["CurSave"]					=	CValue(-1, 1);

/*#if USE_STEAM == TRUE
	mConfigureMap["for_steam"]					=	CValue(1, 1);
#else
	mConfigureMap["for_steam"]					=	CValue(0, 1);
#endif*/

#if (OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS)

	mConfigureMap["Full Screen"]				=
#ifdef _DEBUG
		CValue(false, 1);
#else
		CValue(true, 1);
#endif

#ifdef USE_XPROMO
	mConfigureMap["Fit Screen"]					=	CValue(true, 1);
#else
	mConfigureMap["Fit Screen"]					=	CValue(false, 1);
#endif


	mConfigureMap["Floating-point mode"]		=	CValue("Fastest", 1);
//	mConfigureMap["aa_level"]					=	CValue(2, 1);
//	mConfigureMap["mip_num"]					=	CValue(0, 1);
	mConfigureMap["Allow NVPerfHUD"]			=	CValue(false, 1);

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE

	#ifdef USE_XPROMO
		mConfigureMap["Standard cursor"]			=	CValue(true, 1);
		mConfigureMap["System cursor"]				=	CValue(false, 1);
	#else
		mConfigureMap["Standard cursor"]			=	CValue(true, 1);
		mConfigureMap["System cursor"]				=	CValue(true, 1);
	#endif

#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX
        mConfigureMap["Standard cursor"]			=	CValue(false, 1);
		mConfigureMap["System cursor"]				=	CValue(false, 1);
#else


	mConfigureMap["System cursor"]				=	CValue(true, 1);
	mConfigureMap["Standard cursor"]			=	CValue(false, 1);

#endif


#endif
}

void CAppState::configureLoadDefaults()
{
	mConfigureMap.clear();
	configureInit();

	configureLoadConfig(CMainApplication::getCfgPath("settings.cfg"), false);

    // write default
    if(!fileExists(getFullName("options.cfg",true)))
        configureSave();
	configureLoad();


}

void CAppState::configureLoad()
{
	configureLoadConfig(getFullName("options.cfg",true));
}

void CAppState::configureSave() const
{
	configureSaveConfig(getFullName("options.cfg",true));
}

bool CAppState::IsParamSaveToCfg(const String& s)
{
	ConfigureMapIterator iter = mConfigureMap.find(s);

	if (iter != mConfigureMap.end())
	{
		if (iter->second.saveToReg)
			return true;
	}

	return false;
}

bool CAppState::fileExists(const Ogre::String& filename) const
{
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
    FILE *f = fopen(filename.c_str(),"r");
	if(!f)
	{
		return false;
	}
	fclose(f);

    return true;
#else
	struct stat sb;
	int32_t res = stat(filename.c_str(), &sb);
	if(res == 0 && sb.st_mode & S_IFREG)
		return true;

	AAsset* asset = AAssetManager_open(mAssetMgr, filename.c_str(), AASSET_MODE_UNKNOWN);
	if(asset)
	{
		AAsset_close(asset);
		return true;
	}
	return false;
#endif
}

void CAppState::configureLoadConfig(const String& filename, bool save_params)
{
	//checking existence
	if(!fileExists(filename))
	{
		toLogEx("\nERROR: Can't find confing file %s", filename.c_str());
		return;
	}

	Ogre::ConfigFile cf;

	try
	{
#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
		cf.load(filename);
#else
		if(save_params)
		{
			cf.load(filename);
		}
		else
		{
			cf.load(openAPKFile(filename));
		}
#endif
	}
	catch (Ogre::Exception ex)
	{
		toLogEx("\nERROR: File %s not found!\n", filename.c_str());
		return;
	}

	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	String secName, param, value;
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			param = i->first;
			value = i->second;
			mConfigureMap[param] = CValue(value, save_params);
		}
	}
}

void CAppState::configureSaveConfig(const String& filename) const
{
	toLogEx("\n configureSaveConfig %s", filename.c_str());

	FILE *pFile = fopen(filename.c_str(), "w+");
	if (pFile)
	{
		fprintf(pFile,"[Options]\n");

		for(ConfigureMapIteratorConst iter = mConfigureMap.begin();
			iter != mConfigureMap.end();
			++iter )
		{
			if (iter->second.saveToReg)
				fprintf(pFile, "%s=%s\n", iter->first.c_str(), iter->second.text.c_str());
		}
		fclose(pFile);
	}
	else
		toLogEx("ERROR: Can't open config file '%s' for writing\n", filename.c_str());
}

String CAppState::getLocalTimeString()
{
	time_t rawtime;
	time ( &rawtime );
	String s( ctime(&rawtime) );
	s.erase(s.end()-1,s.end());
	return s;
}

String CAppState::getAppDataDirectoryName() const
{
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	return CMainApplication::getInternalPath();
#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX
    String s = app.mFSLayer->getWritablePath("");
    s = s.substr(0,s.length()-1);

    toLogEx("getAppDataDirectoryName: %s", s.c_str());
    return s;
#else
	char path[MAX_PATH];
	String appdatapath;
	bool path_ok = false;

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	HRESULT status = SHGetFolderPath(NULL, CSIDL_APPDATA|CSIDL_FLAG_CREATE, NULL, 0, path);
	if (status == S_OK)
	{
		appdatapath += String(path);
		path_ok= true;
	}
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	OSStatus status = noErr;
	FSRef prefsFSRef;
	status = FSFindFolder(kUserDomain, kPreferencesFolderType, TRUE, &prefsFSRef);
	if (status == noErr)
	{
		status = FSRefMakePath(&prefsFSRef, (UInt8 *) path, sizeof(path));
		if (status == noErr)
		{
			path_ok = true;
			appdatapath += String(path);
		}
	}
#else
	return CMainApplication::getBundlePath();
#endif
	if (path_ok)
	{
		String appname;
		if (hasValue("opt_name"))
			appname = getValue("opt_name").text;
		else
			appname = app.getAppFullName();
		appdatapath += DIR_DELIMITER + app.getCompanyName() + DIR_DELIMITER + appname;
	}
	else
		toLogEx("ERROR: CAppState::getAppDataDirectoryName(): can't find user preferences folder\n");
	return appdatapath;
#endif
}

String CAppState::getFullName(const String& name, bool create) const
{
	String s = getAppDataDirectoryName();
	if (create)
	{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		SHCreateDirectoryEx(NULL,s.c_str(),NULL);
#elif  OGRE_PLATFORM == OGRE_PLATFORM_LINUX
#else
        #pragma mark TODO: test me
		String mkdir_shell_command = String("mkdir -p ") + "\"" + s + "\"";
		system(mkdir_shell_command.c_str());
#endif
	}

	s += DIR_DELIMITER + name;

	return s;
}

void CAppState::variableInit(const String& name)
{
	mSave.clear();
	mSave.name = name;
}

bool CAppState::variableLoad(const String& name)
{
	return mapLoad(mSave, name);
}

void CAppState::variableSave(const String& name)
{
	mapSave(mSave, name);
}

int CAppState::getNameFromSave(const String& file, String& value)
{
	Save save;
	if (mapLoad(save, file))
	{
		value = save.name;
		return true;
	}
	else
	{
		value = "";
		return false;
	}
}

int CAppState::setNameToSave(const String& filename, const String& new_name)
{
	//! Need check
	Save save;
	if (mapLoad(save, filename))
	{
		save.name = new_name;
		mapSave(save, filename);
		return true;
	}
	else
		return false;
}

void CAppState::deleteVariableFile(const String& filename)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	SetFileAttributes(getFullName(filename).c_str(), 0);
#endif
	unlink(getFullName(filename).c_str());
}

bool CAppState::getMapStatus(const String& filename)
{
	Save save;
	return mapLoad(save, filename);
}

bool CAppState::mapLoad(Save &save, const String& filename)
{
	bool ret = false;
	save.clear();
	String path = getFullName(filename);

	FILE * file = fopen(path.c_str(), "rb");

#if OGRE_PLATFORM != OGRE_PLATFORM_LINUX
	if(!file)
	{
		path = CMainApplication::getResourcesPath(mConfigureMap["AdditionalSavePath"].text + filename);
		file = fopen(path.c_str(), "rb");
	}
#endif

	if (file)
	{
	    struct stat st;
		stat(path.c_str(),&st);

		Ogre::DataStreamPtr fp(new Ogre::FileHandleDataStream(file));
		if (!fp.isNull())
		{
			Ogre::ulong readUncompSize = 0;
			Ogre::ulong readSize = 0;
			Ogre::ulong check_summ = 0;
			fp->read(&readUncompSize, sizeof(readUncompSize));
			fp->read(&readSize, sizeof(readSize));
			fp->read(&check_summ, sizeof(check_summ));

			int id_size = 0;

			#if (OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS) && USE_XPROMO

				char od[41];
				fp->read(od, sizeof(od)-1);
				od[40] = 0;

				//REMOVE
				//toLogEx("!!!Code %s to %s\n", od,ODINCStr());

				if(strcmp(od,ODINCStr()) != 0)
				{
					//REMOVE
					//toLogEx("!!!Not the same");
					return false;
				}
				else
				{
					//toLogEx("!!!The same! Good!");

				}

				id_size = sizeof(od) - 1;

			#endif


			if (readUncompSize>0 && readSize>0 &&
				readSize+sizeof(readSize)*4 + id_size == st.st_size &&
				check_summ == readSize+readUncompSize)
			{
				Ogre::uchar *readBuf = new Ogre::uchar[readSize];
				if (readBuf)
				{
					fp->read(readBuf, readSize);
					fp->read(&check_summ, sizeof(check_summ));
					if (check_summ == readSize+readUncompSize)
					{
						Ogre::uchar *decompBuf = new Ogre::uchar[readUncompSize+1];
						memset(decompBuf,0,readUncompSize);
						int res = uncompress(decompBuf,&readUncompSize, readBuf, readSize);
						if (res == Z_OK)
						{
							decompBuf[readUncompSize] = '\0';

							////////////////////////////////////////////////////
							try
							{
								XmlParser parser((char*)decompBuf, false);
//								if (!doc.Error())
								{
									const XmlNode* root = parser.getDoc().first_node(MAPXML_ROOT);
									if(root)
									{
										const XmlNode* name = root->first_node(MAPXML_NAME);
										if (name)
										{
											const char* text = name->value();
											save.name = text ? text : Ogre::EmptyString;
										}

										const XmlNode* curstage = root->first_node(MAPXML_CURSTAGE);
										if (curstage)
										{
											for(const XmlNode* elem = curstage->first_node();
												elem;
												elem = elem->next_sibling())
											{
												const XmlAttrib* value = elem->first_attribute(MAPXML_VALUE);
												if (value)// && !value->empty())
												{
													const char* text = elem->value();
													save.curStage[value->value()] = text ? text : Ogre::EmptyString;
												}
											}
										}

										const XmlNode * stages = root->first_node(MAPXML_STAGES);
										if (stages)
										{
											Stage saveStage;

											int index = 0;
											for(const XmlNode* stage = stages->first_node(MAPXML_STAGE);
												stage;
												stage = stage->next_sibling(MAPXML_STAGE), ++index)
											{
												if (stage)
												{
													save.stage.push_back(saveStage);

													for(const XmlNode* elem = stage->first_node();
														elem;
														elem = elem->next_sibling())
													{
														const XmlAttrib* value = elem->first_attribute(MAPXML_VALUE);
														if (value)// && !value->empty())
														{
															const char* text = elem->value();
															save.stage[index][value->value()] = text ? text : Ogre::EmptyString;
														}
													}
												}
											}
										}
										ret = true;
									}
								}
//								else
//									toLogEx("Error parsing %s file with '%s'\n", path.c_str(), doc.ErrorDesc());
							}
							catch (...)
							{
								toLogEx("Error parsing %s file\n", path.c_str());
							}
							//////////////////////////////////////////////////////
						}
						SAFE_DEL_A(decompBuf);
					}
					SAFE_DEL_A(readBuf);
				}
			}
			fp.setNull();
		}


#if OGRE_PLATFORM != OGRE_PLATFORM_LINUX
    fclose(file);
#endif



	}
	return ret;
}

void CAppState::mapSave(Save &save, const String& filename)
{
	String text;

	text += "<" + String(MAPXML_ROOT) + ">\n";
	text += "\t<" + String(MAPXML_NAME) + ">" + save.name + "</" + String(MAPXML_NAME) + ">\n";
	text += "\t<" + String(MAPXML_CURSTAGE) + ">\n";

	for (StageIteratorConst iter = save.curStage.begin(); iter != save.curStage.end(); ++iter)
	{
		text += "\t\t<" + String(MAPXML_ITEM) + " " + String(MAPXML_VALUE) + "=\"" + iter->first + "\">" + iter->second + "</" + String(MAPXML_ITEM) + ">\n";
	}
	text += "\t</" + String(MAPXML_CURSTAGE) + ">\n";
	text += "\t<" + String(MAPXML_STAGES) + ">\n";
	for(size_t i = 0; i != save.stage.size(); i++)
	{
		text += "\t\t<" + String(MAPXML_STAGE) + " " + String(MAPXML_VALUE) + "=\"" + Ogre::StringConverter::toString(i) + "\">\n";
		for (StageIteratorConst iter = save.stage[i].begin(); iter != save.stage[i].end(); ++iter)
		{
			text += "\t\t\t<" + String(MAPXML_ITEM) + " " + String(MAPXML_VALUE) + "=\"" + iter->first + "\">" + iter->second + "</" + String(MAPXML_ITEM) + ">\n";
		}
		text += "\t\t</" + String(MAPXML_STAGE) + ">\n";
	}
	text += "\t</" + String(MAPXML_STAGES) + ">\n";
	text += "</" + String(MAPXML_ROOT) + ">\n";


	const char* data = text.c_str();
	uLong uncomp_size = strlen(data);
	uLong comp_size = compressBound(uncomp_size);

	Ogre::uchar *packedData = new Ogre::uchar[comp_size];
	compress(packedData, &comp_size, (Bytef*)data, uncomp_size);

//#ifdef _DEBUG
if(app.logEnabled())
{
	FILE *f = fopen(getFullName(filename + String(".xml"), true).c_str(), "w");
	fwrite(data,uncomp_size,1,f);
	fclose(f);
}
//#endif

	//-----------
	FILE* mpfFile = fopen(getFullName(filename, true).c_str(), "wb");
	if (mpfFile)
	{
		mStream = Ogre::DataStreamPtr(new Ogre::FileHandleDataStream(mpfFile, Ogre::DataStream::WRITE));
		if (!mStream.isNull())
		{
			uLong check_summ = uncomp_size + comp_size;
			writeData(&uncomp_size, sizeof(uLong), 1);
			writeData(&comp_size, sizeof(uLong), 1);
			writeData(&check_summ, sizeof(uLong), 1);

			#if (OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS) && USE_XPROMO

			const char *od = ODINCStr();

			//REMOVE
			//toLogEx("!!!Saving %s \n", od);

			writeData(od, sizeof(char), 40);

			#endif


			writeData(packedData, comp_size, 1);
			writeData(&check_summ, sizeof(uLong), 1);
		}
		else
			toLogEx("Error: DataStream is null! \n");
		mStream->close();
	}
	else
	{
		toLogEx("Error: can not open '%s'! \n", getFullName(filename, true).c_str());
	}
	SAFE_DEL_A(packedData);
}

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
Ogre::DataStreamPtr CAppState::openAPKFile(const Ogre::String& fileName)
{
    Ogre::DataStreamPtr stream;
    AAsset* asset = AAssetManager_open(mAssetMgr, fileName.c_str(), AASSET_MODE_BUFFER);
    if(asset)
    {
        off_t length = AAsset_getLength(asset);
        void* membuf = OGRE_MALLOC(length, Ogre::MEMCATEGORY_GENERAL);
        memcpy(membuf, AAsset_getBuffer(asset), length);
        AAsset_close(asset);

        stream = Ogre::DataStreamPtr(new Ogre::MemoryDataStream(membuf, length, true, true));
    }
	else
	{
		toLogEx("Error: can not open '%s' inside the apk! \n", fileName.c_str());
	}
    return stream;
}
#endif
