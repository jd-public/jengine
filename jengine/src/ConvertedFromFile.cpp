
#include "stdafx.h"

#include "MainApplication.h"
#include "ConvertedFromFile.h"
#include "ScriptMsg.h"
#include "TextRenderer.h"


CGameState GameState;

CConvertedFromFile::CConvertedFromFile(const String& scrName)
	: CVirtualScript(scrName)
{
	mScriptType = CONVERTED_SCRIPT;
}

void CConvertedFromFile::proceedMsg(const CScriptMsg * pMsg)
{
	try
	{		
		switch(pMsg->messageId)
		{
		case CScriptMsg::MSG_CREATE:
			toLogEx("\nON_CREATE: %s", mScriptName.c_str());
			ON_CREATE(); 
			break;
		case CScriptMsg::MSG_DESTROY: 
			ON_DESTROY(); 
			break;
		case CScriptMsg::MSG_SETFOCUS: 
			ON_SETFOCUS(); 
			break;
		case CScriptMsg::MSG_KILLFOCUS: 
			ON_KILLFOCUS(); 
			break;
		case CScriptMsg::MSG_ACTIVATE: 
			ON_ACTIVATE(); 
			break;
		case CScriptMsg::MSG_DEACTIVATE: 
			ON_DEACTIVATE(); 
			break;

		case CScriptMsg::MSG_TIME: 
			ON_TIME(pMsg->param.rparam);
			break;

		case CScriptMsg::MSG_LBUTTONDOWN: 
			ON_LBUTTONDOWN(Vector3(pMsg->param.x, pMsg->param.y, pMsg->param.z)); 
			ON_LBUTTONDOWN2(Vector2(pMsg->param.x, pMsg->param.y), getSourceHandle(pMsg)); 
			break;
		case CScriptMsg::MSG_LBUTTONUP:
			toLogEx("\n ConvertedFromFile lbuttonup %s %s", mScriptName.c_str(), CVirtualScript::getMyName(pMsg->pTargetObj->mHandle).c_str());
			ON_LBUTTONUP(Vector3(pMsg->param.x, pMsg->param.y, pMsg->param.z)); 
			ON_LBUTTONUP2(Vector2(pMsg->param.x, pMsg->param.y), getSourceHandle(pMsg)); 
			break;
		case CScriptMsg::MSG_RBUTTONDOWN: 
			ON_RBUTTONDOWN(Vector3(pMsg->param.x, pMsg->param.y, pMsg->param.z));
			ON_RBUTTONDOWN2(Vector2(pMsg->param.x, pMsg->param.y), getSourceHandle(pMsg)); 
			break;
		case CScriptMsg::MSG_RBUTTONUP: 
			ON_RBUTTONUP(Vector3(pMsg->param.x, pMsg->param.y, pMsg->param.z));
			ON_RBUTTONUP2(Vector2(pMsg->param.x, pMsg->param.y), getSourceHandle(pMsg)); 
			break;

		case CScriptMsg::MSG_NOTIFY:
			if (pMsg->pSourceObj)
			{
				ON_NOTIFY(CMsg(pMsg->param.lparam, pMsg->param.rparam, getSourceHandle(pMsg)));
			}
			else
			{
				ON_NOTIFY(CMsg(pMsg->param.lparam, pMsg->param.rparam, 0));
			}
			break; 
		
		case CScriptMsg::MSG_SOUND: 
			ON_SOUND(pMsg->param.lparam); 
			break;

		default:
			break;
		}
	}
	catch(Ogre::Exception ex)
	{
		::toLogEx("ERROR: Exception in script %s (%s)\n", mScriptName.c_str(), ex.getFullDescription().c_str());
		std::string text = mScriptName+":"+ex.getFullDescription();
		app.getInfoText()->addTextBox("TextError", text, 0, 50, 1024, 20);
		app.getInfoText()->setText("TextError", text);
	}
}
