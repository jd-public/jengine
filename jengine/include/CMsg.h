#pragma once

class GAME_API CMsg
{
public:
	CMsg(int l=0, int r=0, OBJECT_HANDLE s=0) : lparam(l), rparam(r), sender(s){}

	void clear()
	{
		lparam = 0;
		rparam = 0;
		sender = 0;
	}

public:
	int lparam, rparam;
	OBJECT_HANDLE sender;

	CMsg replaceSender(OBJECT_HANDLE sender)	{return CMsg(lparam, rparam, sender);}
};

class CValue
{
public:
	CValue()  : saveToReg(0) {}
	CValue(bool s, int r = 0) 
	{ 
		char buf[128];
		sprintf(buf,"%d",s);
		
		_init(buf,r);
	}

	CValue(int s, int r = 0)  
	{ 
		char buf[128];
		sprintf(buf,"%d",s);

		_init(buf,r);
	}

	CValue(float s, int r = 0) 
	{
		char buf[128];
		sprintf(buf,"%f",s);

		_init(buf,r);
	}
	
	CValue(const char* s, int r = 0) {	_init(s,r); }
	CValue(const Ogre::String& s, int r = 0)  { _init(s,r); }

	Ogre::String text;
	int saveToReg;

	void _init(const Ogre::String &s, int r) {	text = s; saveToReg = r; }
};