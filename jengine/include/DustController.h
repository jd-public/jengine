#pragma once

#include <OgreSimpleRenderable.h>

//#include "GameObject.h"
//#include "SfxCore.h"

//using namespace Ogre;

namespace Ogre{

class DustController : public SimpleRenderable 
{
//	static const int PARTICLE_COUNT = 60;

	class ParamIndexer
	{
		size_t mOffset;
		size_t *mSize;
	public:
		ParamIndexer() : mOffset(0), mSize(0){}
		ParamIndexer(size_t _offset, size_t *_size) : mOffset(_offset), mSize(_size) {}

		size_t getIndex(size_t vertex_offset) const;
	};

	class VertexCounter
	{
		size_t mCount;
	public:
		VertexCounter(): mCount(0){}
		size_t getVertexIndex();
		size_t getCount();
	};
	class Triangle
	{
		size_t mVertexIndex[3];
		static void setVector3(float *pos_buf, int i, const Ogre::Vector3 &pos);
		static void setVector2(float *pos_buf, int i, const Ogre::Vector2 &pos);
	public:
		void getIndex(VertexCounter &counter);
		void setParam(float *pos_buf, const ParamIndexer& indexer, const Ogre::Vector3 &v1, const Ogre::Vector3 &v2, const Ogre::Vector3 &v3);
		void setParam(float *pos_buf, const ParamIndexer& indexer, const Ogre::Vector2 &v1, const Ogre::Vector2 &v2, const Ogre::Vector2 &v3);
	};

	class DustParticle
	{
		static const int TRIANGLE_COUNT = 2;

		Triangle mTriangle[TRIANGLE_COUNT];
		Ogre::Vector2 mPosition;
		Ogre::Vector2 mVelocity;
		Ogre::Real mSizeX;
		Ogre::Real mSizeY;
		Ogre::Real mPhase;
		Ogre::Real mPhaseSpeed;


		Ogre::Vector2 getCorner(int hor, int vert);
		static Ogre::Vector3 convertPos(Ogre::Vector2);

	public:
		void initialise(VertexCounter& counter, Ogre::Real size_x, Ogre::Real size_y);
		void update(Ogre::Real ellapseTime);
		void updatePosition(float *pos_buf, const ParamIndexer& indexer);
		void updateTCoord1(float *pos_buf, const ParamIndexer& indexer);
		void updateTCoord2(float *pos_buf, const ParamIndexer& indexer);
	};
	class Buffer
	{
		HardwareVertexBufferSharedPtr mBuffer;
		int mId;
		size_t mSize;

	public:
		Buffer(int id): mId(id){}
		ParamIndexer addElement(const RenderOperation &renderOp, VertexElementType, VertexElementSemantic, unsigned short tcoord = 0);
		void create(const RenderOperation &renderOp, HardwareBuffer::Usage usage);
		float* lock();
		void unlock();
	};

public:
	DustController(int count, Ogre::Real width, Ogre::Real height);
	~DustController();

	void update(Ogre::Real ellapseTime);
	void setUserAny(const Any& anything);

private:
	Buffer mDynamicBuffer;
	Buffer mStaticBuffer;

	Controller<Ogre::Real>* mTimeController;
	std::vector<DustParticle> mParticle;
	int mParticleCount;
	float mWidth, mHeight;
	VertexCounter mVertexCounter;
	ParamIndexer mPositionIndexer;
	ParamIndexer mTCoord1Indexer;
	ParamIndexer mTCoord2Indexer;

	void getWorldTransforms(Matrix4* xform) const;
	Ogre::Real getBoundingRadius(void) const {return 0;}
	Ogre::Real getSquaredViewDepth(const Camera* cam) const {return 0;};

	void initialise();
	void updateDynamicBuffer();
	void updateConstBuffer();
};



/** Factory object */
class DustControllerFactory : public MovableObjectFactory
{
public:
	DustControllerFactory();
	~DustControllerFactory();

	static const String FACTORY_TYPE_NAME;

	const String& getType(void) const;
	void destroyInstance( MovableObject* obj);  

protected:
	MovableObject* createInstanceImpl( const String& name, const NameValuePairList* params);
};




}