#pragma once

#include "GameObject.h"

class DynamicLines;

class CAnimatableObject : public CGameObject
{
public:
	enum BlendType
	{
		NoneBlend,
		TimeBlend,
		SpeedBlend		
	};
	
	struct AnimationStateEx
	{
		Ogre::AnimationState *pAnimState;
		
		BlendType blendType;
		Real blendSpeed;
		Real blendZeroTime;
		Real blendOneTime;
		
		Real timeToBlend;	
		Real maxBlendSpeed;	
		
		Real moveSpeed;
		Real rotSpeed;

		Real animSpeed;
		bool enable;
		size_t curPoint;

		size_t layer;

		Real totalPlayTime;

		Real fLen;
	};

	struct BlendLayer
	{
		String mName;
		int id;
		std::vector<Ogre::Node*> mBones;

		BlendLayer(int _id, const String& _mName) { id = _id; mName = _mName; }
	};

	struct AnimationLayer
	{
		int anim;			//-1 - layer not used
		mutable int ntimes;
		int max_ntimes;
		int anim_speed;
		size_t layer;
		float cur_time;
		float start_time;
		float len_time;
		int type;			//1 - ntimes, 0 - end_time
		size_t sound_id;
	};

	class AnimationPoint
	{
	public:
		#define MAX_LAYERS 10;

		void init(CAnimatableObject& animObj, size_t previous=(size_t)-1);

		void addLayer(size_t layer, int anim, int ntimes, int anim_speed)
		{
			AnimationLayer a;
			a.anim = anim;
			a.layer = layer;
			a.ntimes = ntimes;
			a.max_ntimes = ntimes;
			a.anim_speed = anim_speed;
			a.type = 1;
			a.sound_id = 0;

			mLayers.push_back(a);
		}

		void addLayer(int layer, int anim, float len_time)
		{
			AnimationLayer a;
			a.anim = anim;
			a.type = 0;
			a.len_time = len_time;
			a.layer = layer;
			a.sound_id = 0;

			mLayers.push_back(a);
		}

		void addLayer(int layer, int anim, size_t sound_id)
		{
			AnimationLayer a;
			a.anim = anim;
			a.type = 0;
			a.sound_id = sound_id;
			a.layer = layer;

			mLayers.push_back(a);
		}

		size_t layers() const
		{
			return mLayers.size();
		} 

		size_t hasLayer(size_t layer) const
		{
			for(size_t i = 0; i < layers(); i++)
			{
				if (mLayers[i].layer == layer)
					return i+1;
			}
			return 0;
		} 

		size_t maxLayer() const
		{
			size_t max = 0;
			for(size_t i = 0; i < layers(); i++)
			{
				if (mLayers[i].layer > max)
					max = mLayers[i].layer;
			}

			return max;
		}

		size_t sound_id(size_t layer) const	{ size_t il=hasLayer(layer); if (il>0) return mLayers[il-1].sound_id; else return -1; }
		int anim(size_t layer)	const		{ size_t il=hasLayer(layer); if (il>0) return mLayers[il-1].anim; else return -1; }
		int anim_speed(size_t layer) const	{ size_t il=hasLayer(layer); if (il>0) return mLayers[il-1].anim_speed; else return -1; }
		int& ntimes(size_t layer) const		{ size_t il=hasLayer(layer); if (il>0) return mLayers[il-1].ntimes; else { static int none_il=-1; return none_il; } }

	public:
		Vector3 mStartPos;
		Vector3 mEndPos;
		Quaternion mRotation;
		
		std::vector<AnimationLayer> mLayers;
		
		bool mNeedToRot;
		bool mNeedToMove;

		Real mMoveSpeed;
        		
		OBJECT_HANDLE mTarget;
		int mParam;
	};

public:
	CAnimatableObject();
	CAnimatableObject(const String& scrName);
	virtual ~CAnimatableObject();

	void init();
	void initLayers();
	void initAnim();

	virtual void processFrame( const Ogre::FrameEvent& evt);

	bool isNextAnimNotSame(size_t layer, bool skip_all=false);
	bool incrementCurPoint();
	void normalizeAnimBlendings();

	int findBoneInLayer(Ogre::Node *, size_t layer);
	
	void addAnimation(const String& skeleton, 
		const String& anim, 
		Real moveSpeed, 
		Real blendPercent,
		Real,
		int);

	int findAnim(const String& name);
	Ogre::Animation *findLinkedAnimByName(const String& name);

	float getAnimationRotSpeed(const String& animName);
	void setAnimationParam(const String& skeleton, int param, float val);

	static Quaternion _getRotationTo(const Vector3& src, const Vector3& dest,
						  const Vector3& fallbackAxis = Vector3::ZERO);
	void setDestRotationToPoint(Vector3 &point);

	void processMovement(Real inc, const AnimationPoint& point);
	void processAnimations(Real inc, const AnimationPoint& point);
	void updateSkeleton();
	void normaliseAnimWeights(size_t n);
	void setBlendState(int anim, Real speed);

	void addPoint(const String& anim, int ntimes, int speed, OBJECT_HANDLE target, int param);
	void addPoint(const String& anim, int ntimes, int speed, const String& anim2, size_t snd_id, OBJECT_HANDLE target, int param);
	void addPoint(const String& anim, const Vector3 &endPos, const Quaternion& endRot, bool carePos, bool careRot, OBJECT_HANDLE target, int param);
	void addPoint(const String& anim, const Vector3 &endPos, const Quaternion& endRot, bool carePos, bool careRot, const String& anim2, size_t snd_id, OBJECT_HANDLE target, int param);
	void addPoint(const AnimationPoint& point);
	void finishPoint(const AnimationPoint& point, bool all=false);

	void clear();
	void start();
	void stop();
	void skip(bool skip_all=false);

	void setLooped(bool set) { mIsLooped = set; }

private:
	void sendMsgToAnimPointTarget(const AnimationPoint& p);
	size_t nextPointIdx();
	bool isEndPoint(size_t point_idx);

public:
	bool mIsLooped;
	bool mIsPlay;

	size_t mCurPoint;
	Quaternion mDestRot;
	Quaternion mScrRot;
	bool mRotation;
	Real mRotProgress;
	Real mRotSpeed;

	std::vector<AnimationPoint> mPointList;
	std::vector<AnimationStateEx> mAnimsList;
	std::vector<BlendLayer> mLayers;

private:
	Ogre::SkeletonInstance *pBaseSkel;
};