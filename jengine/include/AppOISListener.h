#pragma once

#include "config.h"

#include <OgrePrerequisites.h>
#include <OgreVector2.h>

#if USE_OIS_FOR_INPUT == ON
	#include <OIS.h>
	#include "EventPlayer.h"

	class EventPlayer;
#endif


//////////////////////////////////////////////////////////
class CAppOISListener

#if USE_OIS_FOR_INPUT == TRUE
	:
	#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE || OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
		public OIS::MultiTouchListener
	#else
		public OIS::KeyListener,
		public OIS::MouseListener
	#endif

#endif
{
public:

		CAppOISListener(Ogre::Root *pRoot,
#if USE_OIS_FOR_INPUT == TRUE
							EventPlayer::TPlayerMode eventPlayerMode,
#endif
						const Ogre::String& filename);

	~CAppOISListener();

	void addCursorCustom(int id, const Ogre::String& name);
	void addCursorStandard(int id, char* name);
	void addCursorMaterial(int id, const Ogre::String& material);

	bool isShowCursor() const { return mCursorShow; }
	void showCursor(bool fshow = true);
	void hideCursor() { showCursor(false); }

	void setCursorDefaultId(int id) { mCursorDefaultId = id; }
	void setCursor(int id);
	void setCursorDefault() { setCursor(mCursorDefaultId); }
	void updateCursorImage();
	void updateCursorCustom();
	void updateCursorStandard();
	void updateCursorUseTextures(bool visible);
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	void SetWin32Cursor(HCURSOR hCur);
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	void SetMacCursor(bool visible);
	bool mouseOtherEvents( int idd );
	bool mMacCursorVisible;
#endif
	//void movePhysicalCursorToGameCursor(bool in_fullscreen);

	void setCursorStandard(bool state);
	void setCursorUseTextures(bool state);
	void initCustomTextureCursors();
	void updateCursorPosition();

	const Ogre::Vector2& GetMousePosition() const { return m_vMousePos; }
	void updateMousePosition() {m_vMousePos = m_vMousePosNew;}

	void Update();


	void _appMouseMoved(float abs_screen_x, float abs_screen_y, float rel_z, bool lbutton, bool rbutton);
	void _appMousePressed ( int button ); //0 - left, 1 - right, 2 - middle
	void _appMouseReleased( int button ); //0 - left, 1 - right, 2 - middle

#if USE_OIS_FOR_INPUT == TRUE
	#	if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE || OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
		static void transformInputState(OIS::MultiTouchState& state);
	#	else
		static void transformInputState(OIS::MouseState& state);
	#	endif


	#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE || OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
		bool touchMoved(const OIS::MultiTouchEvent& arg);
		bool touchPressed(const OIS::MultiTouchEvent& arg);
		bool touchReleased(const OIS::MultiTouchEvent& arg);
		bool touchCancelled(const OIS::MultiTouchEvent& arg);
		bool appMouseMoved(const OIS::MultiTouchEvent& arg);
		bool appMousePressed(const OIS::MultiTouchEvent& arg);
		bool appMouseReleased(const OIS::MultiTouchEvent& arg);

		OIS::MultiTouch* mMouse;        // multitouch device
		OIS::JoyStick* mAccelerometer;  // accelerometer device
	#else


		virtual bool mouseMoved( const OIS::MouseEvent &arg ) OVERRIDE;
		virtual bool mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id) OVERRIDE;
		virtual bool mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id) OVERRIDE;
		virtual bool keyPressed(const OIS::KeyEvent& arg) OVERRIDE;
		virtual bool keyReleased(const OIS::KeyEvent& arg) OVERRIDE;
		bool appMouseMoved(const OIS::MouseEvent& arg);
		bool appMousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id);
		bool appMouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id);

		OIS::Mouse *mMouse;
		OIS::Keyboard *mKeyboard;
	#endif

	bool appKeyPressed( const OIS::KeyEvent& arg);
	bool appKeyReleased( const OIS::KeyEvent& arg);


	OIS::InputManager* mInputManager;
	EventPlayer *mEventPlayer;

#endif


	void checkMouseSwap();

	void setWndSize(unsigned int width, unsigned int height);

public:
	bool bMouseButtonsSwapped;

	bool mKeys[256];
	bool m_bLMouseButton;
	bool m_bRMouseButton;
	bool mCursorShow;
	bool mAzVisible;
	bool mCursorStandard;
	bool mCursorUseTextures;

	int mCursorId;
	Ogre::Overlay *mCursorOverlay;
	Ogre::OverlayContainer *mCursorContainer;


	Ogre::Root* m_pRoot;
	Ogre::Vector2 m_vMousePos;
	Ogre::Vector2 m_vMousePosNew;



	unsigned int wndW;
	unsigned int wndH;



	float mSensivity;

	int mCursorDefaultId;
	std::map<int, String> mCursorsMaterial;

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	std::map<int, HCURSOR> mCursorsCustom;
	std::map<int, HCURSOR> mCursorsStandard;
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	WindowRef mWindow;
	int mCursorsCustom[10];
#endif


};


