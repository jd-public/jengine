#pragma once

#include <OgrePrerequisites.h>

class SStringTable
{
public:

	SStringTable();
	~SStringTable();

	void add(const String& key, Ogre::DisplayString &data);
	void load(const String& fileName);
	Ogre::DisplayString getString(const String& id);
	bool isStringExist(const String& id);

	Ogre::DisplayString getString(int i);

	void createFontdef();

	typedef OGRE_HashMap<String, Ogre::DisplayString> TStringMap;
private:
	TStringMap mTable;

	bool mScanCodePoints;
	std::set<Ogre::UTFString::code_point> mCodePoints;

public:
		// 
	TStringMap& getStringMap();
	TStringMap* getStringMapPtr();
};
