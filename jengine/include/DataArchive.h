#pragma once

#include <OgrePrerequisites.h>

#include <OgreArchive.h>
#include <OgreArchiveFactory.h>
#include <vector>

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	//#define __USE_WINDOWS_LOW_LEVEL_IO__
#endif

/** Specialisation of the Archive class to allow reading of files from a URL.
*/
class MemBuffer
{
public:
	MemBuffer() { init(); }
	MemBuffer(Ogre::ulong bytes) { init(); alloc(bytes); }

	void init()
	{
		pBuf = 0;
		used = 0;
		size = 0;
	}

	Ogre::uchar* alloc(Ogre::ulong bytes) 
	{
		pBuf = new Ogre::uchar[bytes];
		if (pBuf)
		{
			size = bytes;
			used = 1;
			return pBuf;
		}

		return NULL;
	}

	void delloc()
	{
		SAFE_DEL_A(pBuf);
		init();
	}

	void _free() { used = 0; }

	int isUsed() { return used; }
	void setUsed(int u) { used = u; }
	Ogre::uchar* getPtr() { return pBuf; }
	Ogre::ulong getSize() { return size; }

private:
	Ogre::uchar *pBuf;
	int used;
	Ogre::ulong size;
};

class SimpleMemManager
{
public:
	// 1 Mb for buffers
	SimpleMemManager() { mMemLimit = 1024*1024; }

	int findUnused(Ogre::ulong bytes)
	{
		int res = -1;
		Ogre::ulong prev_size = (Ogre::ulong)-1;
		for(size_t i = 0; i < mMemArray.size(); i++)
		{	
			MemBuffer *m = &mMemArray[i];

			if (!m->isUsed())
			{
				if (res == -1)
					res = static_cast<int>(i);

				if (m->getSize() < prev_size && m->getSize() >= bytes)
					res = static_cast<int>(i);
				prev_size = m->getSize();
			}
		}
		return res;
	}

	int findByPointer(Ogre::uchar *ptr)
	{
		int res = -1;

		for(size_t i = 0; i < mMemArray.size(); i++)
		{	
			if (mMemArray[i].getPtr() == ptr)
				return static_cast<int>(i);
		}

		return res;
	}

	Ogre::uchar *addNewBuffer(Ogre::ulong bytes)
	{
		MemBuffer m(bytes);
		mMemArray.push_back(m);
		return m.getPtr();
	}

	Ogre::uchar* alloc(Ogre::ulong bytes)
	{
		size_t id = findUnused(bytes);

		if (id == (size_t)-1)
		{
			return addNewBuffer(bytes);
		}
		else
		{
			MemBuffer *m = &mMemArray[id];

			if (bytes <= m->getSize())
			{
				m->setUsed(1);
			}
			else
			{
				if (m->getSize())
					m->delloc();

				m->alloc(bytes);
			}
			return m->getPtr();
		}
	}

	void _free(Ogre::uchar *buf)
	{
		if (!buf)
			return;

		int id = findByPointer(buf);

		if (id == -1)
			return;

		if (getTotalMem() > mMemLimit)
		{
			mMemArray[id].delloc();
		}
		else
			mMemArray[id]._free();
	}

	Ogre::ulong getTotalMem()
	{
		int total = 0;
		for(size_t i = 0; i < mMemArray.size(); i++)
			total+=mMemArray[i].getSize();
		return total;
	}

	void freeAll()
	{
		for(size_t i = 0; i < mMemArray.size(); i++)
		{
			mMemArray[i].delloc();
		}
		mMemArray.clear();
	}

private:
	std::vector<MemBuffer> mMemArray;
	unsigned int mMemLimit;
};

#ifdef __USE_WINDOWS_LOW_LEVEL_IO__

class FileDataStream
{
public:

	struct _data
	{
		Ogre::uchar *buf;
		OVERLAPPED ov;
		Ogre::ulong pos;
		Ogre::ulong count;
	};

public:
	virtual ~FileDataStream();
	static void open(const String& name);
	static size_t read(void* buf, Ogre::ulong count);
	static void skip(Ogre::ulong count);
	static void seek(Ogre::ulong pos );
	static size_t tell();
	static bool eof();
	static void close();

public:
	static HANDLE hFile; 
	static Ogre::ulong mPos;
	static Ogre::ulong mState;

	static OVERLAPPED ov;
	static Ogre::uchar *mBuf;
	static size_t mBufSize;
	static size_t mBufPos;
};

#endif

class DataDataStream : public Ogre::DataStream
{
public:
	DataDataStream(Ogre::ulong _dataPos, Ogre::ulong _fileSize, Ogre::ulong _filePackedSize, Ogre::DataStream *_mainStream, const Ogre::String& name);
	~DataDataStream();

	void _init();
	void _readData();

	size_t readLine(char* buf, size_t maxCount, const Ogre::String& delim = "\n");
	size_t skipLine(const Ogre::String& delim = "\n");
	size_t read(void* buf, size_t count);
	void skip(long count);
	void seek( size_t pos );
	size_t tell() const;
	bool eof() const;
	void close();

private:
	Ogre::ulong mDataPos;
	Ogre::ulong mFileSize;
	Ogre::DataStream *mMainStream;

	Ogre::uchar *read_buf;
	Ogre::ulong buf_read_size;
	Ogre::ulong curPos;
	Ogre::ulong mPackedSize;

	bool mDataReady;

	Ogre::uchar *mPos;
	Ogre::uchar *mEnd;

	Ogre::uchar *mReadBuffer;
	Ogre::ulong mReadBufferSize;
	Ogre::uchar *mDecompressBuffer;
	Ogre::ulong mDecompBufferSize;
};

class DataArchive : public Ogre::Archive 
{
public:
	DataArchive(const Ogre::String& name, const Ogre::String& archType );
	~DataArchive();

	/// @copydoc Archive::isCaseSensitive
	bool isCaseSensitive() const { return false; }

	/// @copydoc Archive::load
	void load();
	/// @copydoc Archive::unload
	void unload();

	/// @copydoc Archive::open
#if OGRE_VERSION_MINOR == 9
	Ogre::DataStreamPtr open(const Ogre::String& filename, bool readOnly=true) const;
#else
	Ogre::DataStreamPtr open(const Ogre::String& filename, bool readOnly=true);
#endif

	time_t getModifiedTime(const Ogre::String& filename) { return (time_t)0; }

	/// @copydoc Archive::list
	Ogre::StringVectorPtr list(bool recursive = true, bool dirs = false);

	/// @copydoc Archive::listFileInfo
	Ogre::FileInfoListPtr listFileInfo(bool recursive = true, bool dirs = false);

	/// @copydoc Archive::find
	Ogre::StringVectorPtr find(const Ogre::String& pattern, bool recursive = true, bool dirs = false);

	/// @copydoc Archive::findFileInfo
#if OGRE_VERSION_MINOR == 9
	Ogre::FileInfoListPtr findFileInfo(const Ogre::String& pattern, bool recursive = true, bool dirs = false) const;
#else
	Ogre::FileInfoListPtr findFileInfo(const Ogre::String& pattern, bool recursive = true, bool dirs = false);
#endif

	/// @copydoc Archive::exists
	bool exists(const Ogre::String& filename);

private:
	struct FileInfo
	{
		Ogre::String name;
		Ogre::String path;
		Ogre::ulong size;
		Ogre::ulong packed_size;
		Ogre::ulong pos;
		unsigned int flags;
		unsigned int unused;
	};
	typedef OGRE_HashMap<Ogre::String, FileInfo>::const_iterator FileListIterator;

private:
	bool findFile(Ogre::String& _filename, FileListIterator &it) const;

public:
	static SimpleMemManager mMem;

private:
	Ogre::DataStreamPtr mFileMemStream;
	//DataDataStream *pNext;

	FILE *file;
	int mSize;
	long totalSize;

	OGRE_HashMap<Ogre::String, FileInfo> mFileList;
	Ogre::ulong mDataPos;
};

/** Specialisation of ArchiveFactory for DataArchive files. */
class _OgrePrivate DataArchiveFactory : public Ogre::ArchiveFactory
{
public:
	virtual ~DataArchiveFactory() {}
	/// @copydoc FactoryObj::getType
	const Ogre::String& getType() const;
	/// @copydoc FactoryObj::createInstance
	Ogre::Archive *createInstance( const Ogre::String& name, bool readOnly ) 
	{
		return new DataArchive(name, "Data");
	}

	/// @copydoc FactoryObj::destroyInstance
	void destroyInstance( Ogre::Archive* arch) { delete arch; }
};

