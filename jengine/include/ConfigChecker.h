/*
 *  ConfigChecker.h
 *  JEngine
 *
 *  Created by Cool on 11.10.11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include "config.h"

#ifndef __CONFIGFILE__
#	error "Rename config.default to config.h"
#endif

#ifndef JENGINE_MOUSEOLDWORK
#	error "Need define JENGINE_MOUSEOLDWORK"
#endif

#ifndef USE_OIS_FOR_INPUT
#	error "Need define USE_OIS_FOR_INPUT"
#endif

//#ifndef USING_THEORA
//#	error "Need define USING_THEORA"
//#endif

#ifndef USE_BULLET
#	error "Need define USE_BULLET"
#endif
