#pragma once

#ifndef __STDAFX__H__
#define __STDAFX__H__

#include <OgrePlatform.h>

#include <OgrePrerequisites.h>
#include <OgreOverlayElement.h>
#include <OgreStringConverter.h>
#include <OgreException.h>
#include <OgreFrameListener.h>
#include <OgreNode.h>
#include <OgreSceneNode.h>
#include <OgreAnimationState.h>

#include <OgreOverlayElement.h>
#include <OgreOverlaySystem.h>

#include <OgreManualObject.h>

#if OGRE_VERSION_MINOR == 9
	#define  OGRE_HashMap HashMap
#endif

//using namespace Ogre;

#include "ConfigChecker.h"
#include "config.h"

#include "JEnginePlatform.h"
#include "CMsg.h"

#if USE_STEAM == TRUE
	#include "SteamGlue.h"
#endif

namespace Ogre
{
	static String EmptyString = "";
}

//OGRE_PLATFORM_IPHONE ����� ��������� ������, � �� ������ �� ������
//#if !defined(OGRE_PLATFORM_IPHONE) && defined (OGRE_PLATFORM_APPLE_IOS)
#define OGRE_PLATFORM_IPHONE OGRE_PLATFORM_APPLE_IOS
//#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	#define OVERRIDE override
#else
	#define OVERRIDE 
#endif


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	define WIN32_LEAN_AND_MEAN
#	define NOMINMAX		// required to stop windows.h messing up std::min
//#	define JENGINE_MOUSECIRCLECAST 0
	#include <windows.h>
	#include <mmsystem.h>

#elif OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
#	define OGRE_STATIC_LIB
#	define OGRE_STATIC_ParticleFX
#	define OGRE_STATIC_GLES
#	define USE_CADISPLAYLINK		0
//#	define JENGINE_MOUSECIRCLECAST	0
#   ifdef __OBJC__
#       import <UIKit/UIKit.h>
#   endif
#else
#	ifdef __APPLE_CC__
#	include <carbon/carbon.h>
#	endif
#endif
#include "log.h"

typedef Ogre::Real Real;
typedef Ogre::String String;
typedef Ogre::DisplayString DisplayString; 
typedef Ogre::Vector2 Vector2;
typedef Ogre::Vector3 Vector3;
typedef Ogre::Quaternion Quaternion;
typedef Ogre::Radian Radian;
typedef Ogre::Degree Degree;
typedef Ogre::Math Math;


#endif
