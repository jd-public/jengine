/*
 *  mainIOS.h
 *  JEngine
 *
 *  Created by user on 5/19/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <OgrePrerequisites.h>
#include <OgreFrustum.h>

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS

class CMainApplication;

class CAppGestureViewCpp
{
public:
	virtual ~CAppGestureViewCpp() {}
	virtual void init(CMainApplication *a) = 0;
	virtual void shutdown() = 0;
};

Ogre::OrientationMode getViewportOrientation();

void showKeyboard(bool bClearPlayerName);
void hideKeyboard();
std::string getKeyboardText();
void setKeyboardText(const std::string& text);

void applicationShowPausePicture();
void applicationHidePausePicture();
bool applicationIsPaused();

void applicationShowActivityIndicator();
void applicationHideActivityIndicator();

float applicationPlayVideo(const char *chFileName);
void applicationPauseVideo();
void applicationUnpauseVideo();
void applicationStopVideo();

void applicationUpdateCameraAspect();

bool isRetina();
bool isIPad();

#endif

