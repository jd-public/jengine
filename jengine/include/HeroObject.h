#pragma once

#include "GameObject.h"

class DynamicLines;

class CHeroObject : public CGameObject
{
public:
	enum AnimStates
	{
		ANI_STATIC,
		ANI_WALK,
		ANI_RUN,
		ANI_CUSTOM_01,
        ANI_CUSTOM_02,
		ANI_CUSTOM_03,
        ANI_CUSTOM_04,
		ANI_CUSTOM_05,

		ANI_DLG_01, //good
		ANI_DLG_02, //negative
		ANI_DLG_03, //bad
		ANI_DLG_04, //evil
		ANI_DLG_05, //pain

		ANI_LAST
	};

	struct AnimationBlendState
	{
		Real blendSpeed;
	};

public:
	CHeroObject();
	CHeroObject(const String& scrName);
	virtual ~CHeroObject();

	void init();
	void initAnim();

	virtual void processFrame( const Ogre::FrameEvent& evt);

	void resetTo(const Vector3 &pos, const Vector3 &rot);
	void _assignTo(CGameObject* pWalkPlane,
			const Vector3& pos, 
			const Vector3& rot,		
			bool isRun,		
			bool accuratePos,	
			bool assignRot,	
			CGameObject* _pTarget,
			bool _isMsgSend); 
	void assignTo(CGameObject* pWalkPlane,
				  const Vector3& pos, 
				  const Vector3& rot,		//It's not important, if assignRot not assigned
				  bool isRun,		//default: false
				  bool accuratePos,	//default: false
				  bool assignRot,	//default: false
				  CGameObject* pTarget); //default: this
	void statAnim(CGameObject* pTarget, const String& name); 
	void assignToAlt(CGameObject* pWalkPlane,
				  const Vector3& pos, 
				  const Vector3& rot,		//It's not important, if assignRot not assigned
				  bool isRun,		//default: false
				  bool accuratePos,	//default: false
				  bool assignRot,	//default: false
				  CGameObject* pTarget); //default: this

	void stop();

	void lookTo(const Vector3 &pos, CGameObject* _pTarget);
	 
	void _statAnim(CGameObject* _pTarget, const String& name);

	void setState(AnimStates state, Real weight = 1.0f, bool updateSkel = true);
	void updateSkeleton();
	void normaliseAnimWeights(size_t n);
	
	void createWayPoints(CGameObject* pWalkPlane, const Vector3& endPos);
	void findWPs(CGameObject* pWalkPlane, const Vector3& startPos, const Vector3& endPos);

	void setDestRotationToPoint(const Vector3& point);

	void setWaterMode(bool mode);

	void setBlendState(AnimStates state, Real speed = 0.1f);
	void playAnim(const String& name);
	
	Ogre::AnimationState *findAnimState(const String& skelName) const;

	static Quaternion _getRotationTo(const Vector3& src, const Vector3& dest,
									  const Vector3& fallbackAxis);

	void processMovement(Real inc);

public:
	std::vector<Vector3>	mWayPoints;
	unsigned short			mCurPoint;

	CGameObject* pTarget;

	DynamicLines *pWayLine,*pWayLine2;
	
	Quaternion scrRot;
	Quaternion destRot;
	Quaternion finalRot;
	bool mAssignRot;
	bool mIsRun;
	bool mIsTaunt;
	int mElapsedTaunt;
	int mInterval;
	int mEndTime;

	bool bWaterMode;
	bool isMsgSend;
	bool mRotation;
	Real mRotProgress;
	bool mMoving;

	Vector3 mStartPos;
	Vector3 mEndPos;
	Vector3 mLastPos;

	Ogre::AnimationState *pAnimState[ANI_LAST];
	AnimationBlendState mAnimBlendState[ANI_LAST];
};