#pragma once

#include "ScriptMsg.h"

#ifndef _COMPILED_SCRIPTS_

class seerInstance;
class seerScript;

class CFileScript : public CVirtualScript
{
public:
	CFileScript();
	~CFileScript();

	static std::map<String, seerScript*> mScriptsMap;
	//bool Create(LPCSTR scrName, LPCSTR resGroup = "General");

	bool			create(const String &scrName, const String &resGroup = String("General"));
	virtual			void proceedMsg(const CScriptMsg * pMsg);
	seerInstance	*GetInstance(void) { return prog; }
	void			*getVarAddr(char *name);
	void			setThis(unsigned int me); 
	
	static void	InitializeEnvironment( void );

protected:
	seerScript *script;
	seerInstance *prog;

	int msgFunc[CScriptMsg::MSG_LAST];

	String mName;
};

#endif //#ifndef _COMPILED_SCRIPTS_