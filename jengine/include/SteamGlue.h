#pragma once

namespace Steam
{

void init();
void shutdown();

const char*  getUserName();

void setAchievement(const Ogre::String &ach);

void clearAchievement(const Ogre::String &ach);

const char*  getLanguage();

void runCallbacks();

void showOverlay(int id);

void showWebPage(int id);

bool isOverlaysEnabled();

}