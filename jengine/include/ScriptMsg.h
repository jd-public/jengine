#pragma once

class CScriptMsg
{
public:
	enum MsgId
	{		
		MSG_CREATE,		
		MSG_DESTROY,		
		MSG_TIME,			
		MSG_LBUTTONDOWN,	
		MSG_LBUTTONUP,	
		MSG_RBUTTONDOWN,	
		MSG_RBUTTONUP,	
		MSG_SETFOCUS,		
		MSG_KILLFOCUS,	
		MSG_NOTIFY,
		MSG_SOUND,
		MSG_ACTIVATE,
		MSG_DEACTIVATE,

		SYS_EXTMESSAGE,
		SYS_SCENELOAD,	
		SYS_SCENEDELETE,

		MSG_LAST
	};

public:
	CScriptMsg ();

public:
	char targetName[32];
	CGameObject* pTargetObj;
	CGameObject* pSourceObj;
	MsgId messageId;

	union
	{
		struct { int lparam, rparam;};
		struct { float x, y, z;};
		struct { char str[64]; bool fUseLoader; };
		float v[16];
		int i[16];
		void * pVoid;
	} param;
};

