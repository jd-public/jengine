package com.jetdogs.SinisterCity;

import android.app.Activity;
import android.content.res.AssetManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;

public class JEngineAndroidBridge {
	public native static void initEngine(Activity actvity, AssetManager mgr, String internalDataPath);
	public native static void deInitEngine();
	
	public native static void initRenderWindow(Surface surface);
	public native static void termRenderWindow();
	public native static void renderOneFrame();
	
	public native static boolean onKeyDown(int keyCode, KeyEvent event);
	public native static boolean onKeyUp (int keyCode, KeyEvent event);
	public native static boolean onTouchEvent (MotionEvent event);
}
