package com.jetdogs.SinisterCity;

import android.app.Activity;
import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.inputmethod.InputMethodManager;

public class JInputInjection {
	private SurfaceView renderView = null;
	private Activity host = null;
	
	public JInputInjection(Activity parentAct, SurfaceView renderer) {
		host = parentAct;
		renderView = renderer;
	}
	
	public void showKeyboard(boolean show) {
		InputMethodManager inputManager = (InputMethodManager) host.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(show) {
			inputManager.showSoftInput(renderView, 0);			
		}
		else
		{			
			inputManager.hideSoftInputFromWindow(renderView.getWindowToken(), 0);
		}
	}
	
	public boolean injectKeyDown(int keyCode, KeyEvent event)
	{
		return JEngineAndroidBridge.onKeyDown(keyCode, event);		
	}
	
	public boolean injectKeyUp (int keyCode, KeyEvent event)
	{
		return JEngineAndroidBridge.onKeyUp(keyCode, event);		
	}
	
	public boolean injectTouchEvent (MotionEvent event)
	{
		float top = (float)renderView.getTop();
		float left = (float)renderView.getLeft();
		float right = (float)renderView.getRight();
		float bottom = (float)renderView.getBottom();
		
		float y = event.getRawY();
		float x = event.getRawX();
		
		if(y < top)
			y = 0;
		else if(y > bottom)
			y = bottom;
		else
			y = y - top;
				
		if(x < left)
			x = 0;
		else if(x > right)
			x = right;
		else
			x = x - left;
		event.setLocation(x, y);
		return JEngineAndroidBridge.onTouchEvent(event);		
	}
}
