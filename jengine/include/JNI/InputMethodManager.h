#pragma once

#include <jni.h>

class InputMethodManager
{
public:
	InputMethodManager(JNIEnv* env, jobject inputMethodManager);
	~InputMethodManager();
	
	void showSoftKeyboard(jobject view);
	void hideSoftKeyboard(jobject view);
	
private:
	JNIEnv* mEnv; // Pointer to the containing java enviroment
	jobject mIMM; // Handle to dalvik object
	jclass mIMMClazz; // Class description 
	jclass mViewClazz; // View class description
	jmethodID mGetWindowToken; // Method entry
	jmethodID mShowSoftInput; // Method entry
	jmethodID mHideSoftInput; // Method entry
};