package com.jetdogs.SinisterCity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

public class JRateDialog {

	public JRateDialog(final Activity actvitiy) {
		AlertDialog.Builder builder = new AlertDialog.Builder(actvitiy);
		builder.setMessage("Rate this app!")
	       .setTitle("Rate...")
	       .setPositiveButton("Rate", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("http://jetdogs.com/"));				
				actvitiy.startActivity(i);				
			}
		})
		.setNegativeButton("No way!", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {				
			}
		});
		Dialog dlg = builder.create();
		dlg.show();
	}
}
