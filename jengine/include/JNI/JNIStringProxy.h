#pragma once

#include <jni.h>

class JNIStringProxy 
{
public:
    JNIStringProxy(JNIEnv *env, jstring str)
        : mEnv(env), mJString(str)
    { 
    	mCString = mEnv->GetStringUTFChars(mJString, 0); 
    }

    ~JNIStringProxy() 
    { 
    	mEnv->ReleaseStringUTFChars(mJString, mCString); 
    }

    operator const char *() const 
    { 
    	return mCString; 
    }
    
    const char* toCString()
    {
	    return mCString;
    }
    
    String toStdString()
    {
	    return String(mCString);
    }
    
private:
    JNIStringProxy(const JNIStringProxy &x) {}
    JNIStringProxy &operator=(const JNIStringProxy &x) {}

    JNIEnv* mEnv;
    jstring mJString;
    const char* mCString;
};