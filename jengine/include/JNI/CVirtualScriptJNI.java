package com.jetdogs.SinisterCity;

public class CVirtualScriptJNI {
	public native static void setSS(String name, String value);
	public native static void setSB(String name, Boolean value);
	public native static void setSI(String name, int value);
	public native static void setSF(String name, float value);
	public native static void setII(int num, int value);
	
	public native static void sendCMsg(String name, int lparam, int objHandle);
	public native static void sendCMsgNow(String name, int lparam, int objHandle);
	
	public native static Boolean getCfgB(String name);
	public native static int getCfgI(String name);
	public native static float getCfgF(String name);
	public native static String getCfgS(String name);
}
