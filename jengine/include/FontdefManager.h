

#ifndef __FONTDEF__MANAGER___H__
#define __FONTDEF__MANAGER___H__


#include <OgrePrerequisites.h>
#include <OgreSingleton.h>
#include <OgreStringVector.h>
#include <OgreScriptLoader.h>
#include <OgreOverlaySystem.h>

class SStringTable;

namespace Ogre
{
	class GAME_API CFontdefexManager : public Ogre::Singleton<CFontdefexManager>, public Ogre::ScriptLoader
	{
	public:

		struct CFontdefex
		{
				// ��� ������� (�� ����������� ��� ������ Ogre::Font)
			Ogre::String strName;
			Ogre::String strSource;
			int nResolution;
				// from - to
			std::vector<std::pair<int, int> > listCodePointRanges;
				// fontsize - charheight
			std::vector<std::pair<int, int> > listSizePairs;

				// ���� �������� ������������ ������ �� ������ ����� � ��������
				// ��������� ������ ������ ��� ������ nWantedCharHeight
				// @return: ���������� ����� ������� � ������ (-1 ���)
			int findNearestCharHeight(int nWantedCharHeight)
			{
				int nSizeNum = (int)listSizePairs.size();
				for (int i = 0; i < nSizeNum; i++)
					if (listSizePairs[i].second > nWantedCharHeight)
					{
						if (i > 0)
							return i - 1;
						else
							return 0;
					}
				return nSizeNum - 1;
			}
		};


	private:

			// 
		Ogre::StringVector mScriptPatterns;


	protected:

			// 
		std::vector<CFontdefex*> mFontdefexes;

	public:
			// 
		CFontdefexManager();
			// 
		~CFontdefexManager();

	protected:
			// 
		CFontdefex* addFontdefex();
			// 
		void clearFontdefexes();

			// ���� ������ �� �����
		CFontdefex* getFontdefex(const Ogre::String &strFontdefex);

			// 
		Ogre::FontPtr createTrueTypeFont(const Ogre::String &strFont, const Ogre::String &strSource,
			int nResolution, int nSize, std::vector<std::pair<int, int> > *pCodePointRanges);

	public:
			// ������� � ��������� Ogre::Font
			// ������� ������, ������� ������, � ������� � ������������� � ������� CodePointRanges
			// @strFontdefex: ��� ������� ������ � ����� fontdefex
			// @strFont: ��� ������ Ogre::Font
			// @nCharHeight: �������� ������ ����
			// @return: NULL, ���� ������ �� ������ ��� �� ������� �������
		Ogre::FontPtr createAndLoadFont(const Ogre::String &strFontdefex, const Ogre::String &strFont, int nCharHeight);

			// ������ ���������� �������, ������ CodePointRanges ������� �� pStringTable, � �� �� �������
		Ogre::FontPtr createAndLoadFont(const Ogre::String &strFontdefex, const Ogre::String &strFont, int nCharHeight, SStringTable *pStringTable);


			// virtual
		Real getLoadingOrder() const;

			// virtual
		const Ogre::StringVector& getScriptPatterns() const;
			// virtual
		void parseScript(Ogre::DataStreamPtr& stream, const Ogre::String& groupName);


			// ���������� ������� codepointranges
		static void calcCodePointRanges(SStringTable *pStringTable, std::vector<std::pair<int, int> > *pCodePointRanges);

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			// ���������� ������� codepointranges, ������� � ������� ������ �� �������� �� �������,
			// � ��������� ��� � ����
			// @pStringTable: app.getStringTable()
			// @strName: ��� ������ (font1)
			// @strSource: ��� ����� ������ (arial.ttf), ������� ���� � ��������
			// @nResolution: ��� (96)
			// @strFileName: ��� �����, � ������� ��� ��������� (C:\\font1.fontdefex)
		static void createFontdefexFile(SStringTable *pStringTable, const Ogre::String &strName,
			const Ogre::String &strSource, int nResolution, const Ogre::String &strFileName);
#endif

		static CFontdefexManager& getSingleton();
		static CFontdefexManager* getSingletonPtr();

	};

};


#endif // __FONTDEF__MANAGER___H__

