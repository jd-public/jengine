/*
 *  MathUtils.h
 *
 *  Created by Cool on 3/29/11.
 *  Copyright 2011 JetDogs. All rights reserved.
 *
 */
#ifndef __MATH_UTILS__H__
#define __MATH_UTILS__H__

#include <CMsg.h>
#include "OgreFrustum.h"

namespace MathUtils
{
	const int SCR_X = 1024;
	const int SCR_Y = 768;

	template <class T>
	struct TVector2
	{
		T x, y;
		TVector2() : x(0), y(0) {}
		TVector2(const T& tx, const T& ty) : x(tx), y(ty) {}
	};
	typedef TVector2<int> TVector2i;
	typedef TVector2<unsigned int> TVector2ui;
	typedef TVector2<float> TVector2f;
	typedef TVector2<double> TVector2d;
	typedef TVector2<Ogre::Real> TVector2r;

	Ogre::Vector2 getScreenPoint2dFromPoint3d(const Ogre::Matrix4& projection, const Ogre::Matrix4& view, Ogre::OrientationMode orientation, const Ogre::Vector3& point);
	bool intersectCircle2dTri2d(const Ogre::Vector2& cirCenter, Ogre::Real cirRadius, const Ogre::Vector2& tri1, const Ogre::Vector2& tri2, const Ogre::Vector2& tri3);

	GAME_API float interp(float x1, float x2, float a);
	GAME_API float interp2(float x1, float x2, int a, int N);
	GAME_API Ogre::Vector2 interp(Ogre::Vector2 v1, Ogre::Vector2 v2, float a);
	GAME_API Ogre::Vector3 interp(Ogre::Vector3 v1, Ogre::Vector3 v2, float a);
	GAME_API float getInterpAlpha(float x1, float x2, float rez);
	GAME_API float cubeFunc(float);
	GAME_API float cubeFuncMirr(float);
	GAME_API float cosFunc(float x);
	GAME_API float cos2Func(float x);
	GAME_API float impFunc(float x);
	GAME_API float trivFunc(float x);
	GAME_API float parabFunc(float x);
	GAME_API float parab2Func(float x);
	GAME_API float quadFunc(float x);
	GAME_API float len2(Ogre::Vector3& x, Ogre::Vector3& y);
	GAME_API float scrX(int x);
	GAME_API float scrY(int y);
	GAME_API float colorF(int c);
	
	GAME_API int mod(int x, int y);
	GAME_API int floor2(float x);
	GAME_API float frame(int);
	GAME_API unsigned int getFlag(int);
	GAME_API Ogre::Vector2 scrVec2(int x, int y);

		// ������������ ������ vecRev �� ���� fDegrees ������ 0
	GAME_API void rotateVec2(Ogre::Vector2 &vecRev, const Ogre::Degree &fDegrees);


}; // namespace MathUtils

#endif
