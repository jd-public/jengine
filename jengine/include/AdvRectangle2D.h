#pragma once


#include "OgreMovableObject.h"
#include "OgreRectangle2D.h"

namespace Ogre {

class AdvRectangle2D : public Rectangle2D
{
#define TEXCOORD_BINDING 2
public:
	AdvRectangle2D(bool fautounload = true);

	virtual void setUserAny(const Any& anything);

	bool isVisible() const;
	void LoadMaterial() const;
	void UnloadMaterial() const;
	void setMaterial( const String& matName );

	void setUVs(const RealRect& rect, bool frotate = false);
	void setUVs(Real left, Real top, Real right, Real bottom, bool frotate = false);

protected:
	void getWorldTransforms(Ogre::Matrix4* xform) const;

private:
	mutable bool fOldVisible;
	bool fAutoUnload;
	
	static const Ogre::Matrix4 MAT_DEGREE_90, MAT_DEGREE_270;
};


/** Factory object for creating BillboardChain instances */
class AdvRectangle2DFactory : public MovableObjectFactory
{
public:
	AdvRectangle2DFactory();
	~AdvRectangle2DFactory();

	static const String FACTORY_TYPE_NAME;

	const String& getType(void) const;
	void destroyInstance( MovableObject* obj);  

protected:
	MovableObject* createInstanceImpl( const String& name, const NameValuePairList* params);
};

} // namespace
