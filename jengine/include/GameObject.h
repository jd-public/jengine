#pragma once

#include "ObjectCollectorTempl.h"

// NOT defnined on android
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
#	define FALSE 0
#	define TRUE  1
#endif

class CVirtualScript;
class CScriptMsg;

class CGameObject;
typedef CGameObject* LPGAMEOBJECT;
typedef	unsigned int SCRIPTOBJECT_HANDLE;

template <class T, int MaxN>
class SimpleStaticVector
{
	int lastNum;
	Ogre::uchar mUsed[MaxN];
	int mSize;
	T mT[MaxN];

public:
	SimpleStaticVector() {	clear(); }

	int size() { return mSize; } 

	int findElem(int z)
	{
		int tid = -1;
		int i = 0;
		for(; i < lastNum+1; i++)
		{
			if (mUsed[i]) tid++;
			if (tid == z) break;
		}
		return i;
	}

	T& operator[] (int z)
	{	
		return *(mT + findElem(z)); //[z]; 
	}


	int push_back(const T &_t)
	{
		for(int i = 0; i < lastNum+1; i++)
		{
			if (mUsed[i] == FALSE)
			{					
				if (i >= lastNum)
					lastNum = i+1;

				if (lastNum + 1 >= MaxN)
				{
					toLogEx("ERROR: WHAT THA F! WE HAVE NO ROOM FOR TIMER!\n");
					return false;
				}

				mT[i] = _t;
				mUsed[i] = TRUE;
				mSize++;

				return i;
			}
		}

		return 0;
	}

	int begin() 
	{	
		return 0;	
	}

	void erase(int i) 
	{	
		if (i < mSize)
		{
			mUsed[findElem(i)] = FALSE; 
			mSize--;
		}
	}
	
	void clear() {	lastNum = 0; mSize = 0; /*memset(mUsed,0,sizeof(mUsed));*/  for(int i = 0; i < MaxN;i++) mUsed[i] = 0; }
	void resize(int new_size) {}
};


//------------------------------------------------

class CGameObject : public Ogre::Node::Listener
{
public:
	enum Type
	{
		T_UNDEFINED,
		T_BASIC,
		T_HERO,
		T_ANIM_OBJ,
		T_PHYS_OBJ,
		T_LAST
	};

/*	class CObjectCollector
	{
	public:
		CObjectCollector();

		OBJECT_HANDLE push_back(LPGAMEOBJECT, const Type &t);
		void eraseByHandle(const OBJECT_HANDLE &h); 
		
		LPGAMEOBJECT findByHandle( const OBJECT_HANDLE &h );
		inline int size();
		inline LPGAMEOBJECT operator[] (int z);

		void removeAll();

	private:
		int lastNum;
		LPGAMEOBJECT mObj[MAX_OBJECT_NUM];	
	};*/

	struct ObjTimer
	{
		unsigned interval;	
		int repeatNum;
		CGameObject* objTarget;
		Ogre::Timer* pTimer;
		int id;

		//new version
		unsigned start_time;
		int unused;
	};

public:
	CGameObject();
	CGameObject(const String& scrName); //script name _with_ extention .sc
	virtual ~CGameObject();

	void addParams(const String& pname, const String& val) { getParams().insert(make_pair(pname,val)); }

	static void frameStarted( const Ogre::FrameEvent& evt );
	virtual void processFrame( const Ogre::FrameEvent& evt );
	int createTimer(unsigned int interval, int repeat, CGameObject* obj);
    void killTimer(CGameObject* obj, int timerID);
	void processTimer();
	void updateAnimTimePosition();

	
	//process script message
	void ProcessScriptMsg(const CScriptMsg *m);

	long getTypeID();
	String getTypeName() const;

	void createHandle( Type t );
	short getType() const;
	short getUid() const;
	void setType( Type t );
	OBJECT_HANDLE getHandle() const { return mHandle; }

	//Listener functions
	virtual void nodeUpdated(const Ogre::Node* node);
	virtual void nodeDestroyed(const Ogre::Node* node);
	virtual void nodeAttached(const Ogre::Node* node);
	virtual void nodeDetached(const Ogre::Node* node);

	static void equalSkeletons(Ogre::SkeletonInstance* ptargetS, Ogre::SkeletonPtr pmaterialS);  

	virtual float	getAnimationPosition() const;
	virtual void	setAnimationPosition(Real position);
	void			setAnimSpeed(Real speed) {mAnimTimeScaler.setAnimSpeed(speed);}
	void			setAnimSpeedPolynom(Real a0, Real a1, Real a2, Real a3) {mAnimTimeScaler.setAnimSpeedPolynom(a0, a1, a2, a3);}
	void			setAnimationBeginEnd(Real begin, Real end) {mAnimTimeScaler.setAnimBeginEnd(begin, end);}

	const char*	getName() {	return pSNode ? pSNode->getName().c_str() : 0; }

	void resetAnimTimeScaler(){mAnimTimeScaler.reset();}

private:
	class CAnimTime
	{
	private:
		Real	mRealTime;
		Real	mAnimSpeed;
		Real	mFragmentBegin; // ������ ��������� �������� � ������������� �������� (�������� 0..1)
		Real	mFragmentEnd; // ����� ��������� �������� � ������������� �������� (�������� 0..1)

		class CAnimTimeScalePolynom
		{
		private:
			Ogre::Vector4 mCoeffs;
		public:
			CAnimTimeScalePolynom(): mCoeffs(0.f, 1.f, 0.f, 0.f){}

			void	setCoeffs(Real a0, Real a1, Real a2, Real a3) {mCoeffs = Ogre::Vector4(a0, a1, a2, a3);}
			Real	getValue(Real x) const {return mCoeffs[0] + x*(mCoeffs[1] + x*(mCoeffs[2] + x*mCoeffs[3]));}
		} mAnimTimeScalePolynom;

		// ��������� �������������� ��������� ��� ������� ��������
		// animLength - ����� ����� ��������
		Real	getScaledAnimTime(Real animLength) const
		{
			Real fragmentLenght = getFragmentLenght(animLength);
			Real res = mFragmentBegin;
			if(fragmentLenght > 0)
				res = interp(mFragmentBegin, mFragmentEnd, mAnimTimeScalePolynom.getValue(mRealTime/fragmentLenght));
			return animLength*(res < 1. ? res : 1);
		}
		Real	interp(Real x1, Real x2, Real a) const  // �� ���� ���������� ������� �� MathUtils, ��-����� ������� ��� ����
		{
			return x1*(1-a) + x2*a;
		}

	public:
		CAnimTime(): mRealTime(0.), mAnimSpeed(1.), mFragmentBegin(0.), mFragmentEnd(1.)  {}

		Real	getFragmentLenght(Real animLength) const {return animLength*(mFragmentEnd - mFragmentBegin);}
		void	setAnimSpeed(Real speed) {mAnimSpeed = speed; mAnimTimeScalePolynom.setCoeffs(0.f, 1.f, 0.f, 0.f);}
		void	setAnimSpeedPolynom(Real a0, Real a1, Real a2, Real a3) {mAnimTimeScalePolynom.setCoeffs(a0, a1, a2, a3);}
		void	setAnimBeginEnd(Real begin, Real end) {mFragmentBegin = begin; mFragmentEnd = end;}

		void	reset() {mRealTime = 0.;}
		void	addRealTime(Real inc) {mRealTime += inc * mAnimSpeed;}
		void	setRealTime(Real time) {mRealTime = time;}
		Real	getRealTime() const {return mRealTime;}
		void	setAnimTimePosition(Ogre::AnimationState* animState) {animState->setTimePosition(getScaledAnimTime(animState->getLength()));}

		Real	getAnimSpeed() const {return mAnimSpeed;}

	} mAnimTimeScaler;

	std::map<String, String> mParams;
public:

	std::map<String, String> &getParams()
	{
		return mParams;
	}

	CVirtualScript	*pScript;
	

	Ogre::Entity		*pEntity;
	Ogre::Camera		*pCamera;
	Ogre::Light			*pLight;
	Ogre::BillboardSet	*pBillboard;
	Ogre::ParticleSystem*pParticle;

	Ogre::SceneNode		*pSNode;
    Ogre::AnimationState*pAnimState;
	Ogre::Entity		*pPearentBone; //���� ����������� � �����, �� ��������� ��������� �� ��������

	int				mTimes;
    CGameObject*	pTarget;
	String			mSceneFileName;

	//------------------------------
	//some trash
	Ogre::AnimationState* _pAnimState[2];
	float _animSpeed[2];
    int _mTimes[2];
    CGameObject* _pTarget[2];
	void blendSkeletons();

	int activeAnim;
	int startBlendTime;
	int endBlendTime;

	//------------------------------
	static CObjectCollectorTempl<CGameObject*,Type> mGameObjList;
	static std::vector<CGameObject*> mMouseEventListenerList;
	//object timers
	SimpleStaticVector<ObjTimer, 16> mTimerList;
	//To process mouse events object have to selectable
	//This property can be set from .scene file 
	bool mIsSelectable; //default: false
	bool mIsFocused;	//Is mouse on object or not
	bool mSoftwareAnim;

	OBJECT_HANDLE mHandle; //First hi word - type, low word - Uid

private:
	int unical_timer_counter;
};
