#pragma once

#include "VirtualScript.h"

enum MoveEvent {me_none, me_zero, me_one};

#define COLOR_OP_NONE			4
#define COLOR_OP_DEFAULT		3
#define COLOR_OP_SUBTRACT		2
#define COLOR_OP_MODULATE		1
#define COLOR_OP_ADD			0

class GAME_API CConvertedFromFile : public CVirtualScript
{
public:
	CConvertedFromFile(const String& scrName);

	virtual void proceedMsg(const CScriptMsg * pMsg);

	virtual void ON_CREATE() {}
	virtual void ON_DESTROY() {}
	virtual void ON_SETFOCUS() {}
	virtual void ON_KILLFOCUS() {}
	virtual void ON_ACTIVATE() {}
	virtual void ON_DEACTIVATE() {}
	virtual void ON_TIME(int param=-1) {}
	virtual void ON_SOUND(int param=-1) {}
	virtual void ON_LBUTTONDOWN(const Vector3& pos = Vector3::ZERO) {}
	virtual void ON_LBUTTONUP(const Vector3& pos = Vector3::ZERO) {}
	virtual void ON_RBUTTONDOWN(const Vector3& pos = Vector3::ZERO) {}
	virtual void ON_RBUTTONUP(const Vector3& pos = Vector3::ZERO) {}
	virtual void ON_NOTIFY(const CMsg& msg) {}

	virtual void ON_LBUTTONDOWN2(const Vector2& pos, OBJECT_HANDLE obj) {}
	virtual void ON_LBUTTONUP2(const Vector2& pos, OBJECT_HANDLE obj) {}
	virtual void ON_RBUTTONDOWN2(const Vector2& pos, OBJECT_HANDLE obj) {}
	virtual void ON_RBUTTONUP2(const Vector2& pos, OBJECT_HANDLE obj) {}
};

////////// CGameState ////////////////////////////////////////////
struct CGameState
{
public:
	int stateInv;
	int scriptVars[2048];

	CGameState() { stateInv=0; memset(scriptVars, 0, 2048*sizeof(int)); }
};

extern GAME_API CGameState GameState;
