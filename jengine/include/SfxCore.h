#pragma once

#ifndef NO_SFX


#define __BASS_LIB__
#ifdef __BASS_LIB__

#include <OgreVector3.h>
#include <OgreDataStream.h>
#include <vector>
#include "SfxCorePrerequisites.h"

// Sample
struct Sample 
{
	float time;
	float level;

	Sample(float t, float l) { time = t; level = l; }
};

// sound channel
struct _SFX_HANDLE_TYPE
{
	int used;
	
	Ogre::ulong sample;
	Ogre::ulong stream;

	Ogre::ulong channel;

	bool looped;
	int fade;		// for music: -1 - fade, 0 - none, 1 - louder
	int group;
	int volume;		// volume - 0 - 100
	int type; 		// 0 - usual, 1 - 3d channel
	SFX_OBJECT_HANDLE notifyObj;

	// for 3d
	float position[3];
	float velocity[3];

	char name[128]; // Sound name
	void *pStream; // pointer to ogre string (! NOT a string, but an another shit)

	// Levels
	int has_levels;
	std::vector<Sample> levels;

	//
	unsigned duration;

	_SFX_HANDLE_TYPE() { reset(); }
	void reset()
	{
		duration = 0;
		used = 0;
		sample = 0;
		stream = 0;
		channel = 0; 
		looped = false;
		fade = 0;
		group = 0;
		volume = 0;
		type = 0;
		notifyObj = 0;
		name[0] = '\x0';
		pStream = NULL;
		has_levels = 0;
		levels.clear();
	}
};

typedef _SFX_HANDLE_TYPE SFX_HANDLE_TYPE;

#define SFX_MAX_HANDLES 256
#define SFX_MAX_GROUPS	16
#define SFX_SND_INTERVAL	50
#define ENV_CACHE_SIZE	17

// Sound Core class
class GAME_API CSfxCore
{
public:
	CSfxCore();
	virtual ~CSfxCore();

	void initialize();
	void deinitialize();
	bool getIsInitialized() const { return mIsSfxCreated; }

	void logLoadedSounds();

    // ��������� ����� ������
	 void loadEnvSounds();
    
	bool testGroupTime(int group);

	void pauseGroup(int Group, int bPause);
	void stopGroup(int Group);

	// volume value 0..100
	void setGroupVolume(int group, int volume);

	void setMusicVolume(int vol) { mGroupVolumes[SFX_MUSIC_GROUP] = vol;  } 
	int getMusicVolume() { return mGroupVolumes[SFX_MUSIC_GROUP]; } 

	// process sound engine, call every frame
	void process();

	size_t findFreeHandle();

	size_t loadSound(const String& name, bool memFlag = true);


	// ������������ �����
	// id - ������������� �����
	// group - ������ ��� �����
	// looped - ����������� ����?
	// notifyObj - ������ ��� ��������� � ����� �����
	// pos - ������� ��� 3�-������� ������
	// source - ����, ��� � �������, �� ������ ����� ������
	void playSound(size_t id, int group, bool looped, SFX_OBJECT_HANDLE notifyObj);

	// ������������ 3d �����
	// id - ������������� �����
	// group - ������ ��� �����
	// looped - ����������� ����?
	// notifyObj - ������ ��� ��������� � ����� �����
	// pos - ������� ��� 3�-������� ������
	void playSound3D(size_t id, int group, bool looped, Ogre::Vector3 &pos, SFX_OBJECT_HANDLE notifyObj);

	// ������������ 3d �����
	// id - ������������� �����
	// group - ������ ��� �����
	// looped - ����������� ����?
	// notifyObj - ������ ��� ��������� � ����� �����
	// source - ����, ��� � �������, �� ������ ����� ������
	void playSoundHandle3D(size_t id, int group, bool looped, SFX_OBJECT_HANDLE source, SFX_OBJECT_HANDLE notifyObj);

	void stopSound(size_t id);
	void freeSound(size_t id);
	void freeAllSounds();
		// ������� ��� �����, ����� "00_env_..."
		// @return: ���-�� ��������� ������
	int freeAllSoundsButEnv();

	void pauseAll(bool bPause);

	// Sound length in milliseconds
	unsigned int getDuration(size_t id);

	// Get current state
//	bool getSoundState(size_t id) const;

	// Set loop
    void setSoundLooped(size_t id, bool isLoop);

	int getVolume(size_t id) const;
	void setVolume(size_t id, int volume);

	// Set listener
	void assignListener(SFX_OBJECT_HANDLE mSfxListener);
	// Update listener position (3d sound only)
	void updateListener();

	// Sound levels
	float getLevel(size_t id, float time);
	bool loadLevels(size_t id);
	void saveLevels(size_t id);
	void loadDuration(size_t id);

	size_t findByName(const String& name);

	void processEnvSounds();
	void addEnvSound(size_t id, bool looped, SFX_OBJECT_HANDLE notifyObj);
	void setEnvLevel(float level);
	void setEnvFadeSpeed(float speed);

	bool isPlaying(size_t id);



	bool mCachedPathsInitialized;
	OGRE_HashMap<Ogre::String, Ogre::String>	mCachedPaths;
	void initializeCachedPath();
	Ogre::String getCachedPath(const Ogre::String &name);

	//-------------------------------
//	static void* UserAlloc(unsigned int size);
//	static void UserFree(void* ptr);
//	static void* UserOpen(const char* Name);

	static const char* getFormat() 
	{
#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE
		return "ogg";
#else
		return "aac";
#endif
	}

public:
	static const size_t mError = (size_t)-1;

private:
	// Is sound engine initialization
	bool mIsSfxCreated;
	// ���������, ��� 3�-������ �������� �������
	SFX_OBJECT_HANDLE mSfxListener;
	// ������ ������� �� �����
	std::vector<SFX_HANDLE_TYPE> mSoundHandles;
	// ��������� �������� ������� (0...100)
	int mGroupVolumes[SFX_MAX_GROUPS];
	// ��� ����������� ��������� �����
	int mGrTimes[SFX_MAX_GROUPS];
	float mFadeSpeed;

	int mEnvCache[ENV_CACHE_SIZE];
	float m_fEnvMaxVolume;
	float m_fEnvCurVolume;
	float m_fEnvSpeedVolume;
	size_t mCurEnvSound;
	size_t mPrevEnvSound;
	float mCurEnvSoundVol, mPrevEnvSoundVol;
};

extern GAME_API CSfxCore sfxCore;

#endif //#ifdef __BASS_LIB__


#endif //#ifndef NO_SFX
