#pragma once

#include <SceneLoader.h>

class CProgressListener : public ProgressListener
{
public:
	void finish();
	
	//sinister city defaults
	void clearProgress(int p, float tw = 0.375f, float sp = 0.039f, float ep = 0.9531f);

	void onProgress() OVERRIDE;
	static void setAlpha(float value, float tw = 0.375f, float sp = 0.039f, float ep = 0.9531f);

private:
	static float total_width;
	static float start_pos;
	static float end_pos;


	int mProgress;
	Ogre::ulong mOldTime;
	int mMaxProgress;
};

