
#include "rapidxml.hpp"
#include "rapidxml_print.hpp"


typedef rapidxml::xml_document<>	XmlDoc;
typedef rapidxml::xml_node<>		XmlNode;
typedef rapidxml::xml_attribute<>	XmlAttrib;


GAME_API Ogre::String getAttrib(const XmlNode* XMLNode, const char *attrib, const Ogre::String &defaultValue = "");
GAME_API Ogre::Real getAttribReal(const XmlNode* XMLNode, const char *attrib, Ogre::Real defaultValue = 0.0f);
GAME_API int getAttribInt(const XmlNode* XMLNode, const char *attrib, int defaultValue = 0);
GAME_API bool getAttribBool(const XmlNode* XMLNode, const char *attrib, bool defaultValue = false); // values "false" or "true"
GAME_API bool getAttribBool2(const XmlNode* XMLNode, const char *attrib, bool defaultValue = false); // values "0" or "1"


class GAME_API XmlParser
{
	XmlDoc mDoc;
	char *mText;

public:
	XmlParser(const char*);
	XmlParser(char*, bool copy);
	~XmlParser();
	const XmlDoc& getDoc();
};