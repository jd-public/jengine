#pragma once

//#include "MainApplication.h"
//#include "GameObject.h"
#include "SfxCorePrerequisites.h"

class CScriptMsg;

class GAME_API CVirtualScript
{
public:
	enum ScrType
	{
		eNone,
		FILE_SCRIPT,		//from .sc file
		VIRTUAL_SCRIPT,		//defined in engine code
		SQ_SCRIPT,			//squrrel script
		ANGEL_SCRIPT,		//angel script
		CONVERTED_SCRIPT
	};

	enum VisibleFlagParam
	{
		vfp_add,
		vfp_set,
		vfp_remove
	};

public:
	CVirtualScript(const Ogre::String& scrName, const Ogre::String& resGroup = String("General"));
	virtual ~CVirtualScript() {}

	OBJECT_HANDLE getHandle() const			{ return mHandle; }
	void setHandle(OBJECT_HANDLE handle)	{ mHandle = handle; }


	virtual void proceedMsg(const CScriptMsg * pMsg) = 0;

	static void setFullscreen(bool fFullScreen);
	static void setFitScreen(bool fFitScreen);
	static void shellExecute(const Ogre::String& str);

	static void animStart(OBJECT_HANDLE handle);
	static void animStop(OBJECT_HANDLE handle);
	static void animClear(OBJECT_HANDLE handle);
	static void animSkip(OBJECT_HANDLE handle);
	static void animSkipAll(OBJECT_HANDLE handle);
	static void animAddAnim(OBJECT_HANDLE handle, const Ogre::String& anim, int ntimes, OBJECT_HANDLE target, int param);
	static void animAddMove(OBJECT_HANDLE handle, const Ogre::String& anim, const Ogre::Vector3& pos, const Ogre::Vector3& rot, bool carePos, bool careRot, OBJECT_HANDLE target, int param);
	static void animAddMove2(OBJECT_HANDLE handle, const Ogre::String& anim, const Ogre::Vector3& pos, const Ogre::Vector3& rot, bool carePos, bool careRot, const Ogre::String& anim2, size_t snd_id, OBJECT_HANDLE target, int param);
	static int animGetCurrentLength(OBJECT_HANDLE handle, const Ogre::String& animationName);
	static void animSetRotSpeed(OBJECT_HANDLE handle, const Ogre::String& animationName, float rot_speed);
	static int animGetRotSpeed(OBJECT_HANDLE handle, const Ogre::String& animationName);
	static void animAddAnim2(OBJECT_HANDLE handle, const Ogre::String& anim, int ntimes,
								 const Ogre::String& anim2, size_t snd_id,
								 OBJECT_HANDLE target, int param);

	static void animSetLoop(OBJECT_HANDLE handle, bool set);

	// visible
	static void setObjVisible(OBJECT_HANDLE handle, bool state);
	static void setObjVisible(const Ogre::String& name, bool state);
	static void setObjVisibleFlag(OBJECT_HANDLE handle, unsigned int mask, VisibleFlagParam param);
	static void setSceneVisible(const Ogre::String& name, bool state);
	static void addSceneVisibleFlag(const Ogre::String& name, unsigned int flags, VisibleFlagParam param);
	static void setSceneVisibleFlagByNames(const Ogre::String& name);
	static void setVisibleMask(unsigned int mask);
	static int getVisibleMask();
	static bool getVisible(OBJECT_HANDLE handle);
	static void sceneDeleteTextures(const Ogre::String& name);
	static void sceneRotate(const Ogre::String& name, const Ogre::Quaternion &q);
	static void sceneTranslate(const Ogre::String& name, const Ogre::Vector3 &v);

	static void setObjRenderQueue(OBJECT_HANDLE handle, int queue);


	//selectable
	static bool getObjSelectable(OBJECT_HANDLE obj);
	static void setObjSelectable(const Ogre::String& name, bool v);
	static void setObjSelectable(OBJECT_HANDLE obj, bool v);
	static void setObjSelectableFlag(OBJECT_HANDLE handle, unsigned int mask);
	static void setSceneSelectable(const Ogre::String& scene, bool v);
	static void setSceneSelectableFlag(const Ogre::String& name, unsigned int mask);
	static void setSelectableMask(unsigned long mask);

	static void lightSetColor(const Ogre::String& name, int param, float x, float y, float z);
	static void lightSetAttenuation(const Ogre::String& name, float r, float c, float l, float q);
	static void interpolateLights(const Ogre::String& res_light, const String& l1, const String& l2, float a);


	static void attachSceneToObject(const Ogre::String& scene, const Ogre::String& object);
	static void toLogLights();
	static void toLogLoadedTextures();

	// Ogre::Math
	static int	rand();
	static void sqrt(float *res);
	static void	sin(Ogre::Vector3 &xyz);
	static void acos(float *res);
	static void interpolateFloat(float *rez, float x1, float x2, float a);
	static void interpolateVec(Ogre::Vector3& rez, const Vector3& x1, const Vector3& x2, float a);


	static float getDepth(OBJECT_HANDLE handle);

	static bool keyGetStatus(int key);

	static void cameraAttachToBone(OBJECT_HANDLE handle, const Ogre::String& boneName);

	static void resetTo(OBJECT_HANDLE handle, const Ogre::Vector3& pos, const Ogre::Vector3& rot);

	static void setWaterMode(OBJECT_HANDLE handle, bool mode);

	static void assignTo(OBJECT_HANDLE handle, OBJECT_HANDLE walk_plane, const Ogre::Vector3& pos, const Ogre::Vector3& rot, bool isRun, bool accuratePos, bool assignRot, OBJECT_HANDLE hTarget);
	static void assignToAlt(OBJECT_HANDLE handle, OBJECT_HANDLE walk_plane, const Ogre::Vector3& pos, const Ogre::Vector3& rot, bool isRun, bool accuratePos, bool assignRot, OBJECT_HANDLE hTarget);
	static void statAnim(OBJECT_HANDLE handle, OBJECT_HANDLE target, const Ogre::String& animName);
	static void heroPlayAnim(OBJECT_HANDLE handle, const Ogre::String& animName);
	static void heroLookToCam(OBJECT_HANDLE handle, OBJECT_HANDLE hTarget);

	static void stop(OBJECT_HANDLE handle);

	static int createTimer(OBJECT_HANDLE handle, unsigned int interv, int rcount, OBJECT_HANDLE hTarget);
	static void killTimer(OBJECT_HANDLE handle, int timerID);

	static void getPosition(OBJECT_HANDLE handle, Ogre::Vector3& pos);
	static void setPosition(OBJECT_HANDLE handle, const Vector3& pos);
	static void getScale (OBJECT_HANDLE handle, Vector3& scale);
	static void setScale (OBJECT_HANDLE handle, const Vector3& scale);
	static void setPositionRotation(OBJECT_HANDLE handle, float x, float y, float z, float axis_z);
	static void equalPositionRotation (OBJECT_HANDLE hSource, OBJECT_HANDLE hTarget);
	static Vector3 getBonePosition (OBJECT_HANDLE handle, const String& name);
	static void setBonePosition (OBJECT_HANDLE handle, const String& name, const Vector3& pos);
	static void setBoneScale (OBJECT_HANDLE handle, const String& name, const Vector3& scale);
	static void setInitalBonePosition(OBJECT_HANDLE handle, const String& name, const Vector3& pos);
	static void getBoneOrientation (OBJECT_HANDLE handle, const String& name, Vector3& rot);
	static void getBoneOrientation2(OBJECT_HANDLE handle, const String& bone, Quaternion& q);
	static void setBoneOrientation (OBJECT_HANDLE handle, const String& name, const Vector3& rot);
	static void setBoneOrientation2(OBJECT_HANDLE handle, const String& bone, const Quaternion& q);
	static bool hasBone(OBJECT_HANDLE handle, const String& name);

	static void attachObjToBone(OBJECT_HANDLE hChild, OBJECT_HANDLE hParent, const String& bone_name);
	static void attachObjToBoneWithNodeOffset(OBJECT_HANDLE hChild, OBJECT_HANDLE hParent, const String& bone_name);
	static void dettachObjFromBone(OBJECT_HANDLE hChild);

	static void setOrientation(OBJECT_HANDLE handle, const Vector3& rot);
	static void getOrientation(OBJECT_HANDLE handle, Vector3& rot);

	static void exitApplication ();

	static void updateInventory ();

	static void sendCMsg(const String& name, const CMsg& m);
	static void sendCMsg(const String& name, int lparam, OBJECT_HANDLE s) { sendCMsg(name, CMsg(lparam, 0, s)); }
	static void sendCMsgHandle(OBJECT_HANDLE h, const CMsg& m);
	static void sendCMsgHandle(OBJECT_HANDLE h, int lparam, OBJECT_HANDLE s) { sendCMsgHandle(h, CMsg(lparam, 0, s)); }
	static void answerCMsg(const CMsg& m, OBJECT_HANDLE sender);
	static void answerCMsgNow(const CMsg& m, OBJECT_HANDLE sender);
	static void sendCMsgNow(const String& name, int lparam, OBJECT_HANDLE s) { sendCMsgNow(name, CMsg(lparam, 0, s)); }
	static void sendCMsgNow(const String& name, const CMsg& m);
	static void sendCMsgNowHandle(OBJECT_HANDLE h, const CMsg& m);

	// Overlays
	static void _fixMipmapFilterBug(Ogre::OverlayContainer* container);

	static void findOverlay(const String& overlayName, const String& containerName, const String& innercontainerName, Ogre::Overlay* &overlay, Ogre::OverlayContainer* &container);

	static void overlayCreate(	  const String& overlayName,
								  const String& containerName,
								  const String& innercontainerName,
								  int zOrder,
								  const String& material,
								  const Ogre::Vector2* pos,
								  const Ogre::Vector2* size);

	static void overlayCreateWithTexture(const String& overlayName,
								   const String& containerName,
								   const String& innercontainerName,
								   int zOrder,
								   const String& base_material,
								   const String& material,
								   const String& texture,
								   const Vector2* pos,
								   const Vector2* size);

	static void overlayCreateFromTemplate(const String& overlayName,
								  const String& containerName,
								  const String& innercontainerName,
								  int zOrder,
								  const String& templateName,
								  const String& typeName,
								  const Ogre::Vector2 *pos,
								  const Ogre::Vector2 *size);

	static void overlayDestroy(const String& overlayName,
							   const String& containerName,
							   const String& innercontainerName);

	static void overlaySetTopColour(const String& overlayName, const String& containerName, const String& innercontainerName, float r, float g, float b, float a);
	static void overlaySetBottomColour(const String& overlayName, const String& containerName, const String& innercontainerName, float r, float g, float b, float a);
	static void overlayShowHide(const Ogre::String& overlayName, bool visible)								{ overlayShowHide(overlayName, Ogre::EmptyString, Ogre::EmptyString, visible); }
	static void overlayShowHide(const Ogre::String& overlayName, const String& containerName, bool visible)	{ overlayShowHide(overlayName, containerName, Ogre::EmptyString, visible); }
	static void overlayShowHide(const String& overlayName, const String& containerName, const String& innercontainerName, bool visible);
	static void overlayGetPosition(const String& overlayName, const String& containerName, const String& innercontainerName, Vector2& position);
	static void overlaySetPosition(const String& overlayName, const String& containerName, const String& innercontainerName, const Vector2& position);
	static void overlaySetPositionNoRound( const String& overlayName, const String& containerName, const String& innercontainerName, const Vector2& position);
	static void overlayGetSize(const String& overlayName, const String& containerName, const String& innercontainerName, Vector2& size);
	static void overlaySetSize(const String& overlayName, const String& containerName, const String& innercontainerName, const Vector2& size);
	static void overlaySetZOrder(const String& overlayName, int zorder);
	static void overlaySetRotation(const String& overlayName, float angle);
	static void overlaySetScale(const String& overlayName, float fScale);
	static void overlaySetUVs(const String& overlayName, const String& containerName, const String& innercontainerName, const Ogre::Vector2& uv1, const Ogre::Vector2& uv2);
	static void overlaySetMaterial(const String& overlayName, const String& containerName, const String& innercontainerName, const String& materialName);
	static void overlaySetBorderMaterial(const String& overlayName, const String& containerName, const String& innercontainerName, const String& materialName);
	static void overlaySetBorderSize(const String& overlayName, const String& containerName, const String& innercontainerName, float sides, float topAndBottom);
	static void overlayGetCaptionWidth(const String& overlayName, const String& containerName,	const String& innercontainerName, float &width);
	static void setLoadingWindowAlpha(float alpha);
	static Vector2 roundToPixel(const Vector2  &pos);

	static void overlayAddNode(const String& overlayName, const String& sceneNode);

	static void overlaySetCaptionFromSTBeginEnd(const String& overlayName,
		const String& containerName,
		const String& innercontainerName,
		int stringNum,
		int startChar,
		int len,
		const String& app);

	static void overlaySetCaptionFromSTAppend(const String& overlayName,
		const String& containerName,
		const String& innercontainerName,
		int stringNum,
		const String& app);

	static void  overlayGetCaption(const String& overlayName,
				const String& containerName,
				const String& innercontainerName,
				Ogre::DisplayString& txt);
	static void overlaySetCaption(const String& overlayName,
				const String& containerName,
				const String& innercontainerName,
				const Ogre::DisplayString& caption);

	static void overlaySetCaptionFromSTAppend2(const String& overlayName,
				const String& containerName,
				const String& innercontainerName,
				int stringNum);

	static void overlayDeleteCaptionSymbol (const String& overlayName,
				const String& containerName,
				const String& innercontainerName);

	static void overlaySetCaptionFromST(const String& overlayName,
				const String& containerName,
				const String& innercontainerName,
				int stringNum);

	static size_t overlayGetCaptionLength(const String& overlayName,
											const String& containerName,
											const String& innercontainerName);
	static int overlaySetCaptionFromSTEx(const String& overlayName, const String& containerName, const String& innercontainerName, int stringNum, size_t strLen = 64);
	static int overlaySetCaptionWithWidth(const String& overlayName, const String& containerName, const String& innercontainerName, const DisplayString& strText, float fMaxWidth, bool rtt = false);
	static int overlaySetCaptionFromSTWithWidth(const String& overlayName, const String& containerName, const String& innercontainerName, int stringNum, float fMaxWidth);

	static void overlaySetCharHeight(const String& overlayName, const String& containerName,
		 const String& innercontainerName, float height);
		// ������ ������ ���� ������� � �������
	static bool overlaySetCharHeightByScreen(const String& overlayName, const String& containerName, const String& innercontainerName);
		// ���������� ������ ��������, ����� ���� ������� � �������
	static float overlayGetCharHeightByScreen(const String& overlayName, const String& containerName, const String& innercontainerName);
		// ������ ��� ������ � ���������������
	static void overlaySetFont(const String& overlayName, const String& containerName, const String& innercontainerName, const String& fontName);
		//

	static float fontGetCharHeightByScreen(const String& fontName);
	static void shiftFontMaterials(float x, float y);

	static size_t getTextStringLength(int strNum);
	static Ogre::DisplayString getTextString(int strNum);
	static bool isTextStringExist(int strNum);

	static void overlaySetCharColors(const String& overlayName, const String& containerName, const String& innercontainerName, size_t start, size_t end, float r, float g, float b);

	// ������� ����� �� ������� � ���������
	static void fontdefexCreateAndLoad(const String &strFontdefex, const String &strFont, int nCharHeight);
	// ������� ����� �� ������� � ���������, � ������������� ������� code points ranges
	static void fontdefexCreateAndLoadRanges(const String &strFontdefex, const String &strFont, int nCharHeight);

	///////////////////////////////////////
	static void nodeAttach(const String& nodeName, const String& childName);
	static void nodeDetach(const String& nodeName, const String& childName);

	static void nodeAttach2(const String& nodeName, const String& childName);
	static void nodeDetachAtAll(const String& nodeName);

	static void cameraAttachOrientation(float x, float y, float z, float w);

	static void materialDeleteTexture(const String& materialName);
	static void materialSetAlpha(const String& materialName, Real alpha);
	static void materialSetAlphaPassUnit(const String& materialName, unsigned short pass, unsigned short tex_unit, Real alpha);
	static void materialSetAlphaAndColor(const String& materialName, Real a);

	static void materialSetFiltering(const String& materialName, int min, int mag, int mip);

	static void materialSetScale(const String& materialName, Real movH, Real movV);
	static void materialSetScalePassUnit (const String& materialName, unsigned short pass, unsigned short unit, Real movH, Real movV);
	static void materialSetScroll(const String& materialName, Real movH, Real movV);
	static void materialSetScrollPassUnit (const String& materialName, unsigned short pass, unsigned short unit, Real movH, Real movV);
	static void materialSetScrollAnim (const String& materialName, Real movH, Real movV);

	static void materialSetColor(const String& materialName, int color_op, Real r, Real g, Real b);
	static void materialSetColorPass(const String& materialName, unsigned short pass, int color_op, Real r, Real g, Real b);
	static void materialSetColorPassUnit (const String& materialName, unsigned short pass, int tex_unit, int color_op, Real r, Real g, Real b);
	static void materialSetAmbientPass(const String& materialName, int pass, Real r, Real g, Real b);

	static void materialSetTexture(const String& materialName, const String& textureName);
	static void materialSetTexturePass(const String& materialName, int pass, const String& textureName);
	static void materialSetTexturePassUnit(const String& materialName, int tex_pass, int tex_unit, const String& textureName);
	static void materialSetRotate(const String& materialName, int tex_unit, float rot);
	static void materialSetBlend(const String& materialName, int blend);
	static void materialSetAnimatedTextureCurrentFrame(const String& materialName, unsigned short npass, unsigned short unit, unsigned int frame);
	static void materialSetAnimatedTexturePlayStop(const String& materialName, unsigned short npass, unsigned short unit, bool play_stop);
	static String materialGetInfoPassUnit(const String& materialName, unsigned short pass, unsigned short unit);
	static bool materialClone(const String& materialTemplate, const String& materialNew);
	static bool materialExist(const String& material);


		// ��������� �������� � ������� XX_YY_... (XX - ����� ������� �� 1 �� 5, YY ����� ������� �� 1 �� 31)
	static void texturesUnloadLocRoom();
		// ���� � �����-��������� ��������, ������������� ��������� matName � ������ ��������� ��������� �� ��������� �� ������
		// @bSingleState: true - ������ ������ ���� ���� ������
		// ���������� ��� �� ������ �����
	static bool texturesChangeToAtlas(const String &matName, const String &texName, bool bSingleState);
	static bool texturesChangeToAtlas(const String &matName);

	static void loadTexture(const String& textureName);
	static void deleteTexture(const String& textureName);
		// ������� ��������, ������� ��� � ������ �����������
	static void deleteAllTextures();
		// ������ �������� ����������� (��������� � mNotUnloadableTextures), �������� ���� ��������� ��� ����������
	static void addNotUnloadableTexture(const Ogre::String &textureName);
	static void setMaterial(OBJECT_HANDLE handle, int subEntity, const String& materialName);

	static bool resourceExists(const String& resName);
	static bool textureExists(const String& texName);
	static void setSubmeshVisible(OBJECT_HANDLE handle, const String& materialName, bool state);

	static void mouseGetPosition(Vector2& pos);
	static bool mouseGetStateLButton();
	static bool mouseGetStateRButton();
    static Ogre::Vector2& mouseGetCursorCastOffset();
    static void mouseSetCursorCastOffset(const Ogre::Vector2 &vecOffset);
    static int mouseGetPinch();
    static void mouseSetPinch(int pinch);

	// Ogre::Animation
	static int  startAnimation(OBJECT_HANDLE handle, const String& animName, int ntimes, OBJECT_HANDLE thandle);
	static int  startAnimation(OBJECT_HANDLE handle, const String& animName, int ntimes, OBJECT_HANDLE thandle, float begin, float end);
	static void stopAnimation(OBJECT_HANDLE handle);
	static void setAnimationSpeed(OBJECT_HANDLE handle, int svalue);
	static void setAnimationBeginEnd(OBJECT_HANDLE handle, float begin, float end);
	static void setAnimationSpeedPolynom(OBJECT_HANDLE handle, int coeff0, int coeff1, int coeff2, int coeff3);
	static void setAnimationPosition(OBJECT_HANDLE handle, float position);
	static float getAnimationPosition(OBJECT_HANDLE handle);

	static int startInterpolateAnimation(OBJECT_HANDLE handle,  OBJECT_HANDLE start_pt, OBJECT_HANDLE end_pt, float len, int ntimes, OBJECT_HANDLE thandle);
	static int startInterpolateAnimation2(OBJECT_HANDLE handle,  const Vector3 &pos_start, const Quaternion &rot_start, const Vector3 &pos_finish, const Quaternion &rot_finish, float len, int ntimes, OBJECT_HANDLE thandle);
	static int startCameraMoveNearPlaneAnimation(OBJECT_HANDLE handle,  OBJECT_HANDLE start_pt, OBJECT_HANDLE end_pt, float len, int ntimes, OBJECT_HANDLE thandle, OBJECT_HANDLE vhandle);

	static int startLinkedAnimation (OBJECT_HANDLE handle,  const String& skeletonName, const String& animName, int ntimes, OBJECT_HANDLE thandle);
	static int startLinkedAnimation (OBJECT_HANDLE handle,  const String& skeletonName, const String& animName, int ntimes, OBJECT_HANDLE thandle, float begin, float end);
	static int startBlendAnimation (OBJECT_HANDLE handle, const String& skeletonName, const String& animName, int ntimes, int blend_time, OBJECT_HANDLE thandle);

	static void ribbonVisible(const String& ribbonName, bool state);

	static void particleShowHide(const String& particleName, bool state);
	static void particlePlayStop(const String& particleName, bool state);
	static void particleSetEmitterSize(const String& particleName, int emitter_idx, Real size_x, Real size_y, Real size_z);
	static void particleSetEmitterColor(const String& particleName, int emitter_idx, Real color);
	static void particleSetEmitterRate(const String& particleName, int emitter_idx, float rate);
	static void particleSetStartTime(const String& particleName, Real startTime);
	static void particleClear(const String& particleName);

	static void billboardSetSize(const String& name, Vector3& size);
	static void billboardSetPos(const String& name, Vector3& pos);
	static void billboardSetRot(const String& name, float angle);
	static void billboardGetSize(const String& name, Vector3& size);
	static void billboardGetPos(const String& name, Vector3& pos);
	static void billboardSetAlpha(const String& name, float alpha);
	static void billboardVisible(const String& name, bool visible);
	static void billboardSetColor(const String& name, int index, float r, float g, float b, float a);

	static void showCursor(bool fshow = true);
	static bool isShowCursor();
	virtual int getCursor() const { return -1; }
	virtual bool isMouseEventListener() const { return false; }
	static void setCursor(int id);
	static void setCursorStandard(bool state);
	static void setCursorUseTextures(bool state);

	static int getPostfixInt(OBJECT_HANDLE h, const String& prefix);
	static void getPrefixInt(OBJECT_HANDLE h, int *i1, int *i2);

	static void _getPrefixInt(String& name, int *i1, int *i2);

	static void updateFocusedObj();
	static OBJECT_HANDLE getFocusedObj();

	static bool getString(OBJECT_HANDLE handle, const String& pname, String& buf);
	static bool getBool(OBJECT_HANDLE handle, const String& pname, bool &rez);
	static int getInt(OBJECT_HANDLE handle, const String& pname);
	static bool getInt(OBJECT_HANDLE handle, const String& pname, int &rez);
	static void setInt(OBJECT_HANDLE handle, const String& pname, int value);
	static void setString(OBJECT_HANDLE handle, const String& pname, const String &value);
	static int getFloat(OBJECT_HANDLE handle, const String& pname, float& rez);

	static int keyExist(const String& str);
	static int getSS(const String&, String& str);
	static const String& getSS(const String&);
	static bool getSB(const String&);
	static int getSI(const String&);
	static float getSF(const String&);
	static int getII(int num);

	static void setSS(const String& name, const String& value);
	static void setSB(const String& name, bool value);
	static void setSI(const String& name, int value);
	static void setSF(const String& name, float value);
	static void setII(int num, int value);

	static bool getCfgB(const String&);
	static int getCfgI(const String&);
	static void getCfgF(const String&, float&);
	static String getCfgS(const String&);

	static void loadCfgValue(const String& name, bool& save);
	static void loadCfgValue(const String& name, int& save);
	static void loadCfgValue(const String& name, String &save);

	static void _setCfgValue(const String& name, const int& value);
	static void _setCfgValue(const String& name, const bool& value);
	static void _setCfgValue(const String& name, const float& value);
	static void _setCfgValue(const String& name, const String& value);
	static void _setCfgValue(const String& name, const CValue& value);
	static void _saveCfgValue(const String& name, const int& value);
	static void _saveCfgValue(const String& name, const bool& value);
	static void _saveCfgValue(const String& name, const String& value);
	static void _saveCfgValue(const String& name, const CValue& value);

	template <class T>
	static void setCfgValue(const String& name, const T& value)
	{
		_setCfgValue(name, value);
	}

	template <class T>
	static void saveCfgValue(const String& name, const T& value)
	{
		_saveCfgValue(name, value);
	}

	static void variableInit(const String& name);
	static int variableLoad(const String& name);
	static void variableSave(const String& name);
	static void variableSelectStage(int i);
	static void variableAddStage(size_t number, const String& name);
	static int variableGetStageName(int i);
	static int variableGetNum();

	static void setVisibleVariables();
	static void getVisibleVariables();
	static void setVisibleVariablesR(Ogre::SceneNode *pRoot);
	static void getVisibleVariablesR(Ogre::SceneNode *pRoot);

	// from emerald
	static void loadVars(const int, const int, int*, int&, const int);
	static void clearVars(int*, int &, const int);
	static void saveVars(const int, const int, int*, int&, const int);
	static int existVars(int, int);
	static String getVarName(const int, const int, const int);

	//
	static void initGame();
	static bool getSaveStatus(const String& filename);
	static String getNameCurrent();
	static int getNameFromSave(const String& filename, String& name);
	static void setNameCurrent(const String& name);
	static int setNameToSave(const String& filename, const String& name);
	static void deleteSave(const String& filename);
	static void unlinkFile(const String& filename);

	static Ogre::Ray getViewportRay(float x, float y);
		// ������������ �� ���, ��������� �� ����� vecScr, � �������� hObject
		// (��� �������� �� visible, selectable � �.�.)
	static bool objectRaycast(const Vector2 &vecScr, OBJECT_HANDLE hObject, Vector3 &vecResult);
	static bool getViewPos(OBJECT_HANDLE handle, Vector2& vp);
	static void get3dCoordFrom2d(const Vector2& vec2d, float z, Vector3& vec3d);
	static void get3dCoordFrom2dAbsolute(const Vector3& vec2d, Vector3& vec3d);
	static void get2dCoordFrom3d(const Vector3& vec3d, Vector2& vec2d);
	static void get2dCoordFrom3dAbsolute(const Vector3& vec3d, Vector2& vec2d);
	static OBJECT_HANDLE findObject(const String& name, int index);

	static void setInfiniteBounds(OBJECT_HANDLE hObj);
		// ������� ���� - ���� ������ � ����������� ������
	static bool getBounds2d(OBJECT_HANDLE hObj, Vector2 &vecLeftTop, Vector2 &vecRightBottom);

	static void intToString(String& str, int i, unsigned short w);
	static void intAddToString(String& str, int i, unsigned short width);
	static String intToString(int i, unsigned short width);
	static void floatToString(String& str, float f);
	static void vecToString(String& str, Vector3 &v);
	static void removeStringFromString(String& str, const String& what_remove);

	static void getMyName(OBJECT_HANDLE obj, String& name);
	static const String& getMyName(OBJECT_HANDLE obj);
	static CVirtualScript* getVirtualScript(OBJECT_HANDLE obj);
	const String& getMyScriptName(OBJECT_HANDLE handle);


	Ogre::ManualObject* createManualObject(const String& name, OBJECT_HANDLE attach_to, Ogre::uint8 queue);


#if OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
	template<class Script>
	static Script* getScript(const String& name)
	{
		CVirtualScript *pScript = getVirtualScript(findObjectByNameIf(name));

		if(pScript)
			return static_cast<Script*>(pScript);
		else
			return NULL;
	}
	template<class Script>
	static Script* getScript(OBJECT_HANDLE obj)
	{
		CVirtualScript *pScript = getVirtualScript(obj);

		if(pScript)
			return static_cast<Script*>(pScript);
		else
			return NULL;
	}
#endif

	static CVirtualScript* addScript(OBJECT_HANDLE obj, const String &script);

	static void getOrientation2(OBJECT_HANDLE handle, Quaternion& q);
	static void setOrientation2(OBJECT_HANDLE handle, const Quaternion& q);
	static void initOrientation(Quaternion& q, const Vector3 &ang_rot);
	static void transformOrientation(Ogre::Quaternion& res, const Ogre::Quaternion& q1, const Ogre::Quaternion& q2);
	static void interpOrientation(Ogre::Quaternion& res, const Ogre::Quaternion& q1, const Ogre::Quaternion& q2, float w);

	static void createRTT(const String& text_name, const String& camera, int sx, int sy, int params, unsigned int visible_mask);
	static void createRTT2(const String& text_name, const String& camera, int sx, int sy, int params, unsigned int visible_mask, float left, float top, float right, float bottom);
	static void _createRTT(const String& text_name, const String& camera, int sx, int sy, int params, unsigned int visible_mask, float left, float top, float right, float bottom, int bpp);
	static void updateRTT(const String& text_name);
	static void setRTTOverlayEnable(const String& text_name, bool enable);
	static void setRTTDimension(const String& text_name, float left, float right, float top, float bottom);
	static void setRTTAutoUpdate(const String& text_name, bool fenable);
	static void setRTTVisibleMask(const String& text_name, int param);
	static void saveScreenShot(const String& file_name, const String& texture_name, int vm, bool over);
	static void saveScreenShot2(const String& file_name, const String& texture_name, int vm, bool over);
	static void saveRTTtoFile(const String& texture_name, const String& file_name);
	static void setViewportOverlayEnable(bool enable);

	static bool cheats();
	static void setCheats(bool value);

	static float cameraGetFOV(const Ogre::String& fov_camera);
	static void cameraSetFOV(const Ogre::String& fov_camera);
	static void cameraSetFOV(float fov);
	static void cameraSetOrientation(const Quaternion &q);
	static void cameraSetPosition(const Vector3 &v);
	static void cameraSetRenderArea(float left, float top, float right, float bottom);
	static void cameraGetRenderArea(float &left, float &top, float &right, float &bottom);
	static void cameraSetAspect(float aspect);
	static float getGameAspect();


	static void nodeSetQueue(OBJECT_HANDLE handle, int queue);

	// ������ ��� SfxCore
	static int loadSound(const char* sound_id);
	static void playSound(int id, int group, bool looped, SFX_OBJECT_HANDLE notifyObj);
	static void stopSound(int id);
	static void freeSound(int id);
	static unsigned getSoundDuration(int id);
	static void setVolume(int id, int volume);
	static void setEnvLevel(float level);
	static void addEnvSound(int id, bool looped, SFX_OBJECT_HANDLE notifyObj);
	static void setEnvFadeSpeed(float speed);
	static void setGroupVolume(int group, int volume);
        // ��������� ����� ������ (���� �� ���� ���������)
    static void loadEnvSounds();
	static int getVolume(int id);
	static bool isPlaying(int id);

	// ������ ��� CMainApplication
	static OBJECT_HANDLE findObjectByName(const String& name); // ������� ������ � ������ �������
															//������������, ����� ����� ��������, ��� ����� ������ ����������
	static OBJECT_HANDLE findObjectByNameIf(const String& name); //�� ������� ������ � ������ �������
															// ������������, ����� �� ��������, ���� �� ����� ������ ��� ���

	static int getFrameTimeMS(); //�����, ��������� � ����������� ����� � �������������
	static float getFrameTimeS(); //�����, ��������� � ����������� ����� � ��������
	static int getTimeMS(); //time since app started, paused, when app unfocused
	static Ogre::ulong getSystemTimeMS();
	static void deleteScene();
	static void loadScene(const String& sceneName, bool useLoader = true);
	static void setViewCamera(const String& name);
	static void setCameraAspect(const String& name, float aspect);

	static OBJECT_HANDLE getSourceHandle(const CScriptMsg * pMsg);

	static String getResourceAsString(const String &filename);
	static String getResourceAsString(const String &filename, const String &group);

    static void initialiseResourceGroup(const String &resourceGroup);

	static void setGameIcon(unsigned short );

	static void get3dFrom2dEx(const Vector2 &pos2d, float zdepth, Vector3 &res);
	void fontSwitchFiltering(bool enable);

	static void setQueryMask(unsigned long mask);

	static Ogre::Camera *getCamera();
	static Ogre::SceneManager *getSceneManager();

//	static CGameObject*  _findObjectByHandle(OBJECT_HANDLE obj);

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	static void addObbLocation();
#endif

//app initialization
	static void addCursor(int id, const String& curCustom, int curStandard, const String &material);
	static void addResourcePath(const String& path, const String& group);
	static void appSetup(const String &company, const String &appname, const String &shortname, SetupScriptFunc func);
	static Vector2 getWindowResolution();
	static Vector2 findResolution(bool fs);
	static void disableObjectsFocus(bool state);
	static bool isMouseLButton();
	static bool isMouseRButton();

	static std::string getKeyboardText();
	static void enterBackground();
	static void showKeyboard(bool show);
	static void setKeyboardText(const std::string& text);

	static void loadRectArrayFromCfg(const String fname, std::vector<std::pair<Ogre::String,Ogre::RealRect> > &rects);

//����� theora


	static float startVideoClip(const String &materialName, const String &videoName);
	static void stopVideoClip(const String &videoName);
	static void destroyVideoClip(const String &textureName, const String &videoName);
	static void destroyVideoClips();
	static void setVideoClipLoop(const String &videoName, bool loop);
	static int getVideoClipWorkerThreads();

#if USING_THEORA
	static int  getNumReadyFrames();
#endif

// ������
#if USE_BULLET == TRUE
	static void attachBoneToBone(OBJECT_HANDLE h, const String& parent_bone, const String& child_bone);
	static void setPhysicsProp(OBJECT_HANDLE h, int prop, Vector3 &val, int ival);
#endif

private:
	static Ogre::DisplayString _overlaySetCharColorsFromCaption(const String& o, const String& c, const String& in, const Ogre::DisplayString &str, bool only_parse = true);

public:
	static std::map<Ogre::String, Ogre::String> mScriptAliases;

public:
	String mScriptName;
	ScrType mScriptType;

private:
	OBJECT_HANDLE mHandle;
};


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
GAME_API int StartApplication(const char* lpCmdLine);
#	define JENGINE_APPLICATION(companyname, appname, shortname, setupScript) \
	void setupScript(); \
	int WINAPI WinMain( HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, LPSTR lpCmdLine, int /*nCmdShow*/) \
	{ \
		CVirtualScript::appSetup(companyname,appname,shortname,setupScript); \
		return StartApplication(lpCmdLine); \
	}
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
GAME_API int StartApplication(const char* lpCmdLine);
#	define JENGINE_APPLICATION(companyname, appname, shortname, setupScript) \
	void setupScript(); \
	int main(int argc, char **argv) \
	{ \
		CVirtualScript::appSetup(companyname,appname,shortname,setupScript); \
		return StartApplication(argc>1 ? argv[1] : ""); \
	}
#elif OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
GAME_API int StartApplication(int argc, char **argv);
#	define JENGINE_APPLICATION(companyname, appname, shortname, setupScript) \
	void setupScript(); \
	int main(int argc, char **argv) \
	{ \
		CVirtualScript::appSetup(companyname,appname,shortname,setupScript); \
		return StartApplication(argc, argv); \
	}
#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
GAME_API int StartApplication(int argc, char **argv);
#	define JENGINE_APPLICATION(companyname, appname, shortname, setupScript) \
	void setupScript(); \
	void android_main_jetdogs() \
	{ \
		CVirtualScript::appSetup(companyname,appname,shortname,setupScript); \
	}
#else
#	error "Unsupport platform"
#endif
