// header: EventPlayer.h
#pragma once

#if USE_OIS_FOR_INPUT == TRUE	

#include <OIS.h>
#include <OgrePrerequisites.h>
#include <OgreVector2.h>

class EventPlayer
{
public:
	enum TPlayerMode
	{
		eNone	= -1,
		eRecord	= 0, 
		ePlay	= 1
	};

	struct Event
	{
		unsigned time;
		int type; //0 - mouse, 1 - kbrd
		int key;
		int pressed;
		Ogre::Vector2 pos;
		Ogre::String focused_obj;
	};

public:
	EventPlayer(TPlayerMode m, const Ogre::String& fname);
	virtual ~EventPlayer();

	bool isPlay() const { return (mPlayerMode == ePlay); }
	OIS::MouseEvent getMouseEvent(Event &e) const;
	
	void process();

	void loadEvents(const Ogre::String& filename);
	void saveEvent(const Event &e);
	void saveEvent(const OIS::KeyEvent &e, int pressed);
	void saveEvent(const OIS::MouseEvent &e, int pressed, OIS::MouseButtonID id = (OIS::MouseButtonID)-1);

public:
	TPlayerMode mPlayerMode;
	unsigned int mCreateTime;
	size_t mCurEventNum;

	FILE *mSaveFile;
	
private:
	std::vector<Event> mEvents;
};

#endif
