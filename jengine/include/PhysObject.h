#pragma once

#if USE_BULLET == TRUE

#ifdef _DEBUG
	#pragma comment (lib,"OgreBulletDynamics_d.lib")
	#pragma comment (lib,"OgreBulletCollisions_d.lib")
	#pragma comment (lib,"bulletcollision.lib")
	#pragma comment (lib,"bulletdynamics.lib")
	#pragma comment (lib,"linearmath.lib")
	#pragma comment (lib,"convexdecomposition.lib")
#else
	#pragma comment (lib,"OgreBulletDynamics.lib")
	#pragma comment (lib,"OgreBulletCollisions.lib")
	#pragma comment (lib,"bulletcollision.lib")
	#pragma comment (lib,"bulletdynamics.lib")
	#pragma comment (lib,"linearmath.lib")
	#pragma comment (lib,"convexdecomposition.lib")	
#endif

#include "GameObject.h"

#include <vector>

#include "OgreBulletDynamicsWorld.h"
#include "OgreBulletCollisionsShape.h"
#include "Shapes/OgreBulletCollisionsBoxShape.h"
#include "Shapes/OgreBulletCollisionsSphereShape.h"
#include "Shapes/OgreBulletCollisionsConeShape.h"
#include "Shapes/OgreBulletCollisionsCylinderShape.h"
#include "Shapes/OgreBulletCollisionsTriangleShape.h"
#include "Shapes/OgreBulletCollisionsStaticPlaneShape.h"
#include "Shapes/OgreBulletCollisionsCapsuleShape.h"
#include "OgreBulletDynamicsRigidBody.h"
#include "Debug/OgreBulletCollisionsDebugDrawer.h"


class CPhysObject : public CGameObject
{
public:
	void init();
	CPhysObject();
	CPhysObject(String& scrName);
	
	virtual ~CPhysObject();
	virtual void processFrame( const Ogre::FrameEvent& evt);

	class CBody
	{
	public:
		enum State
		{
			Slave,
			Master
		};

		Ogre::SceneNode *pSNode;
		Ogre::SceneNode *pRigitBodyNode;
		Ogre::Bone *pBone;
		OgreBulletDynamics::RigidBody *pBody;
		OgreBulletCollisions::CollisionShape *pShape;
		String mBodyName;
		State mState;

		CBody(Ogre::SceneNode *n,
			  Ogre::SceneNode *rbn,
			  Ogre::Bone *b,
			  OgreBulletDynamics::RigidBody *bd,
			  OgreBulletCollisions::CollisionShape *s,
			  const String& name,
			  State st) 
			  : pSNode(n), 
			  pBone(b), 
			  pBody(bd), 
			  pShape(s),
			  mBodyName(name),
			  mState(st),
			  pRigitBodyNode(rbn)
		{
			
		}

		void updateState()
		{
			if (mState == Slave)
			{
				pBody->getBulletRigidBody()->setActivationState(ISLAND_SLEEPING);
			}
			else if (mState == Master)
			{
				pBody->setLinearVelocity(Vector3(0));
				pBody->getBulletRigidBody()->setActivationState(ACTIVE_TAG);
			
				pBone->setManuallyControlled(true);

				Ogre::Node::ChildNodeIterator it = pBone->getChildIterator();
				while (it.hasMoreElements())
				{
					Ogre::Bone *b = static_cast<Ogre::Bone*>(it.getNext());
					
					//b->setManuallyControlled(true);
					
				}

			}
		}

		void update()
		{
			if (mState == Slave)
			{
				Quaternion q0 = pBone->_getDerivedOrientation();

				Quaternion q1 = pBone->getOrientation();

				Vector3 v = pBone->_getDerivedPosition();

				Quaternion q2 = pSNode->_getDerivedOrientation();
				Vector3 v2 = pSNode->_getDerivedPosition();

				v = q2*v+v2;
				Quaternion q = q2*q0;

				
				pBody->setPosition(v);
				pBody->setOrientation(q);
				
				btTransform xForm;
				xForm.setOrigin(btVector3(v.x,v.y,v.z));
				xForm.setRotation(btQuaternion(q.x,q.y,q.z,q.w));
				pBody->getBulletRigidBody()->setWorldTransform(xForm);
			}
			else
			{
				Vector3 v = pBody->getWorldPosition();
				Quaternion q = pBody->getWorldOrientation();

				Ogre::Vector3 parentPos = pSNode->_getDerivedPosition(); // node local pos
				Ogre::Vector3 parentQuatXbonePos = v - parentPos;
				Ogre::Quaternion parentQuat = pSNode->_getDerivedOrientation(); // node local ori
				Ogre::Vector3 bonePos = parentQuat.Inverse() * parentQuatXbonePos;
				pBone->_setDerivedPosition(bonePos);
				Ogre::Quaternion boneQuat = parentQuat.Inverse()*q; // equiv to ("boneQuat = worldQuat / parentQuat")
				pBone->_setDerivedOrientation(boneQuat);
			}
		}
	};

	std::vector<CBody> mBodies;

	void AddBody(CBody &b)
	{
		if (b.mState == CBody::Slave)
		{
			b.updateState();
			b.update();
		}

		//b.pBody->setLinearVelocity(Vector3(0,0,0));
		
		//b.mState = CBody::Master;
		//b.update();

		mBodies.push_back(b);
	}

	void UpdateBodies()
	{
		for(int i = 0; i < mBodies.size(); i++)
		{
			mBodies[i].update();
		}
	}

	void ChangeBodyState(int idd, int state)
	{
		if (idd == -1)
		{
			for(int i = 0; i < mBodies.size(); i++)
			{
				if (mBodies[i].mState != (CBody::State)state)
				{
					mBodies[i].mState = (CBody::State)state;
					mBodies[i].updateState();
				}
			}
		}
		else
		{
			if (mBodies[idd].mState != (CBody::State)state)
			{
				mBodies[idd].mState = (CBody::State)state;
				mBodies[idd].updateState();
			}
		}
	}

	int _findBodyByName(const String& bone_name)
	{
			for(int i = 0; i < mBodies.size(); i++)
			{
				if (bone_name == mBodies[i].pBone->getName())
				{
					return i;
				}
			}

			return -1;
	}

	OgreBulletDynamics::RigidBody *getRigitBody(const String& bone_name)
	{
		int idd = -1;

		idd = _findBodyByName(bone_name);

		if (idd == -1)
			return NULL;
		else
			return mBodies[idd].pBody;
	}

	/*Vector3 getRigitBody(const String& bone_name)
	{
		int idd = -1;

		idd = _findBodyByName(bone_name);

		if (idd == -1)
			return Vector3(0);
		else
			return mBodies[idd].;
	}*/
};

#endif