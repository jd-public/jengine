#ifndef _DYNAMIC_LINES_H_
#define _DYNAMIC_LINES_H_

#include <OgrePrerequisites.h>
#include <vector>
#include "DynamicRenderable.h"

class DynamicLines : public DynamicRenderable
{
public:
	/// Constructor - see setOperationType() for description of argument.
	DynamicLines(Ogre::RenderOperation::OperationType opType=Ogre::RenderOperation::OT_LINE_STRIP);
	virtual ~DynamicLines();

	/// Add a point to the point list
	void addPoint(const Ogre::Vector3 &p);
	/// Add a point to the point list
	void addPoint(Ogre::Real x, Ogre::Real y, Ogre::Real z);

	/// Change the location of an existing point in the point list
	void setPoint(unsigned short index, const Ogre::Vector3 &value);

	/// Return the location of an existing point in the point list
	const Ogre::Vector3& getPoint(unsigned short index) const;

	/// Return the total number of points in the point list
	unsigned short getNumPoints() const;

	/// Remove all points from the point list
	void clear();

	/// Call this to update the hardware buffer after making changes.  
	void update();

	/** Set the type of operation to draw with.
	* @param opType Can be one of 
	*    - Ogre::RenderOperation::OT_LINE_STRIP
	*    - Ogre::RenderOperation::OT_LINE_LIST
	*    - Ogre::RenderOperation::OT_POINT_LIST
	*    - Ogre::RenderOperation::OT_TRIANGLE_LIST
	*    - Ogre::RenderOperation::OT_TRIANGLE_STRIP
	*    - Ogre::RenderOperation::OT_TRIANGLE_FAN
	*    The default is OT_LINE_STRIP.
	*/
	void setOperationType(Ogre::RenderOperation::OperationType opType);
	Ogre::RenderOperation::OperationType getOperationType() const;

protected:
	/// Implementation DynamicRenderable, creates a simple vertex-only decl
	virtual void createVertexDeclaration();
	/// Implementation DynamicRenderable, pushes point list out to hardware memory
	virtual void fillHardwareBuffers();

private:
	std::vector<Ogre::Vector3> mPoints;
	bool mDirty;
};
#endif
