#pragma once

#include <OgreVector3.h>
#include <OgreSerializer.h>
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
#include <jni.h>
#include <android/asset_manager.h>
#endif

#define MAX_ITEMS_SIZE 128
#define MAX_SCRIPT_VARS 2048

/*class CValue
{
public:
	CValue() : saveToReg(0) {}
	CValue(bool s, int r = 0) : text(Ogre::StringConverter::toString(s)), saveToReg(r) {}
	CValue(int s, int r = 0) : text(Ogre::StringConverter::toString(s)), saveToReg(r) {}
	CValue(float s, int r = 0) : text(Ogre::StringConverter::toString(s)), saveToReg(r) {}
	CValue(const char* s, int r = 0) : text(s), saveToReg(r) {}
	CValue(const Ogre::String& s, int r = 0) : text(s), saveToReg(r) {}

	Ogre::String text;
	int saveToReg;
};*/

#include "OgreStringConverter.h"

typedef std::map<Ogre::String, CValue>							ConfigureMap;
typedef std::map<Ogre::String, CValue>::iterator				ConfigureMapIterator;
typedef std::map<Ogre::String, CValue>::const_iterator			ConfigureMapIteratorConst;
typedef std::map<Ogre::String, Ogre::String>					Stage;
typedef std::map<Ogre::String, Ogre::String>::iterator			StageIterator;
typedef std::map<Ogre::String, Ogre::String>::const_iterator	StageIteratorConst;

struct Save
{
	Stage curStage;
	std::vector<Stage> stage;
	Ogre::String name;
	void clear()
	{
		stage.clear();
		curStage.clear();
	}
};

class GAME_API CAppState : public Ogre::Serializer
{
public:
	CAppState();
	~CAppState();

	void configureInit();
	void configureLoadDefaults();
	void configureLoad();
	void configureSave() const;
	void configureLoadConfig(const Ogre::String& filename, bool save_params = true);
	void configureSaveConfig(const Ogre::String& filename) const;
    bool fileExists(const Ogre::String& filename) const;

	//bool configureLoadValue(const Ogre::String& name);
	//void configureSaveValue(const Ogre::String& name);

	bool hasValue(const Ogre::String& name) const;
	const CValue& getValue(const Ogre::String& name) const;
	bool getValueBool(const Ogre::String& name) const;
	int getValueInt(const Ogre::String& name) const;
	unsigned int getValueUint(const Ogre::String& name) const;
	float getValueFloat(const Ogre::String& name) const;
	const String& getValueText(const Ogre::String& name) const;
	template <class T>
	void setValue(const Ogre::String& name, const T& value)		{ mConfigureMap[name].text = Ogre::StringConverter::toString(value); }
	void setValue(const Ogre::String& name, const char* value)	{ mConfigureMap[name].text = value; }
	void setValue(const Ogre::String& name, const String& value){ mConfigureMap[name].text = value; }
	void setValue(const Ogre::String& name, const CValue& value);

	bool IsParamSaveToCfg(const Ogre::String& param);

	void variableInit(const Ogre::String& );
	bool variableLoad(const Ogre::String& name);
	void variableSave(const Ogre::String& name);

	bool mapLoad(Save& save, const Ogre::String& filename);
	void mapSave(Save& save, const Ogre::String& filename);
	bool getMapStatus(const Ogre::String& filename);

	int getNameFromSave(const Ogre::String&, Ogre::String& );
	int setNameToSave(const Ogre::String&, const Ogre::String&);
	void deleteVariableFile(const Ogre::String&);

	Ogre::String getAppDataDirectoryName() const;
	Ogre::String getLocalTimeString();
	Ogre::String getFullName(const Ogre::String&, bool create = false) const;

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	void setAssetManager(AAssetManager* mgr) { mAssetMgr = mgr; }
	Ogre::DataStreamPtr openAPKFile(const Ogre::String& fileName);
#endif

private:
	bool getDirectXVersion(int &major, int &minor);

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	AAssetManager* mAssetMgr;
#endif

public:
	Save mSave;

private:
	ConfigureMap mConfigureMap;
	Ogre::String mSubKeyName;
	static CValue mNullValue;
};
