#ifndef __ATLAS__MANAGER___H__
#define __ATLAS__MANAGER___H__

#include <OgrePrerequisites.h>
#include <OgreSingleton.h>
#include <OgreStringVector.h>
#include <OgreScriptLoader.h>

// NOT defnined on android
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
#	define FALSE 0
#	define TRUE  1
#endif

namespace Ogre
{
struct AtlasTexture
{
	int x, y;
	int width, height;
	bool rotate;
	RealRect rect;
	String name;
	String atlas;

	AtlasTexture() { x=y=width=height=0; rotate = false; }
	bool operator==(const AtlasTexture& tex) const;
	bool operator!=(const AtlasTexture& tex) const { return !operator==(tex);}
};
typedef OGRE_HashMap<String, AtlasTexture> AtlasTextureMap;

struct Atlas
{
	String filename;
	int width, height;

	Atlas() { width=height=0; }
};
typedef OGRE_HashMap<String, Atlas> AtlasMap;

class GAME_API CAtlasManager : public Ogre::Singleton<CAtlasManager>, public Ogre::ScriptLoader
{
public:
	CAtlasManager();
	~CAtlasManager();

	bool hasAtlasTexture(const String& name) const;
		// ���������� �������� ������ �� ��������������� ����� ��������
	const AtlasTexture& getAtlasTexture(const String& name) const;

		// ���������� ��� �� ������ �����
	bool changeMaterialTexturesToAtlas(const String& materialName) const;
		// @bSingleState: true - ������ ������ ���� ���� ������
	bool changeMaterialTexturesToAtlas(const String& materialName, const String& textureName, bool bSingleState) const;

    Real getLoadingOrder() const
    {
        // Load late
        return 89.0f;
    }
	
	const Ogre::StringVector& getScriptPatterns() const { return mScriptPatterns; }
	void parseScript(Ogre::DataStreamPtr& stream, const String& groupName);

	static CAtlasManager& getSingleton();
	static CAtlasManager* getSingletonPtr();

public:
	static AtlasTexture mAtlasTextureNull;

private:
	Ogre::StringVector mScriptPatterns;
	AtlasTextureMap mAtlasTextures;
	AtlasMap mAtlases;
};

}
#endif // __ATLAS__MANAGER___H__