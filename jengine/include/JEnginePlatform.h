//
//  JEnginePlatform.h
//  JEngine
//
//  Created by Lyonya on 14.09.12.
//  Copyright (c) 2012 JetDogs Studios. All rights reserved.
//

#ifndef __JenginePlatform_H__
#define __JenginePlatform_H__

#define J_STD_CURSOR_ARROW		0

#define MT_STOP_ANIMATION 1
typedef	unsigned int OBJECT_HANDLE;
typedef void(*SetupScriptFunc)();


#if (OGRE_PLATFORM == OGRE_PLATFORM_APPLE) || (OGRE_PLATFORM == OGRE_PLATFORM_APPLE_IOS) || (OGRE_PLATFORM == OGRE_PLATFORM_ANDROID)
    typedef Ogre::uint8 BYTE;
	typedef Ogre::uint16 WORD;
	typedef Ogre::uint32 DWORD;
	typedef Ogre::uint64 QWORD;
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#   ifndef JENGINE_STATIC
#       if defined JENGINE_EXPORTS
#           define GAME_API __declspec(dllexport)
#       elif defined JENGINE_IMPORT
#           define GAME_API __declspec(dllimport)
#       else
#           error "Need defined JENGINE_STATIC/JENGINE_EXPORTS/JENGINE_IMPORT"
#       endif
#   else
#       define GAME_API
#   endif
#else
#   define GAME_API
#endif


#define SAFE_DEL_A(x)	if ((x) != NULL)	{ delete[] (x), (x) = NULL; }
#define SAFE_DEL(x)		if ((x) != NULL)	{ delete (x), (x) = NULL; }
#define EPSILONREAL	0.00001f

#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
#	define MAX_PATH 260
#endif

typedef int MouseCursor;


//language id
//used in setCfgI("language_id",XXX)

#define JENGINE_LANG_EN		1
#define JENGINE_LANG_DE		1<<1
#define JENGINE_LANG_ES		1<<2
#define JENGINE_LANG_FR		1<<3
#define JENGINE_LANG_IT		1<<4
#define JENGINE_LANG_RU		1<<5
#define JENGINE_LANG_JA		1<<6
#define JENGINE_LANG_KO		1<<7
#define JENGINE_LANG_NL		1<<8
#define JENGINE_LANG_SV		1<<9
#define JENGINE_LANG_ZH		1<<10
#define JENGINE_LANG_PT		1<<11
#define JENGINE_LANG_BR		1<<12

#define JENGINE_LANG_EU		(JENGINE_LANG_EN | JENGINE_LANG_DE | JENGINE_LANG_ES | JENGINE_LANG_FR | JENGINE_LANG_IT | JENGINE_LANG_RU | JENGINE_LANG_NL | JENGINE_LANG_SV | JENGINE_LANG_PR | JENGINE_LANG_BR)
#define JENGINE_LANG_ASIAN	(JENGINE_LANG_KO | JENGINE_LANG_ZH | JENGINE_LANG_JA)


//------------------------------------------------------------------
//платформы для дж-енжина,
//можно использовать для тестирования айфон интерфейса, например под виндой
//не отменяют и не как не зависят от OGRE_PLATFORM
//сделаны, чтобы как-то сгруппировать технические возможности разных устройств

//сама платформа здесь не задается, она меняется в основном exe-шнике
//кодом, наподобие такого
//#ifdef JENGINE_PLATFORM
//	#undef JENGINE_PLATFORM
//#endif
//
//#define JENGINE_PLATFORM	JENGINE_PLATFORM_IPAD

#ifndef JENGINE_PLATFORM
#define JENGINE_PLATFORM					0
#endif

#define JENGINE_PLATFORM_PC					1
#define JENGINE_PLATFORM_MAC				(1<<1)
#define JENGINE_PLATFORM_LINUX				(1<<2)

#define JENGINE_PLATFORM_IPHONE_3G			(1<<8)
#define JENGINE_PLATFORM_IPHONE_3GS			(1<<9)
#define JENGINE_PLATFORM_IPHONE_4G			(1<<10)
#define JENGINE_PLATFORM_IPHONE_4GS			(1<<11)
#define JENGINE_PLATFORM_IPHONE_5G			(1<<12)

#define JENGINE_PLATFORM_IPAD1				(1<<16)
#define JENGINE_PLATFORM_IPAD2				(1<<17)
#define JENGINE_PLATFORM_IPAD3				(1<<18)

#define JENGINE_PLATFORM_ANDROID_LOW_RES	(1<<24)
#define JENGINE_PLATFORM_ANDROID_MID_RES	(1<<25)
#define JENGINE_PLATFORM_ANDROID_HI_RES		(1<<26)

#define JENGINE_PLATFORM_WP7_LOW_RES		(1<<27)
#define JENGINE_PLATFORM_WP7_MID_RES		(1<<28)
#define JENGINE_PLATFORM_WP7_HI_RES			(1<<29)

//-----

#define JENGINE_PLATFORM_IPHONE_LOW_RES		(JENGINE_PLATFORM_IPHONE_3G | JENGINE_PLATFORM_IPHONE_3GS)
#define JENGINE_PLATFORM_IPHONE_MID_RES		(JENGINE_PLATFORM_IPHONE_4G | JENGINE_PLATFORM_IPHONE_4GS | JENGINE_PLATFORM_IPHONE_5G)

#define JENGINE_PLATFORM_IPHONE				(JENGINE_PLATFORM_IPHONE_LOW_RES | JENGINE_PLATFORM_IPHONE_MID_RES)
#define JENGINE_PLATFORM_IPAD				(JENGINE_PLATFORM_IPAD1 | JENGINE_PLATFORM_IPAD2 | JENGINE_PLATFORM_IPAD3)
#define JENGINE_PLATFORM_IOS				(JENGINE_PLATFORM_IPHONE |JENGINE_PLATFORM_IPAD)

#define JENGINE_HI_RES_PLATFORM				(JENGINE_PLATFORM_PC | JENGINE_PLATFORM_MAC | JENGINE_PLATFORM_LINUX | JENGINE_PLATFORM_IPAD)
#define JENGINE_LOW_RES_PLATFORM			(JENGINE_PLATFORM_IPHONE_LOW_RES | JENGINE_PLATFORM_ANDROID_LOW_RES | JENGINE_PLATFORM_WP7_LOW_RES)
#define JENGINE_MID_RES_PLATFORM			(JENGINE_PLATFORM_IPHONE_MID_RES | JENGINE_PLATFORM_ANDROID_MID_RES | JENGINE_PLATFORM_WP7_MID_RES)

//----

#define IS_JENGINE_PLATFORM_IPHONE			(JENGINE_PLATFORM & JENGINE_PLATFORM_IPHONE)
#define IS_JENGINE_PLATFORM_IPAD			(JENGINE_PLATFORM & JENGINE_PLATFORM_IPAD)
//#define IS_JENGINE_PLATFORM_IOS				(JENGINE_PLATFORM & JENGINE_PLATFORM_IOS)
#define IS_JENGINE_PLATFORM_IOS				((JENGINE_PLATFORM & JENGINE_PLATFORM_IPAD) | (JENGINE_PLATFORM & JENGINE_PLATFORM_IPHONE))


//480x320 и ниже
#define IS_JENGINE_PLATFORM_LOW_RES			(JENGINE_PLATFORM & JENGINE_LOW_RES_PLATFORM)

//960x640 (среднее между лоу и мид)
#define IS_JENGINE_PLATFORM_MID_RES			(JENGINE_PLATFORM & JENGINE_MID_RES_PLATFORM)

//1024x768 и выше
#define IS_JENGINE_PLATFORM_HI_RES			(JENGINE_PLATFORM & JENGINE_HI_RES_PLATFORM)

//------------------------------------------------------------------


#endif // __JenginePlatform_H__
