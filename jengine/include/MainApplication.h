#pragma once

#include "config.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	#include <jni.h>
	#include <android/asset_manager.h>
	#include "Android/OgreAPKFileSystemArchive.h"
	#include "Android/OgreAPKZipArchive.h"

	#ifndef OVERRIDE
		#define OVERRIDE
	#endif
#endif

#if USE_OIS_FOR_INPUT == 1
	#include <OIS.h>
#endif

#if USING_THEORA
#	include <OgreVideoManager.h>
#endif

#if USE_BULLET == 1
#	include <OgreBulletDynamics.h>
#	include <OgreBulletDynamicsWorld.h>
#endif

#ifdef OGRE_STATIC_LIB
#   define OGRE_STATIC_ParticleFX
#   if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#       define OGRE_STATIC_Direct3D9
#   elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
#       define OGRE_STATIC_GL
#   elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
#		define OGRE_STATIC_GLES
#   endif
#endif

#include "AppState.h"
#include "EventPlayer.h"
#include "GameObject.h"
#include "AtlasManager.h"
#include "ScriptMsg.h"
#include "MathUtils.h"
#include "StaticPluginLoader.h"
#include "StringTable.h"
#include "FontdefManager.h"


class CAppGestureViewCpp;
class CMainApplication;
class CAppOISListener;
class CGameObject;
class CHeroObject;
class DataArchiveFactory;
class CProgressListener;
class TextRenderer;
class CBinaryMaterials;

#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
    #include <OgreFileSystemLayer.h>
#endif

//--------------------------
class GAME_API CMainApplication : public Ogre::FrameListener, public Ogre::WindowEventListener
{

public:

#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
    Ogre::FileSystemLayer *mFSLayer;
#endif


	CMainApplication();
	~CMainApplication();

	void initialize();
	void deinitialize();

	const Ogre::String& getCompanyName() const		{ return mCompanyName; }
	const Ogre::String& getAppFullName() const		{ return mAppFullName; }
	const Ogre::String& getAppShortName() const		{ return mAppShortName; }
	void setCompanyName(const Ogre::String& name)	{ mCompanyName=name; }
	void setAppFullName(const Ogre::String& name)	{ mAppFullName=name; }
	void setAppShortName(const Ogre::String& name)	{ mAppShortName=name; }
	void setSetupScriptFunc(SetupScriptFunc func)	{ mSetupScriptFunc = func; }

	bool logEnabled() const							{ return mDoLog; }
	bool soundsEnabled() const						{ return mSoundsEnabled; }
	bool cheatsEnabled() const						{ return mCheats; }
	bool lowMemModeEnabled() const					{ return mLowMemMode; }
	bool screensEnabled() const						{ return mDoScreens; }

	void processCommandLine(const char *lpCmdLine);
	void enableSetupDlg(bool enable = true)			{ mSetDlg = enable; }
	void enableLog(bool enable = true)				{ mDoLog = enable; }
	void enableScreens(bool enable = true)			{ mDoScreens = enable; }
	void enableLowMemMode(bool enable = true)		{ mLowMemMode = enable; }
	void enableLowVideoMemMode(bool enable = true)	{ mLowVideoMemMode = enable; }
	void enableSounds(bool enable = true)			{ mSoundsEnabled = enable; }
	void enableNoTexMode(bool enable = true)		{ mNoTex = enable; }
	void enableCheats(bool enable = true)			{ mCheats = enable; }
	void enableShowInfo(bool enable = true)			{ mShowInfo = enable; }

	Ogre::Root* getRoot()								{ return mRoot; }
	const Ogre::Root* getRoot()	const					{ return mRoot; }
	Ogre::RenderWindow* getWindow()						{ return mWindow; }
	const Ogre::RenderWindow* getWindow() const			{ return mWindow; }

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
	Ogre::Viewport* getViewportBk()						{ return mViewportBk; }
	const Ogre::Viewport* getViewportBk() const			{ return mViewportBk; }
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	void setupAndroid(JavaVM* vm, JNIEnv* env, jobject act, AAssetManager* assetMgr, const String& internalPath);
	void configureAndroid(const String& extWndHandle);

	void showKeyboard(bool show);
	std::string getKeyboardText();
	void setKeyboardText(const std::string& text);

	AAssetManager* getAssetManager() { return mAssetMgr; }
	JavaVM* getJavaVM() { return mJVM; }
	JNIEnv* getJavaEnv() { return mJEnv; }
	jobject getActivity() { return mActivity; }
	static String getInternalPath() { return mInternalPath; }

	static void setOBBPath(String path) { mOBBPath = path; }
	static String getOBBPath() { return mOBBPath; }
#endif

	Ogre::Viewport* getViewport()						{ return mViewport; }
	const Ogre::Viewport* getViewport() const			{ return mViewport; }
	Ogre::RealRect getRenderArea() const				{ return mRenderArea; }
	int getMousePinch() const							{ return mMousePinch; }
	void setMousePinch(int pinch)						{ mMousePinch = pinch; }


	void setRenderArea(Ogre::RealRect area);
	void setCameraFov(Ogre::Real fov);
	void setCameraAspect(float aspect);
	void updateCameraAspect();
	void updateRenderArea();
	void getFrustum();

	Ogre::OrientationMode getViewportOrientation() const;
	Ogre::Camera* getCamera()							{ return mCamera; }
	const Ogre::Camera* getCamera()	const				{ return mCamera; }
	Ogre::SceneManager* getSceneManager()				{ return mSceneMgr; }
	const Ogre::SceneManager* getSceneManager()	const	{ return mSceneMgr; }
	CAppOISListener* getOIS()							{ return mOgreOISListener; }
	const CAppOISListener* getOIS() const				{ return mOgreOISListener; }

    Ogre::Vector2& getCursorCastOffset()				{ return mCursorCastOffset; }
	void setCursorCastOffset(const Ogre::Vector2 &vecOffset)	{ mCursorCastOffset = vecOffset; }

	CAppState& getAppState()							{ return mAppState; }
	const CAppState& getAppState() const				{ return mAppState; }
	SStringTable& getStringTable()						{ assert(mStringTable && "mStringTable is null!"); return *mStringTable; }
	const SStringTable& getStringTable() const			{ assert(mStringTable && "mStringTable is null!"); return *mStringTable; }
	const Quaternion& getCamOrientation() const			{ return pCamOrientation; }
	void setCamOrientation(const Quaternion& orientation)	{ pCamOrientation = orientation; }

#if USE_OIS_FOR_INPUT == TRUE
	void setEventPlayerMode(EventPlayer::TPlayerMode mode)	{ mEventPlayerMode = mode; }
#endif

	bool getScreenshot() const							{ return mDoScreenshot; }
	void setScreenshot(bool enable)						{ mDoScreenshot = enable; }
	TextRenderer* getInfoText()							{ return mInfoText; }
#if USING_THEORA
	Ogre::OgreVideoManager* getVideoManager()			{ return mVideoManager; }
	const Ogre::OgreVideoManager* getVideoManager() const	{ return mVideoManager; }
#endif

	std::string mCommandLine;

	void go();
	bool setup();
	bool configure();
	void close();
	void shutdown();
	void enterBackground();
	void enterForeground();
	void chooseSceneManager();

	const MathUtils::TVector2ui& getGameResolution() const		{ return mGameResolution; }
	const MathUtils::TVector2ui& getWindowActiveResolution() const { return mWindowActiveResolution; }
	MathUtils::TVector2ui getWindowResolution() const;
	bool getFullscreen() const;
	void setFullscreen(bool fFullscreen);

	void setFitScreen(bool fFitScreen);
	bool getFitScreen() { return mFitScreen; }

	void setWindowStyle(bool fFullscreen);
	void addResourcePath(const String& path, const String& filesystem_type, const String& group = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	void setupResourceFile(const String& fn, bool platform_spec = false);
	void setupResources();
	void shiftFontMaterials();
	void toLogFontInfo(Ogre::Font *f);
	const CProgressListener* getProgressListener() const { return pNewPL; }
	CProgressListener* getProgressListener() { return pNewPL; }

	void createFrameListener();
	bool frameStarted(const Ogre::FrameEvent& evt);
#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
	void setGetsureView(CAppGestureViewCpp* gv) { mGestureView = gv; }
#endif
	unsigned int getFrameTime() const;
	unsigned int getTime() const; //time since app started, paused, when app unfocused

	void mouseReleasedAndPressed(bool cursorShow, bool left, bool pressed);
	void mouseSendMsgToMouseEventReceiver(bool left, bool pressed);
	void mouseSendMsgToFocusedObj(bool left, bool pressed);
	void mouseMove(bool update_cursor);
	void mouseZMove(float relZ);

	//-----------------------------
	void loadSceneFile(const String& fileName, bool fUseLoader = true);
	void loadSceneFileEx(const String& fileName, const String& resGroup = "General", bool fUseLoader = true);
	void destroyObject(CGameObject* pObj);

		// ������� ��������, ������� ��� � ������ �����������
	void unloadTextures();
		// ������ �������� ����������� (��������� � mNotUnloadableTextures), �������� ���� ��������� ��� ����������
	void addNotUnloadableTexture(const Ogre::String &textureName);

	const CGameObject* getFocusedObj() const { return mpFocusedObj; }
	void updateFocusedObj() { mpFocusedObj = NULL; }

	OBJECT_HANDLE findObjectByName(const String& name);
	OBJECT_HANDLE findHeroByName(const String& name);

	CGameObject* _findObjectByHandle(const OBJECT_HANDLE &h) const;
	CHeroObject* _findHeroByHandle(const OBJECT_HANDLE &h) const;
	CGameObject* _findObjectByName(const String& name);
	CHeroObject* _findHeroByName(const String& name);
	CGameObject* _findObjectByNode(const Ogre::SceneNode* node);

	void overlaysLoaderShow();
	void overlaysShowAll(bool fShow=true);

	bool postMsg(CScriptMsg & m);
	void processMsgStack();
	void setViewCamera(const String& name);

	void addCursor(int id, const String& curCustom, /*LPSTR*/char* curStandard, const String &material);
	void showCursor(bool fshow = true);
	bool isShowCursor() const;
	bool isMouseLButton() const;
	bool isMouseRButton() const;
	void setCursor(int id);
	void setCursorDefault();
	void setCursorCurrentId(int id);
	void setCursorDefaultId(int id);
	void updateCursor();
	void setCursorStandard(bool state);
	void setCursorUseTextures(bool state);

	void getMeshInformation(	Ogre::Entity* pEntity,
                                size_t &vertex_count,
                                Ogre::Vector3* &vertices,
                                size_t &index_count,
                                unsigned long* &indices,
                                const Ogre::Vector3 &position,
                                const Ogre::Quaternion& orient,
                                const Ogre::Vector3 &scale) const;
	bool polygonCirclecast(const Ogre::Vector2& point, Ogre::Real radius, CGameObject* pObj) const;
	bool polygonRaycast(Ogre::Ray &ray, CGameObject* pObj, Ogre::Vector3 &result) const;

	void countIndicesAndVertices(const Ogre::Entity*  entity, size_t & index_count, size_t & vertex_count) const;
	bool _isVisible(CGameObject* pObj);

    void disableObjectsFocus(bool state);
	void setQueryMask(unsigned long mask);
    void setAdvCollisionActive(bool isAdvColl);
	void setMovCamera(int isMov);
    void getPosCamera(Ogre::Vector3& pos) const;
	void setPosCamera(const Ogre::Vector3& newpos);

    void loadScene(const String& sceneName, bool fUseLoader = true);
    void deleteScene();

	void attachCamToBone(CGameObject* pObj, const String& boneName);
	void processAttachCam();

	void enablePostEffect(bool enable);
	Ogre::String getShowInfoText() const;
	void showInfoWnd();

	static Ogre::String getBundlePath();
	static Ogre::String getResourcesPath(const String &fn);
	static Ogre::String getCfgPath(const String &fn);


private:
	void createCamera();
	void createScene();
	void createViewports();

	Ogre::RenderSystem* findRenderSystem();

	// virtual WindowEventListener
	void windowFocusChange(Ogre::RenderWindow* rw) OVERRIDE;
	bool windowClosing(Ogre::RenderWindow* rw) OVERRIDE;
	void windowClosed(Ogre::RenderWindow* rw) OVERRIDE;

public:
	MathUtils::TVector2ui findResolution(bool fFullScreen) const;
	void setViewportDimensions(Ogre::Viewport* viewport, bool fFullscreen);

private:
#ifdef OGRE_STATIC_LIB
	Ogre::StaticPluginLoader mStaticPluginLoader;
#endif
#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
	CAppGestureViewCpp* mGestureView;
#endif
#if OGRE_PLATFORM == OGRE_PLATFORM_ANDROID
	std::string mCurKeyboardTxt;
	JavaVM* mJVM;
	JNIEnv* mJEnv;
	jobject mActivity;
	static String mInternalPath;
	AAssetManager* mAssetMgr;       // Android asset manager to access files inside apk
	static String mOBBPath;
#endif
	Ogre::String mCompanyName;
	Ogre::String mAppFullName;
	Ogre::String mAppShortName;
	SetupScriptFunc mSetupScriptFunc;

	Ogre::Root* mRoot;
	Ogre::Camera* mCamera;
	Ogre::Camera* mDefaultCamera;
	Ogre::SceneManager* mSceneMgr;
    Ogre::OverlaySystem* mOverlaySystem;
	Ogre::MovableObjectFactory* mAdvRectangle2DFactory;
	Ogre::MovableObjectFactory* mDustControllerFactory;
	Ogre::RenderWindow* mWindow;
	Ogre::RealRect mFrustum;
	Ogre::RealRect mRenderArea;
	float mCameraAspect;
	int mMousePinch;

#if OGRE_PLATFORM != OGRE_PLATFORM_IPHONE
	// ������ ������� ��� ��������
	Ogre::Viewport* mViewportBk;
#endif
	Ogre::Viewport* mViewport;

	DataArchiveFactory* mDataArchFactory;
	CAppOISListener*	mOgreOISListener;
	TextRenderer*		mInfoText;

	CAppState mAppState;
	SStringTable* mStringTable; // not object but pointer because MAC OS X crashes in std::base_string destructor for static variables
	CProgressListener* pNewPL;
	Ogre::CAtlasManager* mAtlasManager;
	Ogre::CFontdefexManager* mFontdefexManager;
	CBinaryMaterials *mBinaryMaterialsManager;

	unsigned int mFrameTime;
	unsigned int mTotalTime;

	CGameObject* pCamAttachObj;
	CGameObject* pCamObj;
	String mBoneAttachName;
	Quaternion pCamOrientation;

#if USE_OIS_FOR_INPUT == TRUE
	EventPlayer::TPlayerMode mEventPlayerMode;
#endif

	std::vector<CScriptMsg> mMsgStack;

	Ogre::RaySceneQuery* mRayQuery;
	unsigned long mQueryMask;

	bool mObjectFocusDisabled;
	CGameObject* mpFocusedObj;
	Vector3 mFocusedPoint;
	int mCursorCurrentId;
        // �������� ���������� ������, �� ������� ��������� ����� ��������
    Vector2 mCursorCastOffset;

	bool mInitialResourceAll;
	bool mDoQuit;
	int	 mMovable;
	bool mSetDlg;
	bool mAdvCollision;
	bool mDoLog;

	bool mPackedRes;
	bool mDoScreenshot;
	bool mDoScreens;
	bool mShowInfo;
	bool mSoundsEnabled;
	bool mCheats;
	bool mNoTex;
	bool mLowMemMode;
	bool mLowVideoMemMode;
	bool mFitScreen; // ��� ��������� ����������� �������� �� ���� ����� (��� ����� ���������)
	bool mFullScreen; // ��������� � ���������� � ������ ������

	MathUtils::TVector2ui mMaxPossibleResolution;

//theora
#if USING_THEORA
	Ogre::OgreVideoManager*	mVideoManager;
#endif

//physics
#if USE_BULLET == TRUE
	Ogre::OgreBulletDynamics::DynamicsWorld* mWorld;
#endif

private:
	MathUtils::TVector2ui mGameResolution;
	MathUtils::TVector2ui mWindowActiveResolution;

		// ����� �������, ������� �� ����������� ��� ������� �����
	std::set<Ogre::String> mNotUnloadableTextures;
};

extern GAME_API CMainApplication app;
