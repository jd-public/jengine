#pragma once

#include "VirtualScript.h"

class CVirtualScript;

#define REGISTER_SCRIPT(classname) class classname; static CScriptCreator<classname> ScriptCreator##classname(__FILE__);

class CScriptCreatorCleaner;
class GAME_API CScriptCreatorBase
{
public:
	CScriptCreatorBase(const String& name);
	virtual ~CScriptCreatorBase() {}
	static CVirtualScript* getScriptObject(const String& name);

	const String& getName() const { return mName; }
//	static String CScriptCreatorBase::getFileName(const String& name);

protected:
	virtual CVirtualScript* createScriptObject() = 0;

//private:
//	CScriptCreatorBase() {} 

private:
	String mName;

//private:
//	friend class CScriptCreatorCleaner;
};

template <class T>
class CScriptCreator : public CScriptCreatorBase
{
public:
	CScriptCreator(const String& name)	: CScriptCreatorBase(name) {}
	virtual ~CScriptCreator() {}

protected:
	CVirtualScript* createScriptObject() { return new T(getName()); }
};


class CScriptCreatorTable
{
public:
	OGRE_HashMap<String, CScriptCreatorBase*>& getScripts();
	virtual ~CScriptCreatorTable();

private:
	static OGRE_HashMap<String, CScriptCreatorBase*>* mScripts;
};


