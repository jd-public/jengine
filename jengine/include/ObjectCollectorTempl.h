#pragma once

#define MAX_OBJECT_NUM	3072
#ifndef LOWORD
#	define LOWORD(a) (unsigned short)(a)
#endif
#ifndef HIWORD
#	define HIWORD(a) (unsigned short)((a)>>16)
#endif
#ifndef MAKELONG
#	define MAKELONG(a,b) (unsigned int)(((a)&0xffff)|((b)<<16))
#endif

template <class Type, class EnumType, class HandleType = OBJECT_HANDLE, int MaxN = MAX_OBJECT_NUM> 
class CObjectCollectorTempl
{
public:

	CObjectCollectorTempl()
	{
		lastNum = 0;
		memset(mObj,0,MaxN*sizeof(Type));
	}

	HandleType push_back(Type obj, const EnumType& t)
	{
		for(size_t i = 0; i < lastNum+1; i++)
		{
			if (mObj[i] == NULL)
			{
				mObj[i] = obj;
				if (i >= lastNum)
					lastNum = i+1;
				if (i >= MaxN)
					toLogEx("ERROR (CObjectCollectorTempl<...>::push_back): What tha F! We have no room for objects!\n");
				return MAKELONG(i,t);
			}
		}
		return 0;
	}
	void eraseByHandle( HandleType h)
	{
		if (h)
		{
			int pos = LOWORD(h);

			mObj[pos] = 0;

			int i = lastNum;
			for(; i > 0; i--)
			{
				if (i > 0 && mObj[i-1] != 0)
				{
					break;
				}
			}

			lastNum = i;
		}
	}

	Type findByHandle(HandleType h) const {	return (h && LOWORD(h)<MaxN) ? mObj[LOWORD(h)] : NULL; }
	size_t size() const { return lastNum; }
	Type operator[] (size_t z) const { return mObj[z]; }

/*	void removeAll() 
	{	
		for(int i = 0; i < lastNum; i++)	
			SAFE_DEL(mObj[i]);
		lastNum = 0;
	}*/

private:
	size_t lastNum;
	Type mObj[MaxN];	
};
