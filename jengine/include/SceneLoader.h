#ifndef DOT_SCENELOADER_H
#define DOT_SCENELOADER_H

// Includes
#include <OgreString.h>
#include <OgreVector3.h>
#include <OgreQuaternion.h>
#include <OgreResourceGroupManager.h>
#include <vector>

#include "RapidXmlFunc.h"
#include "GameObject.h"

// Forward declarations
class ProgressListener
{
public:
	virtual ~ProgressListener() {}
	virtual void onProgress() = 0;
};

namespace Ogre
{
	class SceneManager;
	class SceneNode;
	class RenderWindow;
}


class nodeProperty
{
public:
	Ogre::String nodeName;
	Ogre::String propertyNm;
	Ogre::String valueName;
	Ogre::String typeName;

	nodeProperty(const Ogre::String &node, const Ogre::String &propertyName, const Ogre::String &value, const Ogre::String &type)
		: nodeName(node), propertyNm(propertyName), valueName(value), typeName(type) {}
};

class SceneLoader
{
public:  
	SceneLoader();
	virtual      ~SceneLoader();

	void         parseScene(const Ogre::String &SceneName, 
								const Ogre::String &groupName, 
								Ogre::SceneManager *yourSceneMgr,
								Ogre::SceneNode *pAttachNode,
								Ogre::RenderWindow *pWindow,
								ProgressListener *pProgressListener

								/*Ogre::SceneNode *pAttachNode = NULL, 
								const Ogre::String &sPrependNode = ""*/);

protected:

	#define XML_NODE	XmlNode
	#define XML_ATTRIB	XmlAttrib

	void processScene(XML_NODE* XMLRoot, Ogre::SceneNode *pAttachNode);

	void processNodes(XML_NODE* XMLNode, Ogre::SceneNode *pAttachNode);
	void processIncludes(XML_NODE* XMLNode, Ogre::SceneNode *pAttachNode);
	void processScriptAliases(XML_NODE* XMLNode);

	void processEnvironment(XML_NODE* XMLNode);
	void processResourceLocations(XML_NODE* XMLNode);
	void processLight(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processCamera(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processNode(XML_NODE* XMLNode, Ogre::SceneNode *pParent);
	void processStringsTable(XML_NODE* XMLNode);
	void processEntity(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj, const Ogre::Vector3 &BBS, int type);
	void processSubEntity(XML_NODE* XMLNode, Ogre::Entity *pEntity);
	void processParticleSystem(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processBillboardSet(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processBillboard(XML_NODE* XMLNode, Ogre::BillboardSet *pBS);
	void processParams(XML_NODE* XMLNode, CGameObject* pObj);
	void processDust(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processOverlay(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processRect2d(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processRibbonTrail(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processAnimations(XML_NODE* XMLNode, CGameObject* pObj);
#if USE_BULLET == TRUE
	void processRigitBody(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
	void processConstraint(XML_NODE* XMLNode, Ogre::SceneNode *pParent, CGameObject* pObj);
#endif

	void processFog(XML_NODE* XMLNode);
	void processSkyBox(XML_NODE* XMLNode);
	void processSkyDome(XML_NODE* XMLNode);
	void processSkyPlane(XML_NODE* XMLNode);
	void processClipping(XML_NODE* XMLNode);

	void processLightRange(XML_NODE* XMLNode, Ogre::Light *pLight);
	void processLightAttenuation(XML_NODE* XMLNode, Ogre::Light *pLight);

	Ogre::String getAttribUnique(XML_NODE* XMLNode, const char *attrib);
	void setDefaultVisibleFlag(Ogre::MovableObject *obj);


	Ogre::Vector3 parseVector3(XML_NODE* XMLNode);
	Ogre::Quaternion parseQuaternion(XML_NODE* XMLNode);
	Ogre::ColourValue parseColour(XML_NODE* XMLNode);

	void updateProgress();

	Ogre::SceneManager *mSceneMgr;
	int mUniqueNameCounter;
	int mDefaultFisibleFlag;
	bool mShowLog, mShowError;
	

	//TODO!!!!! for all operations
	Ogre::String mTempStr;
	Ogre::String& _getAttrib(XML_NODE* XMLNode, const char *attrib, const char *def = "\0");

	Ogre::String m_sGroupName;
	Ogre::RenderWindow *mWindow;
	ProgressListener *mProgressListener;
	Ogre::String mCurrentFile;

};

#endif // DOT_SCENELOADER_H
