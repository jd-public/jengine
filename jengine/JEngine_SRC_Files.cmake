set (HEADER_FILES	
	include/RapidXmlFunc.h
	include/VirtualScript.h
	include/AdvRectangle2D.h
	include/AnimObject.h
	include/AppOISListener.h
	include/AppState.h
	include/AtlasManager.h
	include/CMsg.h
	include/ConfigChecker.h
	include/ConvertedFromFile.h
	include/DataArchive.h
	include/DustController.h
	include/DynamicLines.h
	include/DynamicRenderable.h
	include/EventPlayer.h
	include/FileScript.h
	include/FontdefManager.h
	include/GameObject.h
	include/HeroObject.h
	include/JEngine.h
	include/JEnginePlatform.h
	include/log.h
	include/MathUtils.h
	include/ObjectCollectorTempl.h
	include/PhysObject.h
	include/ProgressListener.h
	include/ScriptCreator.h
	include/ScriptMsg.h
	include/SfxCore.h
	include/SfxCorePrerequisites.h
	include/StaticPluginLoader.h
	include/stdafx.h
	include/StringTable.h
	include/TextRenderer.h
	include/mainIOS.h
	include/MainApplication.h
	
	
		
	include/SceneLoader.h
	${JENGINE_BINARY_DIR}/include/config.h
	
)

set (SOURCE_FILES
	src/RapidXmlFunc.cpp
	src/VirtualScript.cpp
	src/AdvRectangle2D.cpp
	src/AnimObject.cpp
	src/AppOISListener.cpp
	src/AppState.cpp
	src/AtlasManager.cpp
	src/ConvertedFromFile.cpp
	src/DataArchive.cpp
	src/DustController.cpp
	src/DynamicLines.cpp
	src/DynamicRenderable.cpp
	src/EventPlayer.cpp
	src/FontdefManager.cpp
	src/GameObject.cpp
	src/HeroObject.cpp
	src/log.cpp
	src/MathUtils.cpp
	src/PhysObject.cpp
	src/ProgressListener.cpp
	src/ScriptCreator.cpp
	src/SfxCore.cpp
	src/stdafx.cpp
	src/StringTable.cpp
	src/TextRenderer.cpp
	src/main.cpp
	src/MainApplication.cpp
	src/SceneLoader.cpp
	

)

if(USE_STEAM)
	set (HEADER_FILES  ${HEADER_FILES} include/SteamGlue.h)
	set (SOURCE_FILES  ${SOURCE_FILES} src/SteamGlue.cpp)
endif()

if(APPLE AND JENGINE_IOS_BUILD)
	set (SOURCE_FILES  ${SOURCE_FILES} src/mainIOS.mm)
endif()

